#![allow(unused_imports)]

use cpp::cpp;
use cstr::cstr;
use qmetaobject::prelude::*;

#[cfg(target_os = "macos")]
use cacao::macos::*;

mod system;

cpp! {{
#include <QtQml>

#include <xQuick.hpp>
#if __APPLE__
#include <xQuickWindow.hpp>
#endif
}}

qrc!(qml_register_resources,
    "/" {
        "qml/xQuick/Controls/Application.qml",
        "qml/xQuick/Controls/Badge.qml",
        "qml/xQuick/Controls/Banner.qml",
        "qml/xQuick/Controls/BannerLoader.qml",
        "qml/xQuick/Controls/BannerManager.qml",
        "qml/xQuick/Controls/ButtonFlat.qml",
        "qml/xQuick/Controls/ButtonRaw.qml",
        "qml/xQuick/Controls/ButtonRound.qml",
        "qml/xQuick/Controls/ButtonSquared.qml",
        "qml/xQuick/Controls/Card.qml",
        "qml/xQuick/Controls/CardBackground.qml",
        "qml/xQuick/Controls/CardMedia.qml",
        "qml/xQuick/Controls/CardRoundThumbnailTitle.qml",
        "qml/xQuick/Controls/CardSupportingText.qml",
        "qml/xQuick/Controls/CardTitle.qml",
        "qml/xQuick/Controls/CheckComboBox.qml",
        "qml/xQuick/Controls/Console.qml",
        "qml/xQuick/Controls/ConsoleDelegate.qml",
        "qml/xQuick/Controls/CursorDelegate.qml",
        "qml/xQuick/Controls/Editor.qml",
        "qml/xQuick/Controls/ErrorSequentialAnimation.qml",
        "qml/xQuick/Controls/Gauge.qml",
        "qml/xQuick/Controls/Icon.qml",
        "qml/xQuick/Controls/Label.qml",
        "qml/xQuick/Controls/LabelBody1.qml",
        "qml/xQuick/Controls/LabelBody2.qml",
        "qml/xQuick/Controls/LabelButton.qml",
        "qml/xQuick/Controls/LabelCaption.qml",
        "qml/xQuick/Controls/LabelEntry.qml",
        "qml/xQuick/Controls/LabelHeadline1.qml",
        "qml/xQuick/Controls/LabelHeadline2.qml",
        "qml/xQuick/Controls/LabelHeadline3.qml",
        "qml/xQuick/Controls/LabelHeadline4.qml",
        "qml/xQuick/Controls/LabelHeadline5.qml",
        "qml/xQuick/Controls/LabelHeadline6.qml",
        "qml/xQuick/Controls/LabelHint1.qml",
        "qml/xQuick/Controls/LabelHint2.qml",
        "qml/xQuick/Controls/LabelIcon.qml",
        "qml/xQuick/Controls/LabelOverline.qml",
        "qml/xQuick/Controls/LabelSubtitle1.qml",
        "qml/xQuick/Controls/LabelSubtitle2.qml",
        "qml/xQuick/Controls/LabelWithCaption.qml",
        "qml/xQuick/Controls/PlaceholderText.qml",
        "qml/xQuick/Controls/Puller.qml",
        "qml/xQuick/Controls/PullerIndicator.qml",
        "qml/xQuick/Controls/SegmentedCell.qml",
        "qml/xQuick/Controls/SegmentedCellSegment.qml",
        "qml/xQuick/Controls/Separator.qml",
        "qml/xQuick/Controls/Shell.qml",
        "qml/xQuick/Controls/ShellCursor.qml",
        "qml/xQuick/Controls/ShellScreen.qml",
        "qml/xQuick/Controls/ShellSelection.qml",
        "qml/xQuick/Controls/ShellText.qml",
        "qml/xQuick/Controls/ScientificSpinBoxReal.qml",
        "qml/xQuick/Controls/ScientificSpinBoxInteger.qml",
        "qml/xQuick/Controls/VisSlider.qml",
        "qml/xQuick/Controls/CollapsibleView.qml",
        "qml/xQuick/Controls/Finder.qml",
        "qml/xQuick/Controls/FinderGridView.qml",
        "qml/xQuick/Controls/FinderGridDelegate.qml",
        "qml/xQuick/Controls/FinderListView.qml",
        "qml/xQuick/Controls/FinderListDelegate.qml",
        "qml/xQuick/Controls/Spinner.qml",
        "qml/xQuick/Controls/TourStep.qml",
        "qml/xQuick/Controls/Tour.qml",
        "qml/xQuick/Controls/qmldir",

        "qml/xQuick/Style/Button.qml",
        "qml/xQuick/Style/ButtonPanel.qml",
        "qml/xQuick/Style/CheckBox.qml",
        "qml/xQuick/Style/CheckDelegate.qml",
        "qml/xQuick/Style/ComboBox.qml",
        "qml/xQuick/Style/DelayButton.qml",
        "qml/xQuick/Style/Dial.qml",
        "qml/xQuick/Style/Dialog.qml",
        "qml/xQuick/Style/DialogButtonBox.qml",
        "qml/xQuick/Style/Drawer.qml",
        "qml/xQuick/Style/Frame.qml",
        "qml/xQuick/Style/GroupBox.qml",
        "qml/xQuick/Style/ItemDelegate.qml",
        "qml/xQuick/Style/Label.qml",
        "qml/xQuick/Style/Menu.qml",
        "qml/xQuick/Style/MenuBar.qml",
        "qml/xQuick/Style/MenuBarItem.qml",
        "qml/xQuick/Style/MenuItem.qml",
        "qml/xQuick/Style/Page.qml",
        "qml/xQuick/Style/Pane.qml",
        "qml/xQuick/Style/Popup.qml",
        "qml/xQuick/Style/ProgressBar.qml",
        "qml/xQuick/Style/RadioButton.qml",
        "qml/xQuick/Style/RadioDelegate.qml",
        "qml/xQuick/Style/RangeSlider.qml",
        "qml/xQuick/Style/ScrollBar.qml",
        "qml/xQuick/Style/ScrollIndicator.qml",
        "qml/xQuick/Style/Slider.qml",
        "qml/xQuick/Style/SplitView.qml",
        "qml/xQuick/Style/Style.qml",
        "qml/xQuick/Style/SwipeDelegate.qml",
        "qml/xQuick/Style/Switch.qml",
        "qml/xQuick/Style/SwitchDelegate.qml",
        "qml/xQuick/Style/TabBar.qml",
        "qml/xQuick/Style/TabButton.qml",
        "qml/xQuick/Style/TextArea.qml",
        "qml/xQuick/Style/TextField.qml",
        "qml/xQuick/Style/ToolBar.qml",
        "qml/xQuick/Style/ToolButton.qml",
        "qml/xQuick/Style/ToolTip.qml",
        "qml/xQuick/Style/qmldir",

        "qml/xQuick/Fonts/Comfortaa-Bold.ttf",
        "qml/xQuick/Fonts/Comfortaa-Light.ttf",
        "qml/xQuick/Fonts/Comfortaa-Medium.ttf",
        "qml/xQuick/Fonts/Comfortaa-Regular.ttf",
        "qml/xQuick/Fonts/Comfortaa-SemiBold.ttf",
        "qml/xQuick/Fonts/Icons.ttf",
        "qml/xQuick/Fonts/Icons.js",
        "qml/xQuick/Fonts/Montserrat-Black.ttf",
        "qml/xQuick/Fonts/Montserrat-BlackItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-Bold.ttf",
        "qml/xQuick/Fonts/Montserrat-BoldItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-ExtraBold.ttf",
        "qml/xQuick/Fonts/Montserrat-ExtraBoldItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-ExtraLight.ttf",
        "qml/xQuick/Fonts/Montserrat-ExtraLightItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-Italic.ttf",
        "qml/xQuick/Fonts/Montserrat-Light.ttf",
        "qml/xQuick/Fonts/Montserrat-LightItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-Medium.ttf",
        "qml/xQuick/Fonts/Montserrat-MediumItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-Regular.ttf",
        "qml/xQuick/Fonts/Montserrat-SemiBold.ttf",
        "qml/xQuick/Fonts/Montserrat-SemiBoldItalic.ttf",
        "qml/xQuick/Fonts/Montserrat-Thin.ttf",
        "qml/xQuick/Fonts/Montserrat-ThinItalic.ttf",
        "qml/xQuick/Fonts/OpenSans-Bold.ttf",
        "qml/xQuick/Fonts/OpenSans-BoldItalic.ttf",
        "qml/xQuick/Fonts/OpenSans-ExtraBold.ttf",
        "qml/xQuick/Fonts/OpenSans-ExtraBoldItalic.ttf",
        "qml/xQuick/Fonts/OpenSans-Italic.ttf",
        "qml/xQuick/Fonts/OpenSans-Light.ttf",
        "qml/xQuick/Fonts/OpenSans-LightItalic.ttf",
        "qml/xQuick/Fonts/OpenSans-Regular.ttf",
        "qml/xQuick/Fonts/OpenSans-SemiBold.ttf",
        "qml/xQuick/Fonts/OpenSans-SemiBoldItalic.ttf",
        "qml/xQuick/Fonts/Roboto-Black.ttf",
        "qml/xQuick/Fonts/Roboto-BlackItalic.ttf",
        "qml/xQuick/Fonts/Roboto-Bold.ttf",
        "qml/xQuick/Fonts/Roboto-BoldItalic.ttf",
        "qml/xQuick/Fonts/Roboto-Italic.ttf",
        "qml/xQuick/Fonts/Roboto-LightItalic.ttf",
        "qml/xQuick/Fonts/Roboto-Medium.ttf",
        "qml/xQuick/Fonts/Roboto-MediumItalic.ttf",
        "qml/xQuick/Fonts/Roboto-Regular.ttf",
        "qml/xQuick/Fonts/Roboto-Thin.ttf",
        "qml/xQuick/Fonts/Roboto-Light.ttf",
        "qml/xQuick/Fonts/Roboto-ThinItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Black.ttf",
        "qml/xQuick/Fonts/SourceCodePro-BlackItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Bold.ttf",
        "qml/xQuick/Fonts/SourceCodePro-BoldItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-ExtraLight.ttf",
        "qml/xQuick/Fonts/SourceCodePro-ExtraLightItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Italic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Light.ttf",
        "qml/xQuick/Fonts/SourceCodePro-LightItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Medium.ttf",
        "qml/xQuick/Fonts/SourceCodePro-MediumItalic.ttf",
        "qml/xQuick/Fonts/SourceCodePro-Regular.ttf",
        "qml/xQuick/Fonts/SourceCodePro-SemiBold.ttf",
        "qml/xQuick/Fonts/SourceCodePro-SemiBoldItalic.ttf",
        "qml/xQuick/Fonts/Ubuntu-Bold.ttf",
        "qml/xQuick/Fonts/Ubuntu-BoldItalic.ttf",
        "qml/xQuick/Fonts/Ubuntu-Italic.ttf",
        "qml/xQuick/Fonts/Ubuntu-Light.ttf",
        "qml/xQuick/Fonts/Ubuntu-LightItalic.ttf",
        "qml/xQuick/Fonts/Ubuntu-Medium.ttf",
        "qml/xQuick/Fonts/Ubuntu-MediumItalic.ttf",
        "qml/xQuick/Fonts/Ubuntu-Regular.ttf",
        "qml/xQuick/Fonts/UbuntuMono-Bold.ttf",
        "qml/xQuick/Fonts/UbuntuMono-BoldItalic.ttf",
        "qml/xQuick/Fonts/UbuntuMono-Italic.ttf",
        "qml/xQuick/Fonts/UbuntuMono-Regular.ttf",
        "qml/xQuick/Fonts/qmldir",
        "qml/xQuick/Models/FilterProxyModel.qml",
        "qml/xQuick/Models/qmldir",
    }
);

#[derive(Copy, Clone, Debug, Eq, PartialEq, QEnum)]
#[repr(u32)]
enum Features {
    Default = 0,
#[cfg(feature = "tts")]
    TTS = 1,
#[cfg(feature = "vis")]
    VIS = 2,
}

pub fn init() {
    qml_register_resources();
    qml_register_enum::<Features>(cstr!("xQuick"), 1, 0, cstr!("Features"));
    qml_register_type::<system::SystemFetcher>(cstr!("xQuick"), 1, 0, cstr!("SystemFetcher"));

    cpp!(unsafe [] {
        x_quick_initialise();
    });
}

pub fn wrap(engine: &mut qmetaobject::QmlEngine) {

    let engine_p = engine.cpp_ptr();

    cpp!(unsafe [engine_p as "QQmlEngine *"] {
#if __APPLE__
        xQuickWindowWrap(reinterpret_cast<QQmlApplicationEngine *>(engine_p));
#else
        Q_UNUSED(engine_p);
#endif
    });
}

#[no_mangle]
pub extern "C" fn x_quick_rinit() {
    init();
}
