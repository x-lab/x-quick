use qmetaobject::*;
use sysinfo::{NetworkExt, NetworksExt, ProcessExt, System, SystemExt, Cpu, CpuExt};

#[allow(non_snake_case)]
#[derive(Default, QObject)]
pub struct SystemFetcher {
    base: qt_base_class!(trait QObject),

    total_memory: qt_property!(u64; READ total_memory NOTIFY total_memory_changed),
    total_memory_changed: qt_signal!(),

    used_memory: qt_property!(u64; READ used_memory NOTIFY used_memory_changed),
    used_memory_changed: qt_signal!(),

    this_memory: qt_property!(u64; READ this_memory NOTIFY this_memory_changed),
    this_memory_changed: qt_signal!(),

    used_cpu: qt_property!(f32; READ used_cpu NOTIFY used_cpu_changed),
    used_cpu_changed: qt_signal!(),

    this_cpu: qt_property!(f32; READ this_cpu NOTIFY this_cpu_changed),
    this_cpu_changed: qt_signal!(),

    total_cpu_count: qt_property!(usize; READ total_cpu_count NOTIFY total_cpu_count_changed),
    total_cpu_count_changed: qt_signal!(),

    system_name: qt_property!(QString; READ system_name NOTIFY system_name_changed),
    system_name_changed: qt_signal!(),

    host_name: qt_property!(QString; READ host_name NOTIFY host_name_changed),
    host_name_changed: qt_signal!(),

    internals: sysinfo::System,
    
    fetch: qt_method!(fn(&mut self)),
}

impl SystemFetcher {

    fn fetch(&mut self) {

        let mut sys = sysinfo::System::new_all();
        
        sys.refresh_all();

        // std::thread::sleep(std::time::Duration::from_millis(500));

        // println!("=> system:");
        
        // // RAM and swap information:
        // println!("total     memory : {} MB", sys.total_memory() / 1024 / 1024);
        // println!("used      memory : {} MB", sys.used_memory() / 1024 / 1024);
        // println!("free      memory : {} MB", sys.free_memory() / 1024 / 1024);
        // println!("available memory : {} MB", sys.available_memory() / 1024 / 1024);
        // println!("total       swap : {} MB", sys.total_swap() / 1024 / 1024);
        // println!("used        swap : {} MB", sys.used_swap() / 1024 / 1024);
        // println!("free        swap : {} MB", sys.free_swap() / 1024 / 1024);
        
        // println!("CPU: {}", sys.global_cpu_info().cpu_usage());

        // Display system information:
        // println!("System name:             {:?}", sys.name());
        // println!("System kernel version:   {:?}", sys.kernel_version());
        // println!("System OS version:       {:?}", sys.os_version());
        // println!("System host name:        {:?}", sys.host_name());

        // let pid = sysinfo::get_current_pid().unwrap();
        // let process = sys.process(pid).unwrap();

        // println!("[{}] {} {:?}", pid, process.name(), *process);

        self.internals = sys;
        
        // self.total_memory_changed();
        //  self.used_memory_changed();

        // self.this_cpu_changed();
        // self.this_memory_changed();
    }

    fn total_memory(&self) -> u64 {
        self.internals.total_memory() / 1024 / 1024
    }

    fn used_memory(&self) -> u64 {
        let mut used: u64 = 0;

        for (_, process) in self.internals.processes() {
            used += process.memory();
        }
        
        used / 1024 / 1204
    }

    fn system_name(&self) -> QString {
        format!("{} {}", self.internals.name().unwrap(), self.internals.kernel_version().unwrap()).into()
    }

    fn host_name(&self) -> QString {
        QString::from(self.internals.host_name().unwrap())
    }

    fn this_memory(&self) -> u64 {
        let process = self.internals.process(sysinfo::get_current_pid().unwrap()).unwrap();
        
        process.memory() / 1024 / 1024
    }

    fn used_cpu(&self) -> f32 {
        self.internals.global_cpu_info().cpu_usage()
    }

    fn this_cpu(&self) -> f32 {
        let process = self.internals.process(sysinfo::get_current_pid().unwrap()).unwrap();
        let ncpus   = self.internals.cpus().len() as f32;

        if cfg!(target_os = "macos") {
            process.cpu_usage()
        }
        else {
            process.cpu_usage() / ncpus
        }
    }

    fn total_cpu_count(&self) -> usize {

        self.internals.cpus().len()
    }
}
