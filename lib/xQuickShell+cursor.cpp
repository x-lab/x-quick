#include "xQuickShell+cursor.hpp"
#include "xQuickShell+block.hpp"
#include "xQuickShell+screen_data.hpp"

#include <QtCore/QLoggingCategory>
#include <QTextCodec>

Q_LOGGING_CATEGORY(lcxQuickShellCursor, "yat.cursor")

xQuickShellCursor::xQuickShellCursor(xQuickShellScreen* screen)
    : QObject(screen)
    , m_screen(screen)
    , m_current_text_style(screen->defaultxQuickShellTextStyle())
    , m_position(0,0)
    , m_new_position(0,0)
    , m_screen_width(screen->width())
    , m_screen_height(screen->height())
    , m_top_margin(0)
    , m_bottom_margin(0)
    , m_scroll_margins_set(false)
    , m_origin_at_margin(false)
    , m_notified(false)
    , m_visible(true)
    , m_new_visibillity(true)
    , m_blinking(false)
    , m_new_blinking(false)
    , m_wrap_around(true)
    , m_content_height_changed(false)
    , m_insert_mode(Replace)
    , m_resize_block(0)
    , m_current_pos_in_block(0)
{
    connect(screen, &xQuickShellScreen::widthAboutToChange, this, &xQuickShellCursor::setxQuickShellScreenWidthAboutToChange);
    connect(screen, &xQuickShellScreen::dataWidthChanged, this, &xQuickShellCursor::setxQuickShellScreenWidth);
    connect(screen, &xQuickShellScreen::dataHeightChanged, this, &xQuickShellCursor::setxQuickShellScreenHeight);
    connect(screen, &xQuickShellScreen::contentHeightChanged, this, &xQuickShellCursor::contentHeightChanged);

    m_gl_text_codec = QTextCodec::codecForName("utf-8")->makeDecoder();
    m_gr_text_codec = QTextCodec::codecForName("utf-8")->makeDecoder();

    for (int i = 0; i < m_screen_width; i++) {
        if (i % 8 == 0) {
            m_tab_stops.append(i);
        }
    }
}

xQuickShellCursor::~xQuickShellCursor()
{

}

void xQuickShellCursor::setxQuickShellScreenWidthAboutToChange(int width)
{
    Q_UNUSED(width);
    auto it = m_screen->currentxQuickShellScreenData()->it_for_row(new_y());
    if (m_screen->currentxQuickShellScreenData()->it_is_end(it))
        return;

    m_resize_block = *it;
    int line_diff = new_y() - m_resize_block->screenIndex();
    m_current_pos_in_block = (line_diff * m_screen_width) + new_x();
}

void xQuickShellCursor::setxQuickShellScreenWidth(int newWidth, int removedBeginning, int reclaimed)
{
    if (newWidth > m_screen_width) {
        for (int i = m_screen_width -1; i < newWidth; i++) {
            if (i % 8 == 0) {
                m_tab_stops.append(i);
            }
        }
    }

    m_screen_width = newWidth;

    auto it = m_screen->currentxQuickShellScreenData()->it_for_block(m_resize_block);
    if (m_screen->currentxQuickShellScreenData()->it_is_end(it)) {
        if (removedBeginning > reclaimed) {
            new_ry() = 0;
            new_rx() = 0;
        } else {
            new_ry() = m_screen_height - 1;
            new_rx() = 0;
        }
    } else {
        new_ry() = (*it)->screenIndex() + m_current_pos_in_block / newWidth;
        new_rx() = m_current_pos_in_block % newWidth;
    }
    m_resize_block = 0;
    m_current_pos_in_block = 0;
    notifyChanged();
}

void xQuickShellCursor::setxQuickShellScreenHeight(int newHeight, int removedBeginning, int reclaimed)
{
    resetScrollArea();
    m_screen_height = newHeight;
    new_ry() -= removedBeginning;
    new_ry() += reclaimed;
    if (new_y() <= 0) {
        new_ry() = 0;
    }
}

bool xQuickShellCursor::visible() const
{
    return m_visible;
}
void xQuickShellCursor::setVisible(bool visible)
{
    m_new_visibillity = visible;
}

bool xQuickShellCursor::blinking() const
{
    return m_blinking;
}

void xQuickShellCursor::setBlinking(bool blinking)
{
    m_new_blinking = blinking;
}

void xQuickShellCursor::setxQuickShellTextStyle(xQuickShellTextStyle::Style style, bool add)
{
    if (add) {
        m_current_text_style.style |= style;
    } else {
        m_current_text_style.style &= !style;
    }
}

void xQuickShellCursor::resetStyle()
{
    m_current_text_style.background = colorPalette()->defaultBackground().rgb();
    m_current_text_style.foreground = colorPalette()->defaultForeground().rgb();
    m_current_text_style.style = xQuickShellTextStyle::Normal;
}

void xQuickShellCursor::scrollUp(int lines)
{
    if (new_y() < top() || new_y() > bottom())
            return;
    for (int i = 0; i < lines; i++) {
        screen_data()->moveLine(bottom(), top());
    }
}

void xQuickShellCursor::scrollDown(int lines)
{
    if (new_y() < top() || new_y() > bottom())
        return;
    for (int i = 0; i < lines; i++) {
        screen_data()->moveLine(top(), bottom());
    }
}

void xQuickShellCursor::setxQuickShellTextCodec(QTextCodec *codec)
{
    m_gl_text_codec = codec->makeDecoder();
}

void xQuickShellCursor::setInsertMode(InsertMode mode)
{
    m_insert_mode = mode;
}

xQuickShellTextStyle xQuickShellCursor::currentxQuickShellTextStyle() const
{
    return m_current_text_style;
}

void xQuickShellCursor::setxQuickShellTextForegroundColor(QRgb color)
{
    m_current_text_style.foreground = color;
}

void xQuickShellCursor::setxQuickShellTextBackgroundColor(QRgb color)
{
    m_current_text_style.background = color;
}

void xQuickShellCursor::setxQuickShellTextForegroundColorIndex(xQuickShellColorPalette::Color color)
{
    qCDebug(lcxQuickShellCursor) << color;
    setxQuickShellTextForegroundColor(colorPalette()->color(color).rgb());
}

void xQuickShellCursor::setxQuickShellTextBackgroundColorIndex(xQuickShellColorPalette::Color color)
{
    qCDebug(lcxQuickShellCursor) << color;
    setxQuickShellTextBackgroundColor(colorPalette()->color(color).rgb());
}

xQuickShellColorPalette *xQuickShellCursor::colorPalette() const
{
    return m_screen->colorPalette();
}

QPoint xQuickShellCursor::position() const
{
    return m_position;
}

int xQuickShellCursor::x() const
{
    return m_position.x();
}

int xQuickShellCursor::y() const
{
    return (m_screen->currentxQuickShellScreenData()->contentHeight() - m_screen->height()) + m_position.y();
}

void xQuickShellCursor::moveOrigin()
{
    m_new_position = QPoint(0,adjusted_top());
    notifyChanged();
}

void xQuickShellCursor::moveBeginningOfLine()
{
    new_rx() = 0;
    notifyChanged();
}

void xQuickShellCursor::moveUp(int lines)
{
    int adjusted_new_y = this->adjusted_new_y();
    if (!adjusted_new_y || !lines)
        return;

    if (lines < adjusted_new_y) {
        new_ry() -= lines;
    } else {
        new_ry() = adjusted_top();
    }
    notifyChanged();
}

void xQuickShellCursor::moveDown(int lines)
{
    int bottom = adjusted_bottom();
    if (new_y() == bottom || !lines)
        return;

    if (new_y() + lines <= bottom) {
        new_ry() += lines;
    } else {
        new_ry() = bottom;
    }
    notifyChanged();
}

void xQuickShellCursor::moveLeft(int positions)
{
    if (!new_x() || !positions)
        return;
    if (positions < new_x()) {
        new_rx() -= positions;
    } else {
        new_rx() = 0;
    }
    notifyChanged();
}

void xQuickShellCursor::moveRight(int positions)
{
    int width = m_screen->width();
    if (new_x() == width -1 || !positions)
        return;
    if (positions < width - new_x()) {
        new_rx() += positions;
    } else {
        new_rx() = width -1;
    }

    notifyChanged();
}

void xQuickShellCursor::move(int new_x, int new_y)
{
    int width = m_screen->width();

    if (m_origin_at_margin) {
        new_y += m_top_margin;
    }

    if (new_x < 0) {
        new_x = 0;
    } else if (new_x >= width) {
        new_x = width - 1;
    }

    if (new_y < adjusted_top()) {
        new_y = adjusted_top();
    } else if (new_y > adjusted_bottom()) {
        new_y = adjusted_bottom();
    }

    if (this->new_y() != new_y || this->new_x() != new_x) {
        m_new_position = QPoint(new_x, new_y);
        notifyChanged();
    }
}

void xQuickShellCursor::moveToLine(int line)
{
    const int height = m_screen->height();
    if (line < adjusted_top()) {
        line = 0;
    } else if (line > adjusted_bottom()) {
        line = height -1;
    }

    if (line != new_y()) {
        new_rx() = line;
        notifyChanged();
    }
}

void xQuickShellCursor::moveToCharacter(int character)
{
    const int width = m_screen->width();
    if (character < 0) {
        character = 1;
    } else if (character > width) {
        character = width;
    }
    if (character != new_x()) {
        new_rx() = character;
        notifyChanged();
    }
}

void xQuickShellCursor::moveToNextTab()
{
    for (int i = 0; i < m_tab_stops.size(); i++) {
        if (new_x() < m_tab_stops.at(i)) {
            moveToCharacter(std::min(m_tab_stops.at(i), m_screen_width -1));
            return;
        }
    }
    moveToCharacter(m_screen_width - 1);
}

void xQuickShellCursor::setTabStop()
{
    int i;
    for (i = 0; i < m_tab_stops.size(); i++) {
        if (new_x() == m_tab_stops.at(i))
            return;
        if (new_x() > m_tab_stops.at(i)) {
            continue;
        } else {
            break;
        }
    }
    m_tab_stops.insert(i,new_x());
}

void xQuickShellCursor::removeTabStop()
{
    for (int i = 0; i < m_tab_stops.size(); i++) {
        if (new_x() == m_tab_stops.at(i)) {
            m_tab_stops.remove(i);
            return;
        } else if (new_x() < m_tab_stops.at(i)) {
            return;
        }
    }
}

void xQuickShellCursor::clearTabStops()
{
    m_tab_stops.clear();
}

void xQuickShellCursor::clearToBeginningOfLine()
{
    screen_data()->clearToBeginningOfLine(m_new_position);
}

void xQuickShellCursor::clearToEndOfLine()
{
    screen_data()->clearToEndOfLine(m_new_position);
}

void xQuickShellCursor::clearToBeginningOfxQuickShellScreen()
{
    clearToBeginningOfLine();
    if (new_y() > 0)
        screen_data()->clearToBeginningOfxQuickShellScreen(m_new_position.y()-1);
}

void xQuickShellCursor::clearToEndOfxQuickShellScreen()
{
    clearToEndOfLine();
    if (new_y() < m_screen->height() -1) {
        screen_data()->clearToEndOfxQuickShellScreen(m_new_position.y()+1);
    }
}

void xQuickShellCursor::clearLine()
{
    screen_data()->clearLine(m_new_position);
}

void xQuickShellCursor::deleteCharacters(int characters)
{
    screen_data()->deleteCharacters(m_new_position, new_x() + characters -1);
}

void xQuickShellCursor::setWrapAround(bool wrap)
{
    m_wrap_around = wrap;
}

void xQuickShellCursor::addAtxQuickShellCursor(const QByteArray &data, bool only_latin)
{
    if (m_insert_mode == Replace) {
        replaceAtxQuickShellCursor(data, only_latin);
    } else {
        insertAtxQuickShellCursor(data, only_latin);
    }
}

void xQuickShellCursor::replaceAtxQuickShellCursor(const QByteArray &data, bool only_latin)
{
    const QString text = m_gl_text_codec->toUnicode(data);

    if (!m_wrap_around && new_x() + text.size() > m_screen->width()) {
        const int size = m_screen_width - new_x();
        QString toxQuickShellBlock = text.mid(0,size);
        toxQuickShellBlock.replace(toxQuickShellBlock.size() - 1, 1, text.at(text.size()-1));
        screen_data()->replace(m_new_position, toxQuickShellBlock, m_current_text_style, only_latin);
        new_rx() += toxQuickShellBlock.size();
    } else {
        auto diff = screen_data()->replace(m_new_position, text, m_current_text_style, only_latin);
        new_rx() += diff.character;
        new_ry() += diff.line;
    }

    if (new_y() >= m_screen_height) {
        new_ry() = m_screen_height - 1;
    }

    notifyChanged();
}

void xQuickShellCursor::insertAtxQuickShellCursor(const QByteArray &data, bool only_latin)
{
    const QString text = m_gl_text_codec->toUnicode(data);
    auto diff = screen_data()->insert(m_new_position, text, m_current_text_style, only_latin);
    new_rx() += diff.character;
    new_ry() += diff.line;
    if (new_y() >= m_screen_height)
        new_ry() = m_screen_height - 1;
    if (new_x() >= m_screen_width)
        new_rx() = m_screen_width - 1;
}

void xQuickShellCursor::lineFeed()
{
    if(new_y() >= bottom()) {
        screen_data()->insertLine(bottom(), top());
    } else {
        new_ry()++;
        notifyChanged();
    }
}

void xQuickShellCursor::reverseLineFeed()
{
    if (new_y() == top()) {
        scrollUp(1);
    } else {
        new_ry()--;
        notifyChanged();
    }
}

void xQuickShellCursor::setOriginAtMargin(bool atMargin)
{
    m_origin_at_margin = atMargin;
    m_new_position = QPoint(0, adjusted_top());
    notifyChanged();
}

void xQuickShellCursor::setScrollArea(int from, int to)
{
    m_top_margin = from;
    m_bottom_margin = std::min(to,m_screen_height -1);
    m_scroll_margins_set = true;
}

void xQuickShellCursor::resetScrollArea()
{
    m_top_margin = 0;
    m_bottom_margin = 0;
    m_scroll_margins_set = false;
}

void xQuickShellCursor::dispatchEvents()
{
    if (m_new_position != m_position|| m_content_height_changed) {
        bool emit_x_changed = m_new_position.x() != m_position.x();
        bool emit_y_changed = m_new_position.y() != m_position.y();
        m_position = m_new_position;
        if (emit_x_changed)
            emit xChanged();
        if (emit_y_changed || m_content_height_changed)
            emit yChanged();
    }

    if (m_new_visibillity != m_visible) {
        m_visible = m_new_visibillity;
        emit visibilityChanged();
    }

    if (m_new_blinking != m_blinking) {
        m_blinking = m_new_blinking;
        emit blinkingChanged();
    }
}

void xQuickShellCursor::contentHeightChanged()
{
    m_content_height_changed = true;
}

