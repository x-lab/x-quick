#pragma once

#include "xQuick+profiler_client.hpp"
#include "xQuick+profiler_data.hpp"

#include <private/qqmldebugconnection_p.h>

#include <QtCore/qcoreapplication.h>
#include <QtCore/qprocess.h>
#include <QtCore/qtimer.h>
#include <QtNetwork/qabstractsocket.h>

class xQuickProfiler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool running READ running NOTIFY runningChanged)

public:
     xQuickProfiler(QObject *parent = 0);
    ~xQuickProfiler(void);

signals:
    void ready(void);
    void runningChanged(void);

public slots:
    Q_INVOKABLE void initialise(void);
    Q_INVOKABLE void start(void);
    Q_INVOKABLE void stop(void);
    Q_INVOKABLE void clear(void);

public slots:
    void flush(void);

public:
    inline bool running(void)
    {
        return m_running;
    }

private slots:
    void tryConnect(void);
    void    connected(void);
    void disconnected(void);

private slots:
    void logError(const QString &);
    void logStatus(const QString &);

private:
    QScopedPointer<QQmlDebugConnection> m_connection;
    QScopedPointer<QmlProfilerClient> m_qmlProfilerClient;
    QScopedPointer<QmlProfilerData> m_profilerData;

private:
    bool m_verbose = false;
    bool m_running = false;

private:
    QTimer   m_flushTimer;
    QTimer m_connectTimer;

    QString  m_flushFile;
    QString m_socketFile;

private:
    uint m_connectionAttempts;
};
