#include "xQuickShell+screen.hpp"
#include "xQuickShell+screen_data.hpp"
#include "xQuickShell+block.hpp"
#include "xQuickShell+cursor.hpp"
#include "xQuickShell+text.hpp"
#include "xQuickShell+scrollback.hpp"
#include "xQuickShell+selection.hpp"
#include "xQuickShell+controll_chars.hpp"
#include "xQuickShell+character_sets.hpp"

#include <QtCore/QTimer>
#include <QtCore/QSocketNotifier>
#include <QtGui/QGuiApplication>

#include <QtCore/QDebug>

#include <float.h>
#include <cmath>

xQuickShellScreen::xQuickShellScreen(QObject *parent)
    : QObject(parent)
    , m_palette(new xQuickShellColorPalette(this))
    , m_parser(this)
    , m_timer_event_id(0)
    , m_width(1)
    , m_height(0)
    , m_primary_data(new xQuickShellScreenData(500, this))
    , m_alternate_data(new xQuickShellScreenData(0, this))
    , m_current_data(m_primary_data)
    , m_old_current_data(m_primary_data)
    , m_selection(new xQuickShellSelection(this))
    , m_flash(false)
    , m_cursor_changed(false)
    , m_application_cursor_key_mode(false)
    , m_fast_scroll(true)
    , m_default_background(m_palette->normalColor(xQuickShellColorPalette::DefaultBackground))
{
    xQuickShellCursor *cursor = new xQuickShellCursor(this);
    m_cursor_stack << cursor;
    m_new_cursors << cursor;

    connect(m_primary_data, SIGNAL(contentHeightChanged()), this, SIGNAL(contentHeightChanged()));
    connect(m_primary_data, &xQuickShellScreenData::contentModified, this, &xQuickShellScreen::contentModified);
    connect(m_primary_data, &xQuickShellScreenData::dataHeightChanged, this, &xQuickShellScreen::dataHeightChanged);
    connect(m_primary_data, &xQuickShellScreenData::dataWidthChanged, this, &xQuickShellScreen::dataWidthChanged);
    connect(m_palette, SIGNAL(changed()), this, SLOT(paletteChanged()));

    setHeight(25);
    setWidth(80);

    connect(&m_pty, &xQuickShellPty::readyRead, this, &xQuickShellScreen::readData);
    connect(&m_pty, &xQuickShellPty::hangupReceived,this, &xQuickShellScreen::hangup);

}

xQuickShellScreen::~xQuickShellScreen()
{

    for(int i = 0; i < m_to_delete.size(); i++) {
        delete m_to_delete.at(i);
    }
    //m_to_delete.clear();

    delete m_primary_data;
    delete m_alternate_data;
}


QColor xQuickShellScreen::defaultForegroundColor() const
{
    return m_palette->normalColor(xQuickShellColorPalette::DefaultForeground);
}

QColor xQuickShellScreen::defaultBackgroundColor() const
{
    return m_palette->normalColor(xQuickShellColorPalette::DefaultBackground);
}

void xQuickShellScreen::emitRequestHeight(int newHeight)
{
    emit requestHeightChange(newHeight);
}

void xQuickShellScreen::setHeight(int height)
{
    height = std::max(1, height);
    if (height == m_height)
        return;

    m_height = height;

    m_primary_data->setHeight(height, currentxQuickShellCursor()->new_y());
    m_alternate_data->setHeight(height, currentxQuickShellCursor()->new_y());

    m_pty.setHeight(height, height * 10);

    emit heightChanged();
    scheduleEventDispatch();
}

int xQuickShellScreen::height() const
{
    return m_height;
}

int xQuickShellScreen::contentHeight() const
{
    return currentxQuickShellScreenData()->contentHeight();
}

void xQuickShellScreen::emitRequestWidth(int newWidth)
{
    emit requestWidthChange(newWidth);
}

void xQuickShellScreen::setWidth(int width)
{
    width = std::max(1,width);
    if (width == m_width)
        return;

    emit widthAboutToChange(width);

    m_width = width;

    m_primary_data->setWidth(width);
    m_alternate_data->setWidth(width);

    m_pty.setWidth(width, width * 10);

    emit widthChanged();
    scheduleEventDispatch();
}

int xQuickShellScreen::width() const
{
    return m_width;
}

void xQuickShellScreen::useAlternatexQuickShellScreenBuffer()
{
    if (m_current_data == m_primary_data) {
        disconnect(m_primary_data, SIGNAL(contentHeightChanged()), this, SIGNAL(contentHeightChanged()));
        disconnect(m_primary_data, &xQuickShellScreenData::contentModified, this, &xQuickShellScreen::contentModified);
        disconnect(m_primary_data, &xQuickShellScreenData::dataHeightChanged, this, &xQuickShellScreen::dataHeightChanged);
        disconnect(m_primary_data, &xQuickShellScreenData::dataWidthChanged, this, &xQuickShellScreen::dataWidthChanged);
        m_current_data = m_alternate_data;
        m_current_data->clear();
        connect(m_alternate_data, SIGNAL(contentHeightChanged()), this, SIGNAL(contentHeightChanged()));
        connect(m_alternate_data, &xQuickShellScreenData::contentModified, this, &xQuickShellScreen::contentModified);
        connect(m_alternate_data, &xQuickShellScreenData::dataHeightChanged, this, &xQuickShellScreen::dataHeightChanged);
        connect(m_alternate_data, &xQuickShellScreenData::dataWidthChanged, this, &xQuickShellScreen::dataWidthChanged);
        emit contentHeightChanged();
    }
}

void xQuickShellScreen::useNormalxQuickShellScreenBuffer()
{
    if (m_current_data == m_alternate_data) {
        disconnect(m_alternate_data, SIGNAL(contentHeightChanged()), this, SIGNAL(contentHeightChanged()));
        disconnect(m_alternate_data, &xQuickShellScreenData::contentModified, this, &xQuickShellScreen::contentModified);
        disconnect(m_alternate_data, &xQuickShellScreenData::dataHeightChanged, this, &xQuickShellScreen::dataHeightChanged);
        disconnect(m_alternate_data, &xQuickShellScreenData::dataWidthChanged, this, &xQuickShellScreen::dataWidthChanged);
        m_current_data = m_primary_data;
        connect(m_primary_data, SIGNAL(contentHeightChanged()), this, SIGNAL(contentHeightChanged()));
        connect(m_primary_data, &xQuickShellScreenData::contentModified, this, &xQuickShellScreen::contentModified);
        connect(m_primary_data, &xQuickShellScreenData::dataHeightChanged, this, &xQuickShellScreen::dataHeightChanged);
        connect(m_primary_data, &xQuickShellScreenData::dataWidthChanged, this, &xQuickShellScreen::dataWidthChanged);
        emit contentHeightChanged();
    }
}

xQuickShellTextStyle xQuickShellScreen::defaultxQuickShellTextStyle() const
{
    xQuickShellTextStyle style;
    style.style = xQuickShellTextStyle::Normal;
    style.foreground = colorPalette()->defaultForeground().rgb();
    style.background = colorPalette()->defaultBackground().rgb();
    return style;
}

void xQuickShellScreen::savexQuickShellCursor()
{
    xQuickShellCursor *new_cursor = new xQuickShellCursor(this);
    if (m_cursor_stack.size())
        m_cursor_stack.last()->setVisible(false);
    m_cursor_stack << new_cursor;
    m_new_cursors << new_cursor;
}

void xQuickShellScreen::restorexQuickShellCursor()
{
    if (m_cursor_stack.size() <= 1)
        return;

    m_delete_cursors.append(m_cursor_stack.takeLast());
    m_cursor_stack.last()->setVisible(true);
}

void xQuickShellScreen::clearxQuickShellScreen()
{
    currentxQuickShellScreenData()->clear();
}


xQuickShellColorPalette *xQuickShellScreen::colorPalette() const
{
    return m_palette;
}

void xQuickShellScreen::fill(const QChar character)
{
    currentxQuickShellScreenData()->fill(character);
}

void xQuickShellScreen::clear()
{
    fill(QChar(' '));
}

void xQuickShellScreen::setFastScroll(bool fast)
{
    m_fast_scroll = fast;
}

bool xQuickShellScreen::fastScroll() const
{
    return m_fast_scroll;
}

xQuickShellSelection *xQuickShellScreen::selection() const
{
    return m_selection;
}

void xQuickShellScreen::doubleClicked(double character, double line)
{
    QPoint start, end;
    int64_t charInt = std::llround(character);
    int64_t lineInt = std::llround(line);
    Q_ASSERT(charInt >= 0);
    //Q_ASSERT(charInt < std::numeric_limits<std::size_t>::max());
    Q_ASSERT(lineInt >= 0);
    //Q_ASSERT(lineInt < std::numeric_limits<std::size_t>::max());
    auto selectionRange = currentxQuickShellScreenData()->getDoubleClickxQuickShellSelectionRange(charInt, lineInt);
    m_selection->setStartX(selectionRange.start.x());
    m_selection->setStartY(selectionRange.start.y());
    m_selection->setEndX(selectionRange.end.x());
    m_selection->setEndY(selectionRange.end.y());
}

void xQuickShellScreen::setTitle(const QString &title)
{
    m_title = title;
    emit screenTitleChanged();
}

QString xQuickShellScreen::title() const
{
    return m_title;
}

QString xQuickShellScreen::platformName() const
{
    return qGuiApp->platformName();
}

void xQuickShellScreen::scheduleFlash()
{
    m_flash = true;
}

void xQuickShellScreen::printxQuickShellScreen() const
{
    currentxQuickShellScreenData()->printStyleInformation();
    qDebug() << "Total height: " << currentxQuickShellScreenData()->contentHeight();
}

void xQuickShellScreen::scheduleEventDispatch()
{
    if (!m_timer_event_id) {
        m_timer_event_id = startTimer(1);
        m_time_since_initiated.restart();
    }

    m_time_since_parsed.restart();
}

void xQuickShellScreen::dispatchChanges()
{
    if (m_old_current_data != m_current_data) {
        m_old_current_data->releasexQuickShellTextObjects();
        m_old_current_data = m_current_data;
    }

    currentxQuickShellScreenData()->dispatchLineEvents();
    emit dispatchxQuickShellTextSegmentChanges();

    //be smarter than this
    static int max_to_delete_size = 0;
    if (max_to_delete_size < m_to_delete.size()) {
        max_to_delete_size = m_to_delete.size();
    }

    if (m_flash) {
        m_flash = false;
        emit flash();
    }

    for (int i = 0; i < m_delete_cursors.size(); i++) {
        int new_index = m_new_cursors.indexOf(m_delete_cursors.at(i));
        if (new_index >= 0)
            m_new_cursors.remove(new_index);
        delete m_delete_cursors.at(i);
    }
    m_delete_cursors.clear();

    for (int i = 0; i < m_new_cursors.size(); i++) {
        emit cursorCreated(m_new_cursors.at(i));
    }
    m_new_cursors.clear();

    for (int i = 0; i < m_cursor_stack.size(); i++) {
        m_cursor_stack[i]->dispatchEvents();
    }

    m_selection->dispatchChanges();
}

void xQuickShellScreen::sendPrimaryDA()
{
    m_pty.write(QByteArrayLiteral("\033[?6c"));

}

void xQuickShellScreen::sendSecondaryDA()
{
    m_pty.write(QByteArrayLiteral("\033[>1;95;0c"));
}

void xQuickShellScreen::setApplicationxQuickShellCursorKeysMode(bool enable)
{
    m_application_cursor_key_mode = enable;
}

bool xQuickShellScreen::applicationxQuickShellCursorKeyMode() const
{
    return m_application_cursor_key_mode;
}

void xQuickShellScreen::ensureVisiblexQuickShellPages(int top_line)
{
    currentxQuickShellScreenData()->ensureVisiblexQuickShellPages(top_line);
}

static bool hasControll(Qt::KeyboardModifiers modifiers)
{
#ifdef Q_OS_MAC
    return modifiers & Qt::MetaModifier;
#else
    return modifiers & Qt::ControlModifier;
#endif
}

static bool hasMeta(Qt::KeyboardModifiers modifiers)
{
#ifdef Q_OS_MAC
    return modifiers & Qt::ControlModifier;
#else
    return modifiers & Qt::MetaModifier;
#endif
}

void xQuickShellScreen::sendKey(const QString &text, Qt::Key key, Qt::KeyboardModifiers modifiers)
{

//    if (key == Qt::Key_Control)
//        printxQuickShellScreen();
    /// UGH, this function should be re-written
    char escape = '\0';
    char  control = '\0';
    char  code = '\0';
    QVector<ushort> parameters;
    bool found = true;

    switch(key) {
    case Qt::Key_Up:
        escape = C0::ESC;
        if (m_application_cursor_key_mode)
            control = C1_7bit::SS3;
        else
            control = C1_7bit::CSI;

        code = 'A';
        break;
    case Qt::Key_Right:
        escape = C0::ESC;
        if (m_application_cursor_key_mode)
            control = C1_7bit::SS3;
        else
            control = C1_7bit::CSI;

        code = 'C';
        break;
    case Qt::Key_Down:
        escape = C0::ESC;
        if (m_application_cursor_key_mode)
            control = C1_7bit::SS3;
        else
            control = C1_7bit::CSI;

            code = 'B';
        break;
    case Qt::Key_Left:
        escape = C0::ESC;
        if (m_application_cursor_key_mode)
            control = C1_7bit::SS3;
        else
            control = C1_7bit::CSI;

        code = 'D';
        break;
    case Qt::Key_Insert:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(2);
        code = '~';
        break;
    case Qt::Key_Delete:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(3);
        code = '~';
        break;
    case Qt::Key_Home:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(1);
        code = '~';
        break;
    case Qt::Key_End:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(4);
        code = '~';
        break;
    case Qt::Key_PageUp:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(5);
        code = '~';
        break;
    case Qt::Key_PageDown:
        escape = C0::ESC;
        control = C1_7bit::CSI;
        parameters.append(6);
        code = '~';
        break;
    case Qt::Key_F1:
    case Qt::Key_F2:
    case Qt::Key_F3:
    case Qt::Key_F4:
        if (m_application_cursor_key_mode) {
            parameters.append((key & 0xff) - 37);
            escape = C0::ESC;
            control = C1_7bit::CSI;
            code = '~';
        }
        break;
    case Qt::Key_F5:
    case Qt::Key_F6:
    case Qt::Key_F7:
    case Qt::Key_F8:
    case Qt::Key_F9:
    case Qt::Key_F10:
    case Qt::Key_F11:
    case Qt::Key_F12:
        if (m_application_cursor_key_mode) {
            parameters.append((key & 0xff) - 36);
            escape = C0::ESC;
            control = C1_7bit::CSI;
            code = '~';
        }
        break;
    case Qt::Key_Control:
    case Qt::Key_Shift:
    case Qt::Key_Alt:
    case Qt::Key_AltGr:
        return;
        break;
    default:
        found = false;
    }

    if (found) {
        int term_mods = 0;
        if (modifiers & Qt::ShiftModifier)
            term_mods |= 1;
        if (modifiers & Qt::AltModifier)
            term_mods |= 2;
        if (modifiers & Qt::ControlModifier)
            term_mods |= 4;

        QByteArray toPty;

        if (term_mods) {
            term_mods++;
            parameters.append(term_mods);
        }
        if (escape)
            toPty.append(escape);
        if (control)
            toPty.append(control);
        if (parameters.size()) {
            for (int i = 0; i < parameters.size(); i++) {
                if (i)
                    toPty.append(';');
                toPty.append(QByteArray::number(parameters.at(i)));
            }
        }
        if (code)
            toPty.append(code);
        m_pty.write(toPty);

    } else {
        QString verifiedxQuickShellText = text.simplified();
        if (verifiedxQuickShellText.isEmpty()) {
            switch (key) {
            case Qt::Key_Return:
            case Qt::Key_Enter:
                verifiedxQuickShellText = "\r";
                break;
            case Qt::Key_Backspace:
                verifiedxQuickShellText = "\010";
                break;
            case Qt::Key_Tab:
                verifiedxQuickShellText = "\t";
                break;
            case Qt::Key_Control:
            case Qt::Key_Meta:
            case Qt::Key_Alt:
            case Qt::Key_Shift:
                return;
            case Qt::Key_Space:
                verifiedxQuickShellText = " ";
                break;
            default:
                return;
            }
        }
        QByteArray to_pty;
        QByteArray key_text;
        if (hasControll(modifiers)) {
            char key_char = verifiedxQuickShellText.toLocal8Bit().at(0);
            key_text.append(key_char & 0x1F);
        } else {
            key_text = verifiedxQuickShellText.toUtf8();
        }

        if (modifiers &  Qt::AltModifier) {
            to_pty.append(C0::ESC);
        }

        if (hasMeta(modifiers)) {
            to_pty.append(C0::ESC);
            to_pty.append('@');
            to_pty.append(FinalBytesNoIntermediate::Reserved3);
        }

        to_pty.append(key_text);
        m_pty.write(to_pty);
    }
}

xQuickShellPty *xQuickShellScreen::pty()
{
    return &m_pty;
}

xQuickShellText *xQuickShellScreen::createxQuickShellTextSegment(const xQuickShellTextStyleLine &style_line)
{
    Q_UNUSED(style_line);
    xQuickShellText *to_return;
    if (m_to_delete.size()) {
        to_return = m_to_delete.takeLast();
        to_return->setVisible(true);
    } else {
        to_return = new xQuickShellText(this);
        emit textCreated(to_return);
    }

    return to_return;
}

void xQuickShellScreen::releasexQuickShellTextSegment(xQuickShellText *text)
{
    m_to_delete.append(text);
}

void xQuickShellScreen::readData(const QByteArray &data)
{
    m_parser.addData(data);

    scheduleEventDispatch();
}

void xQuickShellScreen::paletteChanged()
{
    QColor new_default = m_palette->normalColor(xQuickShellColorPalette::DefaultBackground);
    if (new_default != m_default_background) {
        m_default_background = new_default;
        emit defaultBackgroundColorChanged();
    }
}

void xQuickShellScreen::timerEvent(QTimerEvent *)
{
    if (m_timer_event_id && (m_time_since_parsed.elapsed() > 3 || m_time_since_initiated.elapsed() > 25)) {
        killTimer(m_timer_event_id);
        m_timer_event_id = 0;
        dispatchChanges();
    }
}
