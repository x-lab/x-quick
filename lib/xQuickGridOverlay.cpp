#include <QSGFlatColorMaterial>
#include <QSGGeometryNode>

#include "xQuickGrid.hpp"
#include "xQuickGridOverlay.hpp"

xQuickGridOverlay::xQuickGridOverlay(QQuickItem *parent) : QQuickItem(parent)
{
    this->setFlag(QQuickItem::ItemHasContents, true);

    if(xQuickGrid* g = qobject_cast<xQuickGrid*>(parent)) {

        m_grid = g;

        connect (m_grid, &xQuickGrid::gridChanged,    this, &xQuickGridOverlay::onxQuickGridChanged, Qt::UniqueConnection);
        connect (m_grid, &xQuickGrid::widthChanged,   this, &xQuickGridOverlay::onxQuickGridChanged, Qt::UniqueConnection);
        connect (m_grid, &xQuickGrid::heightChanged,  this, &xQuickGridOverlay::onxQuickGridChanged, Qt::UniqueConnection);
    }
}

xQuickGridOverlay::~xQuickGridOverlay(void)
{
    if (m_grid)
        disconnect(m_grid, Q_NULLPTR, this, Q_NULLPTR);
}

void xQuickGridOverlay::onxQuickGridChanged(void)
{
    update();
}

QSGNode *xQuickGridOverlay::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData)

    if(!m_grid)
        return new QSGGeometryNode;

    QSGGeometryNode *node = nullptr;
    QSGGeometry *geometry = nullptr;
    QSGGeometry::Point2D *vertices;
    QSGFlatColorMaterial *material;

    int points = static_cast<int>(m_grid->columns()) * 3 /* triangles */ * 2 /* 2 per quad */;

    if(!oldNode) {

        geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), points);
        geometry->setVertexDataPattern(QSGGeometry::DynamicPattern);
        geometry->setDrawingMode(QSGGeometry::DrawTriangles);

        material = new QSGFlatColorMaterial;
        material->setColor(m_grid->columnColor());

        node = new QSGGeometryNode;
        node->setGeometry(geometry);
        node->setFlag(QSGNode::OwnsGeometry);
        node->setMaterial(material);
        node->setFlag(QSGNode::OwnsMaterial);

    } else {

        node = static_cast<QSGGeometryNode *>(oldNode);

        geometry = node->geometry();
        geometry->allocate(points);
    }

    vertices = geometry->vertexDataAsPoint2D();

    if(m_grid->showGrid()) {

        int ys = 0;

        float xe = static_cast<float>(m_grid->columnWidth());
        float ye = static_cast<float>(m_grid->height());

        for(int i = 0; i < m_grid->columns(); i++) {

            float xs = m_grid->columnStart(i);
            vertices[(6*i)+0].set(xs, ys);    // v1
            vertices[(6*i)+1].set(xs, ye);    // v2
            vertices[(6*i)+2].set(xs+xe, ye); // v3
            vertices[(6*i)+3].set(xs+xe, ye); // v3
            vertices[(6*i)+4].set(xs+xe, ys); // v4
            vertices[(6*i)+5].set(xs, ys);    // v1
        }
    }

    geometry->markVertexDataDirty();

    node->markDirty(QSGNode::DirtyGeometry | QSGNode::DirtyMaterial);

    return node;
}
