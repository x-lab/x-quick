#pragma once

#include "xQuickShell+text_style.hpp"
#include "xQuickShell+block.hpp"
#include "xQuickShell+selection.hpp"

#include <QtCore/QVector>
#include <QtCore/QPoint>
#include <QtCore/QObject>
#include <QtGui/QClipboard>

#include <QtCore/QDebug>

class xQuickShellScreen;
class xQuickShellScrollback;

class xQuickShellCursorDiff
{
public:
    int line;
    int character;
};

class xQuickShellScreenData : public QObject
{
Q_OBJECT
public:
    xQuickShellScreenData(size_t max_scrollback, xQuickShellScreen *screen);
    ~xQuickShellScreenData();

    int contentHeight() const;

    void clearToEndOfLine(const QPoint &pos);
    void clearToEndOfxQuickShellScreen(int y);
    void clearToBeginningOfLine(const QPoint &pos);
    void clearToBeginningOfxQuickShellScreen(int y);
    void clearLine(const QPoint &pos);
    void clear();
    void releasexQuickShellTextObjects();

    void clearCharacters(const QPoint &pos, int to);
    void deleteCharacters(const QPoint &pos, int to);

    const xQuickShellCursorDiff replace(const QPoint &pos, const QString &text, const xQuickShellTextStyle &style, bool only_latin);
    const xQuickShellCursorDiff insert(const QPoint &pos, const QString &text, const xQuickShellTextStyle &style, bool only_latin);

    void moveLine(int from, int to);
    void insertLine(int insertAt, int topMargin);

    void fill(const QChar &character);

    void dispatchLineEvents();

    void printRuler(QDebug &debug) const;
    void printStyleInformation() const;

    xQuickShellScreen *screen() const;

    void ensureVisiblexQuickShellPages(int top_line);

    xQuickShellScrollback *scrollback() const;

    void sendxQuickShellSelectionToClipboard(const QPoint &start, const QPoint &end, QClipboard::Mode mode);

    inline std::list<xQuickShellBlock *>::iterator it_for_row(int row);
    inline std::list<xQuickShellBlock *>::iterator it_for_block(xQuickShellBlock *block);
    bool it_is_end(std::list<xQuickShellBlock *>::iterator it) const { return m_screen_blocks.end() == it; }

    const xQuickShellSelectionRange getDoubleClickxQuickShellSelectionRange(size_t character, size_t line);
public slots:
    void setHeight(int height, int currentxQuickShellCursorLine);
    void setWidth(int width);

signals:
    void contentHeightChanged();
    void contentModified(size_t lineModified, int lineDiff, int contentDiff);
    void dataHeightChanged(int newHeight, int removedBeginning, int reclaimed);
    void dataWidthChanged(int newHeight, int removedBeginning, int reclaimed);

private:
    const xQuickShellCursorDiff modify(const QPoint &pos, const QString &text, const xQuickShellTextStyle &style, bool replace, bool only_latin);
    void clearxQuickShellBlock(std::list<xQuickShellBlock *>::iterator line);
    std::list<xQuickShellBlock *>::iterator it_for_row_ensure_single_line_block(int row);
    std::list<xQuickShellBlock *>::iterator split_out_row_from_block(std::list<xQuickShellBlock *>::iterator block_it, int row_in_block);
    int push_at_most_to_scrollback(int lines);
    int reclaim_at_least(int lines);
    int remove_lines_from_end(int lines);
    int ensure_at_least_height(int height);
    int content_height_diff(size_t old_content_height);
    xQuickShellScreen *m_screen;
    xQuickShellScrollback *m_scrollback;
    int m_screen_height;
    int m_height;
    int m_width;
    int m_block_count;
    int m_old_total_lines;

    std::list<xQuickShellBlock *> m_screen_blocks;
};

std::list<xQuickShellBlock *>::iterator xQuickShellScreenData::it_for_row(int row)
{
    if (row >= m_screen_height) {
        return m_screen_blocks.end();
    }
    auto it = m_screen_blocks.end();
    int line_for_block = m_screen_height;
    size_t abs_line = contentHeight();
    while (it != m_screen_blocks.begin()) {
        --it;
        line_for_block -= (*it)->lineCount();
        abs_line -= (*it)->lineCount();
        if (line_for_block <= row) {
            (*it)->setxQuickShellScreenIndex(line_for_block);
            (*it)->setLine(abs_line);
            return it;
        }
    }

    return m_screen_blocks.end();
}
inline std::list<xQuickShellBlock *>::iterator xQuickShellScreenData::it_for_block(xQuickShellBlock *block)
{
    if (!block)
        return m_screen_blocks.end();
    auto it = m_screen_blocks.end();
    int line_for_block = m_screen_height;
    size_t abs_line = contentHeight();
    while(it != m_screen_blocks.begin()) {
        --it;
        line_for_block -= (*it)->lineCount();
        abs_line -= (*it)->lineCount();
        if (*it == block) {
            (*it)->setxQuickShellScreenIndex(line_for_block);
            (*it)->setLine(abs_line);
            return it;
        }
    }
    return m_screen_blocks.end();
}
