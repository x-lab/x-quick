#pragma once

#include <QtCore>
#include <QtQuick>

class xQuickGrid;

class xQuickGridOverlay : public QQuickItem
{
    Q_OBJECT

public:
     xQuickGridOverlay(QQuickItem *parent = nullptr);
    ~xQuickGridOverlay(void) override;

public slots:
    void onxQuickGridChanged(void);

protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData) override;

protected:
    xQuickGrid *m_grid = nullptr;
};
