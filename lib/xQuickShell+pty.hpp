#pragma once

#include <unistd.h>

#include <QtCore/QObject>
#include <QtCore/QLinkedList>
#include <QtCore/QMutex>

class QSocketNotifier;

class xQuickShellPty : public QObject
{
    Q_OBJECT
public:
    xQuickShellPty();
    ~xQuickShellPty();

    void write(const QByteArray &data);

    void setWidth(int width, int pixelWidth = 0);
    void setHeight(int height, int pixelHeight = 0);
    QSize size() const;

    int masterDevice() const;

signals:
    void hangupReceived();
    void readyRead(const QByteArray &data);

private:
    void readData();

    pid_t m_terminal_pid;
    int m_master_fd;
    char m_slave_file_name[PATH_MAX];
    struct winsize *m_winsize;
    char m_data_buffer[1024];
    QSocketNotifier *m_reader;
};
