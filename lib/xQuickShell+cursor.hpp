#pragma once

#include "xQuickShell+text_style.hpp"
#include "xQuickShell+screen.hpp"

#include <QtCore/QObject>

class xQuickShellCursor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibilityChanged)
    Q_PROPERTY(bool blinking READ blinking WRITE setBlinking NOTIFY blinkingChanged)
    Q_PROPERTY(int x READ x NOTIFY xChanged)
    Q_PROPERTY(int y READ y NOTIFY yChanged)
public:
    enum InsertMode {
        Insert,
        Replace
    };

    xQuickShellCursor(xQuickShellScreen *screen);
    ~xQuickShellCursor();

    bool visible() const;
    void setVisible(bool visible);

    bool blinking() const;
    void setBlinking(bool blinking);

    void setxQuickShellTextStyle(xQuickShellTextStyle::Style style, bool add = true);
    void resetStyle();
    xQuickShellTextStyle currentxQuickShellTextStyle() const;

    xQuickShellColorPalette *colorPalette() const;
    void setxQuickShellTextForegroundColor(QRgb color);
    void setxQuickShellTextBackgroundColor(QRgb color);
    void setxQuickShellTextForegroundColorIndex(xQuickShellColorPalette::Color color);
    void setxQuickShellTextBackgroundColorIndex(xQuickShellColorPalette::Color color);

    QPoint position() const;
    int x() const;
    int y() const;
    int new_x() const { return m_new_position.x(); }
    int new_y() const { return m_new_position.y(); }

    void moveOrigin();
    void moveBeginningOfLine();
    void moveUp(int lines = 1);
    void moveDown(int lines = 1);
    void moveLeft(int positions = 1);
    void moveRight(int positions = 1);
    void move(int new_x, int new_y);
    void moveToLine(int line);
    void moveToCharacter(int character);

    void moveToNextTab();
    void setTabStop();
    void removeTabStop();
    void clearTabStops();

    void clearToBeginningOfLine();
    void clearToEndOfLine();
    void clearToBeginningOfxQuickShellScreen();
    void clearToEndOfxQuickShellScreen();
    void clearLine();

    void deleteCharacters(int characters);

    void setWrapAround(bool wrap);
    void addAtxQuickShellCursor(const QByteArray &text, bool only_latin = true);
    void insertAtxQuickShellCursor(const QByteArray &text, bool only_latin = true);
    void replaceAtxQuickShellCursor(const QByteArray &text, bool only_latin = true);

    void lineFeed();
    void reverseLineFeed();

    void setOriginAtMargin(bool atMargin);
    void setScrollArea(int from, int to);
    void resetScrollArea();

    void scrollUp(int lines);
    void scrollDown(int lines);

    void setxQuickShellTextCodec(QTextCodec *codec);

    void setInsertMode(InsertMode mode);

    inline void notifyChanged();
    void dispatchEvents();

public slots:
    void setxQuickShellScreenWidthAboutToChange(int width);
    void setxQuickShellScreenWidth(int newWidth, int removedBeginning, int reclaimed);
    void setxQuickShellScreenHeight(int newHeight, int removedBeginning, int reclaimed);

signals:
    void xChanged();
    void yChanged();
    void visibilityChanged();
    void blinkingChanged();

private slots:
    void contentHeightChanged();

private:
    xQuickShellScreenData *screen_data() const { return m_screen->currentxQuickShellScreenData(); }
    int &new_rx() { return m_new_position.rx(); }
    int &new_ry() { return m_new_position.ry(); }
    int adjusted_new_x() const { return m_origin_at_margin ?
        m_new_position.x() - m_top_margin : m_new_position.x(); }
    int adjusted_new_y() const { return m_origin_at_margin ?
        m_new_position.y() - m_top_margin : m_new_position.y(); }
    int adjusted_top() const { return m_origin_at_margin ? m_top_margin : 0; }
    int adjusted_bottom() const { return m_origin_at_margin ? m_bottom_margin : m_screen_height - 1; }
    int top() const { return m_scroll_margins_set ? m_top_margin : 0; }
    int bottom() const { return m_scroll_margins_set ? m_bottom_margin : m_screen_height - 1; }
    xQuickShellScreen *m_screen;
    xQuickShellTextStyle m_current_text_style;
    QPoint m_position;
    QPoint m_new_position;

    int m_screen_width;
    int m_screen_height;

    int m_top_margin;
    int m_bottom_margin;
    bool m_scroll_margins_set;
    bool m_origin_at_margin;

    QVector<int> m_tab_stops;

    bool m_notified;
    bool m_visible;
    bool m_new_visibillity;
    bool m_blinking;
    bool m_new_blinking;
    bool m_wrap_around;
    bool m_content_height_changed;

    QTextDecoder *m_gl_text_codec;
    QTextDecoder *m_gr_text_codec;

    InsertMode m_insert_mode;

    xQuickShellBlock *m_resize_block;
    int m_current_pos_in_block;
};

void xQuickShellCursor::notifyChanged()
{
    if (!m_notified) {
        m_notified = true;
        m_screen->scheduleEventDispatch();
    }
}
