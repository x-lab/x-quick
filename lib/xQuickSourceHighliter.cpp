#include "xQuickSourceHighliter.hpp"
#include "xQuickSourceHighliterData.hpp"
#include "xQuickSourceHighliterThemes.hpp"

#include <QtDebug>
#if QT_VERSION >= 0x060000
#include <QtCore5Compat>
#endif

#include <algorithm>

xQuickSourceHighliter::xQuickSourceHighliter(QTextDocument *doc) : QSyntaxHighlighter(doc), _language(CodeC)
{
    initFormats();
}

xQuickSourceHighliter::xQuickSourceHighliter(QTextDocument *doc, xQuickSourceHighliter::Themes theme) : QSyntaxHighlighter(doc), _language(CodeC)
{
    setTheme(theme);
}

void xQuickSourceHighliter::initFormats(void)
{
    QTextCharFormat format = QTextCharFormat();

    _formats[Token::CodeBlock] = format;
    format = QTextCharFormat();

    format.setForeground(QColor("#F92672"));
    _formats[Token::CodeKeyWord] = format;
    format = QTextCharFormat();

    format.setForeground(QColor("#a39b4e"));
    _formats[Token::CodeString] = format;
    format = QTextCharFormat();

    format.setForeground(QColor("#75715E"));
    _formats[Token::CodeComment] = format;
    format = QTextCharFormat();

    format.setForeground(QColor("#54aebf"));
    _formats[Token::CodeType] = format;

    format = QTextCharFormat();
    format.setForeground(QColor("#db8744"));
    _formats[Token::CodeOther] = format;

    format = QTextCharFormat();
    format.setForeground(QColor("#AE81FF"));
    _formats[Token::CodeNumLiteral] = format;

    format = QTextCharFormat();
    format.setForeground(QColor("#018a0f"));

    _formats[Token::CodeBuiltIn] = format;
}

void xQuickSourceHighliter::setCurrentLanguage(Language language) {
    if (language != _language)
        _language = language;
}

xQuickSourceHighliter::Language xQuickSourceHighliter::currentLanguage() {
    return _language;
}

void xQuickSourceHighliter::setTheme(xQuickSourceHighliter::Themes theme)
{
    _formats = x_quick_source_theme(theme);
    rehighlight();
}

void xQuickSourceHighliter::highlightBlock(const QString &text)
{
    if (currentBlock() == document()->firstBlock()) {
        setCurrentBlockState(_language);
    } else {
        previousBlockState() == _language ?
                    setCurrentBlockState(_language) :
                    setCurrentBlockState(_language + 1);
    }

    highlightSyntax(text);
}

void xQuickSourceHighliter::highlightSyntax(const QString &text)
{
    if (text.isEmpty()) return;

    const auto textLen = text.length();

    QChar comment;
    bool isCSS = false;
    bool isYAML = false;
    bool isMake = false;
    bool isAsm = false;

    LanguageData keywords{},
                others{},
                types{},
                builtin{},
                literals{};

    switch (currentBlockState()) {
        case CodeLua :
        case CodeLuaComment :
            loadLuaData(types, keywords, builtin, literals, others);
            break;
        case CodeCpp :
        case CodeCppComment :
            loadCppData(types, keywords, builtin, literals, others);
            break;
        case CodeJs :
        case CodeJsComment :
            loadJSData(types, keywords, builtin, literals, others);
            break;
        case CodeC :
        case CodeCComment :
            loadCppData(types, keywords, builtin, literals, others);
            break;
        case CodeBash :
            loadShellData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        case CodePHP :
        case CodePHPComment :
            loadPHPData(types, keywords, builtin, literals, others);
            break;
        case CodeQML :
        case CodeQMLComment :
            loadQMLData(types, keywords, builtin, literals, others);
            break;
        case CodePython :
            loadPythonData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        case CodeRust :
        case CodeRustComment :
            loadRustData(types, keywords, builtin, literals, others);
            break;
        case CodeJava :
        case CodeJavaComment :
            loadJavaData(types, keywords, builtin, literals, others);
            break;
        case CodeCSharp :
        case CodeCSharpComment :
            loadCSharpData(types, keywords, builtin, literals, others);
            break;
        case CodeGo :
        case CodeGoComment :
            loadGoData(types, keywords, builtin, literals, others);
            break;
        case CodeV :
        case CodeVComment :
            loadVData(types, keywords, builtin, literals, others);
            break;
        case CodeSQL :
            loadSQLData(types, keywords, builtin, literals, others);
            break;
        case CodeJSON :
            loadJSONData(types, keywords, builtin, literals, others);
            break;
        case CodeXML :
            xmlHighlighter(text);
            return;
        case CodeCSS :
        case CodeCSSComment :
            isCSS = true;
            loadCSSData(types, keywords, builtin, literals, others);
            break;
        case CodeTypeScript:
        case CodeTypeScriptComment:
            loadTypescriptData(types, keywords, builtin, literals, others);
            break;
        case CodeYAML:
            isYAML = true;
            loadYAMLData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        case CodeINI:
            comment = QLatin1Char('#');
            break;
        case CodeVex:
        case CodeVexComment:
            loadVEXData(types, keywords, builtin, literals, others);
            break;
        case CodeCMake:
            loadCMakeData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        case CodeMake:
            isMake = true;
            loadMakeData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        case CodeAsm:
            isAsm = true;
            loadAsmData(types, keywords, builtin, literals, others);
            comment = QLatin1Char('#');
            break;
        default:
            break;
    }

    setFormat(0, textLen, _formats[CodeBlock]);

    auto applyCodeFormat = [this] (int i, const LanguageData &data, const QString &text, const QTextCharFormat &fmt) -> int
    {
        if (i == 0 || (!text.at(i - 1).isLetterOrNumber() &&
                       text.at(i-1) != QLatin1Char('_'))) {
            const auto wordList = data.values(text.at(i).toLatin1());
            for (const QLatin1String &word : wordList) {
                if (word == text.mid(i, word.size()) &&
                    (i + word.size() == text.length() ||
                     (!text.at(i + word.size()).isLetterOrNumber() &&
                      text.at(i + word.size()) != QLatin1Char('_')))) {
                    setFormat(i, word.size(), fmt);
                    i += word.size();
                }
            }
        }
        return i;
    };

    const QTextCharFormat &formatType = _formats[CodeType];
    const QTextCharFormat &formatKeyword = _formats[CodeKeyWord];
    const QTextCharFormat &formatComment = _formats[CodeComment];
    const QTextCharFormat &formatNumLit = _formats[CodeNumLiteral];
    const QTextCharFormat &formatBuiltIn = _formats[CodeBuiltIn];
    const QTextCharFormat &formatOther = _formats[CodeOther];

    for (int i = 0; i < textLen; ++i) {

        if (currentBlockState() % 2 != 0) goto Comment;

        while (i < textLen && !text[i].isLetter()) {
            if (text[i].isSpace()) {
                ++i;
                if (i == textLen) return;
                if (text[i].isLetter()) break;
                else continue;
            }
            if (comment.isNull() && text[i] == QLatin1Char('/')) {
                if((i+1) < textLen){
                    if(text[i+1] == QLatin1Char('/')) {
                        setFormat(i, textLen, formatComment);
                        return;
                    } else if(text[i+1] == QLatin1Char('*')) {
                        Comment:
                        int next = text.indexOf(QLatin1String("*/"));
                        if (next == -1) {
                            if (currentBlockState() % 2 == 0)
                                setCurrentBlockState(currentBlockState() + 1);
                            setFormat(i, textLen,  formatComment);
                            return;
                        } else {
                            if (currentBlockState() % 2 != 0) {
                                setCurrentBlockState(currentBlockState() - 1);
                            }
                            next += 2;
                            setFormat(i, next - i,  formatComment);
                            i = next;
                            if (i >= textLen) return;
                        }
                    }
                }
            } else if (text[i] == comment) {
                setFormat(i, textLen, formatComment);
                i = textLen;
            } else if (text[i].isNumber()) {
               i = highlightNumericLiterals(text, i);
            } else if (text[i] == QLatin1Char('\"')) {
               i = highlightStringLiterals('\"', text, i);
            }  else if (text[i] == QLatin1Char('\'')) {
               i = highlightStringLiterals('\'', text, i);
            }
            if (i >= textLen) {
                break;
            }
            ++i;
        }

        const int pos = i;

        if (i == textLen || !text[i].isLetter()) continue;

        i = applyCodeFormat(i, types, text, formatType);

        if (i == textLen || !text[i].isLetter()) continue;

        i = applyCodeFormat(i, keywords, text, formatKeyword);
        if (i == textLen || !text[i].isLetter()) continue;

        i = applyCodeFormat(i, literals, text, formatNumLit);
        if (i == textLen || !text[i].isLetter()) continue;

        i = applyCodeFormat(i, builtin, text, formatBuiltIn);
        if (i == textLen || !text[i].isLetter()) continue;

        if (( i == 0 || !text.at(i-1).isLetter()) && others.contains(text[i].toLatin1())) {
            const QList<QLatin1String> wordList = others.values(text[i].toLatin1());
            for(const QLatin1String &word : wordList) {
                if (word == text.mid(i, word.size())
                        &&
                        (i + word.size() == text.length()
                         ||
                         !text.at(i + word.size()).isLetter())
                        ) {
                    currentBlockState() == CodeCpp ?
                                setFormat(i - 1, word.size() + 1, formatOther) :
                                setFormat(i, word.size(), formatOther);
                    i += word.size();
                }
            }
        }

        if (pos == i) {
            int count = i;
            while (count < textLen) {
                if (!text[count].isLetter()) break;
                ++count;
            }
            i = count;
        }
    }

    if (isCSS) cssHighlighter(text);
    if (isYAML) ymlHighlighter(text);
    if (isMake) makeHighlighter(text);
    if (isAsm)  asmHighlighter(text);
}

int xQuickSourceHighliter::highlightStringLiterals(const QChar strType, const QString &text, int i) {
    setFormat(i, 1,  _formats[CodeString]);
    ++i;

    while (i < text.length()) {

        if (text.at(i) == strType && text.at(i-1) != QLatin1Char('\\')) {
            setFormat(i, 1,  _formats[CodeString]);
            ++i;
            break;
        }

        if (text.at(i) == QLatin1Char('\\') && (i+1) < text.length()) {
            int len = 0;
            switch(text.at(i+1).toLatin1()) {
            case 'a':
            case 'b':
            case 'e':
            case 'f':
            case 'n':
            case 'r':
            case 't':
            case 'v':
            case '\'':
            case '"':
            case '\\':
            case '\?':
                len = 2;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            {
                if (i + 4 <= text.length()) {
                    bool isCurrentOctal = true;
                    if (!isOctal(text.at(i+2).toLatin1())) {
                        isCurrentOctal = false;
                        break;
                    }
                    if (!isOctal(text.at(i+3).toLatin1())) {
                        isCurrentOctal = false;
                        break;
                    }
                    len = isCurrentOctal ? 4 : 0;
                }
                break;
            }
            case 'x':
            {
                if (i + 3 <= text.length()) {
                    bool isCurrentHex = true;
                    if (!isHex(text.at(i+2).toLatin1())) {
                        isCurrentHex = false;
                        break;
                    }
                    if (!isHex(text.at(i+3).toLatin1())) {
                        isCurrentHex = false;
                        break;
                    }
                    len = isCurrentHex ? 4 : 0;
                }
                break;
            }
            default:
                break;
            }

            if (len == 0) {
                setFormat(i, 1,  _formats[CodeString]);
                ++i;
                continue;
            }

            setFormat(i, len, _formats[CodeNumLiteral]);
            i += len;
            continue;
        }
        setFormat(i, 1,  _formats[CodeString]);
        ++i;
    }
    return i;
}

int xQuickSourceHighliter::highlightNumericLiterals(const QString &text, int i)
{
    bool isPreAllowed = false;
    if (i == 0) isPreAllowed = true;
    else {
        switch(text.at(i - 1).toLatin1()) {
        case ':':
            if (currentBlockState() == CodeCSS)
                isPreAllowed = true;
            break;
        case '$':
            if (currentBlockState() == CodeAsm)
                isPreAllowed = true;
            break;
        case '[':
        case '(':
        case '{':
        case ' ':
        case ',':
        case '=':
        case '+':
        case '-':
        case '*':
        case '/':
        case '%':
        case '<':
        case '>':
            isPreAllowed = true;
            break;
        }
    }

    if (!isPreAllowed) return i;

    const int start = i;

    if ((i+1) >= text.length()) {
        setFormat(i, 1, _formats[CodeNumLiteral]);
        return ++i;
    }

    ++i;

    if (text.at(i) == QChar('x') && text.at(i - 1) == QChar('0'))
        ++i;

    while (i < text.length()) {
        if (!text.at(i).isNumber() && text.at(i) != QChar('.') &&
             text.at(i) != QChar('e')) //exponent
            break;
        ++i;
    }

    bool isPostAllowed = false;
    if (i == text.length()) {
        if (text.at(i - 1) != QChar('e'))
            isPostAllowed = true;
    } else {
        switch(text.at(i).toLatin1()) {
        case ']':
        case ')':
        case '}':
        case ' ':
        case ',':
        case '=':
        case '+':
        case '-':
        case '*':
        case '/':
        case '%':
        case '>':
        case '<':
        case ';':
            isPostAllowed = true;
            break;
        case 'p':
            if (currentBlockState() == CodeCSS)
                if (i + 1 < text.length() && text.at(i+1) == QChar('x')) {
                    if (i + 2 == text.length() || !text.at(i+2).isLetterOrNumber())
                    isPostAllowed = true;
                }
            break;
        case 'e':
            if (currentBlockState() == CodeCSS)
                if (i + 1 < text.length() && text.at(i+1) == QChar('m')) {
                    if (i + 2 == text.length() || !text.at(i+2).isLetterOrNumber())
                    isPostAllowed = true;
                }
            break;
        case 'u':
        case 'l':
        case 'f':
        case 'U':
        case 'L':
        case 'F':
            if (i + 1 == text.length() || !text.at(i+1).isLetterOrNumber()) {
                isPostAllowed = true;
                ++i;
            }
            break;
        }
    }
    if (isPostAllowed) {
        int end = i;
        setFormat(start, end - start, _formats[CodeNumLiteral]);
    }

    return --i;
}

void xQuickSourceHighliter::ymlHighlighter(const QString &text) {
    if (text.isEmpty()) return;
    const auto textLen = text.length();
    bool colonNotFound = false;

    if (text.trimmed().at(0) == QLatin1Char('#'))
        return;

    for (int i = 0; i < textLen; ++i) {
        if (!text.at(i).isLetter()) continue;

        if (colonNotFound && text.at(i) != QLatin1Char('h')) continue;

        if (i != 0 && (text.at(i-1) == QLatin1Char('"') || text.at(i-1) == QLatin1Char('\''))) {
            const int next = text.indexOf(text.at(i-1), i);
            if (next == -1) break;
            i = next;
            continue;
        }

        const int colon = text.indexOf(QLatin1Char(':'), i);

        if (colon == -1) colonNotFound = true;

        if (!colonNotFound) {
            if (colon+1 == textLen) {
                setFormat(i, colon - i, _formats[CodeKeyWord]);
                return;
            } else {
                if (!(text.at(colon+1) == QLatin1Char('\\') && text.at(colon+1) == QLatin1Char('/'))) {
                    setFormat(i, colon - i, _formats[CodeKeyWord]);
                }
            }
        }

        if (text.at(i) == QLatin1Char('h')) {
            if (text.mid(i, 5) == QLatin1String("https") ||
                    text.mid(i, 4) == QLatin1String("http")) {
                int space = text.indexOf(QChar(' '), i);
                if (space == -1) space = textLen;
                QTextCharFormat f = _formats[CodeString];
                f.setUnderlineStyle(QTextCharFormat::SingleUnderline);
                setFormat(i, space - i, f);
                i = space;
            }
        }
    }
}

void xQuickSourceHighliter::cssHighlighter(const QString &text)
{
    if (text.isEmpty()) return;
    const auto textLen = text.length();
    for (int i = 0; i<textLen; ++i) {
        if (text[i] == QLatin1Char('.') || text[i] == QLatin1Char('#')) {
            if (i+1 >= textLen) return;
            if (text[i + 1].isSpace() || text[i+1].isNumber()) continue;
            int space = text.indexOf(QLatin1Char(' '), i);
            if (space < 0) {
                space = text.indexOf('{');
                if (space < 0) {
                    space = textLen;
                }
            }
            setFormat(i, space - i, _formats[CodeKeyWord]);
            i = space;
        } else if (text[i] == QLatin1Char('c')) {
            if (text.mid(i, 5) == QLatin1String("color")) {
                i += 5;
                int colon = text.indexOf(QLatin1Char(':'), i);
                if (colon < 0) continue;
                i = colon;
                i++;
                while(i < textLen) {
                    if (!text[i].isSpace()) break;
                    i++;
                }
                int semicolon = text.indexOf(QLatin1Char(';'));
                if (semicolon < 0) semicolon = textLen;
                const QString color = text.mid(i, semicolon-i);
                QTextCharFormat f = _formats[CodeBlock];
                QColor c(color);
                if (color.startsWith(QLatin1String("rgb"))) {
                    int t = text.indexOf(QLatin1Char('('), i);
                    int rPos = text.indexOf(QLatin1Char(','), t);
                    int gPos = text.indexOf(QLatin1Char(','), rPos+1);
                    int bPos = text.indexOf(QLatin1Char(')'), gPos);
                    if (rPos > -1 && gPos > -1 && bPos > -1) {
                        const QString r = text.mid(t+1, rPos - (t+1));
                        const QString g = text.mid(rPos+1, gPos - (rPos + 1));
                        const QString b = text.mid(gPos+1, bPos - (gPos+1));
                        c.setRgb(r.toInt(), g.toInt(), b.toInt());
                    } else {
                        c = _formats[CodeBlock].background().color();
                    }
                }

                if (!c.isValid()) {
                    continue;
                }

                int lightness{};
                QColor foreground;
                //really dark
                if (c.lightness() <= 20) {
                    foreground = Qt::white;
                } else if (c.lightness() > 20 && c.lightness() <= 51){
                    foreground = QColor("#ccc");
                } else if (c.lightness() > 51 && c.lightness() <= 78){
                    foreground = QColor("#bbb");
                } else if (c.lightness() > 78 && c.lightness() <= 110){
                    foreground = QColor("#bbb");
                } else if (c.lightness() > 127) {
                    lightness = c.lightness() + 100;
                    foreground = c.darker(lightness);
                }
                else {
                    lightness = c.lightness() + 100;
                    foreground = c.lighter(lightness);
                }

                f.setBackground(c);
                f.setForeground(foreground);
                setFormat(i, semicolon - i, QTextCharFormat()); //clear prev format
                setFormat(i, semicolon - i, f);
                i = semicolon;
            }
        }
    }
}

void xQuickSourceHighliter::xmlHighlighter(const QString &text) {
    if (text.isEmpty()) return;
    const auto textLen = text.length();

    setFormat(0, textLen, _formats[CodeBlock]);

    for (int i = 0; i < textLen; ++i) {
        if (text[i] == QLatin1Char('<') && text[i+1] != QLatin1Char('!')) {

            const int found = text.indexOf(QLatin1Char('>'), i);
            if (found > 0) {
                ++i;
                if (text[i] == QLatin1Char('/')) ++i;
                setFormat(i, found - i, _formats[CodeKeyWord]);
            }
        }

        if (text[i] == QLatin1Char('=')) {
            int lastSpace = text.lastIndexOf(QLatin1Char(' '), i);
            if (lastSpace == i-1) lastSpace = text.lastIndexOf(QLatin1Char(' '), i-2);
            if (lastSpace > 0) {
                setFormat(lastSpace, i - lastSpace, _formats[CodeBuiltIn]);
            }
        }

        if (text[i] == QLatin1Char('\"')) {
            const int pos = i;
            int cnt = 1;
            ++i;
            if ( (i+1) >= textLen) return;
            while (i < textLen) {
                if (text[i] == QLatin1Char('\"')) {
                    ++cnt;
                    ++i;
                    break;
                }
                ++i; ++cnt;
                if ( (i+1) >= textLen) {
                    ++cnt;
                    break;
                }
            }
            setFormat(pos, cnt, _formats[CodeString]);
        }
    }
}

void xQuickSourceHighliter::makeHighlighter(const QString &text)
{
    int colonPos = text.indexOf(QLatin1Char(':'));
    if (colonPos == -1)
        return;
    setFormat(0, colonPos, _formats[Token::CodeBuiltIn]);
}

void xQuickSourceHighliter::highlightInlineAsmLabels(const QString &text)
{
#define Q(s) QStringLiteral(s)
    static const QString jumps[27] = {
        //0 - 19
        Q("jmp"), Q("je"), Q("jne"), Q("jz"), Q("jnz"), Q("ja"), Q("jb"), Q("jg"), Q("jge"), Q("jae"), Q("jl"), Q("jle"),
        Q("jbe"), Q("jo"), Q("jno"), Q("js"), Q("jns"), Q("jcxz"), Q("jecxz"), Q("jrcxz"),
        //20 - 24
        Q("loop"), Q("loope"), Q("loopne"), Q("loopz"), Q("loopnz"),
        //25 - 26
        Q("call"), Q("callq")
    };
#undef Q

    auto format = _formats[Token::CodeBuiltIn];
    format.setFontUnderline(true);

    const QString trimmed = text.trimmed();
    int start = -1;
    int end = -1;
    char c{};
    if (!trimmed.isEmpty())
        c = trimmed.at(0).toLatin1();
    if (c == 'j') {
        start = 0; end = 20;
    } else if (c == 'c') {
        start = 25; end = 27;
    } else if (c == 'l') {
        start = 20; end = 25;
    } else {
        return;
    }

    auto skipSpaces = [&text](int& j){
        while (text.at(j).isSpace()) j++;
        return j;
    };

    for (int i = start; i < end; ++i) {
        if (trimmed.startsWith(jumps[i])) {
            int j = 0;
            skipSpaces(j);
            j = j + jumps[i].length() + 1;
            skipSpaces(j);
            int len = text.length() - j;
            setFormat(j, len, format);
        }
    }
}

void xQuickSourceHighliter::asmHighlighter(const QString& text)
{
    highlightInlineAsmLabels(text);

    int colonPos = text.lastIndexOf(QLatin1Char(':'));
    if (colonPos == -1)
        return;

    bool isComment = text.lastIndexOf('#', colonPos) != -1;
    if (isComment) {
        int commentPos = text.lastIndexOf('#', colonPos);
        colonPos = text.lastIndexOf(':', commentPos);
    }

    auto format = _formats[Token::CodeBuiltIn];
    format.setFontUnderline(true);

    if (colonPos >= text.length() - 1) {
        setFormat(0, colonPos, format);
    }

    int i = 0;
    bool isLabel = true;
    for (i = colonPos + 1; i < text.length(); ++i) {
        if (!text.at(i).isSpace()) {
            isLabel = false;
            break;
        }
    }

    if (!isLabel && i < text.length() && text.at(i) == QLatin1Char('#'))
        setFormat(0, colonPos, format);
}
