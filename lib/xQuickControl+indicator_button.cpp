// xQuickControl+indicator_button.cpp

#include "xQuickControl+indicator_button.hpp"

#include "xQuickControl+deferredexecute.hpp"
#include "xQuickControl+spinbox.hpp"

#include <QtQuickTemplates2/private/qquickcontrol_p_p.h>

// /////////////////////////////////////////////////////////////////////////////

static inline QString indicatorName(void) { return QStringLiteral("indicator"); }

void xQuickControlIndicatorButtonPrivate::cancelIndicator()
{
    Q_Q(xQuickControlIndicatorButton);
    xQuickCancelDeferred(q, indicatorName());
}

void xQuickControlIndicatorButtonPrivate::executeIndicator(bool complete)
{
    Q_Q(xQuickControlIndicatorButton);
    if (indicator.wasExecuted())
        return;

    if (!indicator || complete)
        xQuickBeginDeferred(q, indicatorName(), indicator);
    if (complete)
        xQuickCompleteDeferred(q, indicatorName(), indicator);
}

// /////////////////////////////////////////////////////////////////////////////

xQuickControlIndicatorButton::xQuickControlIndicatorButton(QObject *parent)
    : QObject(*(new xQuickControlIndicatorButtonPrivate), parent)
{
}

bool xQuickControlIndicatorButton::isPressed(void) const
{
    Q_D(const xQuickControlIndicatorButton);
    return d->pressed;
}

void xQuickControlIndicatorButton::setPressed(bool pressed)
{
    Q_D(xQuickControlIndicatorButton);
    if (d->pressed == pressed)
        return;

    d->pressed = pressed;
    emit pressedChanged();
}

QQuickItem *xQuickControlIndicatorButton::indicator(void) const
{
    xQuickControlIndicatorButtonPrivate *d = const_cast<xQuickControlIndicatorButtonPrivate *>(d_func());
    if (!d->indicator)
        d->executeIndicator();
    return d->indicator;
}

void xQuickControlIndicatorButton::setIndicator(QQuickItem *indicator)
{
    Q_D(xQuickControlIndicatorButton);
    if (d->indicator == indicator)
        return;

    if (!d->indicator.isExecuting())
        d->cancelIndicator();

    const qreal oldImplicitIndicatorWidth = implicitIndicatorWidth();
    const qreal oldImplicitIndicatorHeight = implicitIndicatorHeight();

    QQuickControl *par = static_cast<QQuickControl *>(parent());

    QQuickControlPrivate::get(par)->removeImplicitSizeListener(d->indicator);
    QQuickControlPrivate::hideOldItem(d->indicator);
    d->indicator = indicator;

    if (indicator) {
        if (!indicator->parentItem())
            indicator->setParentItem(par);
        QQuickControlPrivate::get(par)->addImplicitSizeListener(indicator);
    }

    if (!qFuzzyCompare(oldImplicitIndicatorWidth, implicitIndicatorWidth()))
        emit implicitIndicatorWidthChanged();
    if (!qFuzzyCompare(oldImplicitIndicatorHeight, implicitIndicatorHeight()))
        emit implicitIndicatorHeightChanged();
    if (!d->indicator.isExecuting())
        emit indicatorChanged();
}

bool xQuickControlIndicatorButton::isHovered(void) const
{
    Q_D(const xQuickControlIndicatorButton);
    return d->hovered;
}

void xQuickControlIndicatorButton::setHovered(bool hovered)
{
    Q_D(xQuickControlIndicatorButton);
    if (d->hovered == hovered)
        return;

    d->hovered = hovered;
    emit hoveredChanged();
}

qreal xQuickControlIndicatorButton::implicitIndicatorWidth(void) const
{
    Q_D(const xQuickControlIndicatorButton);
    if (!d->indicator)
        return 0;
    return d->indicator->implicitWidth();
}

qreal xQuickControlIndicatorButton::implicitIndicatorHeight(void) const
{
    Q_D(const xQuickControlIndicatorButton);
    if (!d->indicator)
        return 0;
    return d->indicator->implicitHeight();
}

// /////////////////////////////////////////////////////////////////////////////

#include "moc_xQuickControl+indicator_button.cpp"

// xQuickControl+indicator_button.cpp ends here
