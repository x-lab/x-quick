#include "xQuickShell+selection.hpp"
#include "xQuickShell+screen.hpp"
#include "xQuickShell+screen_data.hpp"
#include "xQuickShell+block.hpp"

#include <QtGui/QGuiApplication>

xQuickShellSelection::xQuickShellSelection(xQuickShellScreen *screen)
    : QObject(screen)
    , m_screen(screen)
    , m_new_start_x(0)
    , m_start_x(0)
    , m_new_start_y(0)
    , m_start_y(0)
    , m_new_end_x(0)
    , m_end_x(0)
    , m_new_end_y(0)
    , m_end_y(0)
    , m_new_enable(false)
{
    connect(screen, &xQuickShellScreen::contentModified, this, &xQuickShellSelection::screenContentModified);

}

void xQuickShellSelection::setStartX(int x)
{
    if (x != m_new_start_x) {
        m_new_start_x = x;
        setValidity();
        m_screen->scheduleEventDispatch();
    }
}

int xQuickShellSelection::startX() const
{
    return m_start_x;
}

void xQuickShellSelection::setStartY(int y)
{
    if (y != m_new_start_y) {
        m_new_start_y = y;
        setValidity();
        m_screen->scheduleEventDispatch();
    }
}

int xQuickShellSelection::startY() const
{
    return m_start_y;
}

void xQuickShellSelection::setEndX(int x)
{
    if (m_new_end_x != x) {
        m_new_end_x = x;
        setValidity();
        m_screen->scheduleEventDispatch();
    }
}

int xQuickShellSelection::endX() const
{
    return m_end_x;
}

void xQuickShellSelection::setEndY(int y)
{
    if (m_new_end_y != y) {
        m_new_end_y = y;
        setValidity();
        m_screen->scheduleEventDispatch();
    }
}

int xQuickShellSelection::endY() const
{
    return m_end_y;
}

void xQuickShellSelection::setEnable(bool enable)
{
    if (m_new_enable != enable) {
        m_new_enable = enable;
        m_screen->scheduleEventDispatch();
    }
}

void xQuickShellSelection::screenContentModified(size_t lineModified, int lineDiff, int contentDiff)
{
    if (!m_new_enable)
        return;

    if (lineModified >= size_t(m_new_start_y) && lineModified <= size_t(m_new_end_y)) {
        setEnable(false);
        return;
    }

    if (size_t(m_new_end_y) < lineModified && lineDiff != contentDiff) {
        m_new_end_y -= lineDiff - contentDiff;
        m_new_start_y -= lineDiff - contentDiff;
        if (m_new_end_y < 0) {
            setEnable(false);
            return;
        }
        if (m_new_start_y < 0) {
            m_new_start_y = 0;
        }
        m_screen->scheduleEventDispatch();
    }
}

void xQuickShellSelection::setValidity()
{
    if (m_new_end_y > m_new_start_y ||
            (m_new_end_y == m_new_start_y &&
             m_new_end_x > m_new_start_x)) {
        setEnable(true);
    } else {
        setEnable(false);
    }
}

void xQuickShellSelection::sendToClipboard() const
{
    m_screen->currentxQuickShellScreenData()->sendxQuickShellSelectionToClipboard(start_new_point(), end_new_point(), QClipboard::Clipboard);
}

void xQuickShellSelection::sendToxQuickShellSelection() const
{
    m_screen->currentxQuickShellScreenData()->sendxQuickShellSelectionToClipboard(start_new_point(), end_new_point(), QClipboard::Selection);
}

void xQuickShellSelection::pasteFromxQuickShellSelection()
{
    m_screen->pty()->write(QGuiApplication::clipboard()->text(QClipboard::Selection).toUtf8());
}

void xQuickShellSelection::pasteFromClipboard()
{
    m_screen->pty()->write(QGuiApplication::clipboard()->text(QClipboard::Clipboard).toUtf8());
}

void xQuickShellSelection::dispatchChanges()
{
    if (!m_new_enable && !m_enable)
        return;

    if (m_new_start_y != m_start_y) {
        m_start_y = m_new_start_y;
        emit startYChanged();
    }
    if (m_new_start_x != m_start_x) {
        m_start_x = m_new_start_x;
        emit startXChanged();
    }
    if (m_new_end_y != m_end_y) {
        m_end_y = m_new_end_y;
        emit endYChanged();
    }
    if (m_new_end_x != m_end_x) {
        m_end_x = m_new_end_x;
        emit endXChanged();
    }
    if (m_new_enable != m_enable) {
        m_enable = m_new_enable;
        emit enableChanged();
    }
}

static const QChar delimiter_array[] = { ' ', '\n', '{', '(', '[', '}', ')', ']' };
static const size_t delimiter_array_size = sizeof(delimiter_array) / sizeof(delimiter_array[0]);
const xQuickShellSelectionRange xQuickShellSelection::getDoubleClickRange(std::list<xQuickShellBlock *>::iterator it, size_t character, size_t line, int width)
{
    const QString &string = (*it)->textLine();
    size_t start_pos = ((line - (*it)->line()) * width) + character;
    if (start_pos > size_t(string.size()))
        return { QPoint(), QPoint() };
    size_t end_pos = start_pos + 1;
    for (bool found = false; start_pos > 0; start_pos--) {
        for (size_t i = 0; i < delimiter_array_size; i++) {
            if (string.at(start_pos - 1) == delimiter_array[i]) {
                found = true;
                break;
            }
        }
        if (found)
            break;
    }

    for (bool found = false; end_pos < size_t(string.size()); end_pos++) {
        for (size_t i = 0; i < delimiter_array_size; i++) {
            if (string.at(end_pos) == delimiter_array[i]) {
                found = true;
                break;
            }
        }
        if (found)
            break;
    }

    size_t start_line = (start_pos / width) + (*it)->line();
    size_t end_line = (end_pos / width) + (*it)->line();

    return { QPoint(static_cast<int>(start_pos), static_cast<int>(start_line)),
             QPoint(static_cast<int>(end_pos)  , static_cast<int>(end_line)) };
}
bool xQuickShellSelection::enable() const
{
    return m_enable;
}
