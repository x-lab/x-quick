#pragma once

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>

class xQuickWindow : public QObject
{
    Q_OBJECT

public:
     xQuickWindow(QQuickWindow *parent = nullptr);
    ~xQuickWindow(void);

private:
    class xQuickWindowPrivate *d;
};

void xQuickWindowWrap(QQmlApplicationEngine *engine);
