#pragma once

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QLinkedList>

#include "xQuickShell+controll_chars.hpp"
#include "xQuickShell+utf8_decoder.hpp"

class xQuickShellScreen;

class xQuickShellParser
{
public:
    xQuickShellParser(xQuickShellScreen *screen);

    void addData(const QByteArray &data);

private:

    enum DecodeState {
        PlainxQuickShellText,
        DecodeC0,
        DecodeC1_7bit,
        DecodeCSI,
        DecodeOSC,
        DecodeCharacterSet,
        DecodeFontSize
    };

    enum DecodeOSCState {
        None,
        ChangeWindowAndIconName,
        ChangeIconTitle,
        ChangeWindowTitle,
        FilePath,
        Unknown
    };

    void decodeC0(uchar character);
    void decodeC1_7bit(uchar character);
    void decodeParameters(uchar character);
    void decodeCSI(uchar character);
    void decodeOSC(uchar character);
    void decodeCharacterSet(uchar character);
    void decodeFontSize(uchar character);

    void setMode(int mode);
    void setDecMode(int mode);
    void resetMode(int mode);
    void resetDecMode(int mode);

    void handleSGR();
    int handleXtermColor(int param, int i);

    void tokenFinished();

    void appendParameter();
    void handleDefaultParameters(int defaultValue);

    DecodeState m_decode_state;
    DecodeOSCState m_decode_osc_state;
    QByteArray m_osc_data;

    QByteArray m_current_data;

    int m_current_token_start;
    int m_current_position;

    QChar m_intermediate_char;

    QByteArray m_parameter_string;
    QVector<int> m_parameters;
    bool m_parameters_expecting_more;
    bool m_dec_mode;
    bool m_gt_param;
    bool m_lnm_mode_set;
    bool m_contains_only_latin;

    int m_decode_graphics_set;
    QTextCodec *m_graphic_codecs[4];
    Utf8Decoder m_utf8_decoder;

    xQuickShellScreen *m_screen;
    friend QDebug operator<<(QDebug debug, DecodeState decodeState);
};

QDebug operator<<(QDebug debug, xQuickShellParser::DecodeState decodeState);
