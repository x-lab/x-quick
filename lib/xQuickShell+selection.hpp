#pragma once

#include <QtCore/QObject>
#include <QtCore/QPoint>

class xQuickShellScreen;
class xQuickShellBlock;

class xQuickShellSelectionRange
{
public:
    QPoint start;
    QPoint end;
};

class xQuickShellSelection : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int startX READ startX WRITE setStartX NOTIFY startXChanged)
    Q_PROPERTY(int startY READ startY WRITE setStartY NOTIFY startYChanged)
    Q_PROPERTY(int endX READ endX WRITE setEndX NOTIFY endXChanged)
    Q_PROPERTY(int endY READ endY WRITE setEndY NOTIFY endYChanged)
    Q_PROPERTY(bool enable READ enable WRITE setEnable NOTIFY enableChanged)

public:
    explicit xQuickShellSelection(xQuickShellScreen *screen);

    void setStartX(int x);
    int startX() const;

    void setStartY(int y);
    int startY() const;

    void setEndX(int x);
    int endX() const;

    void setEndY(int y);
    int endY() const;

    void setEnable(bool enabled);
    bool enable() const;

    Q_INVOKABLE void sendToClipboard() const;
    Q_INVOKABLE void sendToxQuickShellSelection() const;
    Q_INVOKABLE void pasteFromxQuickShellSelection();
    Q_INVOKABLE void pasteFromClipboard();

    void dispatchChanges();

    static const xQuickShellSelectionRange getDoubleClickRange(std::list<xQuickShellBlock *>::iterator it, size_t character, size_t line, int width);
signals:
    void startXChanged();
    void startYChanged();
    void endXChanged();
    void endYChanged();
    void enableChanged();

private slots:
    void screenContentModified(size_t lineModified, int lineDiff, int contentDiff);

private:
    void setValidity();
    QPoint start_new_point() const { return QPoint(m_new_start_x, m_new_start_y); }
    QPoint end_new_point() const { return QPoint(m_new_end_x, m_new_end_y); }

    xQuickShellScreen *m_screen;
    int m_new_start_x;
    int m_start_x;
    int m_new_start_y;
    int m_start_y;
    int m_new_end_x;
    int m_end_x;
    int m_new_end_y;
    int m_end_y;
    bool m_new_enable;
    bool m_enable;
};
