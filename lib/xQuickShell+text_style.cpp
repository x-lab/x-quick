#include "xQuickShell+text.hpp"
#include "xQuickShell+text_style.hpp"
#include "xQuickShell+screen.hpp"

#include <QtCore/QDebug>

xQuickShellTextStyle::xQuickShellTextStyle()
    : style(Normal)
    , foreground(0)
    , background(0)
{
}

bool xQuickShellTextStyle::isCompatible(const xQuickShellTextStyle &other) const
{
    return foreground == other.foreground
            && background == other.background
            && style == other.style;
}

QDebug operator<<(QDebug debug, xQuickShellTextStyleLine line)
{
    debug << "[" << line.start_index << "(" << line.style << ":" << line.foreground << ":" << line.background << ")" << line.end_index << "]";
    return debug;
}

void xQuickShellTextStyleLine::releasexQuickShellTextSegment(xQuickShellScreen *screen)
{
    if (text_segment) {
        text_segment->setVisible(false);
        screen->releasexQuickShellTextSegment(text_segment);
        text_segment = 0;
    }
}
