#include "xQuickShell+pty.hpp"

#include <fcntl.h>
#include <poll.h>

#ifdef LINUX
#include <sys/epoll.h>
#endif

#include <sys/ioctl.h>
#ifdef Q_OS_MAC
#include <util.h>
#else
#include <pty.h>
#endif
#include <utmp.h>

#include <QtCore/QSize>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QSocketNotifier>
#include <QtCore/QDebug>

static char env_variables[][255] = {
    "TERM=xterm-color",
    "COLORTERM=xterm",
    "COLORFGBG=15;0",
    "LINES",
    "COLUMNS",
    "TERMCAP"
};
static int env_variables_size = sizeof(env_variables) / sizeof(env_variables[0]);

xQuickShellPty::xQuickShellPty()
    : m_winsize(0)
{
    m_terminal_pid = forkpty(&m_master_fd,
                             NULL,
                             NULL,
                             NULL);

    if (m_terminal_pid == 0) {
        for (int i = 0; i < env_variables_size; i++) {
            ::putenv(env_variables[i]);
        }
        ::execl("/bin/bash", "/bin/bash", "--login", (const char *) 0);
        exit(0);
    }

    m_reader = new QSocketNotifier(m_master_fd,QSocketNotifier::Read,this);
    connect(m_reader, &QSocketNotifier::activated, this, &xQuickShellPty::readData);
}

xQuickShellPty::~xQuickShellPty()
{
}

void xQuickShellPty::write(const QByteArray &data)
{
    if (::write(m_master_fd, data.constData(), data.size()) < 0) {
        qDebug() << "Something whent wrong when writing to m_master_fd";
    }
}

void xQuickShellPty::setWidth(int width, int pixelWidth)
{
    if (!m_winsize) {
        m_winsize = new struct winsize;
        m_winsize->ws_row = 25;
        m_winsize->ws_ypixel = 0;
    }

    m_winsize->ws_col = width;
    m_winsize->ws_xpixel = pixelWidth;
    ioctl(m_master_fd, TIOCSWINSZ, m_winsize);
}

void xQuickShellPty::setHeight(int height, int pixelHeight)
{
    if (!m_winsize) {
        m_winsize = new struct winsize;
        m_winsize->ws_col = 80;
        m_winsize->ws_xpixel = 0;
    }
    m_winsize->ws_row = height;
    m_winsize->ws_ypixel = pixelHeight;
    ioctl(m_master_fd, TIOCSWINSZ, m_winsize);

}

QSize xQuickShellPty::size() const
{
    if (!m_winsize) {
        xQuickShellPty *that = const_cast<xQuickShellPty *>(this);
        that->m_winsize = new struct winsize;
        ioctl(m_master_fd, TIOCGWINSZ, m_winsize);
    }
    return QSize(m_winsize->ws_col, m_winsize->ws_row);
}

int xQuickShellPty::masterDevice() const
{
    return m_master_fd;
}


void xQuickShellPty::readData()
{
    int size_of_buffer = sizeof m_data_buffer / sizeof *m_data_buffer;
    ssize_t read_size = ::read(m_master_fd,m_data_buffer,size_of_buffer);
    if (read_size > 0) {
        QByteArray to_return = QByteArray::fromRawData(m_data_buffer,read_size);
        emit readyRead(to_return);
    } else if (read_size <= 0) {
        delete m_reader;
        emit hangupReceived();
    }
}
