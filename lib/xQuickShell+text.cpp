#include "xQuickShell+text.hpp"
#include "xQuickShell+screen.hpp"
#include "xQuickShell+block.hpp"

#include <QtQuick/QQuickItem>

#include <QtCore/QDebug>

xQuickShellText::xQuickShellText(xQuickShellScreen *screen)
    : QObject(screen)
    , m_screen(screen)
    , m_text_line(0)
    , m_start_index(0)
    , m_old_start_index(0)
    , m_end_index(0)
    , m_line(0)
    , m_old_line(0)
    , m_width(1)
    , m_style(screen->defaultxQuickShellTextStyle())
    , m_new_style(screen->defaultxQuickShellTextStyle())
    , m_style_dirty(true)
    , m_text_dirty(true)
    , m_visible(true)
    , m_visible_old(true)
    , m_latin(true)
    , m_latin_old(true)
    , m_foregroundColor(m_screen->defaultForegroundColor())
    , m_backgroundColor(m_screen->defaultBackgroundColor())
{
    connect(m_screen->colorPalette(), SIGNAL(changed()), this, SLOT(paletteChanged()));
    connect(m_screen, SIGNAL(dispatchxQuickShellTextSegmentChanges()), this, SLOT(dispatchEvents()));
}

xQuickShellText::~xQuickShellText()
{
}

int xQuickShellText::index() const
{
    return m_start_index % m_width;
}

int xQuickShellText::line() const
{
    return m_line + (m_start_index / m_width);
}

void xQuickShellText::setLine(int line, int width, const QString *textLine)
{
    m_line = line;
    m_width = width;
    m_text_dirty = true;
    m_text_line = textLine;
}

bool xQuickShellText::visible() const
{
    return m_visible;
}

void xQuickShellText::setVisible(bool visible)
{
    m_visible = visible;
}

QString xQuickShellText::text() const
{
    return m_text;
}

QColor xQuickShellText::foregroundColor() const
{
    return m_foregroundColor;
}


QColor xQuickShellText::backgroundColor() const
{
    return m_backgroundColor;
}

void xQuickShellText::setStringSegment(int start_index, int end_index, bool text_changed)
{
    m_start_index = start_index;
    m_end_index = end_index;

    m_text_dirty = text_changed;
}

void xQuickShellText::setxQuickShellTextStyle(const xQuickShellTextStyle &style)
{
    m_new_style = style;
    m_style_dirty = true;
}

bool xQuickShellText::bold() const
{
    return m_style.style & xQuickShellTextStyle::Bold;
}

bool xQuickShellText::blinking() const
{
    return m_style.style & xQuickShellTextStyle::Blinking;
}

bool xQuickShellText::underline() const
{
    return m_style.style & xQuickShellTextStyle::Underlined;
}

void xQuickShellText::setLatin(bool latin)
{
    m_latin = latin;
}

bool xQuickShellText::latin() const
{
    return m_latin_old;
}

static bool differentStyle(xQuickShellTextStyle::Styles a, xQuickShellTextStyle::Styles b, xQuickShellTextStyle::Style style)
{
    return (a & style) != (b & style);
}

void xQuickShellText::dispatchEvents()
{
    int old_line = m_old_line + (m_old_start_index / m_width);
    int new_line = m_line + (m_start_index / m_width);
    if (old_line != new_line) {
        m_old_line = m_line;
        emit lineChanged();
    }

    if (m_latin != m_latin_old) {
        m_latin_old = m_latin;
        emit latinChanged();
    }

    if (m_old_start_index != m_start_index
            || m_text_dirty) {
        m_text_dirty = false;
        QString old_text = m_text;
        m_text = m_text_line->mid(m_start_index, m_end_index - m_start_index + 1);
        if (m_old_start_index != m_start_index) {
            m_old_start_index = m_start_index;
            emit indexChanged();
        }
        emit textChanged();
    }

    if (m_style_dirty) {
        m_style_dirty = false;

        bool emit_foreground = m_new_style.foreground != m_style.foreground;
        bool emit_background = m_new_style.background != m_style.background;
        xQuickShellTextStyle::Styles new_style = m_new_style.style;
        xQuickShellTextStyle::Styles old_style = m_style.style;

        bool emit_bold = false;
        bool emit_blink = false;
        bool emit_underline = false;
        bool emit_inverse = false;
        if (new_style != old_style) {
            emit_bold = differentStyle(new_style, old_style, xQuickShellTextStyle::Bold);
            emit_blink = differentStyle(new_style, old_style, xQuickShellTextStyle::Blinking);
            emit_underline = differentStyle(new_style, old_style, xQuickShellTextStyle::Underlined);
            emit_inverse = differentStyle(new_style, old_style, xQuickShellTextStyle::Inverse);
        }

        m_style = m_new_style;
        if (emit_inverse) {
            setForegroundColor();
            setBackgroundColor();
        } else {
            if (emit_foreground || emit_bold) {
                setForegroundColor();
            }
            if (emit_background) {
                setBackgroundColor();
            }
        }

        if (emit_bold) {
            emit boldChanged();
        }

        if (emit_blink) {
            emit blinkingChanged();
        }

        if (emit_underline) {
            emit underlineChanged();
        }

    }


    if (m_visible_old != m_visible) {
        m_visible_old = m_visible;
        emit visibleChanged();
    }
}

void xQuickShellText::paletteChanged()
{
    setBackgroundColor();
    setForegroundColor();
}

void xQuickShellText::setBackgroundColor()
{
    QColor new_background;
    if (m_style.style & xQuickShellTextStyle::Inverse) {
        new_background = m_style.foreground;
    } else {
        new_background = m_style.background;
    }
    if (new_background != m_backgroundColor) {
        m_backgroundColor = new_background;
        emit backgroundColorChanged();
    }
}

void xQuickShellText::setForegroundColor()
{
    QColor new_foreground;
    if (m_style.style & xQuickShellTextStyle::Inverse) {
        new_foreground = m_style.background;
    } else {
        new_foreground = m_style.foreground;
    }
    if (new_foreground != m_foregroundColor) {
        m_foregroundColor = new_foreground;
        emit foregroundColorChanged();
    }
}
