// xQuickControl+spinbox.cpp

#include "xQuickControl+spinbox.hpp"

#include "xQuickControl+indicator_button.hpp"
#include "xQuickControl+validator.hpp"

#include <QtGui/qvalidator.h>

#include <QtQuickTemplates2/private/qquickcontrol_p_p.h>

#include <QtQuick/private/qquicktextinput_p.h>

#include <cmath>

namespace xQuick { namespace detail {

// copied from qabstractbutton.cpp
static const int AUTO_REPEAT_DELAY = 300;
static const int AUTO_REPEAT_INTERVAL = 100;

} }

// /////////////////////////////////////////////////////////////////////////////
// xQuickControlAbstractSpinBox implementation
// /////////////////////////////////////////////////////////////////////////////

class xQuickControlAbstractSpinBoxPrivate : public QQuickControlPrivate
{
    Q_DECLARE_PUBLIC(xQuickControlAbstractSpinBox)

public:
    // virtual bool setValue(qlonglong, bool) { return false; }
    // virtual bool setValue(double, bool) { return false; }

    virtual void updateValue(void) = 0;
    virtual void storeValue(void) = 0;
    virtual bool hasValueChanged(void) = 0;
    virtual bool hasValueBeenUpdated(void) = 0;

    virtual void updateDisplayText(bool modified = false) = 0;
    void setDisplayText(const QString &displayText, bool modified = false);

    virtual void increase(bool modified) = 0;
    virtual void decrease(bool modified) = 0;

    bool upEnabled(void) const;
    virtual void updateUpEnabled(void) = 0;
    bool downEnabled(void) const;
    virtual void updateDownEnabled(void) = 0;

    void updateHover(const QPointF &pos);

    void startRepeatDelay(void);
    void startPressRepeat(void);
    void stopPressRepeat(void);

    void handlePress(const QPointF &point) override;
    void handleMove(const QPointF &point) override;
    void handleRelease(const QPointF &point) override;
    void handleUngrab(void) override;

    void itemImplicitWidthChanged(QQuickItem *item) override;
    void itemImplicitHeightChanged(QQuickItem *item) override;

    QPalette defaultPalette(void) const override { return QQuickTheme::palette(QQuickTheme::SpinBox); }

    int scaledExponent(double range) const;

    bool editable = false;
    int delayTimer = 0;
    int repeatTimer = 0;
    QString displayText;
    xQuickControlIndicatorButton *up = nullptr;
    xQuickControlIndicatorButton *down = nullptr;
    Qt::InputMethodHints inputMethodHints = Qt::ImhFormattedNumbersOnly;
    xQuickControlAbstractSpinBox::GaugeControl gaugeControl = xQuickControlAbstractSpinBox::ControlPolynomial;
};

void xQuickControlAbstractSpinBoxPrivate::setDisplayText(const QString &text, bool modified)
{
    Q_Q(xQuickControlAbstractSpinBox);

    if (!modified && displayText == text)
        return;

    displayText = text;
    emit q->displayTextChanged();
}

bool xQuickControlAbstractSpinBoxPrivate::upEnabled(void) const
{
    const QQuickItem *upIndicator = up->indicator();
    return upIndicator && upIndicator->isEnabled();
}

bool xQuickControlAbstractSpinBoxPrivate::downEnabled(void) const
{
    const QQuickItem *downIndicator = down->indicator();
    return downIndicator && downIndicator->isEnabled();
}

void xQuickControlAbstractSpinBoxPrivate::updateHover(const QPointF &pos)
{
    Q_Q(xQuickControlAbstractSpinBox);
    QQuickItem *ui = up->indicator();
    QQuickItem *di = down->indicator();
    up->setHovered(ui && ui->isEnabled() && ui->contains(q->mapToItem(ui, pos)));
    down->setHovered(di && di->isEnabled() && di->contains(q->mapToItem(di, pos)));
}

void xQuickControlAbstractSpinBoxPrivate::startRepeatDelay(void)
{
    Q_Q(xQuickControlAbstractSpinBox);
    stopPressRepeat();
    delayTimer = q->startTimer(xQuick::detail::AUTO_REPEAT_DELAY);
}

void xQuickControlAbstractSpinBoxPrivate::startPressRepeat(void)
{
    Q_Q(xQuickControlAbstractSpinBox);
    stopPressRepeat();
    repeatTimer = q->startTimer(xQuick::detail::AUTO_REPEAT_INTERVAL);
}

void xQuickControlAbstractSpinBoxPrivate::stopPressRepeat(void)
{
    Q_Q(xQuickControlAbstractSpinBox);
    if (delayTimer > 0) {
        q->killTimer(delayTimer);
        delayTimer = 0;
    }
    if (repeatTimer > 0) {
        q->killTimer(repeatTimer);
        repeatTimer = 0;
    }
}

void xQuickControlAbstractSpinBoxPrivate::handlePress(const QPointF &point)
{
    Q_Q(xQuickControlAbstractSpinBox);
    QQuickControlPrivate::handlePress(point);
    QQuickItem *ui = up->indicator();
    QQuickItem *di = down->indicator();
    up->setPressed(ui && ui->isEnabled() && ui->contains(ui->mapFromItem(q, point)));
    down->setPressed(di && di->isEnabled() && di->contains(di->mapFromItem(q, point)));

    bool pressed = up->isPressed() || down->isPressed();
    q->setAccessibleProperty("pressed", pressed);
    if (pressed)
        startRepeatDelay();
}

void xQuickControlAbstractSpinBoxPrivate::handleMove(const QPointF &point)
{
    Q_Q(xQuickControlAbstractSpinBox);
    QQuickControlPrivate::handleMove(point);
    QQuickItem *ui = up->indicator();
    QQuickItem *di = down->indicator();
    up->setHovered(ui && ui->isEnabled() && ui->contains(ui->mapFromItem(q, point)));
    up->setPressed(up->isHovered());
    down->setHovered(di && di->isEnabled() && di->contains(di->mapFromItem(q, point)));
    down->setPressed(down->isHovered());

    bool pressed = up->isPressed() || down->isPressed();
    q->setAccessibleProperty("pressed", pressed);
    if (!pressed)
        stopPressRepeat();
}

void xQuickControlAbstractSpinBoxPrivate::handleRelease(const QPointF &point)
{
    Q_Q(xQuickControlAbstractSpinBox);
    QQuickControlPrivate::handleRelease(point);
    QQuickItem *ui = up->indicator();
    QQuickItem *di = down->indicator();

    storeValue();
    if (up->isPressed()) {
        up->setPressed(false);
        if (repeatTimer <= 0 && ui && ui->contains(ui->mapFromItem(q, point)))
            q->increase();
    } else if (down->isPressed()) {
        down->setPressed(false);
        if (repeatTimer <= 0 && di && di->contains(di->mapFromItem(q, point)))
            q->decrease();
    }
    if (hasValueChanged()) {
        emit q->valueModified();
    }

    q->setAccessibleProperty("pressed", false);
    stopPressRepeat();
}

void xQuickControlAbstractSpinBoxPrivate::handleUngrab(void)
{
    Q_Q(xQuickControlAbstractSpinBox);
    QQuickControlPrivate::handleUngrab();
    up->setPressed(false);
    down->setPressed(false);

    q->setAccessibleProperty("pressed", false);
    stopPressRepeat();
}

void xQuickControlAbstractSpinBoxPrivate::itemImplicitWidthChanged(QQuickItem *item)
{
    QQuickControlPrivate::itemImplicitWidthChanged(item);
    if (item == up->indicator())
        emit up->implicitIndicatorWidthChanged();
    else if (item == down->indicator())
        emit down->implicitIndicatorWidthChanged();
}

void xQuickControlAbstractSpinBoxPrivate::itemImplicitHeightChanged(QQuickItem *item)
{
    QQuickControlPrivate::itemImplicitHeightChanged(item);
    if (item == up->indicator())
        emit up->implicitIndicatorHeightChanged();
    else if (item == down->indicator())
        emit down->implicitIndicatorHeightChanged();
}

int xQuickControlAbstractSpinBoxPrivate::scaledExponent(double range) const
{
    if (range == 0) {
        return 1;
    }
    if (gaugeControl == xQuickControlAbstractSpinBox::ControlLinear) {
        return 1;
    }
    int N = std::ceil(std::log10(std::abs(range)));
    return N;
}

// /////////////////////////////////////////////////////////////////////////////

xQuickControlAbstractSpinBox::xQuickControlAbstractSpinBox(QQuickItem *parent, xQuickControlAbstractSpinBoxPrivate* d_ptr)
    : QQuickControl(*d_ptr, parent)
{
    Q_D(xQuickControlAbstractSpinBox);
    d->up = new xQuickControlIndicatorButton(this);
    d->down = new xQuickControlIndicatorButton(this);

    setLocale(QLocale("C"));
    setFlag(ItemIsFocusScope);
    setFiltersChildMouseEvents(true);
    setAcceptedMouseButtons(Qt::LeftButton);
#if QT_CONFIG(cursor)
    setCursor(Qt::ArrowCursor);
#endif
}

xQuickControlAbstractSpinBox::~xQuickControlAbstractSpinBox(void)
{
    Q_D(xQuickControlAbstractSpinBox);
    d->removeImplicitSizeListener(d->up->indicator());
    d->removeImplicitSizeListener(d->down->indicator());
}

auto xQuickControlAbstractSpinBox::gaugeControl(void) const -> GaugeControl
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->gaugeControl;
}

void xQuickControlAbstractSpinBox::setGaugeControl(GaugeControl gaugeControl)
{
    Q_D(xQuickControlAbstractSpinBox);
    if (d->gaugeControl == gaugeControl)
        return;

    d->gaugeControl = gaugeControl;
    emit gaugeControlChanged(gaugeControl);
}

bool xQuickControlAbstractSpinBox::isEditable(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->editable;
}

void xQuickControlAbstractSpinBox::setEditable(bool editable)
{
    Q_D(xQuickControlAbstractSpinBox);
    if (d->editable == editable)
        return;

#if QT_CONFIG(cursor)
    if (d->contentItem) {
        if (editable)
            d->contentItem->setCursor(Qt::IBeamCursor);
        else
            d->contentItem->unsetCursor();
    }
#endif

    d->editable = editable;
    setAccessibleProperty("editable", editable);
    emit editableChanged();
}

xQuickControlIndicatorButton *xQuickControlAbstractSpinBox::up(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->up;
}

xQuickControlIndicatorButton *xQuickControlAbstractSpinBox::down(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->down;
}

Qt::InputMethodHints xQuickControlAbstractSpinBox::inputMethodHints(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->inputMethodHints;
}

bool xQuickControlAbstractSpinBox::isInputMethodComposing(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->contentItem && d->contentItem->property("inputMethodComposing").toBool();
}

QString xQuickControlAbstractSpinBox::displayText(void) const
{
    Q_D(const xQuickControlAbstractSpinBox);
    return d->displayText;
}

void xQuickControlAbstractSpinBox::increase(void)
{
    Q_D(xQuickControlAbstractSpinBox);
    d->increase(false);
}

void xQuickControlAbstractSpinBox::decrease(void)
{
    Q_D(xQuickControlAbstractSpinBox);
    d->decrease(false);
}

void xQuickControlAbstractSpinBox::focusInEvent(QFocusEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::focusInEvent(event);

    // When an editable SpinBox gets focus, it must pass on the focus to its editor.
    if (d->editable && d->contentItem && !d->contentItem->hasActiveFocus())
        d->contentItem->forceActiveFocus(event->reason());
}

void xQuickControlAbstractSpinBox::hoverEnterEvent(QHoverEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::hoverEnterEvent(event);
    d->updateHover(event->position());
}

void xQuickControlAbstractSpinBox::hoverMoveEvent(QHoverEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::hoverMoveEvent(event);
    d->updateHover(event->position());
}

void xQuickControlAbstractSpinBox::hoverLeaveEvent(QHoverEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::hoverLeaveEvent(event);
    d->down->setHovered(false);
    d->up->setHovered(false);
}

void xQuickControlAbstractSpinBox::keyPressEvent(QKeyEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::keyPressEvent(event);

    switch (event->key()) {
    case Qt::Key_Up:
        if (d->upEnabled()) {
            d->increase(true);
            d->up->setPressed(true);
            event->accept();
        }
        break;

    case Qt::Key_Down:
        if (d->downEnabled()) {
            d->decrease(true);
            d->down->setPressed(true);
            event->accept();
        }
        break;

    default:
        break;
    }

    setAccessibleProperty("pressed", d->up->isPressed() || d->down->isPressed());
}

void xQuickControlAbstractSpinBox::keyReleaseEvent(QKeyEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::keyReleaseEvent(event);

    if (d->editable && (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return))
        d->updateValue();

    d->up->setPressed(false);
    d->down->setPressed(false);
    setAccessibleProperty("pressed", false);
}

void xQuickControlAbstractSpinBox::timerEvent(QTimerEvent *event)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::timerEvent(event);
    if (event->timerId() == d->delayTimer) {
        d->startPressRepeat();
    } else if (event->timerId() == d->repeatTimer) {
        if (d->up->isPressed())
            d->increase(true);
        else if (d->down->isPressed())
            d->decrease(true);
    }
}

void xQuickControlAbstractSpinBox::classBegin(void)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::classBegin();

    QQmlContext *context = qmlContext(this);
    if (context) {
        QQmlEngine::setContextForObject(d->up, context);
        QQmlEngine::setContextForObject(d->down, context);
    }
}

void xQuickControlAbstractSpinBox::componentComplete(void)
{
    Q_D(xQuickControlAbstractSpinBox);
    xQuickControlIndicatorButtonPrivate::get(d->up)->executeIndicator(true);
    xQuickControlIndicatorButtonPrivate::get(d->down)->executeIndicator(true);

    QQuickControl::componentComplete();
    if (d->hasValueBeenUpdated()) {
        d->updateDisplayText();
        d->updateUpEnabled();
        d->updateDownEnabled();
    }
}

void xQuickControlAbstractSpinBox::itemChange(ItemChange change, const ItemChangeData &value)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::itemChange(change, value);
    if (d->editable && change == ItemActiveFocusHasChanged && !value.boolValue)
        d->updateValue();
}

void xQuickControlAbstractSpinBox::contentItemChange(QQuickItem *newItem, QQuickItem *oldItem)
{
    Q_D(xQuickControlAbstractSpinBox);
    if (QQuickTextInput *oldInput = qobject_cast<QQuickTextInput *>(oldItem))
        disconnect(oldInput, &QQuickTextInput::inputMethodComposingChanged, this, &xQuickControlAbstractSpinBox::inputMethodComposingChanged);

    if (newItem) {
        newItem->setActiveFocusOnTab(true);
        if (d->activeFocus)
            newItem->forceActiveFocus(d->focusReason);
#if QT_CONFIG(cursor)
        if (d->editable)
            newItem->setCursor(Qt::IBeamCursor);
#endif

        if (QQuickTextInput *newInput = qobject_cast<QQuickTextInput *>(newItem))
            connect(newInput, &QQuickTextInput::inputMethodComposingChanged, this, &xQuickControlAbstractSpinBox::inputMethodComposingChanged);
    }
}

void xQuickControlAbstractSpinBox::localeChange(const QLocale &newLocale, const QLocale &oldLocale)
{
    QQuickControl::localeChange(newLocale, oldLocale);
}

QFont xQuickControlAbstractSpinBox::defaultFont(void) const
{
    return QQuickTheme::font(QQuickTheme::SpinBox);
}

#if QT_CONFIG(accessibility)
QAccessible::Role xQuickControlAbstractSpinBox::accessibleRole(void) const
{
    return QAccessible::SpinBox;
}

void xQuickControlAbstractSpinBox::accessibilityActiveChanged(bool active)
{
    Q_D(xQuickControlAbstractSpinBox);
    QQuickControl::accessibilityActiveChanged(active);

    if (active)
        setAccessibleProperty("editable", d->editable);
}
#endif

// /////////////////////////////////////////////////////////////////////////////
// xQuickControlSpinBoxIntegerPrivate implementation
// /////////////////////////////////////////////////////////////////////////////

class xQuickControlSpinBoxIntegerPrivate : public xQuickControlAbstractSpinBoxPrivate
{
    Q_DECLARE_PUBLIC(xQuickControlSpinBoxInteger)

public:
    qlonglong from(void) const { return validator->bot_impl(); }
    qlonglong to(void) const { return validator->top_impl(); }

    void updateValue(void) override;
    void storeValue(void) override;
    bool hasValueChanged(void) override;
    bool hasValueBeenUpdated(void) override;


    bool setValue(qlonglong value, bool modified);
    void increase(bool modified) override;
    void decrease(bool modified) override;

    void updateDisplayText(bool modified = false);

    void updateUpEnabled(void) override;
    void updateDownEnabled(void) override;

    qlonglong value = 0;
    qlonglong old_value = 0;
    int increment = 1;
    xQuickControlValidatorInteger *validator = nullptr;
};

void xQuickControlSpinBoxIntegerPrivate::updateValue(void)
{
    Q_Q(xQuickControlSpinBoxInteger);
    if (contentItem) {
        QVariant text = contentItem->property("text");
        if (text.isValid()) {
            setValue(text.toLongLong(), /* modified = */ true);
        }
    }
}

void xQuickControlSpinBoxIntegerPrivate::storeValue(void)
{
    old_value = value;
}

bool xQuickControlSpinBoxIntegerPrivate::hasValueChanged(void)
{
    return (old_value != value);
}

bool xQuickControlSpinBoxIntegerPrivate::hasValueBeenUpdated(void)
{
    return setValue(value, false);
}

// modified indicates if the value was modified by the user and not programatically
// this is then passed on to updateDisplayText to indicate that the user has modified
// the value so it may need to trigger an update of the contentItem's text too

bool xQuickControlSpinBoxIntegerPrivate::setValue(qlonglong newValue, bool modified)
{
    Q_Q(xQuickControlSpinBoxInteger);
    qlonglong correctedValue = newValue;
    if (q->isComponentComplete())
         correctedValue = qBound(from(), newValue, to());

    if (!modified && newValue == correctedValue && newValue == value)
        return false;

    const bool emitSignals = (value != correctedValue);
    value = correctedValue;

    updateDisplayText(modified);
    updateUpEnabled();
    updateDownEnabled();

    // Only emit the signals if the corrected value is not the same as the
    // original value to avoid unnecessary updates
    if (emitSignals) {
        emit q->valueChanged();
        if (modified)
            emit q->valueModified();
    }
    return true;
}

void xQuickControlSpinBoxIntegerPrivate::increase(bool modified)
{
    setValue(value + increment, modified);
}

void xQuickControlSpinBoxIntegerPrivate::decrease(bool modified)
{
    setValue(value - increment, modified);
}

void xQuickControlSpinBoxIntegerPrivate::updateDisplayText(bool modified)
{
    setDisplayText(QString::number(value), modified);
}

void xQuickControlSpinBoxIntegerPrivate::updateUpEnabled(void)
{
    QQuickItem *upIndicator = up->indicator();
    if (!upIndicator)
        return;

    upIndicator->setEnabled(value < to());
}

void xQuickControlSpinBoxIntegerPrivate::updateDownEnabled(void)
{
    QQuickItem *downIndicator = down->indicator();
    if (!downIndicator)
        return;

    downIndicator->setEnabled(value > from());
}

// /////////////////////////////////////////////////////////////////////////////

xQuickControlSpinBoxInteger::xQuickControlSpinBoxInteger(QQuickItem *parent)
    : xQuickControlAbstractSpinBox(parent, new xQuickControlSpinBoxIntegerPrivate)
{
    Q_D(xQuickControlSpinBoxInteger);
    d->validator = new xQuickControlValidatorInteger(std::numeric_limits<qlonglong>::lowest()/2,
                                                     std::numeric_limits<qlonglong>::max()/2,
                                                     this);
    d->validator->setLocale(d->locale);
    d->updateDisplayText();
    d->gaugeControl = xQuickControlAbstractSpinBox::ControlLinear;
}

xQuickControlSpinBoxInteger::~xQuickControlSpinBoxInteger(void)
{
}

QVariant xQuickControlSpinBoxInteger::from(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    return d->validator->bottom();
}

void xQuickControlSpinBoxInteger::setFrom(QVariant f)
{
    qlonglong from = f.value<qlonglong>();
    Q_D(xQuickControlSpinBoxInteger);
    if (d->from() == from)
        return;

    d->validator->setBottom(std::max(std::min(from, d->to()), std::numeric_limits<qlonglong>::lowest()/2));
    emit fromChanged();
    if (isComponentComplete()) {
        if (!d->setValue(d->value, /* modified = */ false)) {
            d->updateUpEnabled();
            d->updateDownEnabled();
        }
    }
}

QVariant xQuickControlSpinBoxInteger::to(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    return d->validator->top();
}

void xQuickControlSpinBoxInteger::setTo(QVariant t)
{
    qlonglong to = t.value<qlonglong>();
    Q_D(xQuickControlSpinBoxInteger);
    if (d->to() == to)
        return;

    d->validator->setTop(std::min(std::max(d->from(), to), std::numeric_limits<qlonglong>::max()/2));
    emit toChanged();
    if (isComponentComplete()) {
        if (!d->setValue(d->value, /* modified = */ false)) {
            d->updateUpEnabled();
            d->updateDownEnabled();
        }
    }
}

QVariant xQuickControlSpinBoxInteger::value(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    return QVariant::fromValue(d->value);
}

void xQuickControlSpinBoxInteger::setValue(QVariant value)
{
    Q_D(xQuickControlSpinBoxInteger);
    d->setValue(value.value<qlonglong>(), /* modified = */ false);
}

int xQuickControlSpinBoxInteger::increment(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    return d->increment;
}

void xQuickControlSpinBoxInteger::setIncrement(int increment)
{
    Q_D(xQuickControlSpinBoxInteger);
    if (d->increment == increment)
        return;

    d->increment = increment;
    emit incrementChanged();
}

QValidator *xQuickControlSpinBoxInteger::validator(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    return d->validator;
}

double xQuickControlSpinBoxInteger::scaleValueFromRatio(double ratio) const
{
    Q_D(const xQuickControlSpinBoxInteger);

    // The general polynomial formulae is: alpha * (r)^N + beta
    // There are 3 cases:
    // - to > from > 0 => scale value is positive
    // - from < to < 0 => scale value is negative
    // - from < 0 < to => positive and negative cases must be distinguished
    // The N exponent is equal to log10(|to - from|).
    double r;
    double r_0;
    double alpha;
    double beta;

    int N = d->scaledExponent(1.0 * (d->to() - d->from()));
    if (N > 13) { // It is nonsense to have such a disparity in the scale
        return d->value;
    }

    if (ratio >= 1) { // Avoid useless computation
        return d->to();
    }
    if (ratio <= 0) { // Same
        return d->from();
    }

    if (d->from() >= 0) { // the scale value is positive between [from, to]
                          // r = ratio
                          // beta = from
                          // alpha = (to - from) / (1^N) = to - from
        beta = d->from();
        alpha = 1. * (d->to() - d->from());
        r = ratio;

    } else if (d->to() <= 0) { // the scale value is negative between [from, to]
                               // Computation are done with
                               // r = 1 - ratio
                               // beta = to
                               // alpha = (from - to) / (1^N) = from - to
        beta = d->to();
        r = 1 - ratio;
        alpha = 1. * (d->from() - d->to());

    } else { // the scale value is either in [from, 0] or [0, to]
             // We need to compute the ratio r0 for which the scale value is zero:
             // r0 = (0 - from) / (to - from) = from / (from - to)
             // Thus it follows that:
             // - r = ratio - r0
             // - beta = 0
             // alpha varies whether r is positive or negative as follows:
             // - r > 0 => alpha = (to - 0) / (1 - r0)^N
             // - r < 0 => alpha = (from - 0) / |0 - r0|^N
        beta = 0;
        r_0 = 1. * d->from() / (d->from() - d->to());
        if (ratio >= r_0) {
            r = ratio - r_0;
            alpha = d->to() / std::pow((1. - r_0), N);
        } else {
            r = r_0 - ratio;
            alpha = d->from() / std::pow(r_0, N);
        }
    }
    return alpha * std::pow(r, N) + beta;
}

double xQuickControlSpinBoxInteger::scaleValue(void) const
{
    Q_D(const xQuickControlSpinBoxInteger);
    int N = d->scaledExponent(d->to() - d->from());
    if (N <= 13) {
        return (1. * (d->value - d->from()) / (d->to() - d->from()));
    }
    return 0;
}

// /////////////////////////////////////////////////////////////////////////////
// xQuickControlSpinBoxReal implementation
// /////////////////////////////////////////////////////////////////////////////

class xQuickControlSpinBoxRealPrivate : public xQuickControlAbstractSpinBoxPrivate
{
    Q_DECLARE_PUBLIC(xQuickControlSpinBoxReal)

public:
    double from(void) const { return validator->bottom(); }
    double to(void) const { return validator->top(); }

    void updateValue(void) override;
    void storeValue(void) override;
    bool hasValueChanged(void) override;
    bool hasValueBeenUpdated(void) override;

    bool setValue(double value, bool modified);
    void increase(bool modified) override;
    void decrease(bool modified) override;
    double effectiveIncrement(void);
    double truncateValue(double val);

    void updateDisplayText(bool modified = false) override;

    void updateUpEnabled(void);
    void updateDownEnabled(void);

    double value = 0;
    double old_value = 0;
    double increment = 1;
    bool is_scientific_notation = true;
    QDoubleValidator *validator = nullptr;
};

void xQuickControlSpinBoxRealPrivate::updateValue(void)
{
    Q_Q(xQuickControlSpinBoxReal);
    if (contentItem) {
        QVariant text = contentItem->property("text");
        if (text.isValid()) {
            setValue(text.toDouble(), /* modified = */ true);
        }
    }
}

void xQuickControlSpinBoxRealPrivate::storeValue(void)
{
    old_value = value;
}

bool xQuickControlSpinBoxRealPrivate::hasValueChanged(void)
{
    return (old_value != value);
}

bool xQuickControlSpinBoxRealPrivate::hasValueBeenUpdated(void)
{
    return setValue(value, false);
}

// modified indicates if the value was modified by the user and not programatically
// this is then passed on to updateDisplayText to indicate that the user has modified
// the value so it may need to trigger an update of the contentItem's text too

bool xQuickControlSpinBoxRealPrivate::setValue(double newValue, bool modified)
{
    Q_Q(xQuickControlSpinBoxReal);
    double correctedValue = truncateValue(newValue);
    newValue = correctedValue;
    if (q->isComponentComplete())
         correctedValue = qBound(from(), newValue, to());

    if (!modified && newValue == correctedValue && newValue == value)
        return false;

    const bool emitSignals = (value != correctedValue);
    value = correctedValue;

    updateDisplayText(modified);
    updateUpEnabled();
    updateDownEnabled();

    // Only emit the signals if the corrected value is not the same as the
    // original value to avoid unnecessary updates
    if (emitSignals) {
        emit q->valueChanged();
        if (modified)
            emit q->valueModified();
    }
    return true;
}

void xQuickControlSpinBoxRealPrivate::increase(bool modified)
{
    setValue(value + effectiveIncrement(), modified);
}

void xQuickControlSpinBoxRealPrivate::decrease(bool modified)
{
    setValue(value - effectiveIncrement(), modified);
}

double xQuickControlSpinBoxRealPrivate::effectiveIncrement(void)
{
    qlonglong exponent = -1 * validator->decimals();
    if (is_scientific_notation) {
        if (value != 0) {
            exponent += std::floor(std::log10(std::abs(value)));
        }
    }
    return increment * std::pow(10, exponent);
}

double xQuickControlSpinBoxRealPrivate::truncateValue(double val)
{
    if (val != 0) {
        qlonglong exponent = -1 * validator->decimals();
        if (is_scientific_notation) {
            exponent += std::floor(std::log10(std::abs(val)));
        }
        return std::round(val / std::pow(10, exponent)) * std::pow(10, exponent);
    }
    return val;
}

void xQuickControlSpinBoxRealPrivate::updateDisplayText(bool modified)
{
    Q_Q(xQuickControlSpinBoxReal);

    if (is_scientific_notation) {
        setDisplayText(QString::number(value, 'E', q->decimals()), modified);
    } else {
        setDisplayText(QString::number(value, 'f', q->decimals()), modified);
    }
}

void xQuickControlSpinBoxRealPrivate::updateUpEnabled(void)
{
    QQuickItem *upIndicator = up->indicator();
    if (!upIndicator)
        return;

    upIndicator->setEnabled(value < to());
}

void xQuickControlSpinBoxRealPrivate::updateDownEnabled(void)
{
    QQuickItem *downIndicator = down->indicator();
    if (!downIndicator)
        return;

    downIndicator->setEnabled(value > from());
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

xQuickControlSpinBoxReal::xQuickControlSpinBoxReal(QQuickItem *parent)
    : xQuickControlAbstractSpinBox(parent, new xQuickControlSpinBoxRealPrivate)
{
    Q_D(xQuickControlSpinBoxReal);
    d->validator = new QDoubleValidator(std::numeric_limits<double>::lowest()/2,
                                        std::numeric_limits<double>::max()/2,
                                        10,
                                        this);
    d->validator->setLocale(d->locale);
    d->is_scientific_notation = (d->validator->notation() == QDoubleValidator::ScientificNotation);
    d->updateDisplayText();
}

xQuickControlSpinBoxReal::~xQuickControlSpinBoxReal(void)
{
}

double xQuickControlSpinBoxReal::from(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->from();
}

void xQuickControlSpinBoxReal::setFrom(double from)
{
    Q_D(xQuickControlSpinBoxReal);
    if (d->from() == from)
        return;

    d->validator->setBottom(std::max(std::min(from, d->to()), std::numeric_limits<double>::lowest()/2));
    emit fromChanged();
    if (isComponentComplete()) {
        if (!d->setValue(d->value, /* modified = */ false)) {
            d->updateUpEnabled();
            d->updateDownEnabled();
        }
    }
}

double xQuickControlSpinBoxReal::to(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->to();
}

void xQuickControlSpinBoxReal::setTo(double to)
{
    Q_D(xQuickControlSpinBoxReal);
    if (d->to() == to)
        return;

    d->validator->setTop(std::min(std::max(d->from(), to), std::numeric_limits<double>::max()/2));
    emit toChanged();
    if (isComponentComplete()) {
        if (!d->setValue(d->value, /* modified = */ false)) {
            d->updateUpEnabled();
            d->updateDownEnabled();
        }
    }
}

double xQuickControlSpinBoxReal::value(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->value;
}

void xQuickControlSpinBoxReal::setValue(double value)
{
    Q_D(xQuickControlSpinBoxReal);
    d->setValue(value, /* modified = */ false);
}

int xQuickControlSpinBoxReal::decimals(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->validator->decimals();
}

void xQuickControlSpinBoxReal::setDecimals(int decimals)
{
    Q_D(xQuickControlSpinBoxReal);
    if (this->decimals() == decimals)
        return;

    d->validator->setDecimals(decimals);
    emit decimalsChanged(decimals);

    d->setValue(d->value, true);
}

double xQuickControlSpinBoxReal::increment(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->increment;
}

void xQuickControlSpinBoxReal::setIncrement(double increment)
{
    Q_D(xQuickControlSpinBoxReal);
    if (d->increment == increment)
        return;

    d->increment = increment;
    emit incrementChanged();
}

QValidator *xQuickControlSpinBoxReal::validator(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->validator;
}

void xQuickControlSpinBoxReal::setNotation(Notation n)
{
    Q_D(xQuickControlSpinBoxReal);
    if (n == notation())
        return;

    switch (n) {
    case ScientificNotation:
        d->validator->setNotation(QDoubleValidator::ScientificNotation);
        d->is_scientific_notation = true;
        break;
    case StandardNotation:
        d->validator->setNotation(QDoubleValidator::StandardNotation);
        d->is_scientific_notation = false;
        break;
    default:
        d->validator->setNotation(QDoubleValidator::ScientificNotation);
        d->is_scientific_notation = true;
        break;
    }
    emit notationChanged(n);

    d->setValue(d->value, true);
}

auto xQuickControlSpinBoxReal::notation(void) const -> Notation
{
    Q_D(const xQuickControlSpinBoxReal);
    return d->is_scientific_notation ? ScientificNotation : StandardNotation;
}

double xQuickControlSpinBoxReal::scaleValueFromRatio(double ratio) const
{
    Q_D(const xQuickControlSpinBoxReal);

    // The general polynomial formulae is: alpha * (r)^N + beta
    // There are 3 cases:
    // - to > from > 0 => scale value is positive
    // - from < to < 0 => scale value is negative
    // - from < 0 < to => positive and negative cases must be distinguished
    // The N exponent is equal to log10(|to - from|).
    double r;
    double r_0;
    double alpha;
    double beta;

    int N = d->scaledExponent(d->to() - d->from());
    if (N > 13) { // It is nonsense to have such a disparity in the scale
        return d->value;
    }

    if (ratio >= 1) { // Avoid useless computation
        return d->to();
    }
    if (ratio <= 0) { // Same
        return d->from();
    }

    if (d->from() >= 0) { // the scale value is positive between [from, to]
                          // r = ratio
                          // beta = from
                          // alpha = (to - from) / (1^N) = to - from
        beta = d->from();
        alpha = d->to() - d->from();
        r = ratio;

    } else if (d->to() <= 0) { // the scale value is negative between [from, to]
                               // Computation are done with
                               // r = 1 - ratio
                               // beta = to
                               // alpha = (from - to) / (1^N) = from - to
        beta = d->to();
        r = 1 - ratio;
        alpha = d->from() - d->to();

    } else { // the scale value is either in [from, 0] or [0, to]
             // We need to compute the ratio r0 for which the scale value is zero:
             // r0 = (0 - from) / (to - from) = from / (from - to)
             // Thus it follows that:
             // - r = ratio - r0
             // - beta = 0
             // alpha varies whether r is positive or negative as follows:
             // - r > 0 => alpha = (to - 0) / (1 - r0)^N
             // - r < 0 => alpha = (from - 0) / |0 - r0|^N
        beta = 0;
        r_0 = d->from() / (d->from() - d->to());
        if (ratio >= r_0) {
            r = ratio - r_0;
            alpha = d->to() / std::pow((1. - r_0), N);
        } else {
            r = r_0 - ratio;
            alpha = d->from() / std::pow(r_0, N);
        }
    }
    return alpha * std::pow(r, N) + beta;
}

double xQuickControlSpinBoxReal::scaleValue(void) const
{
    Q_D(const xQuickControlSpinBoxReal);
    int N = d->scaledExponent(d->to() - d->from());
    if (N <= 13) {
        return ((d->value - d->from()) / (d->to() - d->from()));
    }
    return 0;
}

// /////////////////////////////////////////////////////////////////////////////

#include "moc_xQuickControl+spinbox.cpp"

// xQuickControl+spinbox.cpp ends here
