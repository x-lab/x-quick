// xQuickControl+deferredexecute.hpp

#pragma once

#include <QtCore/qglobal.h>
#include <QtQuickTemplates2/private/qquickdeferredpointer_p_p.h>
#include <QtQuickTemplates2/private/qtquicktemplates2global_p.h>

#include <QtQml/private/qqmlvme_p.h>


class QString;
class QObject;

namespace xQuick { namespace detail {

void beginDeferred(QObject *object, const QString &property);
void cancelDeferred(QObject *object, const QString &property);
void completeDeferred(QObject *object, const QString &property);

} }

template<typename T>
void xQuickBeginDeferred(QObject *object, const QString &property, QQuickDeferredPointer<T> &delegate)
{
    if (!QQmlVME::componentCompleteEnabled())
           return;

    delegate.setExecuting(true);
    xQuick::detail::beginDeferred(object, property);
    delegate.setExecuting(false);
}

inline void xQuickCancelDeferred(QObject *object, const QString &property)
{
    xQuick::detail::cancelDeferred(object, property);
}

template<typename T>
void xQuickCompleteDeferred(QObject *object, const QString &property, QQuickDeferredPointer<T> &delegate)
{
    Q_ASSERT(!delegate.wasExecuted());
    xQuick::detail::completeDeferred(object, property);
    delegate.setExecuted();
}

// xQuickControl+deferredexecute.hpp ends here
