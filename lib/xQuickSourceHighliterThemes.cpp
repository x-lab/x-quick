#include "xQuickSourceHighliterThemes.hpp"

static QHash<xQuickSourceHighliter::Token, QTextCharFormat> formats()
{
    QHash<xQuickSourceHighliter::Token, QTextCharFormat> _formats;

    QTextCharFormat defaultFormat = QTextCharFormat();

    _formats[xQuickSourceHighliter::Token::CodeBlock] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeKeyWord] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeString] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeComment] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeType] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeOther] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeNumLiteral] = defaultFormat;
    _formats[xQuickSourceHighliter::Token::CodeBuiltIn] = defaultFormat;

    return _formats;
}

static QHash<xQuickSourceHighliter::Token, QTextCharFormat> monokai()
{
    QHash<xQuickSourceHighliter::Token, QTextCharFormat> _formats = formats();

    _formats[xQuickSourceHighliter::Token::CodeBlock].setForeground(QColor(227, 226, 214));
    _formats[xQuickSourceHighliter::Token::CodeKeyWord].setForeground(QColor(249, 38, 114));
    _formats[xQuickSourceHighliter::Token::CodeString].setForeground(QColor(230, 219, 116));
    _formats[xQuickSourceHighliter::Token::CodeComment].setForeground(QColor(117, 113, 94));
    _formats[xQuickSourceHighliter::Token::CodeType].setForeground(QColor(102, 217, 239));
    _formats[xQuickSourceHighliter::Token::CodeOther].setForeground(QColor(249, 38, 114));
    _formats[xQuickSourceHighliter::Token::CodeNumLiteral].setForeground(QColor(174, 129, 255));
    _formats[xQuickSourceHighliter::Token::CodeBuiltIn].setForeground(QColor(166, 226, 46));

    return _formats;
}

QHash<xQuickSourceHighliter::Token, QTextCharFormat> x_quick_source_theme(xQuickSourceHighliter::Themes theme) {
    switch (theme) {
    case xQuickSourceHighliter::Themes::Monokai:
        return monokai();
    }
    return QHash<xQuickSourceHighliter::Token, QTextCharFormat>();
}
