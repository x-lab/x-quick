#pragma once

#include <QtCore/QObject>

#include "xQuickShell+text_style.hpp"

class xQuickShellText;
class xQuickShellScreen;

class xQuickShellBlock
{
public:
    xQuickShellBlock(xQuickShellScreen *screen);
    ~xQuickShellBlock();

    Q_INVOKABLE xQuickShellScreen *screen() const;

    void clear();
    void clearToEnd(int from);
    void clearCharacters(int from, int to);
    void deleteCharacters(int from, int to);
    void deleteToEnd(int from);
    void deleteLines(int from);

    void replaceAtPos(int i, const QString &text, const xQuickShellTextStyle &style, bool only_latin = true);
    void insertAtPos(int i, const QString &text, const xQuickShellTextStyle &style, bool only_latin = true);

    void setxQuickShellScreenIndex(int index) { m_screen_index = index; }
    int screenIndex() const { return m_screen_index; }
    size_t line() { return m_new_line; }
    void setLine(size_t line) {
        if (line != m_new_line) {
            m_changed = true;
            m_new_line = line;
        }
    }

    const QString &textLine() const;
    int textSize() { return m_text_line.size(); }

    int width() const { return m_width; }
    void setWidth(int width);
    int lineCount() const { return (std::max((m_text_line.size() - 1),0) / m_width) + 1; }
    int lineCountAfterModified(int from_char, int text_size, bool replace) {
        int new_size = replace ? std::max(from_char + text_size, m_text_line.size())
            : std::max(from_char, m_text_line.size()) + text_size;
        return ((new_size - 1) / m_width) + 1;
    }

    void setVisible(bool visible);
    bool visible() const;

    xQuickShellBlock *split(int line);
    xQuickShellBlock *takeLine(int line);
    void removeLine(int line);

    void moveLinesFromxQuickShellBlock(xQuickShellBlock *block, int start_line, int count);

    void dispatchEvents();
    void releasexQuickShellTextObjects();

    QVector<xQuickShellTextStyleLine> style_list();

    void printStyleList() const;
    void printStyleList(QDebug &debug) const;
    void printStyleListWidthxQuickShellText() const;

private:
    void mergeCompatibleStyles();
    void ensureStyleAlignWithLines(int i);
    xQuickShellScreen *m_screen;
    QString m_text_line;
    QVector<xQuickShellTextStyleLine> m_style_list;
    size_t m_line;
    size_t m_new_line;
    int m_screen_index;

    int m_width;

    bool m_visible;
    bool m_changed;
    bool m_only_latin;
};
