#include "xQuickShell+character_sets.hpp"

#include <QtCore/QString>
#include <QtCore/QTextCodec>

class xQuickShellNrcTextCodec : public QTextCodec
{
public:
    xQuickShellNrcTextCodec(const QByteArray &name, int mib, const QChar character_set[]);
    QByteArray  name() const;
    int mibEnum() const;

    static void initialize();
protected:
    QString convertToUnicode(const char *in, int length, ConverterState *state) const;
    QByteArray convertFromUnicode(const QChar *in, int length, ConverterState *state) const;

private:
    const QByteArray m_name;
    const int m_mib;
    const QChar *m_character_set;
};
