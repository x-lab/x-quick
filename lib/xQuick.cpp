#include "xCodeLSP.hpp"
#include "xCodeLSPClient.hpp"

#include "xQuick.hpp"
//#include "xQuick+profiler.hpp"
#include "xQuickGrid.hpp"
#include "xQuickGridOverlay.hpp"
#include "xQuickMetrics.hpp"
#include "xQuickSourceHighliter.hpp"

#include "xQuickControl+indicator_button.hpp"
#include "xQuickControl+spinbox.hpp"
#include "xQuickControl+validator.hpp"

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>
#include <QtQuickControls2>

#include <private/qquicktextedit_p.h>

// /////////////////////////////////////////////////////////////////////////////
// NOTE: Terminal integration inception
// /////////////////////////////////////////////////////////////////////////////

//#include "xQuickShell+extension_plugin.hpp"

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class xQuickEditorHelper : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int line READ line NOTIFY lineChanged)
    Q_PROPERTY(int column READ column NOTIFY columnChanged)

public:
    xQuickEditorHelper(QObject *parent = nullptr) : QObject(parent) {}

signals:
    void   lineChanged(void);
    void columnChanged(void);

public:
    int   line(void) { return _line; }
    int column(void) { return _column; }

public:
    Q_INVOKABLE int wordCount(const QString& text)
    {
        if(text.trimmed().isEmpty())
            return 0;

        return text.trimmed().split(QRegularExpression("\\s+")).count();
    }

public:
    Q_INVOKABLE void setup(QQuickTextEdit *edit)
    {
        connect(edit, &QQuickTextEdit::cursorPositionChanged, [=] (void) -> void
        {
            QTextBlock block = edit->textDocument()->textDocument()->findBlock(edit->cursorPosition());

              _line = block.blockNumber() + 1;
            _column = edit->cursorPosition() - block.position() + 1;

            emit   lineChanged();
            emit columnChanged();
        });
    }

public:
    Q_INVOKABLE QString read(const QString&);

private:
    int _line { 1 };
    int _column { 1 };
};

QString xQuickEditorHelper::read(const QString& path)
{
    QString path_cleaned = path;
    path_cleaned.remove("qrc");

    QFile file(path_cleaned);

    if(!file.open(QIODevice::ReadOnly))
        return QString();

    QString contents = file.readAll();

    file.close();

    return contents;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class xQuickSourceHighliterHelper : public QObject
{
    Q_OBJECT

public:
    xQuickSourceHighliterHelper(QObject *parent = nullptr) : QObject(parent) {}

public:
    Q_INVOKABLE void setup(QQuickTextDocument *, int);
};

void xQuickSourceHighliterHelper::setup(QQuickTextDocument *document, int language)
{
    xQuickSourceHighliter *highlighter = new xQuickSourceHighliter(document->textDocument());
    highlighter->setCurrentLanguage((xQuickSourceHighliter::Language) language);
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class xCodeHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString server READ server NOTIFY serverChanged)

public:
    xCodeHelper(QObject *parent = nullptr) : QObject(parent) {}
   ~xCodeHelper(void);

signals:
    void monitoring(void);

signals:
    void diagnostics(QJsonArray items);
    void completions(QJsonArray items);

signals:
    void serverChanged(void);

public:
    Q_INVOKABLE void setup(const QString& source, QQuickTextEdit *, QQuickTextEdit *);

public:
    Q_INVOKABLE QString server(void);

public:
    QTextDocument *code_d;
    QTextDocument *outp_d;

public:
    LSPClient *client;

public:
    QFile     file;
    QFileInfo file_info;

public:
    int   _line { -1 };
    int _column { -1 };

public:
    QTimer timer;
};

xCodeHelper::~xCodeHelper(void)
{
    client->didClose("file://" + file.fileName().toStdString());
    client->shutdown();

    delete client;
}

void xCodeHelper::setup(const QString& source, QQuickTextEdit *edit, QQuickTextEdit *outp)
{
    this->code_d = edit->textDocument()->textDocument();
    this->outp_d = outp->textDocument()->textDocument();

    if(!this->code_d) return;
    if(!this->outp_d) return;

    timer.setInterval(500);
    timer.setSingleShot(true);
    timer.start();

    if(!source.isEmpty()) {
        file.setFileName(source);
    } else {
        file.setFileName("/tmp/0");
    }

    file.open(QIODevice::ReadWrite);
    file_info.setFile(file);

    if (file_info.suffix() == "cpp")
        client = new LSPClient("clangd", {});
    else
        client = new LSPClient("pylsp", {});

    client->initialize();
    client->didOpen("file://" + file.fileName().toStdString(), "", file_info.suffix().toStdString());

// /////////////////////////////////////////////////////////////////////////////
// Position tracking
// /////////////////////////////////////////////////////////////////////////////

    connect(edit, &QQuickTextEdit::cursorPositionChanged, [=] (void) -> void
    {
        QTextBlock block = edit->textDocument()->textDocument()->findBlock(edit->cursorPosition());

          _line = block.blockNumber() + 1;
        _column = edit->cursorPosition() - block.position() + 1;

// ' '      space
// '\t'     horizontal tab
// '\n'     newline
// '\v'     vertical tab
// '\f'     form feed
// '\r'

        emit this->completions(QJsonArray());
    });

// /////////////////////////////////////////////////////////////////////////////
// Diagnostics request
// /////////////////////////////////////////////////////////////////////////////

    connect(code_d, &QTextDocument::contentsChanged, [=] (void) -> void
    {
        timer.start();

        emit this->monitoring();
    });

// /////////////////////////////////////////////////////////////////////////////
// Completion
// /////////////////////////////////////////////////////////////////////////////

    connect(code_d, &QTextDocument::contentsChange, [=] (int position, int charsRemoved, int charsAdded) -> void
    {
        if(charsRemoved)
            emit this->completions(QJsonArray());
        else if(charsAdded)
            client->completion("file://" + file.fileName().toStdString(), { _line - 1 , _column - 1 }, option<CompletionContext>(CompletionContext {}));

        TextDocumentContentChangeEvent ev;
        ev.text = code_d->toPlainText().toStdString();

        std::vector<TextDocumentContentChangeEvent> ch;
        ch.push_back(ev);

        DocumentUri uri = "file://" + file.fileName().toStdString();

        client->didChange(uri, ch, false);
    });

// /////////////////////////////////////////////////////////////////////////////

    connect(client, &LSPClient::onError, [=] (QJsonObject id, QJsonObject err)
    {
        // qDebug() << id << err << Q_FUNC_INFO << 1;
    });

    connect(client, &LSPClient::onNotify, [=] (QString method, QJsonObject param)
    {
        // qDebug() << method << param << Q_FUNC_INFO << 2;

        QJsonDocument document(param);

        outp_d->setPlainText(document.toJson(QJsonDocument::Indented));

        QJsonArray errors;

        for(int i = 0; i < param["diagnostics"].toArray().count(); i++) {

            errors.append(QJsonObject
            {
            { "line", param["diagnostics"].toArray().at(i)["range"]["start"]["line"].toInt() + 1 },
            { "message", param["diagnostics"].toArray().at(i)["message"].toString() },
            { "severity", param["diagnostics"].toArray().at(i)["severity"].toInt() },
            { "source", param["diagnostics"].toArray().at(i)["source"].toString() }
            });
        }

        emit this->diagnostics(errors);
    });

    connect(client, &LSPClient::onRequest, [=] (QString method, QJsonObject param, QJsonObject id)
    {
        // qDebug() << method << param << id << Q_FUNC_INFO << 3;
    });

    connect(client, &LSPClient::onResponse, [=] (QJsonObject id, QJsonObject response)
    {
        // qDebug() << id << response << Q_FUNC_INFO << 4;

        QJsonArray candidates;

        for(int i = 0; i < response["items"].toArray().count(); i++) {

            candidates.append(QJsonObject
            {
            { "candidate", response["items"].toArray().at(i)["insertText"].toString() },
            { "label", response["items"].toArray().at(i)["label"].toString() },
            { "line", response["items"].toArray().at(i)["textEdit"]["range"]["end"]["line"].toInt() + 1 },
            });
        }

        emit this->completions(candidates);

    });

    connect(client, &LSPClient::onServerError, [=] (QProcess::ProcessError err)
    {
        // qDebug() << Q_FUNC_INFO << 4;
    });

    connect(client, &LSPClient::onServerFinished, [=] (int exitCode, QProcess::ExitStatus status)
    {
        // qDebug() << Q_FUNC_INFO << 5;
    });

    connect(&timer, &QTimer::timeout, this, [=] (void) -> void
    {
        TextDocumentContentChangeEvent ev;
        ev.text = code_d->toPlainText().toStdString();

        std::vector<TextDocumentContentChangeEvent> ch;
        ch.push_back(ev);

        client->didChange("file://" + file.fileName().toStdString(), ch, true);
    });
}

QString xCodeHelper::server(void)
{
    if(file_info.suffix() == "cpp")
        return "clangd";

    return "pylsp";
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void x_quick_initialise(void)
{
    qmlRegisterAnonymousType<xQuickGridAttachedType>("xQuick", 1);

    qmlRegisterType<xQuickEditorHelper>("xQuick", 1, 0, "EditorHelper");
    qmlRegisterType<xCodeHelper>("xQuick", 1, 0, "CodeHelper");
    qmlRegisterType<xQuickGrid>("xQuick", 1, 0, "Grid");
    qmlRegisterType<xQuickMetrics>("xQuick", 1, 0, "Metrics");
    // qmlRegisterType<xQuickProfiler>("xQuick", 1, 0, "Profiler");
    qmlRegisterType<xQuickSourceHighliterHelper>("xQuick", 1, 0, "SourceHighliter");

    qmlRegisterType<xQuickControlValidatorInteger>("xQuick", 1, 0, "IntegerValidator");
    qmlRegisterUncreatableType<xQuickControlAbstractSpinBox>("xQuick", 1, 0, "ScientificAbstractSpinBox", "");
    qmlRegisterType<xQuickControlSpinBoxInteger>("xQuick", 1, 0, "ScientificSpinBoxInteger");
    qmlRegisterType<xQuickControlSpinBoxReal>("xQuick", 1, 0, "ScientificSpinBoxReal");
    qmlRegisterUncreatableType<xQuickControlIndicatorButton>("xQuick", 1, 0, "ScientificIndicatorButton", "");


    // xQuickShellExtensionPlugin plugin; plugin.registerTypes("xQuick.Shell");

#if QT_VERSION <= 0x060000
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::addStylePath("qrc:///qml/xQuick");
    QQuickStyle::addStylePath("qrc:///qml");
    QQuickStyle::setStyle("Style");
#else
    QQuickStyle::setStyle("xQuick.Style");
#endif
}

// /////////////////////////////////////////////////////////////////////////////

#include "xQuick.moc"
