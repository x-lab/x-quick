// xQuickControl+validator.cpp

#include "xQuickControl+validator.hpp"

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

xQuickControlValidatorInteger::xQuickControlValidatorInteger(QObject *parent) : QValidator(parent)
{
}

xQuickControlValidatorInteger::xQuickControlValidatorInteger(qlonglong minimum, qlonglong maximum, QObject * parent) : QValidator(parent)
{
    b = minimum;
    t = maximum;
}

xQuickControlValidatorInteger::xQuickControlValidatorInteger(QVariant minimum, QVariant maximum, QObject * parent) : QValidator(parent)
{
    b = minimum.value<qlonglong>();
    t = maximum.value<qlonglong>();
}

/*!
    Sets the range of the validator to only accept integers between \a
    bottom and \a top inclusive.
*/

void xQuickControlValidatorInteger::setRange(qlonglong bottom, qlonglong top)
{
    bool rangeChanged = false;
    if (b != bottom) {
        b = bottom;
        rangeChanged = true;
        emit bottomChanged(b);
    }

    if (t != top) {
        t = top;
        rangeChanged = true;
        emit topChanged(t);
    }

    if (rangeChanged) {
        emit changed();
    }
}

/*!
    \property xQuickControlValidatorInteger::bottom
    \brief the validator's lowest acceptable value

    By default, this property's value is derived from the lowest signed
    integer available (typically -2147483647).

    \sa setRange()
*/
void xQuickControlValidatorInteger::setBottom(QVariant bottom)
{
    setRange(bottom.value<qlonglong>(), top_impl());
}

/*!
    \property xQuickControlValidatorInteger::top
    \brief the validator's highest acceptable value

    By default, this property's value is derived from the highest signed
    integer available (typically 2147483647).

    \sa setRange()
*/
void xQuickControlValidatorInteger::setTop(QVariant top)
{
    setRange(bot_impl(), top.value<qlonglong>());
}

QValidator::State xQuickControlValidatorInteger::validate(QString& input, int&) const
{
    if (input.isEmpty()) {
        return QValidator::Intermediate;
    }

    const bool startsWithMinus(input.startsWith('-'));
    if (startsWithMinus && b >= 0) {
        return Invalid;
    }

    const bool startsWithPlus(input.startsWith('+'));
    if (startsWithPlus && t < 0) {
        return Invalid;
    }

    bool ok;
    qlonglong value = input.toLongLong(&ok);

    if (!ok) {
        return QValidator::Invalid;
    }

    if (value >= b && value <= t) {
        return QValidator::Acceptable;
    }

    if (value >= 0) {
        // the -entered < b condition is necessary to allow people to type
        // the minus last (e.g. for right-to-left languages)
        // The buffLength > tLength condition validates values consisting
        // of a number of digits equal to or less than the max value as intermediate.

        auto buffLength = input.size();
        if (startsWithPlus)
            buffLength--;
        const qlonglong tLength = t != 0 ? static_cast<qlonglong>(std::log10(qAbs(t))) + 1 : 1;

        return (value > t && -value < b && buffLength > tLength) ? Invalid : Intermediate;
    } else {
        return (value < b) ? Invalid : Intermediate;
    }
}


// xQuickControl+validator.cpp ends here
