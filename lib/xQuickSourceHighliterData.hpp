#pragma once

template<typename key, typename val> class QMultiHash;

class QLatin1String;

using LanguageData = QMultiHash<char, QLatin1String>;

void loadLuaData(LanguageData &typess,
                 LanguageData &keywordss,
                 LanguageData &builtins,
                 LanguageData &literalss,
                 LanguageData &others);

void loadCppData(LanguageData &typess,
             LanguageData &keywordss,
             LanguageData &builtins,
             LanguageData &literalss,
             LanguageData &others);

void loadShellData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadJSData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadPHPData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadQMLData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadPythonData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadRustData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadJavaData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadCSharpData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadGoData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadVData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadSQLData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadJSONData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadCSSData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadTypescriptData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadYAMLData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadVEXData(LanguageData &types,
             LanguageData &keywords,
             LanguageData &builtin,
             LanguageData &literals,
             LanguageData &other);

void loadCMakeData(QMultiHash<char, QLatin1String> &types,
             QMultiHash<char, QLatin1String> &keywords,
             QMultiHash<char, QLatin1String> &builtin,
             QMultiHash<char, QLatin1String> &literals,
             QMultiHash<char, QLatin1String> &other);

void loadMakeData(QMultiHash<char, QLatin1String>& types,
    QMultiHash<char, QLatin1String>& keywords,
    QMultiHash<char, QLatin1String>& builtin,
    QMultiHash<char, QLatin1String>& literals,
    QMultiHash<char, QLatin1String>& other);

void loadAsmData(QMultiHash<char, QLatin1String>& types,
    QMultiHash<char, QLatin1String>& keywords,
    QMultiHash<char, QLatin1String>& builtin,
    QMultiHash<char, QLatin1String>& literals,
    QMultiHash<char, QLatin1String>& other);
