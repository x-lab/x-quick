#pragma once

#include <QtCore>
#include <QtQuick>

// ////////////////////////////////////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////////////////////////////////////

class xQuickGridAttachedType : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int colWidth READ colWidth WRITE setColWidth NOTIFY colWidthChanged)

public:
    xQuickGridAttachedType(QObject *parent);

public:
    int colWidth(void) const;

public:
    void setColWidth(int width);

signals:
    void colWidthChanged(void);

private:
    int m_colWidth = 0;
};

// ////////////////////////////////////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////////////////////////////////////

class xQuickGridOverlay;

class xQuickGrid : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(uint columns MEMBER m_columns NOTIFY gridChanged)
    Q_PROPERTY(double columnWidth READ columnWidth NOTIFY columnWidthChanged)
    Q_PROPERTY(QColor columnColor MEMBER m_column_color NOTIFY gridChanged)
    Q_PROPERTY(uint columnSpacing MEMBER m_column_spacing NOTIFY gridChanged)
    Q_PROPERTY(uint rowSpacing MEMBER m_row_spacing NOTIFY gridChanged)
    Q_PROPERTY(double contentHeight READ contentHeight NOTIFY contentHeightChanged)
    Q_PROPERTY(bool showGrid MEMBER m_showGrid NOTIFY gridChanged)
    Q_PROPERTY(xQuickGrid *helpers READ helpers NOTIFY columnWidthChanged)
    Q_PROPERTY(FillStrategy fillStrategy MEMBER m_fill_strategy NOTIFY gridChanged)

public:
    enum FillStrategy
    {
        ITEM_SQUEEZE,
        ITEM_BREAK
    };
    Q_ENUMS(FillStrategy)

public:
     xQuickGrid(QQuickItem *parent = nullptr);
    ~xQuickGrid() override;

    static xQuickGridAttachedType *qmlAttachedProperties(QObject *object);

    Q_INVOKABLE int columnStart(int column);
    Q_INVOKABLE int columnsWidth(int columns);

    bool showGrid(void);

    double columns(void);
    double columnWidth(void);
    double contentHeight(void);
    QColor columnColor(void);

signals:
    void gridChanged(void);
    void contentHeightChanged(void);
    void columnWidthChanged(void);

public slots:
    void onGridChanged(void);

private:
    void calculateValues(void);

    xQuickGrid *helpers(void);

    double m_columns;
    double m_column_width;
    QColor m_column_color;
    double m_column_spacing;
    double m_content_height;

    uint m_row_spacing;

    FillStrategy m_fill_strategy;

    bool m_showGrid;

    xQuickGridOverlay* m_debug_overlay;
    xQuickGridAttachedType* attachedType(QQuickItem * item) const;

public:
    void componentComplete(void) override;

protected:
    void itemChange(ItemChange, const ItemChangeData &data) override;
    void updatePolish(void) override;
};

QML_DECLARE_TYPE(xQuickGrid)
QML_DECLARE_TYPEINFO(xQuickGrid, QML_HAS_ATTACHED_PROPERTIES)
