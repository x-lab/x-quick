#pragma once

#include <QtQuick/QQuickItem>
#include <QtGui/QRawFont>

class xQuickShellMonoText : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setxQuickShellText NOTIFY textChanged)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(qreal paintedWidth READ paintedWidth NOTIFY paintedWidthChanged)
    Q_PROPERTY(qreal paintedHeight READ paintedHeight NOTIFY paintedHeightChanged)
    Q_PROPERTY(bool latin READ latin WRITE setLatin NOTIFY latinChanged);
public:
    xQuickShellMonoText(QQuickItem *parent=0);
    ~xQuickShellMonoText();

    QString text() const;
    void setxQuickShellText(const QString &text);

    QFont font() const;
    void setFont(const QFont &font);

    QColor color() const;
    void setColor(const QColor &color);

    qreal paintedWidth() const;
    qreal paintedHeight() const;

    bool latin() const;
    void setLatin(bool latin);

signals:
    void textChanged();
    void fontChanged();
    void colorChanged();
    void paintedWidthChanged();
    void paintedHeightChanged();
    void latinChanged();
protected:
    QSGNode *updatePaintNode(QSGNode *old, UpdatePaintNodeData *data) Q_DECL_OVERRIDE;
    void updatePolish() Q_DECL_OVERRIDE;
private:
    Q_DISABLE_COPY(xQuickShellMonoText);
    void updateSize();

    QString m_text;
    QFont m_font;
    QColor m_color;
    bool m_color_changed;
    bool m_latin;
    bool m_old_latin;
    QSizeF m_text_size;
};
