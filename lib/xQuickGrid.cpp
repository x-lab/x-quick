#include <QDebug>
#include <QtMath>

#include "xQuickGrid.hpp"
#include "xQuickGridOverlay.hpp"

// ////////////////////////////////////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////////////////////////////////////

xQuickGridAttachedType *xQuickGrid::qmlAttachedProperties(QObject *object)
{
    return new xQuickGridAttachedType(object);
}

xQuickGridAttachedType::xQuickGridAttachedType(QObject *parent): QObject(parent), m_colWidth(0)
{
}

int xQuickGridAttachedType::colWidth(void) const
{
    return m_colWidth;
}

void xQuickGridAttachedType::setColWidth(int width)
{
    m_colWidth = width;
    emit colWidthChanged();
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
//
// ////////////////////////////////////////////////////////////////////////////////////////////////

xQuickGrid::xQuickGrid(QQuickItem *parent):
    QQuickItem(parent),
    m_columns(12.0),
    m_column_width(0),
    m_column_color("#3Cff0000"),
    m_column_spacing(20.0),
    m_row_spacing(0),
    m_fill_strategy(ITEM_BREAK),
    m_showGrid(false)
{
    m_debug_overlay = new xQuickGridOverlay(this);
    m_debug_overlay->setParentItem(this);
    m_debug_overlay->setZ(1000);
    m_debug_overlay->setVisible(false);
    connect(this, &QQuickItem::widthChanged, this, &xQuickGrid::onGridChanged);
    connect(this, &xQuickGrid::gridChanged, this, &xQuickGrid::onGridChanged);
}

xQuickGrid::~xQuickGrid(void)
{

}

int xQuickGrid::columnStart(int column)
{
    if(column <= 0){
        return 0;
    }
    return qFloor(static_cast<double>(column)*(m_column_width+m_column_spacing));
}

int xQuickGrid::columnsWidth(int columns)
{
    if(columns <= 0){
        return 0;
    }
    return qFloor(static_cast<double>(columns)*(m_column_width+m_column_spacing)-m_column_spacing);
}

double xQuickGrid::columns(void)
{
    return m_columns;
}

void xQuickGrid::onGridChanged(void)
{
    calculateValues();

    m_debug_overlay->setVisible(m_showGrid);
}

void xQuickGrid::calculateValues(void)
{
    int full_width = static_cast<int>(this->width());
    int gutters_width = static_cast<int>((m_columns - 1) * m_column_spacing);
    int avilable_columns_width = full_width - gutters_width;
    m_column_width = avilable_columns_width/m_columns;
    emit columnWidthChanged();
}

double xQuickGrid::columnWidth(void)
{
    return m_column_width;
}

double xQuickGrid::contentHeight(void)
{
    return m_content_height;
}

QColor xQuickGrid::columnColor(void)
{
    return m_column_color;
}

bool xQuickGrid::showGrid(void)
{
    return m_showGrid;
}

xQuickGrid *xQuickGrid::helpers(void)
{
    return this;
}

xQuickGridAttachedType *xQuickGrid::attachedType(QQuickItem *item) const
{
    return qobject_cast<xQuickGridAttachedType *> (qmlAttachedPropertiesObject<xQuickGrid> (item, false));
}

void xQuickGrid::componentComplete(void)
{
    QQuickItem::componentComplete ();
    connect (this, &xQuickGrid::widthChanged,   this, &xQuickGrid::polish, Qt::UniqueConnection);
    connect (this, &xQuickGrid::heightChanged,  this, &xQuickGrid::polish, Qt::UniqueConnection);
    connect (this, &xQuickGrid::gridChanged, this, &xQuickGrid::polish, Qt::UniqueConnection);
    polish();
}

void xQuickGrid::itemChange(QQuickItem::ItemChange change, const QQuickItem::ItemChangeData & data)
{
    QQuickItem::itemChange (change, data);
    switch (int (change)) {
        case ItemChildAddedChange: {
            if (QQuickItem * child { data.item }) {
                connect (child, &QQuickItem::visibleChanged,        this, &xQuickGrid::polish, Qt::UniqueConnection);
                connect (child, &QQuickItem::implicitWidthChanged,  this, &xQuickGrid::polish, Qt::UniqueConnection);
                connect (child, &QQuickItem::implicitHeightChanged, this, &xQuickGrid::polish, Qt::UniqueConnection);
                if (const xQuickGridAttachedType *attached { attachedType(child) }) {
                    connect (attached, &xQuickGridAttachedType::colWidthChanged, this, &xQuickGrid::polish, Qt::UniqueConnection);
                }
                polish ();
            }
            break;
        }
        case ItemChildRemovedChange: {
            if (QQuickItem * child { data.item }) {
                disconnect (child, Q_NULLPTR, this, Q_NULLPTR);
                if (const xQuickGridAttachedType * attached { attachedType (child) }) {
                    disconnect (attached, Q_NULLPTR, this, Q_NULLPTR);
                }
                polish ();
            }
            break;
        }
    }
}

void xQuickGrid::updatePolish(void)
{
    QQuickItem::updatePolish();
    const QList<QQuickItem *> childItemsList { childItems () };
    QVector<QQuickItem *> layoutItemsList { };
    layoutItemsList.reserve (childItemsList.count ());
    for(QQuickItem * child : childItemsList){
        const xQuickGridAttachedType* attached = attachedType(child);
        if(child->isVisible() && attached){
            layoutItemsList.append(child);
        }
    }
    int yOffset = 0;
    int maxY = 0;
    int columns_used = 0;
    for (QQuickItem * child : layoutItemsList) {
        const xQuickGridAttachedType* attached = attachedType(child);
        if (attached && attached->colWidth() > 0) {
            int columns_requested = attached->colWidth();
            switch (m_fill_strategy) {
            case xQuickGrid::FillStrategy::ITEM_SQUEEZE:
                child->setX(columnStart(columns_used));
                child->setY(yOffset);
                if(columns_used + columns_requested > m_columns){
                    qWarning() << "item requested" << columns_requested << "columns. but only" << m_columns-columns_used << "are available. item will be squeezed.";
                    child->setWidth(columnsWidth(static_cast<int>(m_columns-columns_used)));
                    columns_used += static_cast<int>(m_columns-columns_used);
                }else{
                    child->setWidth(columnsWidth(columns_requested));
                    columns_used += columns_requested;
                }
                if(child->height() > maxY){
                    maxY = qFloor(child->height());
                }
                if(columns_used >= m_columns){
                    yOffset += maxY + static_cast<int>(m_row_spacing);
                    maxY = 0;
                    columns_used = 0;
                }
                break;
            case xQuickGrid::FillStrategy::ITEM_BREAK:
                if(columns_used + columns_requested > m_columns){
                    if(columns_requested > m_columns){
                        qWarning() << "item requested" << columns_requested << "columns. but grid has only" << m_columns << "columns. item will be ignored.";
                    }else{
                        yOffset += maxY + static_cast<int>(m_row_spacing);
                        maxY = 0;
                        columns_used = 0;
                        child->setX(columnStart(columns_used));
                        child->setY(yOffset);
                        child->setWidth(columnsWidth(columns_requested));
                        columns_used += columns_requested;
                    }
                }else{
                    child->setX(columnStart(columns_used));
                    child->setY(yOffset);
                    child->setWidth(columnsWidth(columns_requested));
                    columns_used += columns_requested;
                }
                if(child->height() > maxY){
                    maxY = qFloor(child->height());
                }
                if(columns_used >= m_columns){
                    yOffset += maxY + static_cast<int>(m_row_spacing);
                    maxY = 0;
                    columns_used = 0;
                }
                break;
            }


        }
    }
    m_content_height = yOffset + maxY;
    emit contentHeightChanged();
}
