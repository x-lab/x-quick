#pragma once

#include <QtCore>
#include <QtGui>

class xQuickMetrics : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(bool ldpi READ isLDPI CONSTANT)
    Q_PROPERTY(bool mdpi READ isMDPI CONSTANT)
    Q_PROPERTY(bool hdpi READ isHDPI CONSTANT)
    Q_PROPERTY(bool xhdpi READ isXHDPI CONSTANT)
    Q_PROPERTY(bool xxhdpi READ isXXHDPI CONSTANT)
    Q_PROPERTY(bool xxxhdpi READ isXXXHDPI CONSTANT)

    Q_PROPERTY(float desiredWidth READ desiredWidth CONSTANT)
    Q_PROPERTY(float desiredHeight READ desiredHeight CONSTANT)
    Q_PROPERTY(float sizeInInches READ sizeInInches CONSTANT)

    Q_PROPERTY(bool small READ isSmallSize CONSTANT)
    Q_PROPERTY(bool normal READ isNormalSize CONSTANT)
    Q_PROPERTY(bool large READ isLargeSize CONSTANT)
    Q_PROPERTY(bool xlarge READ isXLargeSize CONSTANT)

public:
    xQuickMetrics(const float& refDpi = 386.f, const float& refWidth = 1080.f, const float& refHeight = 1920.f, const float& refSizeInInches = 5.f, QObject *parent = nullptr);

    Q_INVOKABLE qreal dp(const qreal& size);
    Q_INVOKABLE qreal sp(const qreal& size);

    QString lowResourceFolderName(void) const;
    QString mediumResourceFolderName(void) const;
    QString highResourceFolderName(void) const;
    QString xHighResourceFolderName(void) const;
    QString xxHighResourceFolderName(void) const;
    QString xxxHighResourceFolderName(void) const;

    void setLowResourceFolderName(const QString&);
    void setMediumResourceFolderName(const QString&);
    void setHighResourceFolderName(const QString&);
    void setXHighResourceFolderName(const QString&);
    void setXXHighResourceFolderName(const QString&);
    void setXXXHighResourceFolderName(const QString&);

    Q_INVOKABLE QString resourceFolderName(void) const;
    Q_INVOKABLE QString resource(const QString&) const;

    float desiredHeight(void) const;
    float desiredWidth(void) const;

    float sizeInInches(void) const;

    void printScreenInfo(void) const;

    Q_INVOKABLE QString screenInfo(void) const;

private:
    bool isLDPI(void) const;
    bool isMDPI(void) const;
    bool isHDPI(void) const;

    bool isXHDPI(void) const;
    bool isXXHDPI(void) const;
    bool isXXXHDPI(void) const;

    bool isSmallSize(void) const;
    bool isNormalSize(void) const;
    bool isLargeSize(void) const;

    bool isXLargeSize(void) const;
    void calculateRatio(void);

    /**
     * @brief Get the scale that gives a size with preserved aspect ratio.
     * @param origSize The original size of the image
     * @param newSize This is the size you want it to be. Only one property used according to the useHeight parameter.
     * @param useHeight If you want to scale the image according to its height, set to true. Set to false otherwise.
     * @return
     */
    float aspectRatioWidth(const QSize& origSize, const float& newHeight) const;

private:
    class xQuickMetricsPrivate *d;
};
