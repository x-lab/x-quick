#include "xQuickShell+extension_plugin.hpp"

#include <QtCore/QByteArray>

#include <QQmlEngine>

#include "xQuickShell+terminal_screen.hpp"
#include "xQuickShell+object_destruct_item.hpp"
#include "xQuickShell+screen.hpp"
#include "xQuickShell+text.hpp"
#include "xQuickShell+cursor.hpp"
#include "xQuickShell+mono_text.hpp"
#include "xQuickShell+selection.hpp"

static const struct {
    const char *type;
    int major, minor;
} qmldir [] = {
    { "Screen", 1, 0},
    { "Text", 1, 0},
    { "Cursor", 1, 0},
    { "Selection", 1, 0},
};

void xQuickShellExtensionPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QByteArrayLiteral("xQuick.Shell"));

    qmlRegisterType<xQuickShellTerminalScreen>(uri, 1, 0, "TerminalScreen");
    qmlRegisterType<ObjectDestructItem>(uri, 1, 0, "ObjectDestructItem");
    qmlRegisterType<xQuickShellMonoText>(uri, 1, 0, "MonoText");
    qmlRegisterAnonymousType<xQuickShellScreen>(uri, 1);
    qmlRegisterAnonymousType<xQuickShellText>(uri, 1);
    qmlRegisterAnonymousType<xQuickShellCursor>(uri, 1);
    qmlRegisterAnonymousType<xQuickShellSelection>(uri, 1);

    const QString filesLocation = "qrc:///qml/xQuick/Controls";

    for (int i = 0; i < int(sizeof(qmldir)/sizeof(qmldir[0])); i++) {
        qmlRegisterType(QUrl(filesLocation + "/" + qmldir[i].type + ".qml"), uri, qmldir[i].major, qmldir[i].minor, qmldir[i].type);
    }
}

void xQuickShellExtensionPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri);
    Q_UNUSED(engine);
}
