#pragma once

#include <QObject>

#include "xQuickShell+color_palette.hpp"
#include "xQuickShell+parser.hpp"
#include "xQuickShell+pty.hpp"
#include "xQuickShell+text_style.hpp"

#include <QtCore/QPoint>
#include <QtCore/QSize>
#include <QtCore/QStack>
#include <QtCore/QElapsedTimer>

class xQuickShellBlock;
class xQuickShellCursor;
class xQuickShellText;
class xQuickShellScreenData;
class xQuickShellSelection;

class xQuickShellScreen : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int contentHeight READ contentHeight NOTIFY contentHeightChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY screenTitleChanged)
    Q_PROPERTY(xQuickShellSelection *selection READ selection CONSTANT)
    Q_PROPERTY(QColor defaultBackgroundColor READ defaultBackgroundColor NOTIFY defaultBackgroundColorChanged)
    Q_PROPERTY(QString platformName READ platformName CONSTANT)

public:
    explicit xQuickShellScreen(QObject *parent = 0);
    ~xQuickShellScreen();

    void emitRequestHeight(int newHeight);
    void setHeight(int height);
    int height() const;
    int contentHeight() const;

    void emitRequestWidth(int newWidth);
    void setWidth(int width);
    int width() const;

    xQuickShellScreenData *currentxQuickShellScreenData() const { return m_current_data; }
    void useAlternatexQuickShellScreenBuffer();
    void useNormalxQuickShellScreenBuffer();

    xQuickShellCursor *currentxQuickShellCursor() const { return  m_cursor_stack.last(); }
    void savexQuickShellCursor();
    void restorexQuickShellCursor();

    xQuickShellTextStyle defaultxQuickShellTextStyle() const;

    QColor defaultForegroundColor() const;
    QColor defaultBackgroundColor() const;

    xQuickShellColorPalette *colorPalette() const;

    void clearxQuickShellScreen();

    void fill(const QChar character);
    void clear();

    void setFastScroll(bool fast);
    bool fastScroll() const;

    xQuickShellSelection *selection() const;
    Q_INVOKABLE void doubleClicked(double character, double line);

    void setTitle(const QString &title);
    QString title() const;

    QString platformName() const;

    void scheduleFlash();

    Q_INVOKABLE void printxQuickShellScreen() const;

    void scheduleEventDispatch();
    void dispatchChanges();

    void sendPrimaryDA();
    void sendSecondaryDA();

    void setApplicationxQuickShellCursorKeysMode(bool enable);
    bool applicationxQuickShellCursorKeyMode() const;

    Q_INVOKABLE void sendKey(const QString &text, Qt::Key key, Qt::KeyboardModifiers modifiers);

    xQuickShellPty *pty();

    Q_INVOKABLE void ensureVisiblexQuickShellPages(int top_line);
    xQuickShellText *createxQuickShellTextSegment(const xQuickShellTextStyleLine &style_line);
    void releasexQuickShellTextSegment(xQuickShellText *text);

public slots:
    void readData(const QByteArray &data);
    void paletteChanged();
signals:
    void reset();

    void flash();

    void dispatchLineChanges();
    void dispatchxQuickShellTextSegmentChanges();

    void screenTitleChanged();

    void textCreated(xQuickShellText *text);
    void cursorCreated(xQuickShellCursor *cursor);

    void requestHeightChange(int newHeight);
    void heightChanged();
    void contentHeightChanged();

    void requestWidthChange(int newWidth);
    void widthChanged();

    void defaultBackgroundColorChanged();

    void contentModified(size_t lineModified, int lineDiff, int contentDiff);
    void dataHeightChanged(int newHeight, int removedBeginning, int reclaimed);
    void widthAboutToChange(int width);
    void dataWidthChanged(int newWidth, int removedBeginning, int reclaimed);

    void hangup();
protected:
    void timerEvent(QTimerEvent *);

private:
    xQuickShellColorPalette *m_palette;
    xQuickShellPty m_pty;
    xQuickShellParser m_parser;
    QElapsedTimer m_time_since_parsed;
    QElapsedTimer m_time_since_initiated;

    int m_timer_event_id;
    int m_width;
    int m_height;

    xQuickShellScreenData *m_primary_data;
    xQuickShellScreenData *m_alternate_data;
    xQuickShellScreenData *m_current_data;
    xQuickShellScreenData *m_old_current_data;

    QVector<xQuickShellCursor *> m_cursor_stack;
    QVector<xQuickShellCursor *> m_new_cursors;
    QVector<xQuickShellCursor *> m_delete_cursors;

    QString m_title;

    xQuickShellSelection *m_selection;

    bool m_flash;
    bool m_cursor_changed;
    bool m_application_cursor_key_mode;
    bool m_fast_scroll;

    QVector<xQuickShellText *> m_to_delete;

    QColor m_default_background;

    friend class xQuickShellScreenData;
};
