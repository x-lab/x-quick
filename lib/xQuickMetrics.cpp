#include "xQuickMetrics.hpp"

#include <cmath>

#if (!defined(Q_OS_IOS) && !defined(Q_OS_ANDROID)) && (defined(Q_OS_WIN) || defined(Q_OS_MAC))
#define Q_OS_DESKTOP
#endif

#if defined(Q_OS_ANDROID) || defined(Q_OS_IOS)
#define Q_OS_MOBILE
#endif

class xQuickMetricsPrivate
{
public:
    QRect screenRect;

    QSize refSize;
    float refDPI;

    float DPI;
    float DPIVariation;

    float lowDPIValue;
    float mediumDPIValue;
    float highDPIValue;
    float xHighDPIValue;
    float xxHighDPIValue;
    float xxxHighDPIValue;

    QString lowResourceName;
    QString mediumResourceName;
    QString highResourceName;
    QString xHighResourceName;
    QString xxHighResourceName;
    QString xxxHighResourceName;

    float ratio;
    float ratioFont;
    float desiredWidth;
    float desiredHeight;
    float sizeInInches;
    float scale;
};

xQuickMetrics::xQuickMetrics(const float& refDpi, const float& refWidth, const float& refHeight, const float& refSizeInInches, QObject *parent) : QObject(parent)
{
    d = new xQuickMetricsPrivate;

    d->screenRect = QGuiApplication::primaryScreen()->geometry();
    d->refSize = QSize(refWidth, refHeight);
    d->refDPI = refDpi;
#if defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    d->DPI = refDpi;
#else
    d->DPI = QGuiApplication::primaryScreen()->physicalDotsPerInch();
#endif // defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    d->lowDPIValue = 120;
    d->mediumDPIValue = 160;
    d->highDPIValue = 240;
    d->xHighDPIValue = 320;
    d->xxHighDPIValue = 480;
    d->xxxHighDPIValue = 640;
    d->DPIVariation = 20;
    d->ratio = 1.f;
    d->ratioFont = 1.f;
    d->desiredWidth = 0.f;
    d->desiredHeight = 0.f;
    d->sizeInInches = refSizeInInches;
    d->scale = 1.f;

    calculateRatio();

    // printScreenInfo();

    connect(QGuiApplication::primaryScreen(), &QScreen::orientationChanged, this, &xQuickMetrics::calculateRatio);
}

qreal xQuickMetrics::dp(const qreal& size)
{
    return qMax(1, int(size * d->ratio));
}

qreal xQuickMetrics::sp(const qreal& size)
{
    return qMax(1, int(size * d->ratioFont));
}

QString xQuickMetrics::lowResourceFolderName(void) const
{
    return d->lowResourceName;
}

QString xQuickMetrics::mediumResourceFolderName(void) const
{
    return d->mediumResourceName;
}

QString xQuickMetrics::highResourceFolderName(void) const
{
    return d->highResourceName;
}

QString xQuickMetrics::xHighResourceFolderName(void) const
{
    return d->xHighResourceName;
}

QString xQuickMetrics::xxHighResourceFolderName(void) const
{
    return d->xxHighResourceName;
}

QString xQuickMetrics::xxxHighResourceFolderName(void) const
{
    return d->xxxHighResourceName;
}

void xQuickMetrics::setLowResourceFolderName(const QString& resourceName)
{
    d->lowResourceName = resourceName;
}

void xQuickMetrics::setMediumResourceFolderName(const QString& resourceName)
{
    d->mediumResourceName = resourceName;
}

void xQuickMetrics::setHighResourceFolderName(const QString& resourceName)
{
    d->highResourceName = resourceName;
}

void xQuickMetrics::setXHighResourceFolderName(const QString& resourceName)
{
    d->xHighResourceName = resourceName;
}

void xQuickMetrics::setXXHighResourceFolderName(const QString& resourceName)
{
    d->xxHighResourceName = resourceName;
}

void xQuickMetrics::setXXXHighResourceFolderName(const QString& resourceName)
{
    d->xxxHighResourceName = resourceName;
}

QString xQuickMetrics::resourceFolderName(void) const
{
    QString name = "";

#if defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    // If we are on the desktop testing for mobile, use the low DPI assets.
    name = lowResourceFolderName();
#else
    if (isLDPI()) {
        name = lowResourceFolderName();
    }
    else if (isMDPI()) {
        name = mediumResourceFolderName();
    }
    else if (isHDPI()) {
#ifdef Q_OS_MOBILE
        // This is checked in case we are on a low resolution but high DPI device. Like Samsung GT-I9190
        if (isSmallSize()) {
            name = lowResourceFolderName();
        }
        else if (isNormalSize()) {
            name = mediumResourceFolderName();
        }
        else {
            name = highResourceFolderName();
        }
#else
        name = highResourceFolderName();
#endif // Q_OS_MOBILE
    }
    else if (isXHDPI()) {
        name = xHighResourceFolderName();
    }
    else if (isXXHDPI()) {
        name = xxHighResourceFolderName();
    }
    else if (isXXXHDPI()) {
        name = xxxHighResourceFolderName();
    }
#endif // defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)

    return name;
}

QString xQuickMetrics::resource(const QString& fileName) const
{
    return resourceFolderName() + "/" + fileName;
}

float xQuickMetrics::desiredHeight(void) const
{
    return d->desiredHeight;
}

float xQuickMetrics::desiredWidth(void) const
{
    return d->desiredWidth;
}

float xQuickMetrics::sizeInInches(void) const
{
    return d->sizeInInches;
}

bool xQuickMetrics::isLDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) <= (d->lowDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isMDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) > (d->lowDPIValue / d->mediumDPIValue) && (d->DPI / d->mediumDPIValue) < (d->highDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isHDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) >= (d->highDPIValue / d->mediumDPIValue) && (d->DPI / d->mediumDPIValue) < (d->xHighDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isXHDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) >= (d->xHighDPIValue / d->mediumDPIValue) && (d->DPI / d->mediumDPIValue) < (d->xxHighDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isXXHDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) >= (d->xxHighDPIValue / d->mediumDPIValue) && (d->DPI / d->mediumDPIValue) < (d->xxxHighDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isXXXHDPI(void) const
{
    return (d->DPI / d->mediumDPIValue) >= (d->xxxHighDPIValue / d->mediumDPIValue);
}

bool xQuickMetrics::isSmallSize(void) const
{
    return d->sizeInInches <= 4.6f;
}

bool xQuickMetrics::isNormalSize(void) const
{
    return d->sizeInInches > 4.6f && d->sizeInInches <= 5.3f;
}

bool xQuickMetrics::isLargeSize() const
{
    return d->sizeInInches > 5.3f && d->sizeInInches < 6.0f;
}

bool xQuickMetrics::isXLargeSize(void) const
{
    return d->sizeInInches >= 6.0f;
}

void xQuickMetrics::calculateRatio(void)
{
    // The code here is based on the code provided by Qt here: http://doc.qt.io/qt-5/scalability.html
#if defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    d->desiredHeight = qMin((float)d->refSize.height(), (float)QGuiApplication::primaryScreen()->geometry().height() * 0.8f);
    d->desiredWidth = getAspectRatioWidth(d->refSize, d->desiredHeight);
#else
    const QSizeF physicalSize = QGuiApplication::primaryScreen()->physicalSize();
    d->sizeInInches = std::sqrt(std::pow(physicalSize.width(), 2) + std::pow(physicalSize.height(), 2)) * 0.0393701f;
#endif // Q_OS_DESKTOP

#if defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    d->scale = qMax(d->desiredWidth / float(d->refSize.width()), d->desiredHeight / float(d->refSize.height()));
#else
    d->scale = 1.f;
#endif // defined(Q_OS_DESKTOP) && defined(QUTILS_FOR_MOBILE)
    d->ratio = (d->DPI / 160.f) * d->scale;
    d->ratioFont = (d->DPI / 160.f) * d->scale;
}

float xQuickMetrics::aspectRatioWidth(const QSize& origSize, const float& newHeight) const
{
    // Width Formula: original height / original width * new width = new height
    // Height Formula: orignal width / orignal height * new height = new width

    return newHeight / (static_cast<float>(origSize.height()) / static_cast<float>(origSize.width()));
}

void xQuickMetrics::printScreenInfo(void) const
{
    qInfo() << this->screenInfo();
}

QString xQuickMetrics::screenInfo(void) const
{
    return QString(
        "Size in Inches: " + QString::number(d->sizeInInches) + "\n"
        "DPI: " + QString::number(d->DPI) + "\n"
        "isSmallSize: " + (isSmallSize() ? "true" : "false") + "\n"
        "isNormalSize: " + (isNormalSize() ? "true" : "false") + "\n"
        "isLargeSize: " + (isLargeSize() ? "true" : "false") + "\n"
        "isXLargeSize: " + (isXLargeSize() ? "true" : "false") + "\n"
        "isLDPI: " + (isLDPI() ? "true" : "false") + "\n"
        "isMDPI: " + (isMDPI() ? "true" : "false") + "\n"
        "isHDPI: " + (isHDPI() ? "true" : "false") + "\n"
        "isXHDPI: " + (isXHDPI() ? "true" : "false") + "\n"
        "isXXHDPI: " + (isXXHDPI() ? "true" : "false") + "\n"
        "isXXXHDPI: " + (isXXXHDPI() ? "true" : "false") + "\n"
        "Ratio: " + QString::number(d->ratio) + "\n"
        "RatioFont: " + QString::number(d->ratioFont));
}
