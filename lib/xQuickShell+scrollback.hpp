#include "xQuickShell+selection.hpp"

#include <list>

#include <QtCore/qglobal.h>
#include <QtCore/QPoint>

class xQuickShellScreenData;
class xQuickShellBlock;

struct xQuickShellPage {
    int page_no;
    int size;
    std::list<xQuickShellBlock *>::iterator it;
};

class xQuickShellScrollback
{
public:
    xQuickShellScrollback(size_t max_size, xQuickShellScreenData *screen_data);

    void addxQuickShellBlock(xQuickShellBlock *block);
    xQuickShellBlock *reclaimxQuickShellBlock();
    void ensureVisiblexQuickShellPages(int top_line);

    size_t height() const;

    void setWidth(int width);

    size_t blockCount() { return m_block_count; }

    QString selection(const QPoint &start, const QPoint &end) const;
    const xQuickShellSelectionRange getDoubleClickxQuickShellSelectionRange(size_t character, size_t line);
private:
    void ensurexQuickShellPageVisible(xQuickShellPage &page, int new_height);
    void ensurexQuickShellPageNotVisible(xQuickShellPage &page);
    std::list<xQuickShellBlock *>::iterator findIteratorForxQuickShellPage(int page_no);
    std::list<xQuickShellBlock *>::iterator findIteratorForLine(size_t line);
    void adjustVisiblexQuickShellPages();
    xQuickShellScreenData *m_screen_data;

    std::list<xQuickShellBlock *> m_blocks;
    std::list<xQuickShellPage> m_visible_pages;
    size_t m_height;
    size_t m_width;
    size_t m_block_count;
    size_t m_max_size;
    size_t m_adjust_visible_pages;
};
