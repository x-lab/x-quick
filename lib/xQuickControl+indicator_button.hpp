// xQuickControl+indicator_button.hpp

#pragma once

#include <QtCore>
#include <QtCore/private/qobject_p.h>
#include <QtQuickTemplates2/private/qquickdeferredpointer_p_p.h>

class QQuickItem;

class xQuickControlIndicatorButtonPrivate;

class xQuickControlIndicatorButton : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool pressed READ isPressed WRITE setPressed NOTIFY pressedChanged FINAL)
    Q_PROPERTY(QQuickItem *indicator READ indicator WRITE setIndicator NOTIFY indicatorChanged FINAL)
    Q_PROPERTY(bool hovered READ isHovered WRITE setHovered NOTIFY hoveredChanged FINAL)
    Q_PROPERTY(qreal implicitIndicatorWidth READ implicitIndicatorWidth NOTIFY implicitIndicatorWidthChanged FINAL)
    Q_PROPERTY(qreal implicitIndicatorHeight READ implicitIndicatorHeight NOTIFY implicitIndicatorHeightChanged FINAL)
    Q_CLASSINFO("DeferredPropertyNames", "indicator")

public:
    explicit xQuickControlIndicatorButton(QObject *parent);

    bool isPressed(void) const;
    void setPressed(bool pressed);

    QQuickItem *indicator(void) const;
    void setIndicator(QQuickItem *indicator);

    bool isHovered(void) const;
    void setHovered(bool hovered);

    qreal implicitIndicatorWidth(void) const;
    qreal implicitIndicatorHeight(void) const;

Q_SIGNALS:
    void pressedChanged(void);
    void indicatorChanged(void);

    void hoveredChanged(void);

	void implicitIndicatorWidthChanged(void);
	void implicitIndicatorHeightChanged(void);

private:
    Q_DISABLE_COPY(xQuickControlIndicatorButton)
    Q_DECLARE_PRIVATE(xQuickControlIndicatorButton)
};

// /////////////////////////////////////////////////////////////////////////////

class xQuickControlIndicatorButtonPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(xQuickControlIndicatorButton)

public:
    static xQuickControlIndicatorButtonPrivate *get(xQuickControlIndicatorButton *button)
    {
        return button->d_func();
    }

    void cancelIndicator(void);
    void executeIndicator(bool complete = false);

    bool pressed = false;
    bool hovered = false;
    QQuickDeferredPointer<QQuickItem> indicator;
};

// xQuickControl+indicator_button.hpp ends here
