#include "xQuick+profiler.hpp"

xQuickProfiler::xQuickProfiler(QObject *parent) : QObject(parent),
    m_flushFile("/tmp/f"),
    m_socketFile("/tmp/1")
{
    quint64 features = 0;
    features = features | (static_cast<quint64>(1) << ProfileJavaScript);
	features = features | (static_cast<quint64>(1) << ProfileMemory);
    features = features | (static_cast<quint64>(1) << ProfilePixmapCache);
    features = features | (static_cast<quint64>(1) << ProfileSceneGraph);
    features = features | (static_cast<quint64>(1) << ProfileAnimations);
    features = features | (static_cast<quint64>(1) << ProfilePainting);
    features = features | (static_cast<quint64>(1) << ProfileCompiling);
    features = features | (static_cast<quint64>(1) << ProfileCreating);
    features = features | (static_cast<quint64>(1) << ProfileBinding);
    features = features | (static_cast<quint64>(1) << ProfileHandlingSignal);
    features = features | (static_cast<quint64>(1) << ProfileInputEvents);
    features = features | (static_cast<quint64>(1) << ProfileDebugMessages);

    m_connection.reset(new QQmlDebugConnection);

    m_profilerData.reset(new QmlProfilerData);

    m_qmlProfilerClient.reset(new QmlProfilerClient(m_connection.data(), m_profilerData.data()));
    m_qmlProfilerClient->setRequestedFeatures(features);

    m_connectTimer.setInterval(1000);
      m_flushTimer.setInterval(1000);

    connect(&m_connectTimer, &QTimer::timeout, this, &xQuickProfiler::tryConnect);
    connect(&m_flushTimer,   &QTimer::timeout, this, &xQuickProfiler::flush);

    connect(m_connection.data(), &QQmlDebugConnection::connected,    this, &xQuickProfiler::connected);
    connect(m_connection.data(), &QQmlDebugConnection::disconnected, this, &xQuickProfiler::disconnected);

    connect(m_profilerData.data(), &QmlProfilerData::error, this, &xQuickProfiler::logError);
    connect(m_profilerData.data(), &QmlProfilerData::dataReady, this, &xQuickProfiler::flush);
}

xQuickProfiler::~xQuickProfiler(void)
{

}

void xQuickProfiler::initialise(void)
{
    if (m_socketFile.isEmpty())
        return;

    logStatus(QString("Listening on %1 ...").arg(m_socketFile));

    m_connection->startLocalServer(m_socketFile);
    m_connectTimer.start();
}

void xQuickProfiler::start(void)
{
    m_running = true;

    emit runningChanged();

    m_qmlProfilerClient->setRecording(true);
}

void xQuickProfiler::stop(void)
{
    m_running = false;

    emit runningChanged();

    m_qmlProfilerClient->setRecording(false);
}

void xQuickProfiler::clear(void)
{
    m_qmlProfilerClient->clearAll();
}

void xQuickProfiler::flush(void)
{
    if (m_profilerData->save(m_flushFile)) {
        m_profilerData->clear();
        if (!m_flushFile.isEmpty())
            logStatus(QString("Data written to %1.").arg(m_flushFile));
    } else {
        logStatus(tr("Saving failed."));
    }

    m_qmlProfilerClient->clearAll();

    emit ready();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void xQuickProfiler::tryConnect(void)
{
    Q_ASSERT(!m_connection->isConnected());

    ++m_connectionAttempts;

    if (!m_verbose && !(m_connectionAttempts % 5)) {
        if (m_verbose) {
            logError(QString::fromLatin1("No connection received on %1 for %2 seconds ...").arg(m_socketFile).arg(m_connectionAttempts));
        }
    }
}

void xQuickProfiler::connected(void)
{
    m_connectTimer.stop();

    logStatus(QString("Connected to %1.").arg(m_socketFile));
}

void xQuickProfiler::disconnected(void)
{
    m_flushTimer.stop();

    m_qmlProfilerClient->clearAll();

    logStatus(QString("Disconnected from %1.").arg(m_socketFile));
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void xQuickProfiler::logError(const QString &error)
{
    qWarning() << error;
}

void xQuickProfiler::logStatus(const QString &status)
{
    if (!m_verbose)
        return;

    qDebug() << status;
}
