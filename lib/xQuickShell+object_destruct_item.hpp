#pragma once

#include <QtQuick/QQuickItem>
#include <QtCore/QObject>

class ObjectDestructItem : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QObject *objectHandle READ objectHandle WRITE setObjectHandle NOTIFY objectHandleChanged)

public:
    ObjectDestructItem(QQuickItem *parent = 0);

    QObject *objectHandle() const;
    void setObjectHandle(QObject *line);

signals:
    void objectHandleChanged();

private slots:
    void objectDestroyed();

private:
    QObject *m_object;
};
