#include "xQuickShell+terminal_screen.hpp"

xQuickShellTerminalScreen::xQuickShellTerminalScreen(QQuickItem *parent)
    : QQuickItem(parent)
    , m_screen(new xQuickShellScreen(this))
{
    setFlag(QQuickItem::ItemAcceptsInputMethod);
    connect(m_screen, &xQuickShellScreen::hangup, this, &xQuickShellTerminalScreen::hangupReceived);
}

xQuickShellTerminalScreen::~xQuickShellTerminalScreen()
{
    delete m_screen;
}

xQuickShellScreen *xQuickShellTerminalScreen::screen() const
{
    return m_screen;
}

QVariant xQuickShellTerminalScreen::inputMethodQuery(Qt::InputMethodQuery query) const
{
    switch (query) {
    case Qt::ImEnabled:
        return QVariant(true);
    case Qt::ImHints:
        return QVariant(Qt::ImhNoAutoUppercase | Qt::ImhNoPredictiveText);
    default:
        return QVariant();
    }
}

void xQuickShellTerminalScreen::inputMethodEvent(QInputMethodEvent *event)
{
    QString commitString = event->commitString();
    if (commitString.isEmpty()) {
        return;
    }

    Qt::Key key = Qt::Key_unknown;
    if (commitString == " ") {
        key = Qt::Key_Space; // screen requires
    }

    m_screen->sendKey(commitString, key, 0);
}

void xQuickShellTerminalScreen::hangupReceived()
{
    emit aboutToBeDestroyed(this);
    deleteLater();
}
