#pragma once

#include <QtGui>

class xQuickSourceHighliter : public QSyntaxHighlighter
{
public:
    enum Themes {
        Monokai = 1
    };

    explicit xQuickSourceHighliter(QTextDocument *doc);

    xQuickSourceHighliter(QTextDocument *doc, Themes theme);

    enum Language {
        CodeCpp = 200,
        CodeCppComment = 201,
        CodeJs = 202,
        CodeJsComment = 203,
        CodeC = 204,
        CodeCComment = 205,
        CodeBash = 206,
        CodePHP = 208,
        CodePHPComment = 209,
        CodeQML = 210,
        CodeQMLComment = 211,
        CodePython = 212,
        CodeRust = 214,
        CodeRustComment = 215,
        CodeJava = 216,
        CodeJavaComment = 217,
        CodeCSharp = 218,
        CodeCSharpComment = 219,
        CodeGo = 220,
        CodeGoComment = 221,
        CodeV = 222,
        CodeVComment = 223,
        CodeSQL = 224,
        CodeJSON = 226,
        CodeXML = 228,
        CodeCSS = 230,
        CodeCSSComment = 231,
        CodeTypeScript = 232,
        CodeTypeScriptComment = 233,
        CodeYAML = 234,
        CodeINI = 236,
        CodeVex = 238,
        CodeVexComment = 239,
        CodeCMake = 240,
        CodeMake = 242,
        CodeAsm = 244,
        CodeLua = 246,
        CodeLuaComment = 247
    };
    Q_ENUM(Language)

    enum Token {
        CodeBlock,
        CodeKeyWord,
        CodeString,
        CodeComment,
        CodeType,
        CodeOther,
        CodeNumLiteral,
        CodeBuiltIn,
    };
    Q_ENUM(Token)

    void setCurrentLanguage(Language language);
    Q_REQUIRED_RESULT Language currentLanguage();
    void setTheme(Themes theme);

protected:
    void highlightBlock(const QString &text) override;

private:
    void highlightSyntax(const QString &text);
    Q_REQUIRED_RESULT int highlightNumericLiterals(const QString &text, int i);
    Q_REQUIRED_RESULT int highlightStringLiterals(const QChar strType, const QString &text, int i);

    Q_REQUIRED_RESULT static constexpr inline bool isOctal(const char c) {
        return (c >= '0' && c <= '7');
    }

    Q_REQUIRED_RESULT static constexpr inline bool isHex(const char c) {
        return (
            (c >= '0' && c <= '9') ||
            (c >= 'a' && c <= 'f') ||
            (c >= 'A' && c <= 'F')
        );
    }

    void cssHighlighter(const QString &text);
    void ymlHighlighter(const QString &text);
    void xmlHighlighter(const QString &text);
    void makeHighlighter(const QString &text);
    void highlightInlineAsmLabels(const QString& text);
    void asmHighlighter(const QString& text);
    void initFormats();

    QHash<Token, QTextCharFormat> _formats;
    Language _language;
};
