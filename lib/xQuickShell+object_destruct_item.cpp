#include "xQuickShell+object_destruct_item.hpp"

ObjectDestructItem::ObjectDestructItem(QQuickItem *parent)
    : QQuickItem(parent)
    , m_object(0)
{
}

QObject *ObjectDestructItem::objectHandle() const
{
    return m_object;
}

void ObjectDestructItem::setObjectHandle(QObject *object)
{
    bool emit_changed = m_object != object;
    if (m_object) {
        m_object->disconnect(this);
    }

    m_object = object;
    connect(m_object, SIGNAL(destroyed()), this, SLOT(objectDestroyed()));

    if (emit_changed)
        emit objectHandleChanged();
}

void ObjectDestructItem::objectDestroyed()
{
    delete this;
}

