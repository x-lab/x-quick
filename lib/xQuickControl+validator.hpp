// xQuickControl+validator.hpp

#pragma once

#include <QtGui>
#include <QtQml/qqml.h>

class xQuickControlValidatorInteger : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(QVariant bottom READ bottom WRITE setBottom NOTIFY bottomChanged FINAL)
    Q_PROPERTY(QVariant top READ top WRITE setTop NOTIFY topChanged FINAL)

public:
    explicit xQuickControlValidatorInteger(QObject * parent = nullptr);
    xQuickControlValidatorInteger(qlonglong bottom, qlonglong top, QObject *parent = nullptr);
    xQuickControlValidatorInteger(QVariant bottom, QVariant top, QObject *parent = nullptr);
    ~xQuickControlValidatorInteger(void) = default;

    void setBottom(QVariant);
    QVariant bottom(void) const;

    void setTop(QVariant);
    QVariant top(void) const;

    void setRange(qlonglong bottom, qlonglong top);

    QValidator::State validate(QString& input, int&) const;

Q_SIGNALS:
    void bottomChanged(qlonglong bottom);
    void topChanged(qlonglong top);

public:
    qlonglong bot_impl(void) const;
    qlonglong top_impl(void) const;

private:
    Q_DISABLE_COPY(xQuickControlValidatorInteger);

    qlonglong b = std::numeric_limits<qlonglong>::lowest();
    qlonglong t = std::numeric_limits<qlonglong>::max();
};

// /////////////////////////////////////////////////////////////////////////////

inline QVariant xQuickControlValidatorInteger::bottom(void) const
{
    return QVariant::fromValue(b);
}

inline QVariant xQuickControlValidatorInteger::top(void) const
{
    return QVariant::fromValue(t);
}

inline qlonglong xQuickControlValidatorInteger::bot_impl(void) const
{
    return b;
}

inline qlonglong xQuickControlValidatorInteger::top_impl(void) const
{
    return t;
}

// /////////////////////////////////////////////////////////////////////////////

QML_DECLARE_TYPE(xQuickControlValidatorInteger)

// xQuickControl+validator.hpp ends here
