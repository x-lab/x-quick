#pragma once

#include <QtCore/QString>
#include <QtGui/QColor>
#include <QtCore/QObject>
#include <QtCore/QSize>

#include "xQuickShell+text_style.hpp"

class xQuickShellScreen;
class QQuickItem;

class xQuickShellText : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int index READ index NOTIFY indexChanged)
    Q_PROPERTY(int line READ line NOTIFY lineChanged)
    Q_PROPERTY(bool visible READ visible NOTIFY visibleChanged)
    Q_PROPERTY(QString text READ text NOTIFY textChanged)
    Q_PROPERTY(QColor foregroundColor READ foregroundColor NOTIFY foregroundColorChanged)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor NOTIFY backgroundColorChanged)
    Q_PROPERTY(bool bold READ bold NOTIFY boldChanged)
    Q_PROPERTY(bool blinking READ blinking NOTIFY blinkingChanged)
    Q_PROPERTY(bool underline READ underline NOTIFY underlineChanged)
    Q_PROPERTY(bool latin READ latin NOTIFY latinChanged)
public:
    xQuickShellText(xQuickShellScreen *screen);
    ~xQuickShellText();

    int index() const;

    int line() const;
    void setLine(int line, int width, const QString *textLine);

    bool visible() const;
    void setVisible(bool visible);

    QString text() const;
    QColor foregroundColor() const;
    QColor backgroundColor() const;

    void setStringSegment(int start_index, int end_index, bool textChanged);
    void setxQuickShellTextStyle(const xQuickShellTextStyle &style);

    bool bold() const;
    bool blinking() const;
    bool underline() const;

    void setLatin(bool latin);
    bool latin() const;

    QObject *item() const;

public slots:
    void dispatchEvents();

signals:
    void indexChanged();
    void lineChanged();
    void visibleChanged();
    void textChanged();
    void foregroundColorChanged();
    void backgroundColorChanged();
    void boldChanged();
    void blinkingChanged();
    void underlineChanged();
    void latinChanged();

private slots:
    void paletteChanged();

private:
    void setBackgroundColor();
    void setForegroundColor();

    xQuickShellScreen *m_screen;
    QString m_text;
    const QString *m_text_line;
    int m_start_index;
    int m_old_start_index;
    int m_end_index;
    int m_line;
    int m_old_line;
    int m_width;

    xQuickShellTextStyle m_style;
    xQuickShellTextStyle m_new_style;

    bool m_style_dirty;
    bool m_text_dirty;
    bool m_visible;
    bool m_visible_old;
    bool m_latin;
    bool m_latin_old;

    QColor m_foregroundColor;
    QColor m_backgroundColor;
};
