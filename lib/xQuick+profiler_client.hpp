#pragma once

#include <private/qqmlprofilerclient_p.h>
#include <private/qqmlprofilerclientdefinitions_p.h>
#include <private/qqmlprofilereventlocation_p.h>

class QmlProfilerData;
class QmlProfilerClientPrivate;
class QmlProfilerClient : public QQmlProfilerClient
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QmlProfilerClient)

public:
    QmlProfilerClient(QQmlDebugConnection *connection, QmlProfilerData *data);

signals:
    void enabledChanged(bool enabled);
    void error(const QString &error);

private:
    void onStateChanged(State state);
};
