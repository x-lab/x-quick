#pragma once

#include <QtCore>
#include <QtQuick>
#include <QtGui>

#include "xQuickShell+screen.hpp"

class xQuickShellTerminalScreen : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(xQuickShellScreen *screen READ screen CONSTANT)
public:
    xQuickShellTerminalScreen(QQuickItem *parent = 0);
    ~xQuickShellTerminalScreen();

    xQuickShellScreen *screen() const;

    QVariant inputMethodQuery(Qt::InputMethodQuery query) const;

public slots:
    void hangupReceived();
signals:
    void aboutToBeDestroyed(xQuickShellTerminalScreen *screen);

protected:
    void inputMethodEvent(QInputMethodEvent *event);

private:
    xQuickShellScreen *m_screen;
};
