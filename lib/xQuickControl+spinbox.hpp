// xQuickControl+spinbox.hpp

#pragma once

#include <QtCore>

#include <QtQuickTemplates2/private/qquickcontrol_p.h>
#include <QtQml/qjsvalue.h>

class QValidator;
class xQuickControlIndicatorButton;

class xQuickControlAbstractSpinBoxPrivate;

class xQuickControlAbstractSpinBox : public QQuickControl
{
    Q_OBJECT
    Q_PROPERTY(bool editable READ isEditable WRITE setEditable NOTIFY editableChanged FINAL)
    Q_PROPERTY(xQuickControlIndicatorButton *up READ up CONSTANT FINAL)
    Q_PROPERTY(xQuickControlIndicatorButton *down READ down CONSTANT FINAL)
    Q_PROPERTY(Qt::InputMethodHints inputMethodHints READ inputMethodHints CONSTANT FINAL)
    Q_PROPERTY(bool inputMethodComposing READ isInputMethodComposing NOTIFY inputMethodComposingChanged FINAL)
    Q_PROPERTY(QString displayText READ displayText NOTIFY displayTextChanged FINAL)
    Q_PROPERTY(GaugeControl gaugeControl READ gaugeControl WRITE setGaugeControl NOTIFY gaugeControlChanged FINAL)

public:
    explicit xQuickControlAbstractSpinBox(QQuickItem *parent = nullptr, xQuickControlAbstractSpinBoxPrivate *d_ptr = nullptr);
    ~xQuickControlAbstractSpinBox(void);

    enum GaugeControl {
        ControlLinear,
        ControlPolynomial
    };
    Q_ENUM(GaugeControl)

    bool isEditable(void) const;
    void setEditable(bool editable);

    xQuickControlIndicatorButton *up(void) const;
    xQuickControlIndicatorButton *down(void) const;

    Qt::InputMethodHints inputMethodHints(void) const;

    bool isInputMethodComposing(void) const;

    QString displayText(void) const;

    GaugeControl gaugeControl(void) const;
    void setGaugeControl(GaugeControl gaugeControl);

public Q_SLOTS:
    void increase(void);
    void decrease(void);

Q_SIGNALS:
    void editableChanged(void);
    void valueModified(void);
    void displayTextChanged(void);

    void inputMethodComposingChanged(void);

    void gaugeControlChanged(GaugeControl gaugeControl);

protected:
    void focusInEvent(QFocusEvent *event) override;
    void hoverEnterEvent(QHoverEvent *event) override;
    void hoverMoveEvent(QHoverEvent *event) override;
    void hoverLeaveEvent(QHoverEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void timerEvent(QTimerEvent *event) override;

    void classBegin(void) override;
    void componentComplete(void) override;
    void itemChange(ItemChange change, const ItemChangeData &value) override;
    void contentItemChange(QQuickItem *newItem, QQuickItem *oldItem) override;
    void localeChange(const QLocale &newLocale, const QLocale &oldLocale) override;

    QFont defaultFont(void) const override;

#if QT_CONFIG(accessibility)
    QAccessible::Role accessibleRole() const override;
    void accessibilityActiveChanged(bool active) override;
#endif

protected:
    Q_DISABLE_COPY(xQuickControlAbstractSpinBox)
    Q_DECLARE_PRIVATE(xQuickControlAbstractSpinBox)
};

// /////////////////////////////////////////////////////////////////////////////

class xQuickControlSpinBoxIntegerPrivate;

class xQuickControlSpinBoxInteger : public xQuickControlAbstractSpinBox
{
    Q_OBJECT
    Q_PROPERTY(QVariant from READ from WRITE setFrom NOTIFY fromChanged FINAL)
    Q_PROPERTY(QVariant to READ to WRITE setTo NOTIFY toChanged FINAL)
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged FINAL)
    Q_PROPERTY(int increment READ increment WRITE setIncrement NOTIFY incrementChanged FINAL)
    Q_PROPERTY(QValidator *validator READ validator CONSTANT FINAL)

public:
    explicit xQuickControlSpinBoxInteger(QQuickItem *parent = nullptr);
    ~xQuickControlSpinBoxInteger(void);

    QVariant from(void) const;
    void setFrom(QVariant from);

    QVariant to(void) const;
    void setTo(QVariant to);

    QVariant value(void) const;
    void setValue(QVariant value);

    int increment(void) const;
    void setIncrement(int increment);

    QValidator *validator(void) const;

Q_SIGNALS:
    void fromChanged(void);
    void toChanged(void);
    void valueChanged(void);
    void incrementChanged(void);

public:
    Q_INVOKABLE double scaleValueFromRatio(double ratio) const;
    Q_INVOKABLE double scaleValue(void) const;

private:
    Q_DISABLE_COPY(xQuickControlSpinBoxInteger)
    Q_DECLARE_PRIVATE(xQuickControlSpinBoxInteger)
};

// /////////////////////////////////////////////////////////////////////////////

class xQuickControlSpinBoxRealPrivate;

class xQuickControlSpinBoxReal : public xQuickControlAbstractSpinBox
{
    Q_OBJECT
    Q_PROPERTY(double from READ from WRITE setFrom NOTIFY fromChanged FINAL)
    Q_PROPERTY(double to READ to WRITE setTo NOTIFY toChanged FINAL)
    Q_PROPERTY(int decimals READ decimals WRITE setDecimals NOTIFY decimalsChanged FINAL)
    Q_PROPERTY(double value READ value WRITE setValue NOTIFY valueChanged FINAL)
    Q_PROPERTY(double increment READ increment WRITE setIncrement NOTIFY incrementChanged FINAL)
    Q_PROPERTY(QValidator *validator READ validator CONSTANT FINAL)
    Q_PROPERTY(Notation notation READ notation WRITE setNotation NOTIFY notationChanged FINAL)

public:
    explicit xQuickControlSpinBoxReal(QQuickItem *parent = nullptr);
    ~xQuickControlSpinBoxReal(void);

    enum Notation {
        StandardNotation,
        ScientificNotation
    };
    Q_ENUM(Notation)

    double from(void) const;
    void setFrom(double from);

    double to(void) const;
    void setTo(double to);

    double value(void) const;
    void setValue(double value);

    double increment(void) const;
    void setIncrement(double increment);

    int decimals(void) const;
    void setDecimals(int decimals);

    QValidator *validator(void) const;

    void setNotation(Notation);
    Notation notation(void) const;

Q_SIGNALS:
    void fromChanged(void);
    void toChanged(void);
    void decimalsChanged(int decimals);
    void valueChanged(void);
    void incrementChanged(void);
    void notationChanged(xQuickControlSpinBoxReal::Notation notation);

public:
    Q_INVOKABLE double scaleValueFromRatio(double ratio) const;
    Q_INVOKABLE double scaleValue(void) const;

private:
    Q_DISABLE_COPY(xQuickControlSpinBoxReal)
    Q_DECLARE_PRIVATE(xQuickControlSpinBoxReal)
};

// /////////////////////////////////////////////////////////////////////////////

QML_DECLARE_TYPE(xQuickControlSpinBoxInteger)
QML_DECLARE_TYPE(xQuickControlSpinBoxReal)

// xQuickControl+spinbox.hpp ends here
