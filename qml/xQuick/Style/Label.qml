import QtQuick           2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Label {

    id: control

    font: X.Style.typography.body1;

    color: enabled ? X.Style.textColor : X.Style.textColorDisabled;
    linkColor: X.Style.linkColor;
}
