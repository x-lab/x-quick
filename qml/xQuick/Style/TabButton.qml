import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import Qt5Compat.GraphicalEffects

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.TabButton
{
  id: _control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    implicitContentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    implicitContentHeight + topPadding + bottomPadding)

  leftPadding: ((icon.source != "" && !mirrored) ? 6 :
    8) + leftInset
  rightPadding: ((icon.source != "" && mirrored) ? 6 :
    8) + rightInset

  topInset: 0
  bottomInset: 0
  leftInset: 0
  rightInset: 0

  topPadding: 6
  bottomPadding: 6
  spacing: 12

  font: X.Style.typography.button

  icon.width: 18
  icon.height: 18
  icon.color: foregroundColor

  property color foregroundColor: !enabled ? X.Style.textColorDisabled : X.Style.accentColor;
  property color backgroundColor: X.Style.backgroundColor

  display: AbstractButton.TextBesideIcon

  contentItem: X.LabelIcon
  {
    id: _iconLabel
    font: _control.font
    spacing: _control.spacing
    iconSource: _control.icon.source
    iconSize: _control.icon.width
    iconColor: _control.icon.color
    text: _control.text
    color: _control.foregroundColor
  }

  background: Rectangle
  {
    implicitWidth: 70
    implicitHeight: 30

    clip: true
    color: _control.backgroundColor
  }
}
