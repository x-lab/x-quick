import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.DelayButton {
    id: control;

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding,
                             implicitIndicatorHeight + topPadding + bottomPadding)

    contentItem: Text {
        text: control.text
        font: control.font
        opacity: enabled ? 1.0 : 0.3
        color: X.Style.foregroundColor;
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40

        opacity: enabled ? 1 : 0.3

        color: control.down ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor;
        border.width: 1
        border.color: X.Style.borderColor;
        radius: X.Style.controls.radius;

        anchors.centerIn: parent

        clip: true;

        Rectangle {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.bottom: parent.bottom;
            anchors.margins: 1;

            width: control.progress * parent.width - 2;
            radius: X.Style.controls.radius - 1;

            border.width: 0;
            border.color: "#00000000";

            color: X.Style.backgroundColor;
        }

        // Canvas {
        //     id: canvas
        //     anchors.fill: parent
        //
        //     Connections {
        //         target: control
        //         function onProgressChanged() { canvas.requestPaint(); }
        //     }
        //
        //     onPaint: {
        //         var ctx = getContext("2d")
        //         ctx.clearRect(0, 0, width, height)
        //         ctx.strokeStyle = "white"
        //         ctx.lineWidth = parent.size / 20
        //         ctx.beginPath()
        //         var startAngle = Math.PI / 5 * 3
        //         var endAngle = startAngle + control.progress * Math.PI / 5 * 9
        //         ctx.arc(width / 2, height / 2, width / 2 - ctx.lineWidth / 2 - 2, startAngle, endAngle)
        //         ctx.stroke()
        //     }
        // }
    }
}
