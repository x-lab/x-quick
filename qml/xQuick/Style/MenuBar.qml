import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.MenuBar
{
    id: _control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
        contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
        contentHeight + topPadding + bottomPadding)

    delegate: X.MenuBarItem {}

    contentItem: Row
    {
        spacing: _control.spacing
        Repeater
        {
            model: _control.contentModel
        }
    }

    background: Rectangle
    {
        implicitHeight: X.Style.menu.implicitHeight
        color: X.Style.backgroundColor
    }
}
