import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.SwipeDelegate {

    id: control

    implicitWidth: Math.max(background ? implicitBackgroundWidth : 0,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? implicitBackgroundHeight : 0,
                             Math.max(implicitContentHeight,
                                      indicator ? indicator.implicitHeight : 0) + topPadding + bottomPadding) + bottomInset

    signal clicked;

    leftPadding: 10;
    rightPadding: 10;

    Component {
        id: component

        Rectangle {
            color: pressed ? Qt.darker(X.Style.backgroundColor, 2) : Qt.darker(X.Style.backgroundColor)
            width: parent.width
            height: parent.height
            clip: true

            Label {
                text: qsTr("Press me!")
                color: X.Style.alertColor;
                anchors.centerIn: parent
            }

            T.SwipeDelegate.onClicked: control.clicked();
        }
    }

    swipe.left: component
    swipe.right: component

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 0.8 : 0.3
        color: X.Style.backgroundColor

        Rectangle {
            width: parent.width
            height: 1
            color: X.Style.borderColor;
            anchors.bottom: parent.bottom
        }
    }

    contentItem: Text {
        text: control.text
        font: control.font
        color: control.enabled ? X.Style.foregroundColor : "#bdbebf"
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter

        Behavior on x {
            enabled: !control.down
            NumberAnimation {
                easing.type: Easing.InOutCubic
                duration: 400
            }
        }
    }
}
