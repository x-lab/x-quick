import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.ToolTip
{
    id: control

    property double backgroundRadius: X.Style.controls.radius

    property int position: X.Style.Position.Top

    property color backgroundColor: X.Style.increased(X.Style.backgroundColor, 1.2);

    x:
    {
        if(!parent) return 0;
        switch(position)
        {
        case X.Style.Position.BottomLeft:
        case X.Style.Position.TopLeft:
            return -width - margins / 2

        case X.Style.Position.BottomStart:
        case X.Style.Position.TopStart:
            return 0

        case X.Style.Position.Bottom:
        case X.Style.Position.Center:
        case X.Style.Position.Top:
            return (parent.width - width) / 2

        case X.Style.Position.BottomEnd:
        case X.Style.Position.TopEnd:
            return parent.width - width

        case X.Style.Position.BottomRight:
        case X.Style.Position.TopRight:
            return parent.width + margins / 2

        case X.Style.Position.RightStart:
        case X.Style.Position.Right:
        case X.Style.Position.RightEnd:
            return parent.width + margins / 2

        case X.Style.Position.LeftEnd:
        case X.Style.Position.Left:
        case X.Style.Position.LeftStart:
            return -width - margins / 2
        }
        X.Logger.warn(`Unknown Position : ${position}`)
        return 0;
    }

    y:
    {
        if(!parent) return 0;
        switch(position)
        {
        case X.Style.Position.TopLeft:
        case X.Style.Position.TopStart:
        case X.Style.Position.Top:
        case X.Style.Position.TopEnd:
        case X.Style.Position.TopRight:
            return -height - margins / 2

        case X.Style.Position.BottomLeft:
        case X.Style.Position.BottomStart:
        case X.Style.Position.Bottom:
        case X.Style.Position.BottomEnd:
        case X.Style.Position.BottomRight:
            return parent.height + margins / 2

        case X.Style.Position.RightStart:
        case X.Style.Position.LeftStart:
            return 0

        case X.Style.Position.Right:
        case X.Style.Position.Center:
        case X.Style.Position.Left:
            return (parent.height - height) / 2

        case X.Style.Position.RightEnd:
        case X.Style.Position.LeftEnd:
            return parent.height - height
        }
        X.Logger.warn(`Unknown Position : ${position}`)
        return -height - margins / 2;
    }

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
        implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
        implicitContentHeight + topPadding + bottomPadding)

    margins: 12
    horizontalPadding: 16
    verticalPadding: 0

    closePolicy: T.Popup.CloseOnEscape | T.Popup.CloseOnPressOutsideParent | T.Popup.CloseOnReleaseOutsideParent

    enter: Transition
    {
        NumberAnimation
        {
            property: "opacity";from: 0.0;to: 1.0;easing.type: Easing.OutQuad;duration: 100
        }
    }

    exit: Transition
    {
        NumberAnimation
        {
            property: "opacity";from: 1.0;to: 0.0;easing.type: Easing.InQuad;duration: 200
        }
    }

    contentItem: Label
    {
        text: control.text
        font: control.font
        color: X.Style.foregroundColor;
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle
    {
        implicitWidth: 200
        implicitHeight: 32
        color: control.backgroundColor;
        opacity: 0.9
        radius: X.Style.controls.radius;
    }
}
