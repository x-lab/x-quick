import QtQuick           2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Controls  2.15

import xQuick.Style      1.0 as X

T.MenuBarItem
{
    id: _control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
        implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
        implicitContentHeight + topPadding + bottomPadding)

    topInset: X.Style.menuBarItem.topInset
    bottomInset: X.Style.menuBarItem.bottomInset
    leftInset: X.Style.menuBarItem.leftInset
    rightInset: X.Style.menuBarItem.rightInset

    leftPadding: (icon.source != "" && !mirrored || text == "" ? X.Style.menuBarItem.iconPadding : X.Style.menuBarItem.padding) + leftInset
    rightPadding: (icon.source != "" && mirrored || text == "" ? X.Style.menuBarItem.iconPadding : X.Style.menuBarItem.padding) + rightInset
    topPadding: 0
    bottomPadding: 0
    spacing: X.Style.menuBarItem.spacing

    property bool outlined: false

    property color foregroundColor: X.Style.foregroundColor;
    property color backgroundColor: "transparent"

    property double radius: X.Style.menuBarItem.cornerRadius

    icon.width: X.Style.menuBarItem.iconWidth
    icon.height: X.Style.menuBarItem.iconWidth
    icon.color: foregroundColor

    property double backgroundImplicitWidth: X.Style.menuBarItem.minWidth
    property double backgroundImplicitHeight: X.Style.menuBarItem.minHeight

    focusPolicy: Qt.StrongFocus

    background: Rectangle
    {
        implicitWidth: _control.backgroundImplicitWidth
        implicitHeight: _control.backgroundImplicitHeight
        radius: _control.radius
        color: _control.backgroundColor
    }
}
