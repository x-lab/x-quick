import QtQuick           2.15
import QtQuick.Templates 2.15 as T

Rectangle {
    id: panel

    property Item control
    property bool highlighted: control.highlighted

    visible: !control.flat || control.down || control.checked

    color: "transparent";
    radius: 2
}
