import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.SwitchDelegate {
    id: control

    implicitWidth: Math.max(background ? implicitBackgroundWidth : 0,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? implicitBackgroundHeight : 0,
                             Math.max(implicitContentHeight,
                                      indicator ? indicator.implicitHeight : 0) + topPadding + bottomPadding) + bottomInset

    leftPadding: 10;
    rightPadding: 10;

    contentItem: Text {
        rightPadding: control.spacing
        text: control.text
        font: control.font
        color: X.Style.foregroundColor
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
    }

    indicator: Rectangle {
        implicitWidth: 48
        implicitHeight: 26

        x: control.width - width - control.rightPadding
        y: parent.height / 2 - height / 2

        radius: 13
        color: control.checked ? X.Style.foregroundColor : X.Style.backgroundColor
        border.color: control.checked ? X.Style.foregroundColor : X.Style.borderColor

        Rectangle {
            x: control.checked ? parent.width - width : 0
            width: 26
            height: 26
            radius: 13
            color: X.Style.backgroundColor
            border.color: X.Style.borderColor
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 0.8 : 0.3
        color: (control.down || control.highlighted || control.hovered) ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor
        radius: X.Style.controls.radius;
    }
}
