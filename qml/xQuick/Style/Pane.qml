import QtQuick           2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Pane {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding)

    font: X.Style.typography.body1

    background: Rectangle {

        color: X.Style.alternateBaseColor;
    }
}
