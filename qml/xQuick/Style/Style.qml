pragma Singleton

import QtQuick          2.15
import QtQuick.Controls 2.15

import Qt.labs.settings 1.0

Item {

    id: _style;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    property var flavor:
          _style.flavors == 'UBUNTU' ? flavor_ubuntu
        : _style.flavors == 'FEDORA' ? flavor_fedora
        : _style.flavors == 'MACOS'  ? flavor_macos
        : flavor_ubuntu; // <= Default fallback // NOTE: Use Qt.platform.os

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    property string flavors: "UBUNTU";
    property string variant: "DARK";

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    property var flavor_macos: QtObject {

        property color base00: "#252322"; // Black
        property color base01: "#383838"; //
        property color base02: "#555555"; //
        property color base03: "#777777"; //
        property color base04: "#999999"; //
        property color base05: "#bbbbbb"; //
        property color base06: "#dddddd"; //
        property color base07: "#ffffff"; // White
        property color base08: "#ff4136"; // Red
        property color base09: "#0074d9"; // Orange !
        property color base0A: "#ffdc00"; // Yellow
        property color base0B: "#2ecc40"; // Green
        property color base0C: "#7fdbff"; // Aqua
        property color base0D: "#E95420"; // Blue
        property color base0E: "#77216F"; // Purple
        property color base0F: "#85144b"; // Maroon

        property string fontFamily: "Roboto";
        property string monoFamily: "Source Code Pro";
    }


    property var flavor_ubuntu: QtObject {

        property color base00: "#252322"; // Black
        property color base01: "#383838"; //
        property color base02: "#555555"; //
        property color base03: "#777777"; //
        property color base04: "#999999"; //
        property color base05: "#aaaaaa"; //
        property color base06: "#cccccc"; //
        property color base07: "#eeeeee"; // White
        property color base08: "#ff4136"; // Red
        property color base09: "#E95420"; // Orange !
        property color base0A: "#ffdc00"; // Yellow
        property color base0B: "#2ecc40"; // Green
        property color base0C: "#7fdbff"; // Aqua
        property color base0D: "#0074d9"; // Blue
        property color base0E: "#77216F"; // Purple
        property color base0F: "#85144b"; // Maroon

        property string fontFamily: "Ubuntu";
        property string monoFamily: "Ubuntu Mono";
    }

    property var flavor_fedora: QtObject {

        property color base00: "#252322"; // Black
        property color base01: "#383838"; //
        property color base02: "#555555"; //
        property color base03: "#777777"; //
        property color base04: "#999999"; //
        property color base05: "#bbbbbb"; //
        property color base06: "#dddddd"; //
        property color base07: "#ffffff"; // White
        property color base08: "#ff4136"; // Red
        property color base09: "#3c6eb4"; //
        property color base0A: "#ffdc00"; // Yellow
        property color base0B: "#2ecc40"; // Green
        property color base0C: "#E95420"; // Aqua
        property color base0D: "#0074d9"; // Blue
        property color base0E: "#77216F"; // Purple
        property color base0F: "#85144b"; // Maroon

        property string fontFamily: "Open Sans";
        property string monoFamily: "Source Code Pro";
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    // base00 - Default Background
    // base01 - Lighter Background (Used for status bars, line number and folding marks)
    // base02 - Selection Background
    // base03 - Comments, Invisibles, Line Highlighting
    // base04 - Dark Foreground (Used for status bars)
    // base05 - Default Foreground, Caret, Delimiters, Operators
    // base06 - Light Foreground (Not often used)
    // base07 - Light Background (Not often used)
    // base08 - Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
    // base09 - Integers, Boolean, Constants, XML Attributes, Markup Link Url
    // base0A - Classes, Markup Bold, Search Text Background
    // base0B - Strings, Inherited Class, Markup Code, Diff Inserted
    // base0C - Support, Regular Expressions, Escape Characters, Markup Quotes
    // base0D - Functions, Methods, Attribute IDs, Headings
    // base0E - Keywords, Storage, Selector, Markup Italic, Diff Changed
    // base0F - Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>

    // palette.alternateBase: color   // Used as the alternate background color in item views with alternating row colors.
    // palette.base: color            // Used mostly as the background color for text editor controls and items views. It is usually white or another light color.
    // palette.brightText: color      // A text color that is very different from palette.windowText, and contrasts well with e.g. palette.dark. Typically used for text that needs to be drawn where palette.text, palette.windowText or palette.buttonText would give poor contrast, such as on highlighted buttons.
    // palette.button: color          // The general button background color. This background can be different from palette.window as some styles require a different background color for buttons.
    // palette.buttonText: color      // A foreground color used with the palette.button color.
    // palette.dark: color            // Darker than palette.button.
    // palette.highlight: color       // A color to indicate a selected item or the current item.
    // palette.highlightedText: color // A text color that contrasts with palette.highlight.
    // palette.light: color           // Lighter than palette.button.
    // palette.link: color            // A text color used for hyperlinks.
    // palette.linkVisited: color     // A text color used for already visited hyperlinks.
    // palette.mid: color             // Between palette.button and palette.dark.
    // palette.midlight: color        // Between palette.button and palette.light.
    // palette.shadow: color          // A very dark color.
    // palette.text: color            // The foreground color used with palette.base. This is usually the same as the palette.windowText, in which case it must provide good contrast with palette.window and palette.base.
    // palette.toolTipBase: color     // Used as the background color for tooltips.
    // palette.toolTipText: color     // Used as the foreground color for tooltips.
    // palette.window: color          // A general background color.
    // palette.windowText: color      // A general foreground color.

    property color alternateBaseColor: _style.flavor.base00;
    property color          baseColor: _style.flavor.base01;
    property color          tintColor: "#88000000";
    property color    foregroundColor: _style.flavor.base05;
    property color    backgroundColor: _style.flavor.base02;
    property color        borderColor;
    property color       overlayColor: "#bb000000";
    property color        accentColor: _style.flavor.base09;
    property color         alertColor: _style.flavor.base08;
    property color          textColor: _style.flavor.base05;
    property color          textColorDisabled: _style.flavor.base04;
    property color          linkColor: _style.flavor.base0E;
    property color          infoColor: _style.flavor.base0D;
    property color       warningColor: _style.flavor.base0C;
    property color         errorColor: _style.flavor.base0B;
    property color         debugColor: _style.flavor.base0A;
    property color     selectionColor: _style.flavor.base09;

    property string fontFamily: flavor.fontFamily;
    property string monoFamily: flavor.monoFamily;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    function increased(shade, factor)
    {
        return _style.variant == 'LIGHT'
            ? Qt.darker(shade, factor)
            : Qt.lighter(shade, factor);
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    states: [
        State {
            name: "DARK"
            when: _style.variant == 'DARK'
            PropertyChanges {
                target: _style;

    alternateBaseColor: _style.flavor.base00;
             baseColor: _style.flavor.base01;
             tintColor: "#88000000";
       foregroundColor: _style.flavor.base05;
       backgroundColor: _style.flavor.base02;
           borderColor: Qt.darker(_style.flavor.base00); // base03
          overlayColor: "#bb000000";
           accentColor: _style.flavor.base09;
            alertColor: _style.flavor.base08;
             textColor: _style.flavor.base05;
             textColorDisabled: _style.flavor.base04;
             linkColor: _style.flavor.base0E;
             infoColor: _style.flavor.base0D;
          warningColor: _style.flavor.base0C;
            errorColor: _style.flavor.base0B;
            debugColor: _style.flavor.base0A;
        selectionColor: _style.flavor.base0E;
            }
        },
        State {
            name: "LIGHT"
            when: _style.variant == 'LIGHT'
            PropertyChanges {
                target: _style;

    alternateBaseColor: _style.flavor.base07;
             baseColor: _style.flavor.base06;
             tintColor: "#88000000";
       foregroundColor: _style.flavor.base02;
       backgroundColor: _style.flavor.base05;
           borderColor: Qt.lighter(_style.flavor.base07); // base04
          overlayColor: "#bb000000";
           accentColor: _style.flavor.base09;
            alertColor: _style.flavor.base08;
             textColor: _style.flavor.base02;
             textColorDisabled: _style.flavor.base03;
             linkColor: _style.flavor.base0E;
             infoColor: _style.flavor.base0D;
          warningColor: _style.flavor.base0C;
            errorColor: _style.flavor.base0B;
            debugColor: _style.flavor.base0A;
        selectionColor: _style.flavor.base0E;
            }
        }
    ]

    transitions: [
        Transition {
            to: '*'
            ColorAnimation { target: _style; duration: 200 }
        }
    ]

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    property var card: QtObject {

        property double horizontalPadding: 16;
        property double verticalPadding: 8;
        property double radius: 4;
        property double spacing: 6;

        property double roundImageSpacing: 16;
        property double roundImageSize: 40;

        property double mediaImplicitWidth: 344;
        property double mediaImplicitHeight: 194;
    }

    property var controls: QtObject {
        property double radius: 4;
    }

    property var combo: QtObject {

        property double leftPadding: 16;

        property color backgroundColor: "#dd000000";
    }

    property var dialog: QtObject {

        property double spacing: 10
        property double padding: 10
        property double buttonRectImplicitHeight: 52
        property double radius: 4
        property double horizontalPadding: 24
        property double implicitWidth: 280
        property double maxHeight: 500
        property double indicatorSpacing: 24
    }

    property var menu: QtObject
    {
      property double verticalPadding: 8
      property double implicitHeight: 38
      property double implicitWidth: 200
      property double radius: 0
    }

    property var menuBarItem: QtObject
    {
        property double minWidth: 64
        property double minHeight: 32
        property double padding: 6
        property double iconPadding: 4
        property double spacing: 4
        property double iconWidth: 18
        property double cornerRadius: 4

        property double topInset: 4
        property double bottomInset: 4
        property double leftInset: 0
        property double rightInset: 0
    }

    property var menuItem: QtObject
    {
        property double minWidth: 200
        property double minHeight: 32
        property double padding: 12
        property double iconPadding: 12
        property double spacing: 16
        property double iconWidth: 18
        property double cornerRadius: 0
        property double shortcutImplicitWidth: 96

        property double topInset: 0
        property double bottomInset: 0
        property double leftInset: 2
        property double rightInset: 2
    }

    property var textField: QtObject
    {
        property double cursorWidth: 2
        property double implicitWidth: 96
        property double horizontalPadding: 0
        property double leadingSpacing: 12
        property double textSpacing: 2

        property double topPadding: 6
        property double topPaddingTitle: 18
        property double topPaddingTitleOffset: 2
        property double bottomPadding: 8
        property double bottomPaddingHint: 24
        property double bottomPaddingHintOffset: 2

        property double iconSize: 20
        property double iconWidth: 32

        property double backgroundBorderHeight: 2
        property double backgroundVerticalOffset: 6
  }

    enum Position
    {
        TopLeft,
        TopStart,
        Top,
        TopEnd,
        TopRight,
        RightStart,
        Right,
        RightEnd,
        BottomRight,
        BottomEnd,
        Bottom,
        BottomStart,
        BottomLeft,
        LeftEnd,
        Left,
        LeftStart,
        Center
    }

    enum TextType
    {
        Display3,
        Display2,
        Display1,
        Heading,
        Title,
        Subheading,
        ListText,
        ListSecText,
        Overline,
        Body2,
        Body1,
        Caption,
        Hint,
        Button,
        Menu,
        MenuHint
    }

    enum DragDirection
    {
        TOPTOBOTTOM,
        BOTTOMTOTOP
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    property var typography: QtObject {

    property font headline1: Qt.font({
        family: _style.fontFamily,
        pixelSize: 96,
        weight: Font.Thin,
        capitalization: Font.MixedCase,
        letterSpacing: -1.5
    })

    property font headline2: Qt.font({
        family: _style.fontFamily,
        pixelSize: 60,
        weight: Font.Thin,
        capitalization: Font.MixedCase,
        letterSpacing: -0.5
    })

    property font headline3: Qt.font({
        family: _style.fontFamily,
        pixelSize: 48,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0
    })

    property font headline4: Qt.font({
        family: _style.fontFamily,
        pixelSize: 34,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0.25
    })

    property font headline5: Qt.font({
        family: _style.fontFamily,
        pixelSize: 24,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0
    })

    property font headline6: Qt.font({
        family: _style.fontFamily,
        pixelSize: 20,
        weight: Font.Medium,
        capitalization: Font.MixedCase,
        letterSpacing: 0.15
    })

    property font subtitle1: Qt.font({
        family: _style.fontFamily,
        pixelSize: 16,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0.15
    })

    property font subtitle2: Qt.font({
        family: _style.fontFamily,
        pixelSize: 14,
        weight: Font.Medium,
        capitalization: Font.MixedCase,
        letterSpacing: 0.10
    })

    property font body1: Qt.font({
        family: _style.fontFamily,
        pixelSize: 12,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0.5
    })

    property font body2: Qt.font({
        family: _style.fontFamily,
        pixelSize: 11,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0.25
    })

    property font button: Qt.font({
        family: _style.fontFamily,
        pixelSize: 11,
        weight: Font.Medium,
        capitalization: Font.AllUpperCase,
        letterSpacing: 1.25
    })

    property font caption: Qt.font({
        family: _style.fontFamily,
        pixelSize: 12,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0.4
    })

    property font overline: Qt.font({
        family: _style.fontFamily,
        pixelSize: 12,
        weight: Font.Normal,
        capitalization: Font.AllUpperCase,
        letterSpacing: 1.5
    })

    property font hint1: Qt.font({
        family: _style.fontFamily,
        pixelSize: 11,
        weight: Font.Medium,
        capitalization: Font.MixedCase,
        letterSpacing: 0
    })

    property font hint2: Qt.font({
        family: _style.fontFamily,
        pixelSize: 11,
        weight: Font.Normal,
        capitalization: Font.MixedCase,
        letterSpacing: 0
    })

    }

    property var languages: QtObject {
        property int    cpp: 200;
        property int     js: 202;
        property int    qml: 210;
        property int python: 212;
        property int   json: 226;
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    Settings {

        id: _settings;

        property alias flavors: _style.flavors;
        property alias variant: _style.variant;
    }
}
