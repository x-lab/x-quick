import QtQuick           2.15
import QtQuick.Templates 2.15 as T

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.ToolBar {
    id: _control;

    property int situation: X.Style.Position.Top;

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding);
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding);

    background: Rectangle {

        implicitHeight: 26;

        color: X.Style.alternateBaseColor;

        X.Separator {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.right: parent.right;

            visible: _control.situation == X.Style.Position.Bottom;
        }

        X.Separator {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;

            visible: _control.situation == X.Style.Position.Top;
        }
    }
}
