import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.TextArea {

    id: control

    // implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    //                         implicitContentWidth + leftPadding + rightPadding)
    // implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    //                          implicitContentHeight + topPadding + bottomPadding)

    font: X.Style.typography.body1;
    color: X.Style.textColor;

    cursorDelegate: X.CursorDelegate {}

    topPadding: 2;
    leftPadding: 2;

    selectionColor: X.Style.foregroundColor;

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 40
        border.color: X.Style.borderColor;
        border.width: 1;
        color: X.Style.backgroundColor;
        radius: X.Style.controls.radius;
    }
}
