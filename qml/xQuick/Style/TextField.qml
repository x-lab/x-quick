import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

/**
 * CONTENT
 * - text
 * - title
 * - helperText
 * - placeholderText
 * - prefixText
 * - suffixText
 *
 * - leadingIconSource
 * - leadingSpacing
 * - leadingVisible
 *
 * - trailingContent
 * - tralingVisible
 * - tralingSpacing
 *
 * COLOR
 *
 * - color / textColor
 * - titleTextColor
 * - helperTextColor
 * - prefixTextColor
 * - suffixTextColor
 * - leadingIconColor
 * - trailingIconColor
 *
 * BEHAVIOR
 *
 * LOOK
 * - echoMode (TextInput.Normal | Password  | NoEcho | PasswordEchoOnEdit )
 * - leadingIconInline
 * - trailingIconInline
 *
 * DOC
 * https://doc.qt.io/qt-5/qml-qtquick-textinput.html
 * https://doc.qt.io/qt-5/qml-qtquick-controls2-textfield.html
 */

T.TextField
{
    id: _control

    // CONTENT
    property string title
    property string helperText
    property string prefixText
    property string suffixText
    property string leadingIconSource

    // LOOK
    property bool leadingIconInline: false
    property bool leadingIconVisible: leadingIconSource != ""
    property bool leadingIconErrorAnimation: false
    property bool trailingInline: true

    property bool titleUp: _control.activeFocus || _control.length || _control.preeditText
    readonly property bool anyHintVisible: (_control.helperText != "" || _control._errorText != "") || _lineCountLabel
        .visible

    font: X.Style.typography.subtitle1

    // SIZE
    implicitWidth: implicitBackgroundWidth + leftInset + rightInset ||
        Math.max(contentWidth, placeholder.implicitWidth) + _control.virtualLeftPadding + _control.virtualRightPadding
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding,
                             placeholder.implicitHeight + topPadding + bottomPadding)

    topPadding: _control.title != "" ? X.Style.textField.topPaddingTitle : X.Style.textField.topPadding
    bottomPadding: anyHintVisible ? X.Style.textField.bottomPaddingHint : X.Style.textField.bottomPadding

    property double virtualLeftPadding: ((leadingIconVisible) ? _leadingIcon.width + leadingSpacing : 0) + ((
        leadingIconVisible && !leadingIconInline) ? X.Style.textField.horizontalPadding : 0)

    property double virtualRightPadding: ((trailingVisible) ? _trailingContent.width + trailingSpacing : 0) + ((
        trailingVisible && !trailingInline) ? X.Style.textField.horizontalPadding : 0)

    leftPadding: virtualLeftPadding + (prefixText != "" ? _prefixLabel.contentWidth + textSpacing : 0)
    rightPadding: virtualRightPadding + (suffixText != "" ? _suffixLabel.contentWidth + textSpacing : 0)
    property double leadingSpacing: X.Style.textField.leadingSpacing
    property double textSpacing: X.Style.textField.textSpacing
    property double trailingSpacing: 0

    leftInset: (!leadingIconInline && leadingIconVisible) ? _leadingIcon.width + leadingSpacing : 0
    rightInset: (!trailingInline && trailingVisible) ? _trailingContent.width + trailingSpacing : 0

    Behavior on bottomPadding
    {
        NumberAnimation
        {
            easing.type: Easing.OutCubic
            duration: 200
        }
    }

    // COLORS
    property alias textColor: _control.color
    color: enabled ? X.Style.textColor : X.Style.textColorDisabled
    selectionColor: X.Style.accentColor
    selectedTextColor: X.Style.selectionColor
    placeholderTextColor: X.Style.textColorDisabled
    verticalAlignment: TextInput.AlignVCenter

    property color titleTextColor: enabled ? (errorState && titleUp ? X.Style.errorColor : X.Style
                                              .textColorDisabled) : X.Style.borderColor
    property color helperTextColor: enabled ? (errorState ? X.Style.errorColor : X.Style
                                               .textColorDisabled) : X.Style.borderColor
    property color prefixTextColor: enabled ? (X.Style.textColorDisabled) : X.Style.borderColor
    property color suffixTextColor: enabled ? (X.Style.textColorDisabled) : X.Style.borderColor
    property color leadingIconColor: enabled ? (activeFocus ? X.Style.accentColor : X.Style
                                                .textColor) : X.Style.textColorDisabled;

    // BEHAVIOR
    selectByMouse: true

    // CURSOR
    cursorDelegate: X.CursorDelegate {}

    // LEADING ICON
    X.Icon
    {
        id: _leadingIcon
        color: _control.leadingIconColor
        icon: _control.leadingIconSource
        size: X.Style.textField.iconSize
        width: X.Style.textField.iconWidth
        height: X.Style.textField.iconWidth
        y: X.Style.textField.topPadding

        opacity: _control.leadingIconVisible ? color.a : 0.0
        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic
                duration: 200
            }
        }
    }

    // trailing ICON
    property alias trailingContent: _trailingContent.sourceComponent
    property bool trailingVisible: _trailingContent.sourceComponent != undefined

    Loader
    {
        id: _trailingContent
        y: _control.title != "" ? X.Style.textField.topPadding : 0
        x: _control.width - width
        opacity: _control.trailingVisible ? color.a : 0.0
        onSourceComponentChanged:
        {
            if(item && ((typeof item.textField) == "object"))
            {
                item.textField = _control
            }
        }

        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic
                duration: 200
            }
        }
    }

    // TITLE
    X.Label
    {
        id: _titleLabel
        visible: _control.title != ""
        text: _control.title

        font: _control.titleUp ? X.Style.typography.caption : _control.font
        color: _control.titleTextColor
        x: _control.virtualLeftPadding
        y: titleUp ? X.Style.textField.topPaddingTitleOffset : _control.topPadding
        width: _control.width - _control.virtualLeftPadding - _control.virtualRightPadding
        verticalAlignment: _control.verticalAlignment
        horizontalAlignment: _control.horizontalAlignment
        renderType: _control.renderType
        elide: Text.ElideRight

        Behavior on y
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic;
                duration: 200
            }
        }

        Behavior on color
        {
            ColorAnimation
            {
                easing.type: Easing.OutCubic;
                duration: 200
            }
        }
    }

    // HELPER TEXT
    property bool errorState: _errorText != ""
    property string _errorText: ""
    property string errorText: helperText
    property bool autoSubmit: true
    property bool editedAtLeastOnce: false
    property bool error: (!acceptableInput || (length > maximumLengthCount))
    onErrorChanged: if(editedAtLeastOnce && autoSubmit) Qt.callLater(submitInput)
    onTextEdited:
    {
        editedAtLeastOnce = true
        if(autoSubmit)
            Qt.callLater(submitInput)
    }
    X.ErrorSequentialAnimation { id: _errorAnimation;target: _titleLabel;x: _control.virtualLeftPadding }
    X.ErrorSequentialAnimation { id: _errorLeadingAnimation;target: _leadingIcon;x: _control.width - _leadingIcon.width }

    function submitInput()
    {
        if(!error)
            clearError()
        else
            setError(errorText)
    }

    function clearError()
    {
        if(errorState)
            _errorText = ""
    }

    function setError(s)
    {
        if(!errorState)
        {
            if(titleUp)
                _errorAnimation.start()
            if(leadingIconErrorAnimation)
                _errorLeadingAnimation.start()
            _errorText = s ? s : " "
            if(!s)
                console.log("Error: No Error text provided, please provide errorText property to guide your user")
        }
    }

    X.LabelHint1 // Hint
    {
        opacity: (_control.helperText != "" || _control._errorText != "") ? 1.0 : 0.0
        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic;
                duration: 100
            }
        }
        text: _control.errorState ? _control._errorText : _control.helperText

        color: _control.helperTextColor
        width: _control.width - _control.virtualLeftPadding - _control.virtualRightPadding - _lineCountLabel.width
        x: _control.virtualLeftPadding
        y: _control.height - height - X.Style.textField.bottomPaddingHintOffset
        verticalAlignment: _control.verticalAlignment
        renderType: _control.renderType
        elide: Text.ElideRight
    }

    property int maximumLengthCount: maximumLength

    X.LabelHint1 // LineCounter
    {
        id: _lineCountLabel
        visible: _control.maximumLengthCount > 0 && _control.maximumLengthCount < 32767
        text: _control.length + "/" + _control.maximumLengthCount

        color: _control.helperTextColor
        x: _control.width - width - (_control.trailingInline ? 0 : _control.virtualRightPadding)
        y: _control.height - height - X.Style.textField.bottomPaddingHintOffset
        verticalAlignment: _control.verticalAlignment
        renderType: _control.renderType
    }

    X.Label // Prefix Label
    {
        id: _prefixLabel

        height: _control.contentHeight
        x: _control.virtualLeftPadding
        y: _control.topPadding
        text: _control.prefixText
        color: _control.prefixTextColor
        font: _control.font
        verticalAlignment: _control.verticalAlignment
        renderType: _control.renderType
        opacity: (_control.prefixText != "" && _control.activeFocus || _control.length) ? 1.0 : 0.0

        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic
                duration: 200
            }
        }
    }

    X.Label // Suffix Label
    {
        id: _suffixLabel

        height: _control.contentHeight
        x: _control.width - width - _control.virtualRightPadding
        y: _control.topPadding
        font: _control.font
        text: _control.suffixText
        color: _control.suffixTextColor
        verticalAlignment: _control.verticalAlignment
        renderType: _control.renderType
        opacity: (_control.suffixText != "" && _control.activeFocus || _control.length) ? 1.0 : 0.0

        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.OutCubic
                duration: 200
            }
        }
    }

    // PLACEHOLDER
    X.PlaceholderText
    {
        id: placeholder
        x: _control.leftPadding
        y: _control.topPadding
        width: _control.width - (_control.leftPadding + _control.rightPadding)
        height: _control.height - (_control.topPadding + _control.bottomPadding)
        text: _control.placeholderText
        color: _control.placeholderTextColor
        font: _control.font
        verticalAlignment: _control.verticalAlignment
        horizontalAlignment: _control.horizontalAlignment
        elide: Text.ElideRight
        renderType: _control.renderType
        readonly property bool shouldBeVisible: (_control.title == "" || _control.activeFocus) && !_control.length && !
        _control.preeditText && (!_control.activeFocus || _control.horizontalAlignment !== Qt.AlignHCenter)
        opacity: shouldBeVisible ? 1.0 : 0.0

        Behavior on opacity
        {
            NumberAnimation
            {
                easing.type: Easing.InQuad
                duration: placeholder.shouldBeVisible ? 50 : 200
            }
        }
    }

    // BACKGROUND
    property double backgroundBorderHeight: X.Style.textField.backgroundBorderHeight
    property color backgroundColor: errorState ? X.Style.errorColor : X.Style.textColorDisabled
    property color backgroundHighlightColor: errorState ? X.Style.errorColor : X.Style.accentColor

    background: Rectangle
    {
        y: _control.height - height - _control.bottomPadding + 8
        implicitWidth: X.Style.textField.implicitWidth
        width: parent.width
        height: _control.activeFocus || _control.hovered ? _control.backgroundBorderHeight : 1
        color: _control.backgroundColor

        Rectangle
        {
            height: _control.backgroundBorderHeight
            color: _control.backgroundHighlightColor
            width: _control.activeFocus ? parent.width : 0
            x: _control.activeFocus ? 0 : parent.width / 2

            Behavior on width
            {
                enabled: !_control.activeFocus
                NumberAnimation
                {
                    easing.type: Easing.OutCubic;
                    duration: 300
                }
            }

            Behavior on x
            {
                enabled: !_control.activeFocus
                NumberAnimation
                {
                    easing.type: Easing.OutCubic;
                    duration: 300
                }
            }

        }
    }
}
