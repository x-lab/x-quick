import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.TabBar
{
  id: _control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    contentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    contentHeight + topPadding + bottomPadding)

    spacing: 1

    property color backgroundColor: X.Style.backgroundColor
    property color accentColor: X.Style.accentColor

  contentItem: ListView
  {
    model: _control.contentModel
    currentIndex: _control.currentIndex

    spacing: _control.spacing
    orientation: ListView.Horizontal
    boundsBehavior: Flickable.StopAtBounds
    flickableDirection: Flickable.AutoFlickIfNeeded
    snapMode: ListView.SnapToItem

    highlightMoveDuration: 250
    highlightResizeDuration: 0
    highlightFollowsCurrentItem: true
    highlightRangeMode: ListView.ApplyRange
    preferredHighlightBegin: 48
    preferredHighlightEnd: width - 48

    highlight: Item
    {
      z: 2
      Rectangle
      {
        height: 2
        width: parent.width
        y: _control.position === TabBar.Footer ? 0 : parent.height - height
        color: _control.accentColor
        visible: _control.enabled
      }
    }
  }

  background: Rectangle
  {
    color: _control.backgroundColor
  }
}
