import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Switch {

    id: control

    implicitWidth: 48;
    implicitHeight: 32;

    contentItem: Text {
        leftPadding: control.indicator.width + control.spacing
        text: control.text
        font: control.font
        color: X.Style.foregroundColor
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
        opacity: enabled ? 1.0 : 0.3
    }

    indicator: Rectangle {
        implicitWidth: control.width;
        implicitHeight: control.height;

        x: control.leftPadding
        y: parent.height/2 - height/2;

        radius: control.height/2;
        color: control.checked ? Qt.lighter(X.Style.backgroundColor) : Qt.darker(X.Style.alternateBaseColor)

        // border.color: X.Style.borderColor
        border.color: "transparent";

        Behavior on color { ColorAnimation { duration: 150; }}

        Rectangle {
            x: control.checked ? parent.width - width - 1 : 1
            y: 2;
            width: parent.height-4;
            height: parent.height-4;
            radius: height/2;
            color: X.Style.backgroundColor

            // border.color: X.Style.borderColor
            border.color: "transparent"
            border.width: 1;

            Behavior on x { NumberAnimation { duration: 150; }}
        }
    }
}
