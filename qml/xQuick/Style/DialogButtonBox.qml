import QtQuick           2.15
import QtQuick.Templates as T
import QtQuick.Controls  2.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.DialogButtonBox
{
  id: root

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    contentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    contentHeight + topPadding + bottomPadding)

  spacing: X.Style.dialog.spacing
  padding: X.Style.dialog.padding
  alignment: Qt.AlignRight
  buttonLayout: T.DialogButtonBox.MacLayout

  property color backgroundColor: X.Style.backgroundColor;
  palette.base: X.Style.baseColor;

  delegate: X.ButtonFlat {}

  contentItem: Row
  {
    spacing: root.spacing

    rightPadding: root.padding;

    Repeater
    {
      model: root.contentModel
    }
  }

  background: Rectangle
  {
    implicitHeight: X.Style.dialog.buttonRectImplicitHeight
    radius: X.Style.dialog.radius
    color: X.Style.alternateBaseColor

    Rectangle
    {
      anchors
      {
        right: parent.right
        left: parent.left
      }

      y: root.position === T.DialogButtonBox.Footer ? 0 : parent.height - height
      height: 1

      color: X.Style.borderColor
    }
  }
}
