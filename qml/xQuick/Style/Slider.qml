import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Slider {

    id: control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    implicitHandleWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    implicitHandleHeight + topPadding + bottomPadding)

    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 4
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: X.Style.backgroundColor;
        border.width: 1;
        border.color: X.Style.borderColor;

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            color: X.Style.foregroundColor;
            radius: 2
        }
    }

    handle: Rectangle {
        x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 22
        implicitHeight: 22
        radius: 11
        color: control.pressed ? Qt.darker(X.Style.foregroundColor) : X.Style.foregroundColor;
        border.color: Qt.lighter(X.Style.borderColor, 2);
    }
}
