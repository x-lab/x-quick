import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.ComboBox {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding,
                             implicitIndicatorHeight + topPadding + bottomPadding)

    leftPadding: padding + (!indicator || !indicator.visible ? 0 : indicator.width + spacing)
    rightPadding: padding + (!indicator || !indicator.visible ? 0 : indicator.width + spacing)

    delegate: ItemDelegate {
        width: control.width
        contentItem: Text {
            text: model[textRole] ? model[textRole] : model.modelData
            color:  "#bbbbbb"; // unreadeable on light theme with foregroundColor
            font: control.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
            // leftPadding: X.Style.combo.leftPadding;
        }
        highlighted: control.highlightedIndex === index
    }

    indicator: Canvas {
        id: canvas
        x: control.width - width - control.rightPadding
        y: control.topPadding + (control.availableHeight - height) / 2
        width: 12
        height: 8
        contextType: "2d"

        Connections {
            target: control
            function onPressedChanged() { canvas.requestPaint(); }
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = control.pressed ? Qt.darker(X.Style.foregroundColor) : X.Style.foregroundColor;
            context.fill();
        }
    }

    contentItem: Text {
        leftPadding: X.Style.combo.leftPadding;
        rightPadding: control.indicator.width + control.spacing

        text: control.displayText
        font: control.font
        color: control.down ? Qt.darker(X.Style.foregroundColor) : X.Style.foregroundColor;
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: 120
        implicitHeight: 40

        color: control.down ? Qt.lighter(X.Style.backgroundColor) : X.Style.backgroundColor;
        border.width: 1
        border.color: X.Style.borderColor;
        radius: X.Style.controls.radius;
    }

    popup: Popup {
        y: control.height + 10;
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            color: X.Style.combo.backgroundColor;
            border.color: X.Style.borderColor;
            radius: X.Style.controls.radius;
        }
    }
}
