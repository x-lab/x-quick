import QtQuick           2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Page {
    id: control

    background: Rectangle {
        color: X.Style.alternateBaseColor;
    }

    clip: true;
}
