import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.RadioButton {

    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding + implicitIndicatorWidth)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding,
                             implicitIndicatorHeight + topPadding + bottomPadding)

    horizontalPadding: 10
    verticalPadding: 10
    spacing: 10

    contentItem: Text {
        rightPadding: control.spacing
        text: control.text
        font: control.font
        color: X.Style.foregroundColor
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
        leftPadding: control.indicator.width + control.spacing
    }

    indicator: Rectangle {
        implicitWidth: 26
        implicitHeight: 26
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        color: control.down ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor;
        border.width: 1
        border.color: X.Style.borderColor;
        radius: 13;

        Rectangle {
            width: 14
            height: 14
            x: 6
            y: 6
            radius: 7
            color: control.down ? Qt.darker(X.Style.foregroundColor) : X.Style.foregroundColor
            visible: control.checked
        }
    }
}
