import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.RadioDelegate {
    id: control

    implicitWidth: Math.max(background ? implicitBackgroundWidth : 0,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? implicitBackgroundHeight : 0,
                             Math.max(implicitContentHeight,
                                      indicator ? indicator.implicitHeight : 0) + topPadding + bottomPadding) + bottomInset

    leftPadding: 10;
    rightPadding: 10;

    contentItem: Text {
        rightPadding: control.spacing
        text: control.text
        font: control.font
        color: X.Style.foregroundColor
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
    }

    indicator: Rectangle {
        implicitWidth: 26
        implicitHeight: 26
        x: control.width - width - control.rightPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2

        color: control.down ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor;
        border.width: 1
        border.color: X.Style.borderColor;
        radius: 13;

        Rectangle {
            width: 14
            height: 14
            x: 6
            y: 6
            radius: 7
            color: control.down ? Qt.darker(X.Style.foregroundColor) : X.Style.foregroundColor
            visible: control.checked
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 0.8 : 0.3
        color: (control.down || control.highlighted || control.hovered) ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor
        radius: X.Style.controls.radius;
    }
}
