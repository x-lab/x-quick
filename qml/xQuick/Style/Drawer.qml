import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.Drawer {

    id: control;

    background: Rectangle {
        color: X.Style.baseColor;

        X.Separator {
            anchors.top: parent.top;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
        }
    }
}
