import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.ProgressBar {

    id: control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    implicitContentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    implicitContentHeight + topPadding + bottomPadding)

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 6
        color: X.Style.backgroundColor;
        radius: 3
    }

    contentItem: Item {

        implicitWidth: 200
        implicitHeight: 4

        Rectangle {
            width: control.visualPosition * contentItem.width;
            height: parent.height
            radius: 2
            color: X.Style.foregroundColor;
        }
    }
}
