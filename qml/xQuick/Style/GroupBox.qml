import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.GroupBox {

    id: control


    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding)

    spacing: 10

    horizontalPadding: 10
    verticalPadding: 10

    background: Rectangle {
        y: control.topPadding - control.bottomPadding
        width: parent.width
        height: parent.height - control.topPadding + control.bottomPadding
        color: X.Style.backgroundColor;
        border.color: X.Style.borderColor;
        radius: X.Style.controls.radius;
    }

    label: Label {
        x: control.leftPadding
        width: control.availableWidth
        text: control.title
        color: X.Style.foregroundColor;
        elide: Text.ElideRight
    }
}
