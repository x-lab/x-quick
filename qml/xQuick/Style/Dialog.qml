import QtQuick           2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Controls  2.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.Dialog
{
  id: root

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    contentWidth + leftPadding + rightPadding,
    implicitHeaderWidth,
    implicitFooterWidth)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    contentHeight + topPadding + bottomPadding +
    (implicitHeaderHeight > 0 ? implicitHeaderHeight + spacing : 0) +
    (implicitFooterHeight > 0 ? implicitFooterHeight + spacing : 0))

  padding: X.Style.dialog.horizontalPadding
  topPadding: X.Style.dialog.topPadding //+ (drawSeparator ? 1 : 0)
  bottomPadding: (drawSeparator ? 1 : (X.Style.dialog.horizontalPadding + 1))
  font: X.Style.typography.body1

  enter: Transition
  {
    NumberAnimation { property: "scale";from: 0.9;to: 1.0;easing.type: Easing.OutQuint;duration: 220 }
    NumberAnimation { property: "opacity";from: 0.0;to: 1.0;easing.type: Easing.OutCubic;duration: 150 }
  }

  exit: Transition
  {
    NumberAnimation { property: "scale";from: 1.0;to: 0.9;easing.type: Easing.OutQuint;duration: 220 }
    NumberAnimation { property: "opacity";from: 1.0;to: 0.0;easing.type: Easing.OutCubic;duration: 150 }
  }

  property alias backgroundColor: root.palette.base
  property color overlayColor: X.Style.overlayColor
  property bool drawSeparator: false

  palette.base: X.Style.baseColor

  background: Rectangle
  {
    radius: X.Style.dialog.radius
    color: root.palette.base

    X.Separator
    {
      visible: root.drawSeparator && header.visible
      //anchors.top: header.bottom
      //anchors.topMargin: root.topPadding
      anchors.horizontalCenter: parent.horizontalCenter
      implicitWidth: root.width
      y: root.header ? root.header.height + root.topPadding - 1 : 0
    } // HorizontalLineSeparator

    X.Separator
    {
      visible: root.drawSeparator && footer.visible
      y: root.footer ? root.height - footer.height - root.bottomPadding : 0
      //anchors.bottom: footer.top
      //anchors.bottomMargin: root.bottomPadding
      anchors.horizontalCenter: parent.horizontalCenter
      implicitWidth: root.width
    }
  }

  header: X.LabelHeadline6
  {
    text: root.title
    visible: root.title
    elide: Label.ElideRight
    padding: X.Style.dialog.horizontalPadding
    bottomPadding: 0
    background: Rectangle
    {
      radius: X.Style.dialog.radius
      color: root.palette.base
      //clip: true
    }
  }

  footer: X.DialogButtonBox
  {
    visible: count > 0
    palette.base: root.palette.base
  }

  T.Overlay.modal: Rectangle
  {
    color: root.overlayColor
    Behavior on opacity { NumberAnimation { duration: 150 } }
  }

  T.Overlay.modeless: Rectangle
  {
    color: root.overlayColor
    Behavior on opacity { NumberAnimation { duration: 150 } }
  }
}
