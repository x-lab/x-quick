import QtQuick               2.15
import QtQuick.Templates     2.15 as T
import QtQuick.Controls.impl 2.15

import xQuick.Style          1.0 as X

T.ToolButton {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    padding: 5
    spacing: 5

    icon.width: 16
    icon.height: 16

    contentItem: IconLabel {
        spacing: control.spacing
        mirrored: control.mirrored
        display: control.display

        icon: control.icon
        text: control.text
        font: control.font
        color: "white"; // TODO
    }

    background: X.ButtonPanel {
        implicitWidth: 20
        implicitHeight: 20

        control: control
        visible: control.down || control.checked || control.highlighted || control.visualFocus || control.hovered
    }
}
