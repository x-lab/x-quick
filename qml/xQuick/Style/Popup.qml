import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.Popup
{
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset, contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset, contentHeight + topPadding + bottomPadding)

    padding: 12

    property color backgroundColor: X.Style.backgroundColor;

    enter: Transition
    {
        NumberAnimation
        {
            property: "scale";from: 0.9;to: 1.0;easing.type: Easing.OutQuint;duration: 220
        }
        NumberAnimation
        {
            property: "opacity";from: 0.0;to: 1.0;easing.type: Easing.OutCubic;duration: 150
        }
    }

    exit: Transition
    {
        NumberAnimation
        {
            property: "scale";from: 1.0;to: 0.9;easing.type: Easing.OutQuint;duration: 220
        }
        NumberAnimation
        {
            property: "opacity";from: 1.0;to: 0.0;easing.type: Easing.OutCubic;duration: 150
        }
    }

    background: Rectangle
    {
        implicitWidth: X.Style.menu.implicitWidth
        implicitHeight: X.Style.menu.implicitHeight

        radius: X.Style.menu.radius
        color: control.backgroundColor

    }

    // T.Overlay.modal: Rectangle
    // {
    //     color: X.Style.overlayColor
    //     Behavior on opacity
    //     {
    //         NumberAnimation { duration: 150 }
    //     }
    // }
    //
    // T.Overlay.modeless: Rectangle
    // {
    //     color: X.Style.overlayColor
    //     Behavior on opacity
    //     {
    //         NumberAnimation { duration: 150 }
    //     }
    // }
}
