import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Window    2.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.Menu
{
  id: _control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    contentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    contentHeight + topPadding + bottomPadding)

  margins: 0
  verticalPadding: X.Style.menu.verticalPadding

  transformOrigin: !cascade ? Item.Top : (mirrored ? Item.TopRight : Item.TopLeft)

  delegate: X.MenuItem {}

  enter: Transition
  {
    NumberAnimation
    {
      property: "scale";from: 0.9;to: 1.0;easing.type: Easing
        .OutQuint;duration: 220
    }
    NumberAnimation
    {
      property: "opacity";from: 0.0;to: 1.0;easing.type: Easing
        .OutCubic;duration: 150
    }
  }

  exit: Transition
  {
    NumberAnimation
    {
      property: "scale";from: 1.0;to: 0.9;easing.type: Easing
        .OutQuint;duration: 220
    }
    NumberAnimation
    {
      property: "opacity";from: 1.0;to: 0.0;easing.type: Easing
        .OutCubic;duration: 150
    }
  }

  contentItem: ListView
  {
    implicitHeight: contentHeight

    model: _control.contentModel
    interactive: Window.window ? contentHeight > Window.window.height : false
    clip: true
    currentIndex: _control.currentIndex

    ScrollIndicator.vertical: X.ScrollIndicator {}
  }

  property color backgroundColor: X.Style.baseColor

  background: Rectangle
  {
    implicitWidth: X.Style.menu.implicitWidth
    implicitHeight: X.Style.menu.implicitHeight

    radius: X.Style.menu.radius
    color: _control.backgroundColor

  }

  T.Overlay.modal: Rectangle
  {
    color: X.Style.overlayColor
    Behavior on opacity
    {
      NumberAnimation { duration: 150 }
    }
  }

  T.Overlay.modeless: Rectangle
  {
    color: X.Style.overlayColor
    Behavior on opacity
    {
      NumberAnimation { duration: 150 }
    }
  }
}
