import QtQuick           2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Controls  2.15

import xQuick.Controls   1.0 as X
import xQuick.Fonts      1.0 as X
import xQuick.Style      1.0 as X

T.MenuItem
{
  id: _control

  implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
    implicitContentWidth + leftPadding + rightPadding)
  implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
    implicitContentHeight + topPadding + bottomPadding)

  property bool drawline: false

  width: parent ? parent.width : 100

  display: AbstractButton.TextBesideIcon

  topInset: X.Style.menuItem.topInset
  bottomInset: X.Style.menuItem.bottomInset
  leftInset: X.Style.menuItem.leftInset
  rightInset: X.Style.menuItem.rightInset

  leftPadding: (icon.source != "" && !mirrored || text == "" ? X.Style.menuItem.iconPadding : X.Style
    .menuItem.padding) + leftInset
  rightPadding: (icon.source != "" && mirrored || text == "" ? X.Style.menuItem.iconPadding : X.Style
    .menuItem.padding) + rightInset
  topPadding: 0
  bottomPadding: 0
  spacing: X.Style.menuItem.spacing

  property bool outlined: false

  property color foregroundColor: X.Style.textColor
  property color backgroundColor: "transparent"

  property double radius: X.Style.menuItem.cornerRadius

  property
  var checkedIcon: QtObject
  {
    readonly property double width: X.Style.menuItem.iconWidth
    readonly property double height: X.Style.menuItem.iconWidth
    readonly property color color: foregroundColor
      readonly property string source: X.Icons.icons.radio_button_checked
  }

  property
  var radioIcon: QtObject
  {
    readonly property double width: X.Style.menuItem.iconWidth
    readonly property double height: X.Style.menuItem.iconWidth
    readonly property color color: foregroundColor
    readonly property url source: _control.checked ? X.Icons.icons.radio_button_checked : X.Icons.icons.radio_button_unchecked
  }

  font: X.Style.typography.body2

  icon.width: X.Style.menuItem.iconWidth
  icon.height: X.Style.menuItem.iconWidth
  icon.color: foregroundColor
  icon.source: checkable && checked ? X.Icons.icons.radio_button_checked : ""

  indicator: X.LabelHint2
  {
    x: _control.mirrored ? _control.leftPadding : _control.width - width - _control.rightPadding
    y: _control.topPadding + (_control.availableHeight - height) / 2
    //width: _control.icon.width
    //height: _control.icon.height
    width: X.Style.menuItem.shortcutImplicitWidth
    text: dummyShortcut.nativeText
    visible: _control.action && dummyShortcut.nativeText != ""
    horizontalAlignment: _control.mirrored ? Qt.AlignRight : Qt.AlignLeft

    Shortcut
    {
      id: dummyShortcut
      enabled: false
      sequence: _control.action ? _control.action.shortcut : ""
    }

  }

  arrow: X.Icon
  {
    x: _control.mirrored ? _control.padding : _control.width - width - _control.padding
    y: _control.topPadding + (_control.availableHeight - height) / 2

    visible: _control.subMenu
    color: _control.foregroundColor
    icon: X.Icons.icons.arrow_right
  }

  contentItem: X.LabelIcon
  {
    id: _iconLabel

    readonly property real arrowPadding: _control.subMenu && _control.arrow ? _control.arrow.width + _control
      .spacing : 0
    readonly property real indicatorPadding: _control.indicator && _control.indicator.visible ? _control.indicator
      .width + _control.spacing : 0
    anchors.leftMargin: _control.mirrored ? indicatorPadding + arrowPadding : 0
    anchors.rightMargin: !_control.mirrored ? indicatorPadding + arrowPadding : 0

    elide: Text.ElideRight

    spacing: _control.spacing
    font: _control.font
    iconSource: _control.icon.source
    iconSize: _control.icon.width
    // icon.color: _control.icon.color
    text: _control.text
    color: _control.foregroundColor
  }

  property double backgroundImplicitWidth: X.Style.menuItem.minWidth
  property double backgroundImplicitHeight: X.Style.menuItem.minHeight

  background: Rectangle
  {
    implicitWidth: _control.backgroundImplicitWidth
    implicitHeight: _control.backgroundImplicitHeight
    radius: _control.radius
    color: _control.backgroundColor
  }
}
