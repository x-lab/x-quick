import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Style      1.0 as X

T.ItemDelegate {

    id: control

    implicitWidth: implicitBackgroundWidth;
    implicitHeight: implicitBackgroundHeight;

    leftPadding: 10;

    contentItem: X.Label {
        text: control.text
        font: control.font
        color: X.Style.foregroundColor
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 0.8 : 0.3
        color: (control.down || control.highlighted || control.hovered) ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor
        radius: X.Style.controls.radius;
    }
}
