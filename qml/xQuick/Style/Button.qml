import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Templates 2.15 as T

import xQuick.Fonts      1.0 as X
import xQuick.Style      1.0 as X

T.Button {
    id: _control

    font: X.Style.typography.button

    implicitWidth: 100
    implicitHeight: 32

    contentItem: Text {
        text: _control.text
        font: _control.font
        opacity: enabled ? 1.0 : 0.3
        color: X.Style.foregroundColor;
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitWidth: _control.implicitWidth;
        implicitHeight: _control.implicitHeight;
        opacity: enabled ? 1 : 0.3
        color: _control.down ? Qt.darker(X.Style.backgroundColor) : X.Style.backgroundColor;
        border.width: 1
        border.color: X.Style.borderColor;
        radius: X.Style.controls.radius;
    }
}
