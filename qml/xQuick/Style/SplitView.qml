import QtQuick           2.15
import QtQuick.Templates 2.15 as T

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

T.SplitView {
    id: _control

    property var handleFunc: function() {
        return X.Style.borderColor;
    };

    handle: X.Separator {

        color: _control.handleFunc();
    }
}
