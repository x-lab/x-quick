pragma Singleton

import QtQuick 2.15

QtObject
{
    property var bannerLoader;

    function show(config)
    {
        if(typeof config === 'string')
            config = {
                text: config
            }

        if (bannerLoader)
            bannerLoader.show(config)
    }

    function popBanner()
    {
        if (bannerLoader)
            bannerLoader.popBanner()
    }
}
