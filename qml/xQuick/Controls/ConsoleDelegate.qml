import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Style     1.0 as X

Control {

    height: 28;
    width: parent ? parent.width : 0;

    background: Rectangle {
        color: index % 2 ? "transparent" : "#11000000";
    }

    RowLayout {

        anchors.fill: parent;

        Label {
            text: modelData.label;

            font.bold: modelData.type > 1;

            color:
              modelData.type == 0 ? X.Style.infoColor
            : modelData.type == 2 ? X.Style.warningColor
            : modelData.type == 3 ? X.Style.errorColor
            :                       X.Style.debugColor;

            Layout.minimumWidth: 64;
            Layout.maximumWidth: 64;

            leftPadding: 4;
        }

        Label {
            text: modelData.description;
            clip: true;

            leftPadding: 6;

            Layout.fillWidth: true;
        }

        Label {
            text: modelData.time;

            Layout.minimumWidth: 64;
            Layout.maximumWidth: 64;
        }
    }
}
