import QtQuick      2.15

import xQuick.Style 1.0 as X

Item
{
    id: _control
    implicitHeight: Math.max(_cardTitle.implicitHeight, _roundImage.implicitHeight) + topPadding + bottomPadding
    implicitWidth: _roundImage.implicitWidth + _cardTitle.implicitWidth + rightPadding + topPadding + spacing

    property alias headerText: _cardTitle.headerText
    property alias subHeaderText: _cardTitle.subHeaderText
    property alias thumbnail: _roundImage.source
    property double imageSize: Qaterial.Style.card.roundImageSize
    property double spacing: Qaterial.Style.card.roundImageSpacing
    property alias textSpacing: _cardTitle.spacing
    property bool mirrored: false

    readonly property bool imagePresent: _roundImage.source != ""

    X.CardTitle
    {
        id: _cardTitle
        anchors.right: _control.mirrored && _control.imagePresent ? _roundImage.left : _control.right
        anchors.left: !_control.imagePresent || _control.mirrored ? _control.left : _roundImage.right
        anchors.rightMargin: _control.mirrored && _control.imagePresent ? _control.spacing : _control.rightPadding
        anchors.leftMargin: !_control.mirrored && _control.imagePresent ? _control.spacing : _control.leftPadding
    }

    X.RoundImage
    {
        id: _roundImage
        width: imageSize
        height: imageSize
        anchors.rightMargin: _control.mirrored ? _control.rightPadding : 0
        anchors.leftMargin: _control.mirrored ? 0 : _control.leftPadding
        anchors.verticalCenter: parent.verticalCenter

        function reanchors()
        {
            anchors.right = undefined
            anchors.left = undefined
            if (_control.mirrored)
                anchors.right = _control.right
            else
                anchors.left = _control.left
        }
    }

    function reanchors()
    {
        _roundImage.reanchors()
    }

    onMirroredChanged: Qt.callLater(reanchors)

    Component.onCompleted: reanchors()
}
