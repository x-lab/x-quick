import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X

RowLayout {

    id: entry;

    property alias text: captionLabel.text;
    property alias font_: label.font;
    property bool enabled: true;

    Item {
        Layout.minimumWidth: 20;
        Layout.maximumWidth: 20;
    }

    X.LabelCaption {

        id: captionLabel;

        Layout.preferredWidth: 65;
        enabled: entry.enabled;
    }

    X.Label {

        id: label;

        text: entry.text + " " + font.pixelSize + "sp";
        enabled: entry.enabled;
    }
}
