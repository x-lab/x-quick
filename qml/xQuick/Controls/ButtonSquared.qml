import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.ButtonRaw
{
    flat: true
    highlighted: checked

    foregroundColor:
    {
        if(!enabled)
            return X.Style.textColorDisabled

        if(highlighted)
            return X.Style.palette.accent

        return X.Style.textColor;
    }
}
