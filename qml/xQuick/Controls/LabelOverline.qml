import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.overline.family;
    font.pixelSize:      X.Style.typography.overline.pixelSize;
    font.weight:         X.Style.typography.overline.weight;
    font.capitalization: X.Style.typography.overline.capitalization;
    font.letterSpacing:  X.Style.typography.overline.letterSpacing;
}
