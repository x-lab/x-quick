import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.headline5.family;
    font.pixelSize:      X.Style.typography.headline5.pixelSize;
    font.weight:         X.Style.typography.headline5.weight;
    font.capitalization: X.Style.typography.headline5.capitalization;
    font.letterSpacing:  X.Style.typography.headline5.letterSpacing;
}
