import QtQuick         2.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Item
{
    id: _control;

    implicitHeight: _column.implicitHeight;
    implicitWidth: 200;

    property alias headerText: _title.text;
    property alias subHeaderText: _text.text;
    property alias spacing: _column.spacing;

    property bool mirrored: false;

    property alias padding: _column.padding
    property alias leftPadding: _column.leftPadding
    property alias rightPadding: _column.rightPadding
    property alias topPadding: _column.topPadding
    property alias bottomPadding: _column.bottomPadding

    Column
    {
        id: _column

        anchors.right: parent.right
        anchors.left: parent.left

        X.LabelHeadline6
        {
            id: _title
            elide: Text.ElideRight
            horizontalAlignment: _control.mirrored ? Text.AlignRight : Text.AlignLeft
            width: parent.width - _control.rightPadding - _control.leftPadding
        }

        X.LabelBody1
        {
            id: _text
            elide: Text.ElideRight
            horizontalAlignment: _control.mirrored ? Text.AlignRight : Text.AlignLeft
            width: parent.width - _control.rightPadding - _control.leftPadding
        }
    }
}
