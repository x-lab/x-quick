import QtQuick            2.15
//import QtGraphicalEffects 1.15
import Qt5Compat.GraphicalEffects

import xQuick.Style       1.0 as X

Image
{
    id: _control

    height: X.Style.card.mediaImplicitHeight
    width: X.Style.card.mediaImplicitWidth

    fillMode: Image.PreserveAspectCrop

    property bool clipTop: false
    property double radius: X.Style.card.radius

    layer.enabled: clipTop
    layer.effect: OpacityMask
    {
        maskSource: Rectangle
        {
            width: _control.width
            height: _control.height
            radius: _control.radius

            Rectangle
            {
                width: parent.width
                y: _control.radius
                height: parent.height - radius
            }
        }
    }
}
