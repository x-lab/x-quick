import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Layouts   1.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X


X.ComboBox {
    id: self
    width: parent.width/2

    displayText: "Select"
	
	property var values: []

	delegate: Item {
        width: parent.width
        height: checkDelegate.height

        function toggle() { checkDelegate.toggle() }

        CheckDelegate {
            id: checkDelegate
            anchors.fill: parent

            text: name
            checked: ischecked

            highlighted: ischecked

            onCheckedChanged: {
	            if(checked)
	            {
	            	if(!values.includes(name)) {
	            		values.push(name);
	            	}
	                ischecked = true;
	            } else {
	                let pos = values.indexOf(name);
	                if( pos != -1) {
	                    values.splice(pos, 1);
	                }
	                ischecked = false;
	            }
	            self.displayText = values.toString()
        	}
        }
    }
}