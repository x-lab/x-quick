import QtQuick
import QtQuick.Controls

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Pane
{
    id: root

    padding: 0

    property real radius: X.Style.controls.radius;

    property color backgroundColor: X.Style.variant == "LIGHT"
        ? Qt.lighter(X.Style.backgroundColor, 1.2)
        : Qt.darker(X.Style.backgroundColor, 1.2);

    property color borderColor: enabled ? X.Style.borderColor : Qt.darker(X.Style.borderColor, 1.2);
    property bool outlined: true;

    property bool isActive: hovered || pressed || visualFocus

    property string headerText
    property string subHeaderText
    property string supportingText
    property string media
    property string thumbnail

    property bool pressed: false

//    palette.base: X.Style.backgroundColor;

    bottomPadding: 10;

    background: X.CardBackground
    {
        isActive: root.isActive
        enabled: root.enabled
        radius: root.radius
        color: root.backgroundColor
        borderColor: root.borderColor
        outlined: root.outlined
    }
}
