import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.subtitle1.family;
    font.pixelSize:      X.Style.typography.subtitle1.pixelSize;
    font.weight:         X.Style.typography.subtitle1.weight;
    font.capitalization: X.Style.typography.subtitle1.capitalization;
    font.letterSpacing:  X.Style.typography.subtitle1.letterSpacing;
}
