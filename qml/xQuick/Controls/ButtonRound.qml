import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.ButtonRaw
{
    flat: true;
    highlighted: checked;
    radius: height / 2;

    foregroundColor:
    {
        if(!enabled)
            return X.Style.textColorDisabled;

        if(highlighted)
            return X.Style.accentColor;

        return X.Style.textColor;
    }
}
