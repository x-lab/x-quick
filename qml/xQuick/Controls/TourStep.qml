import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Style     1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Controls 1.0 as X

Control {

    id: _self;

    property string title: "";
    property string description: "";
    property int numSteps: 1;
    property int step: 1;
    property bool hasButton: false;
    property alias buttonText: _next_step_button.text;
    property int arrowPosition: X.Tour.Position.Top;
    property int arrowVerticalAlignment: X.Tour.VerticalAlignment.Center;
    property int arrowHorizontalAlignment: X.Tour.HorizontalAlignment.Center;

    signal next();
    signal end();

    height: 200;
    width: 300;
    z: 1000;

    background: Rectangle {
        color: X.Style.backgroundColor;
        radius: 5;
        border.width: 1;
        border.color: "black";
    }

    Text {
        id: _title;

        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: parent.top;
        anchors.margins: 10;

        height: parent.height * 0.2 - 20;


        text: _self.title;
    }

    X.Separator {
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: _title.bottom;
        color: X.Style.baseColor;
    }

    Control {

        id: _description_area;

        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.top: _title.bottom;
        anchors.margins: 10;
        height: parent.height * 0.5 - 20;

        Text {
            id: _description;

            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.top: parent.top;

            wrapMode: Text.Wrap;

            text: _self.description;
        }


        Button {

            id: _next_step_button;

            anchors.right: parent.right;
            anchors.bottom: parent.bottom;

            text: "Next";
            visible: _self.hasButton && _self.step !== _self.numSteps;

            onClicked: _self.next();
        }

    }

    X.Separator {
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: _footer.top;
        color: X.Style.baseColor;
    }

    Control {
        id: _footer;

        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.margins: 10;

        height: parent.height * 0.3 - 20;

        Row {
            id: _step_indicator;

            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: _footer.left;
            spacing: _footer.height / 8;

            Repeater {

                model: _self.numSteps;
                delegate: Rectangle {
                    height: _footer.height / 4;
                    width: _footer.height / 4;
                    radius: _footer.height / 4;
                    color: index === _self.step - 1 ? X.Style.accentColor : X.Style.foregroundColor;
                }
            }
        }

        Button {
            text: _self.step === _self.numSteps ? "End Tour" : "Skip Tour"
            anchors.right: _footer.right;
            anchors.verticalCenter: parent.verticalCenter;

            onClicked: _self.end();
        }
    }

    function getArrowRotation () {
        if(_self.arrowPosition === X.Tour.Position.Left) return -90
        if(_self.arrowPosition === X.Tour.Position.Right) return 90
        if(_self.arrowPosition === X.Tour.Position.Top) return 0
        if(_self.arrowPosition === X.Tour.Position.Bottom) return 180
    }

    function getArrowPosition (h, w) {
        const point = {x: 0, y: 0}
        if(_self.arrowPosition === X.Tour.Position.Left || _self.arrowPosition === X.Tour.Position.Right) {
            point.x = _self.arrowPosition === X.Tour.Position.Left ? - h - 10 : _self.width - h;
            if(_self.arrowVerticalAlignment === X.Tour.VerticalAlignment.Top) point.y = 15
            if(_self.arrowVerticalAlignment === X.Tour.VerticalAlignment.Center) point.y = _self.height / 2 - w / 2;
            if(_self.arrowVerticalAlignment === X.Tour.VerticalAlignment.Bottom) point.y = _self.height - 25
        }

        if(_self.arrowPosition === X.Tour.Position.Top || _self.arrowPosition === X.Tour.Position.Bottom) {
            point.y = _self.arrowPosition === X.Tour.Position.Top ? - h : _self.height;
            if(_self.arrowHorizontalAlignment === X.Tour.HorizontalAlignment.Left) point.x = 15
            if(_self.arrowHorizontalAlignment === X.Tour.HorizontalAlignment.Center) point.x = _self.width / 2 -  w / 2;
            if(_self.arrowHorizontalAlignment === X.Tour.HorizontalAlignment.Right) point.x = _self.width - 15
        }

        return point
    }

    Canvas {
        id: _canvas

        property var pos: _self.getArrowPosition(height, width)

        height: parent.height / 20
        width: parent.width / 10
        x: pos.x;
        y: pos.y;
        z: 1000;

        antialiasing: true

        rotation: _self.getArrowRotation()
        transformOrigin: Item.Center;

        onPaint: {
            var ctx = _canvas.getContext('2d')

            ctx.fillStyle = X.Style.backgroundColor
            ctx.strokeStyle = "black"
            ctx.lineWidth = _canvas.height * 0.05
            ctx.beginPath()
            ctx.moveTo(_canvas.width * 0.05, _canvas.height)
            ctx.lineTo(_canvas.width / 2, _canvas.height * 0.1)
            ctx.lineTo(_canvas.width * 0.95, _canvas.height)
            ctx.fill()
        }
    }

}
