import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Style     1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Controls 1.0 as X


Item {

    id: _self;

    property int currentStep: 0;
    property bool tourActive: false;

    enum Position {
        Top,
        Bottom,
        Left,
        Right
    }

    enum VerticalAlignment {
        Top,
        Center,
        Bottom
    }

    enum HorizontalAlignment {
        Left,
        Center,
        Right
    }

    property var steps: []

    QtObject {
        id: _internal;
        property var current_object: undefined;
        property var pointed_object: undefined;
        property var binding_point: undefined;
        property int defaultHeight: 200;
        property int defaultWidth: 300;
        property int gap: 20;
    }

    function startTour() {
        _self.currentStep = 1;
        _self.tourActive = true;
        const options = steps[_self.currentStep - 1]
        if(_internal.current_object)
            _internal.current_object.destroy();
        const component = Qt.createComponent("TourStep.qml")

        _internal.pointed_object = _self.findChild(options.name)
        const arrowPosition = _self.getArrowPosition(options)

        if(component.status === Component.Ready) {
            _internal.current_object = component.createObject(_self.parent, {
                height: options.height ? options.height : _internal.defaultHeight,
                width: options.width ? options.width : _internal.defaultWidth,
                title: options.title,
                description: options.description,
                numSteps: _self.steps.length,
                step: options.step,
                hasButton: options.hasButton,
                buttonText: options.buttonText ? options.buttonText : "Next",
                arrowPosition: arrowPosition.position,
                arrowVerticalAlignment: arrowPosition.alignment,
                arrowHorizontalAlignment: arrowPosition.alignment,
            })

            _self.bindPosition(options.position, options.alignment)
            _next_connect.target = _internal.current_object
            _end_connect.target = _internal.current_object
        }
    }

    function nextStep() {
        _self.currentStep += 1;
        const options = steps[_self.currentStep - 1]
        _internal.current_object.destroy();
        const component = Qt.createComponent("TourStep.qml")

        _internal.pointed_object = _self.findChild(options.name)
        const arrowPosition = _self.getArrowPosition(options)

        if(component.status === Component.Ready) {
            _internal.current_object = component.createObject(_self.parent, {
                title: options.title,
                description: options.description,
                numSteps: _self.steps.length,
                step: options.step,
                hasButton: options.hasButton,
                buttonText: options.buttonText ? options.buttonText : "Next",
                arrowPosition: arrowPosition.position,
                arrowVerticalAlignment: arrowPosition.alignment,
                arrowHorizontalAlignment: arrowPosition.alignment,
            })

            _self.bindPosition(options)

            _next_connect.target = _internal.current_object
            _end_connect.target = _internal.current_object
        }

    }

    function endTour() {
        _self.currentStep = 0;
        _self.tourActive = false;
        _internal.current_object.destroy();
    }

    function bindPosition(options) {

        _internal.current_object.x = Qt.binding(function() {
            const point = _self.getPosition(options)
            return point.x
        })

        _internal.current_object.y = Qt.binding(function() {
            const point = _self.getPosition(options)
            return point.y
        })

    }


/* ***************************************************************************
;; TRIED BINDING THE POINT TOO BUT DOES NOT WORK
;; ****************************************************************************/
    /* function bindPosition(options) { */

    /*     const position = options.position */
    /*     const alignment = options.alignment */
    /*     const h = options.height ? options.height : _internal.defaultHeight */
    /*     const w = options.width ? options.width : _internal.defaultWidth */

    /*     _internal.binding_point = Qt.binding(function() { */
    /*         return _internal.pointed_object.mapToItem(null, _internal.pointed_object.x, _internal.pointed_object.y) */
    /*     }) */

    /*     _internal.current_object.x = Qt.binding(function() { */

    /*         if(position === Tour.Position.Left) return _internal.binding_point.x - w - _internal.gap */
    /*         else if(position === Tour.Position.Right) return _internal.binding_point.x + _internal.pointed_object.width + _internal.gap */
    /*         else { */
    /*             if(alignment === Tour.HorizontalAlignment.Center) */
    /*                 return _internal.binding_point.x + _internal.pointed_object.width / 2 - w / 2 */
    /*             if(alignment === Tour.HorizontalAlignment.Left) */
    /*                 return _internal.binding_point.x - w + _internal.pointed_object.width */
    /*             return _internal.binding_point.x */
    /*         } */

    /*     }) */

    /*     _internal.current_object.y = Qt.binding(function() { */

    /*         if(position === Tour.Position.Bottom) return _internal.binding_point.y  + _internal.pointed_object.height + _internal.gap */
    /*         else if(position === Tour.Position.Bottom) return _internal.binding_point.y + _internal.pointed_object.height + _internal.gap */
    /*         else { */
    /*             if(alignment === Tour.VerticalAlignment.Center) */
    /*                 return _internal.binding_point.y - h / 2 + _internal.pointed_object.height / 2 */
    /*             if(alignment === Tour.VerticalAlignment.Top) */
    /*                 return _internal.binding_point.y - h + _internal.pointed_object.height */
    /*             return _internal.binding_point.y */
    /*         } */

    /*     }) */

    /* } */

    function findChild(childName) {
        const queue = []
        queue.push(_self.parent)
        while(queue.length) {
            const currentItem = queue.shift()
            if(currentItem.objectName === childName) return currentItem;
            for(let i = 0; i < currentItem.children.length; i++)
                queue.push(currentItem.children[i])
        }
    }

    function getArrowPosition(options) {

        const pointed_object = _self.findChild(options.name)
        const point = pointed_object.parent.mapToItem(_self.parent, pointed_object.x, pointed_object.y)
        const h = options.height ? options.height : _internal.defaultHeight
        const w = options.width ? options.width : _internal.defaultWidth
        const position = options.position !== undefined ? options.position : _self.estimatePlacement(point, pointed_object, h, w)

        if(position === Tour.Position.Left || position === Tour.Position.Right) {
            const alignment = options.alignment !== undefined ? options.alignment : _self.estimateVerticalAlignment(point, pointed_object, h)
            return {
                position: position === Tour.Position.Left ? Tour.Position.Right : Tour.Position.Left,
                alignment: alignment === Tour.VerticalAlignment.Center ? alignment :
                    alignment === Tour.VerticalAlignment.Top ? Tour.VerticalAlignment.Bottom : Tour.VerticalAlignment.Top
            }
        }

        else {
            const alignment = options.alignment !== undefined ? options.alignment : _self.estimateHorizontalAlignment(point, pointed_object, w)
            return {
                position: position === Tour.Position.Top ? Tour.Position.Bottom : Tour.Position.Top,
                alignment: alignment === Tour.HorizontalAlignment.Center ? alignment :
                    alignment === Tour.HorizontalAlignment.Left ? Tour.HorizontalAlignment.Right : Tour.HorizontalAlignment.Left
            }
        }

    }

    function getPosition(options) {

        const pointed_object = _internal.pointed_object
        const point = parent.mapFromItem(pointed_object, pointed_object.x, pointed_object.y)
        console.log("objectname", parent.objectName)
        console.log("point", pointed_object.x, pointed_object.y)
        console.log("mapped", point.x, point.y)
        const h = options.height ? options.height : _internal.defaultHeight
        const w = options.width ? options.width : _internal.defaultWidth
        const position = options.position !== undefined ? options.position : _self.estimatePlacement(point, pointed_object, h, w)

        if(position === Tour.Position.Left) point.x = point.x - w - _internal.gap
        if(position === Tour.Position.Right) point.x = point.x + pointed_object.width + _internal.gap
        if(position === Tour.Position.Bottom) point.y = point.y + pointed_object.height + _internal.gap
        if(position === Tour.Position.Top) point.y = point.y - h - _internal.gap

        if(position === Tour.Position.Left || position === Tour.Position.Right) {
            const alignment = options.alignment !== undefined ? options.alignment : _self.estimateVerticalAlignment(point, pointed_object, h)
            if(alignment === Tour.VerticalAlignment.Center) point.y = point.y - h / 2 + pointed_object.height / 2
            if(alignment === Tour.VerticalAlignment.Top) point.y = point.y - h + pointed_object.height
        }

        if(position === Tour.Position.Top || position === Tour.Position.Bottom) {
            const alignment = options.alignment !== undefined ? options.alignment : _self.estimateHorizontalAlignment(point, pointed_object, w)
            if(alignment === Tour.HorizontalAlignment.Center) {
                point.x = point.x + pointed_object.width / 2 - w / 2
            }
            if(alignment === Tour.HorizontalAlignment.Left) point.x = point.x - w + pointed_object.width
        }

        return point;
    }

    function estimatePlacement(point, pointed_object, popup_h, popup_w) {

        if(point.x > popup_w + _internal.gap) {
            return Tour.Position.Left
        }
        else if(_self.parent.width - (point.x + pointed_object.width) > popup_w + _internal.gap) {
            return Tour.Position.Right
        }
        else if(_self.parent.height - (point.y + pointed_object.height) > popup_h + _internal.gap) {
            return Tour.Position.Bottom
        }
        else {
            return Tour.Position.Top
        }

    }

    function estimateHorizontalAlignment(point, pointed_object, popup_w) {

        const widthConstraint = popup_w / 2 + _internal.gap

        if(point.x > widthConstraint && _self.parent.width - (point.x + pointed_object.width / 2) > widthConstraint || pointed_object.width > popup_w) {
            return Tour.HorizontalAlignment.Center
        }
        if(point.x < widthConstraint) {
            return Tour.HorizontalAlignment.Right
        }
        else {
            return Tour.HorizontalAlignment.Left
        }
    }

    function estimateVerticalAlignment(point, pointed_object, popup_h) {

        const heightConstraint = popup_h / 2 + _internal.gap

        if(point.y > heightConstraint && _self.parent.height - (point.y + pointed_object.height / 2) > heightConstraint || pointed_object.height > popup_h) {
            return Tour.VerticalAlignment.Center
        }
        if(point.y < heightConstraint) {
            return Tour.VerticalAlignment.Bottom
        }
        else {
            return Tour.HorizontalAlignment.Top
        }
    }

    Connections {
        id: _next_connect

        target: null
        function onNext() {
            _self.nextStep()
        }
    }

    Connections {
        id: _end_connect

        target: null
        function onEnd() {
            _self.endTour()
        }
    }
}
