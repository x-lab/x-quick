
import QtQuick           2.15
import QtQuick.Controls  2.15

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Fonts    1.0 as X

X.ScientificSpinBoxReal {

    id: _self

    property bool enableGauge: true;

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentItem.implicitWidth + 2 * padding +
                            up.implicitIndicatorWidth +
                            down.implicitIndicatorWidth)

    implicitHeight: Math.max(implicitContentHeight + topPadding + bottomPadding,
                             implicitBackgroundHeight,
                             up.implicitIndicatorHeight,
                             down.implicitIndicatorHeight)

    padding: 6

    rightPadding: padding + (_self.mirrored ? (down.indicator ? down.indicator.width : 0) : (up.indicator ? up.indicator.width : 0))

    contentItem: Control {

        id: _content;

        property alias text: _text_area.text;

        background: Rectangle {
            anchors.fill: _content;

            color: X.Style.baseColor;
        }

        TextInput {

            id: _text_area

            anchors.right: parent.right

            z: Infinity;

            text: _self.displayText

            //font: _self.font
            color: X.Style.foregroundColor;
            font.weight: Font.Normal;
            font.pointSize: 11;
            //selectionColor: _self.palette.highlight
            //selectedTextColor: _self.palette.highlightedText
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter

            readOnly: !_self.editable
            validator: _self.validator
            inputMethodHints: _self.inputMethodHints

        }

        Rectangle {
            id: _gauge

            height: parent.height
            radius: 2
            width: _content.width * _self.scaleValue();

            visible: _self.enableGauge;

            color: X.Style.backgroundColor;
        }

        MouseArea {
            id: _slider
            anchors.fill: parent;
            anchors.topMargin: -6;
            anchors.bottomMargin: -6;
            enabled: _gauge.visible
        }

    }

    up.indicator: X.ButtonRaw {
        outlined: true
        flat: true
        autoRepeat: true
        x: _self.mirrored ? 0 : parent.width - width
        y: 0;
        height: parent.height / 2
        implicitWidth: 20
        implicitHeight: 20
        iconSource: "keyboard_arrow_up"
        onClicked: _self.increase();
    }


    down.indicator: X.ButtonRaw {
        outlined: true
        flat: true
        autoRepeat: true
        x: _self.mirrored ? 0 : parent.width - width
        y: height;
        height: parent.height / 2
        implicitWidth: 20
        implicitHeight: 20
        iconSource: "keyboard_arrow_down"
        onClicked: _self.decrease();
    }

    background: Rectangle {
        implicitWidth: 140
        color: X.Style.baseColor;
    }

    Component.onCompleted: {
        if (_self.locale.name !== Qt.locale("C").name) {
            console.warn("Locale cannot be changed. It is always 'C'. Change is ignored.");
            _self.locale = Qt.locale("C");
        }
        _self.localeChanged.connect((locale) => {
            if (locale.name !== _self.locale.name && locale.name !== Qt.locale("C").name) {
                console.warn("Locale must be 'C'. 'C' locale is enforced.");
                _self.locale = Qt.locale("C");
            }
        });
    }

    Binding on value {
        when: _slider.pressed
        value: _self.scaleValueFromRatio(_slider.mouseX / _content.width);
        restoreMode: Binding.RestoreNone
    }

    onValueChanged: { _gauge.width = _content.width * _self.scaleValue(); }
}
