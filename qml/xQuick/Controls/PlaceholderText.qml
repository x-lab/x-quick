import QtQuick           2.15
import QtQuick.Controls  2.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

X.Label
{
    color: X.Style.textColorDisabled;
}
