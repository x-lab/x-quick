import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import Qt.labs.platform 1.1
import Qt.labs.folderlistmodel 2.15
import Qt.labs.settings 1.0

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Models   1.0 as X

GridView {

    id: _self

    clip: true;

    Item {
        id: _internal;
        property int selected: -1;
    }

    signal folderClicked(string folderUrl)
    signal fileClicked(string fileUrl)
    signal folderDoubleClicked(string folderUrl)
    signal fileDoubleClicked(string fileUrl)
    signal fileRightClicked(string fileUrl)

    delegate: X.FinderGridDelegate {
        flickable: _self;
        isSelected: model.index === _internal.selected;
        onDoubleClicked: () => {
            if(model.fileIsDir) {
                folderDoubleClicked(model.fileUrl)
            }
            else {
                fileDoubleClicked(model.fileUrl)
            }
        }
        onClicked: (mouse) => {
            _internal.selected = model.index
            if(model.fileIsDir) {
                folderClicked(model.fileUrl)
            }
            else {
                if(mouse.button == Qt.RightButton) {
                    fileRightClicked(model.fileUrl)
                } else {
                    fileClicked(model.fileUrl)
                }
            }
        }
    }

    ScrollBar.vertical: ScrollBar { visible: _self.contentHeight > _self.height; }
}
