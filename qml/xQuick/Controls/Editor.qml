import QtQuick            2.15
import QtQuick.Controls   2.15
import Qt5Compat.GraphicalEffects

import xQuick             1.0 as X
import xQuick.Controls    1.0 as X
import xQuick.Fonts       1.0 as X
import xQuick.Style       1.0 as X

Control {
    id: root;

    property alias edit: _textEdit;
    property alias document: _textEdit.textDocument;
    property alias actualContents: _textEdit.text;

    property alias errors: _error_model;
    property alias completions: _completion_model;

    property string contents;

    property alias display_lines: _lines.visible;
    property alias display_highl: _highl.visible;

    clip: true;

    ListModel {
        id: _completion_model;
    }

    Flickable {
        id: _flickable;

        anchors.left: _lines.right;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        anchors.right: parent.right;

        contentWidth: _textEdit.width;
        contentHeight: _textEdit.height;

        boundsBehavior: Flickable.StopAtBounds

        function updateScrollX(x) {
            if (contentX >= x) {
                contentX = x;
            } else if (contentX + width <= x) {
                contentX = x + 1 - width;
            }
        }

        function updateScrollY(y) {
            if (contentY >= y) {
                contentY = y;
            } else if (contentY + height <= y + _textEdit.cursorHeight) {
                contentY = y + _textEdit.cursorHeight - height;
            }
        }

        Rectangle {

            id: _highl;

            anchors.left: parent.left;
            anchors.right: parent.right;

            y: _textEdit.cursorY;
            height: _textEdit.cursorHeight;

            color: "#22000000";
        }

        TextEdit {
            id: _textEdit;

            readOnly: false;

            font.family: X.Style.monoFamily;
            font.pointSize: 11;

            selectByMouse: true
            mouseSelectionMode: TextEdit.SelectCharacters
            selectionColor: X.Style.selectionColor;
            color: X.Style.textColor;

            textFormat: TextEdit.PlainText
            wrapMode: TextEdit.WordWrap

            width: root.width - _lines.width;
            height: Math.max(implicitHeight, root.height);

            leftPadding: 4;

            property int cursorX: cursorRectangle.x;
            property int cursorY: cursorRectangle.y;
            property int cursorHeight: cursorRectangle.height;
            property bool showCursor: true;

            text: root.contents;

            onCursorXChanged: {
                _flickable.updateScrollX(cursorX);
            }

            onCursorYChanged: {
                _flickable.updateScrollY(cursorY);
            }

            cursorDelegate: Rectangle {
                width: 1;
                color: "orange";
                visible: _textEdit.showCursor;

                X.Popup {
                    id: _completion_popup;

                    y: _textEdit.cursorHeight + 10;

                    width: 200
                    height: Math.min(_completion_model.count * 32, 300); // _completion_delegate.height

                    clip: true;
                    modal: false;
                    focus: false;
                    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

                    visible: _completion_model.count != 0;

                    topPadding: 0;
                    leftPadding: 0;
                    rightPadding: 0;
                    bottomPadding: 0;

                    ListView {

                        id: _completion_popup_view;

                        anchors.fill: parent;

                        model: _completion_model;

                        clip: true;

                        delegate: ItemDelegate {

                            id: _completion_delegate;

                            height: 32;

                            text: model.candidate;
                            width: _completion_popup_view.width;

                            onClicked: _completion_popup.close();

                            font: _textEdit.font;
                        }
                    }

                    // layer.enabled: true
                    // layer.effect: OpacityMask {
                    //     maskSource: Rectangle {
                    //         width: _completion_popup.width
                    //         height: _completion_popup.height
                    //         radius: 10
                    //     }
                    // }
                }
            }
        }

        Repeater {

            model: ListModel {
                id: _error_model;
            }

            delegate: Rectangle {
                id: root;

                anchors.left: parent.left;
                anchors.right: parent.right;

                height: _textEdit.cursorHeight;
                y: (Math.max(model.line - 1, 0)) * height;
                color: "#22C54632";

                X.Icon {
                    anchors.verticalCenter: parent.verticalCenter;
                    anchors.right: parent.left;

                    icon: X.Icons.icons.chevron_right;
                    color: "#933628";
                    size: _textEdit.font.pointSize;
                }

                Rectangle {
                    anchors.right: parent.right;
                    anchors.top: parent.top;
                    anchors.bottom: parent.bottom;
                    anchors.rightMargin: 2;

                    width: _error.width + 16;
                    radius: 2;
                    color: "#933628"

                    X.Icon {
                        anchors.verticalCenter: parent.verticalCenter;
                        anchors.right: parent.left;
                        anchors.rightMargin: 4;

                        icon: X.Icons.icons.switch_left;
                        color: "#933628";
                        size: _textEdit.font.pointSize;
                    }

                    Row {
                        id: _error;

                        anchors.centerIn: parent;
                        spacing: 8;

                        X.Icon {
                            anchors.verticalCenter: parent.verticalCenter;
                            icon: X.Icons.icons.warning;
                            size: _textEdit.font.pointSize - 1;
                            color: "#eeeeee";
                        }

                        X.Label {
                            anchors.verticalCenter: parent.verticalCenter;
                            font.pointSize: _textEdit.font.pointSize;
                            text: model.message;
                            color: "#eeeeee";
                        }
                    }
                }
            }
        }

        ScrollIndicator.horizontal: ScrollIndicator {
            visible: _flickable.contentWidth > _flickable.width;
        }
        ScrollIndicator.vertical: ScrollIndicator {
            visible: _flickable.contentHeight > _flickable.height;
        }
    }

    Item {
        id: _lines;

        anchors.left: parent.left;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;

        width: _lines.visible ? _linesCol.width + 4 : 0;

        Column {
            id: _linesCol;

            y: -_flickable.contentY;

            Repeater {
                model: _textEdit.lineCount;

                delegate: Text {
                    anchors.right: parent.right;
                    rightPadding: 8;
                    leftPadding: 8;

                    font.family: _textEdit.font.family;
                    font.pointSize: _textEdit.font.pointSize;

                    verticalAlignment: Text.AlignVCenter;

                    color: X.Style.textColor;
                    text: index + 1;
                }
            }
        }

        X.Separator {
            anchors.top: parent.top;
            anchors.bottom: parent.bottom;
            anchors.right: parent.right;
        }
    }
}
