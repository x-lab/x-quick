import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Fonts    1.0 as X

Pane
{
    id: self;
    state: "hide";

    default property alias _contentChildren: _content.data

    property color backgroundColor: X.Style.variant == "LIGHT"
        ? Qt.lighter(X.Style.backgroundColor, 1.2)
        : Qt.darker(X.Style.backgroundColor, 1.2);

    property color borderColor: X.Style.borderColor;
    property bool outlined: false;
    property string orientation: "left";
    property real size: parent.width / 3;
    property bool centerIcon: false;

    anchors.top: self.orientation === "left" || self.orientation === "right" ? parent.top : undefined;
    anchors.bottom: self.orientation === "left" || self.orientation === "right" ? parent.bottom : undefined;

    anchors.left: self.orientation === "top" || self.orientation === "bottom" ? parent.left : undefined;
    anchors.right: self.orientation === "top" || self.orientation === "bottom" ? parent.right : undefined;

    width: self.orientation === "left" || self.orientation === "right" ? self.size : undefined
    height: self.orientation === "top" || self.orientation === "bottom" ? self.size : undefined


    Pane {
        id: _content
        anchors.fill: parent
        anchors.margins: 30
    }

    Rectangle {
        id: _button
        height: 30
        width: 30
        color: "transparent"

        X.Icon {
            id: _icon;
            anchors.fill: parent;
            icon: self.orientation === "left"
                ? X.Icons.icons.keyboard_arrow_right
                : self.orientation === "right"
                ? X.Icons.icons.keyboard_arrow_left
                : self.orientation === "top"
                ? X.Icons.icons.keyboard_arrow_down
                : X.Icons.icons.keyboard_arrow_up
            size: 30;

        }

        anchors.right: self.orientation === "right" || (self.centerIcon && self.orientation !== "left")
            ? undefined
            : parent.right
        anchors.left: self.orientation === "right"
            ? parent.left
            : undefined

        anchors.verticalCenter: (self.orientation === "left" || self.orientation === "right") && self.centerIcon ? parent.verticalCenter : undefined
        anchors.horizontalCenter: (self.orientation === "top" || self.orientation === "bottom") && self.centerIcon ? parent.horizontalCenter : undefined
        anchors.top: self.orientation === "top" || (self.centerIcon && self.orientation !== "bottom")
            ? undefined : parent.top
        anchors.bottom: self.orientation === "top" ? parent.bottom : undefined

        /* anchors.topMargin: width/2 */

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                self.state === "hide" ? self.state = "show" : self.state = "hide"
            }
        }
    }

    states: [
        State{
            name: "hide"
            PropertyChanges {
                target: self;
                x: orientation === "left"
                    ? -size + 20
                    : orientation === "right"
                    ? parent.width - 20
                    : 0
                ;
                y : orientation === "top"
                    ? -size + 20
                    : orientation === "bottom"
                    ? parent.height - 20
                    : 0
            }
            PropertyChanges {
                target: _icon;
                rotation: 0;
            }
            PropertyChanges {
                target: _content;
                opacity: 0;
            }
        },
        State{
            name: "show"
            PropertyChanges {
                target: _icon;
                rotation: 180;
            }
            PropertyChanges {
                target: self;
                x: orientation === "right" ? parent.width - size : 0;
                y: orientation === "bottom" ? parent.height - size : 0;
            }
            PropertyChanges {
                target: _content;
                opacity: 1;
            }
        }
    ]

    transitions: [
        Transition{
            to:"hide"
            NumberAnimation{
                duration:500
                properties: "x, y, rotation, opacity"
                easing.type: Easing.InCubic
            }
        },
        Transition{
            to:"show"
            NumberAnimation{
                duration:500
                properties: "x, y, rotation, opacity"
                easing.type: Easing.OutCubic
            }
        }
    ]
}
