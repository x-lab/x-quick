import QtQuick      2.15

import xQuick.Style 1.0 as X

Text
{
    id: _control;

    FontLoader {
    	id: _control_loader;
        source: "qrc:///qml/xQuick/Fonts/Icons.ttf";
    }

    property alias icon: _control.text; // TODO: See https://fonts.google.com/icons?selected=Material+Icons
    property alias size: _control.font.pixelSize;
    property bool flip: false;

    signal clicked;

    color: enabled ? X.Style.textColor : Qt.darker(X.Style.textColor);

    font.family: _control_loader.name;

    visible: _control.icon != ""

    transform: Scale {

        id: _control_transform;

        origin.x: _control.width/2;

        xScale: _control.flip ? -1 : 1;
    }
}
