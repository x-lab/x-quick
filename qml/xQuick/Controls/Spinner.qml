/*                              -*- Mode: Qml -*-
;;
;; Version: $Id$
;;
*/

/*Commentary:
;;
*/

/*Change Log:
;;
*/

/*Code:*/


import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import Qt.labs.platform 1.1
import Qt.labs.folderlistmodel 2.15
import Qt.labs.settings 1.0

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Fonts    1.0 as X


Item {
    id: _self;

    property int size: 60;
    property string color: X.foregroundColor;
    property string type: "circles";
    property bool running: true;

    height: _self.size;
    width: _self.size;

    Item {
        id: _internal;
        property var sources: {
            circles: _circles;
        }
    }

    Loader {
        anchors.fill: parent;
        sourceComponent: _internal.sources[_self.type];
    }

    Item {
        id: _circles;

        property int index: 0;

        Repeater {

            id: _circles_repeater;

            model: 8;

            Rectangle {

                id: _rect;

                height: _self.size / 4;
                width: _self.size / 4;
                radius: _self.size / 4;

                color: "black";

                x: 0;
                y: 0;
                opacity: _circles.getOpacity(index);

                transform: Rotation {
                    origin.x: _self.size / 2;
                    origin.y: _self.size / 2;
                    angle: index * (360 / 8)
                }

            }
        }

        Timer {

            id: _circles_timer;

            interval: 100;
            running: _self.running;
            repeat: true;

            onTriggered: {
                _circles.index = (_circles.index + 1) % 8;
                for(let i = 0; i < 8; i++) {
                    _circles_repeater.itemAt(i).opacity = _circles.getOpacity(i);
                }
            }
        }

        function getOpacity(index) {

            let opacityIndex = (index - _circles.index)
            if(opacityIndex >= 0 && opacityIndex < 4) return (100 - opacityIndex * 20) / 100;
            return 0.2;

        }

    }
}

/*Spinner.qml ends here*/
