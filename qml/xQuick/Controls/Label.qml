import QtQuick           2.15
import QtQuick.Controls  2.15

import xQuick.Style      1.0 as X

Label
{
    id: root

    font {

        family: X.Style.fontFamily;
        pixelSize: 12
        weight: Font.Normal
        capitalization: Font.MixedCase
    }

    color: X.Style.textColor;
}
