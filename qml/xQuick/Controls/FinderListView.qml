import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import Qt.labs.platform 1.1
import Qt.labs.folderlistmodel 2.15
import Qt.labs.settings 1.0

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X
import xQuick.Fonts    1.0 as X

ListView {

    id: _self

    clip: true;

    Item {
        id: _internal;
        property int selected: -1;
    }

    signal folderClicked(string folderUrl)
    signal fileClicked(string fileUrl)
    signal folderDoubleClicked(string folderUrl)
    signal fileDoubleClicked(string fileUrl)
    signal fileRightClicked(string fileUrl)

    /* header: RowLayout { */
    /*     id: _header; */

    /*     anchors.left: _self.left; */
    /*     anchors.right: _self.right; */

    /* } */

    delegate: X.FinderListDelegate {

        width: _self.width

        flickable: _self
        isSelected: model.index === _internal.selected;

        onDoubleClicked: () => {
            if(model.fileIsDir) {
                _self.folderDoubleClicked(model.fileUrl)
            }
            else {
                _self.fileDoubleClicked(model.fileUrl)
            }
        }
        onClicked: (mouse) => {
            _internal.selected = model.index;
            if(model.fileIsDir) {
                _self.folderClicked(model.fileUrl)
            }
            else {
                if(mouse.button == Qt.RightButton) {
                  _self.fileRightClicked(model.fileUrl)
                } else {
                  _self.fileClicked(model.fileUrl)
                }
            }
        }
    }

    ScrollBar.vertical: ScrollBar { visible: _self.contentHeight > _self.height; }
}
