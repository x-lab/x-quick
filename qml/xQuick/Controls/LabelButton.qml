import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.button.family;
    font.pixelSize:      X.Style.typography.button.pixelSize;
    font.weight:         X.Style.typography.button.weight;
    font.capitalization: X.Style.typography.button.capitalization;
    font.letterSpacing:  X.Style.typography.button.letterSpacing;
}
