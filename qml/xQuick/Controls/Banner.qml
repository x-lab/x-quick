import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Control
{
    id: _control

    property string text
    property alias action: _action.text

    signal actionPressed();

    padding: 0

    property alias radius: _background.radius;
    property alias color:  _background.color;

    property bool expandable: false;

    contentItem: Item
    {
        id: _content

        implicitWidth: _control.expandable ? (Math.max(200, _label.implicitWidth + _action.implicitWidth) + 10 * (_control.action != "" ? 3 : 2)) : 200;
        implicitHeight: (_label.lineCount > 1) ? 64 : 48;

        X.Label
        {
            id: _label
            text: _control.text
            anchors.verticalCenter: _content.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 10;
            anchors.right: (_control.action != "") ? _action.left : parent.right
            anchors.rightMargin: 10;

            maximumLineCount: _control.expandable ? 1 : 2
            wrapMode: _control.expandable ? Text.NoWrap : Text.WordWrap
            elide: (_control.mirrored ? Text.ElideLeft : Text.ElideRight)

            color: X.Style.foregroundColor;

        }

        X.ButtonFlat
        {
            id: _action
            visible: text
            onPressed: _control.actionPressed()
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: _content.verticalCenter
            anchors.verticalCenterOffset: _label.lineCount > 1 ? implicitHeight / 6 : 0

            backgroundColor: X.Style.variant == "LIGHT"
                ? Qt.lighter(X.Style.backgroundColor, 1.2)
                : Qt.darker(X.Style.backgroundColor, 1.2);
        }
    }

    background: Rectangle {

        id: _background;

        anchors.fill: _control;

        radius:  X.Style.controls.radius;
        color: X.Style.backgroundColor;
    }
}
