import QtQuick      2.15

import xQuick.Shell 1.0 as XS
import xQuick.Style 1.0 as X

XS.ObjectDestructItem {
    id: cursor

    property real fontHeight
    property real fontWidth

    height: fontHeight
    width: fontWidth
    x: objectHandle.x * fontWidth
    y: objectHandle.y * fontHeight
    z: 1.1

    visible: objectHandle.visible

    Rectangle {
        anchors.fill: parent;

        color: X.Style.foregroundColor;
    }
}

