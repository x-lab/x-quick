import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.headline2.family;
    font.pixelSize:      X.Style.typography.headline2.pixelSize;
    font.weight:         X.Style.typography.headline2.weight;
    font.capitalization: X.Style.typography.headline2.capitalization;
    font.letterSpacing:  X.Style.typography.headline2.letterSpacing;
}
