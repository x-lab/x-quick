import QtQuick          2.15
import QtQuick.Controls 2.15

// import Qt5Compat.GraphicalEffects

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Button
{
    id: _control

    font: X.Style.typography.button
    spacing: 0;

    icon.width: 10;
    icon.height: 10;
    icon.color: foregroundColor

    property alias iconSource: _contents.iconSource;

    property bool flipped: false
    property bool outlined: false
    property double radius: X.Style.controls.radius;

    implicitWidth: 100
    implicitHeight: 32

    property color foregroundColor:
    {
        if(!enabled)
            return X.Style.textColorDisabled

        if(flat && highlighted)
            return X.Style.accentColor;

        return highlighted ? X.Style.textColor : X.Style.textColor;
    }

    property color backgroundColor:
    {
        if(flat)
            return X.Style.backgroundColor;

        if(!enabled)
            return Qt.darker(X.Style.backgroundColor, 1.1);

        return highlighted ? X.Style.accentColor : X.Style.backgroundColor
    }

    property color outlinedColor:
    {
        if(!outlined)
            return "transparent"

        if(!enabled)
            return Qt.darker(X.Style.borderColor);

        if(pressed)
            return foregroundColor

        return X.Style.borderColor;
    }

    contentItem: X.LabelIcon
    {
        id: _contents;

        font: _control.font
        spacing: _control.spacing

        iconSize: _control.icon.width
        iconColor: _control.icon.color
        iconFlip: _control.flipped;

        text: _control.text
        color: _control.foregroundColor
    }

    background: Rectangle
    {
        implicitWidth: _control.implicitWidth;
        implicitHeight: _control.implicitHeight;
        radius: _control.radius;
        color: _control.backgroundColor;

        border.width: {
            if (_control.outlined)
                return 1;

            return 0
        }

        border.color: _control.outlinedColor
    }
}
