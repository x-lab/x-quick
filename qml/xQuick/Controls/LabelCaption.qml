import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

X.Label
{
    id: root

    font.family:         X.Style.typography.caption.family;
    font.pixelSize:      X.Style.typography.caption.pixelSize;
    font.weight:         X.Style.typography.caption.weight;
    font.capitalization: X.Style.typography.caption.capitalization;
    font.letterSpacing:  X.Style.typography.caption.letterSpacing;
}
