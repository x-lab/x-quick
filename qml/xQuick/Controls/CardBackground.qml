import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Style     1.0 as X

Rectangle
{
    id: control
    property bool enabled: true
    property bool isActive: false
    property bool outlined: false;

    color: X.Style.cardColor;

    property color borderColor: enabled ? Qt.lighter(X.Style.borderColor) : X.Style.borderColor;

    radius: X.Style.card.radius

    border.width: outlined ? 1 : 0
    border.color: borderColor

    Behavior on border.color
    {
        ColorAnimation
        {
            duration: 250
            easing.type: Easing.OutCubic
        }
    }
}
