import QtQuick            2.15
import QtQuick.Controls   2.15
import Qt5Compat.GraphicalEffects
import QtQuick.Layouts    1.15

import xQuick             1.0 as X
import xQuick.Controls    1.0 as X
import xQuick.Fonts       1.0 as X
import xQuick.Style       1.0 as X

Control {

    id: _control;

    implicitWidth: 100;
    implicitHeight: 32;

    default property alias contents: _layout.children;

    property alias exclusive : _group.exclusive;

    ButtonGroup {
        id: _group;

        buttons: _layout.children;
    }

    RowLayout {

        id: _layout;

        spacing: 0;

        anchors.fill: parent;
    }

    layer.enabled: true
    layer.effect: OpacityMask
    {
        maskSource: Rectangle
        {
            width: _control.width
            height: _control.height
            radius: 50;

            // Rectangle
            // {
            //     width: parent.width
            //     y: _control.radius
            //     height: parent.height - radius
            // }
        }
    }
}
