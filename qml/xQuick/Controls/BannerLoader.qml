import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Item
{
    id: _root

    property bool canBeKilled: (settings && (settings.canBeKilled !== undefined)) ? settings.canBeKilled : true
    property bool displayBanner: false

    z: Infinity

    width: parent ? parent.width : 200

    anchors.top: parent ? parent.top : undefined
    anchors.bottomMargin: 10;
    anchors.bottom: parent ? parent.bottom : undefined

    Component
    {
        id: _banner_component

        X.Banner
        {
            id: _banner

            opacity: 0

            property real bannerTimeout: _root.settings.timeout === undefined ? 4000 : _root.settings.timeout
            property bool actionCalled
            property bool pendingDestroy

            text: _root.settings && _root.settings.text ? _root.settings.text : ""
            action: _root.settings && _root.settings.action ? _root.settings.action : ""

            onActionPressed:
            {
                if(_root.settings && _root.settings.onAccept)
                {
                    _root.settings.onAccept()
                    actionCalled = true
                    goToPendingDestroy()
                }
            }

            function goToPendingDestroy()
            {
                pendingDestroy = true
                fadeDuration = displayBanner ? outFadeDuration : inFadeDuration
                opacity = 0
                _timer.interval = fadeDuration
                _timer.start()
            }

            function destroyBanner()
            {
                _bannerLoader.sourceComponent = null

                if(!actionCalled && _root.settings && _root.settings.onClose)
                _root.settings.onClose()

                _root.popBanner()
            }

            Connections
            {
                target: _root
                function onDisplayBannerChanged()
                {
                    if(!_root.displayBanner && !pendingDestroy)
                    goToPendingDestroy()
                }
            }

            Timer
            {
                id: _timer
                interval: bannerTimeout
                running: true
                onTriggered:
                {
                    if(pendingDestroy)
                    destroyBanner()
                    else
                    goToPendingDestroy()
                }
            }

            readonly property real inFadeDuration: 100
            readonly property real outFadeDuration: 300
            property real fadeDuration

            Behavior on opacity
            {
                NumberAnimation
                {
                    duration: fadeDuration
                    easing.type: Easing.OutQuad
                }
            }

            Component.onCompleted:
            {
                fadeDuration = inFadeDuration
                opacity = 1
            }
        }
    }

    Loader
    {
        id: _bannerLoader

        anchors.top: parent.top;
        anchors.topMargin: 10;
        anchors.right: parent.right;
        anchors.rightMargin: 10;
    }

    property var settings: null
    property var settingsQueue: []

    /**
    * Show a banner immediatly or after the current banner.
    * The settings object have multiple optionnal field
    * - text: Text that will be displayed in the banner
    * - action: Action button text, if not specified, no action button will appear
    * - timeout: Time to display the banner in milliseconds. Default is 2000 ms.
    * - onAccept: Callback function called when the action button is pressed
    * - onClose: Callback function called when the banner disapear on no action button have been pressed
    * - canBeKilled: If true, the banner is immediately replaced by a new one on next call of show. By default true.
    */
    function show(config)
    {
        if(_bannerLoader.sourceComponent)
        {
            if(canBeKilled)
            {
                displayBanner = false
                if (settingsQueue.length)
                    settingsQueue.length = 0
            }
            settingsQueue.push(config)
        }
        else
        {
            settings = config
            displayBanner = true

            _bannerLoader.sourceComponent = _banner_component
        }
    }

    /**
    * Try to load the next available banner in the queue.
    * This function is called when the current banner get destroyed
    */
    function popBanner()
    {
        var nextBannerSetting = settingsQueue.shift()
        if (nextBannerSetting)
            show(nextBannerSetting)
    }
}
