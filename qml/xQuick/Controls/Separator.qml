import QtQuick      2.15

import xQuick.Style 1.0 as X

Rectangle {
    color: X.Style.borderColor;

    implicitWidth: 1;
    implicitHeight: 1;
}
