import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQml            2.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Item {

    id: root

    property string text
    property color color: root.enabled ? X.Style.textColor : X.Style.textColorDisabled
    property font font: X.Style.typography.body2
    property int elide: Text.ElideRight
    property int wrapMode: Text.NoWrap
    property int maximumLineCount: Number.MAX_SAFE_INTEGER

    property alias iconSource: _icon.icon;
    property color iconColor: root.color;
    property int iconSize: 24;
    property bool iconFlip: false;

    property int alignment: Qt.AlignCenter;

    property int spacing: 4;

    implicitHeight: font.pixelSize;

    RowLayout {

        anchors.centerIn: parent;

        X.Icon
        {
            id: _icon;

            color: root.iconColor;
            size: root.iconSize;
            flip: root.iconFlip;
        }

        X.Label
        {
            id: _label;

            text: root.text
            font: root.font
            color: root.color
            elide: root.elide
            wrapMode: root.wrapMode
            maximumLineCount: root.maximumLineCount
        }

        // Rectangle // DEBUG
        // {
        //     color: "transparent"
        //     anchors.fill: parent;
        //     border.width: 1
        //     border.color: "#E91E63"
        // }
    }
}
