import QtQuick          2.15
import QtQuick.Controls 2.15

import Qt.labs.settings 1.0

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

ApplicationWindow {

    id: _control;

    FontLoader { source: "qrc:///qml/xQuick/Fonts/Comfortaa-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Comfortaa-Light.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Comfortaa-Medium.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Comfortaa-Regular.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Comfortaa-SemiBold.ttf"; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Black.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-BlackItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Bold.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-BoldItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-ExtraBold.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-ExtraBoldItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-ExtraLight.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-ExtraLightItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Italic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Light.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-LightItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Medium.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-MediumItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Regular.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-SemiBold.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-SemiBoldItalic.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-Thin.ttf" ; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Montserrat-ThinItalic.ttf" ; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-BoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-ExtraBold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-ExtraBoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-Italic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-Light.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-LightItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-Regular.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-SemiBold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/OpenSans-SemiBoldItalic.ttf"; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Black.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-BlackItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-BoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Italic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-LightItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Medium.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-MediumItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Regular.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Thin.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-Light.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Roboto-ThinItalic.ttf"; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Black.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-BlackItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-BoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-ExtraLight.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-ExtraLightItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Italic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Light.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-LightItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Medium.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-MediumItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-Regular.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-SemiBold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/SourceCodePro-SemiBoldItalic.ttf"; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-BoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-Italic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-Light.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-LightItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-Medium.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-MediumItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/Ubuntu-Regular.ttf"; }

    FontLoader { source: "qrc:///qml/xQuick/Fonts/UbuntuMono-Bold.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/UbuntuMono-BoldItalic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/UbuntuMono-Italic.ttf"; }
    FontLoader { source: "qrc:///qml/xQuick/Fonts/UbuntuMono-Regular.ttf"; }

    visible: true;

    readonly property bool inPortrait: _control.width < _control.height;

    X.BannerLoader {
        id: _bannerLoader;
    }

    Component.onCompleted: {
        X.BannerManager.bannerLoader = _bannerLoader;
    }

    Component.onDestruction: {

    }
}
