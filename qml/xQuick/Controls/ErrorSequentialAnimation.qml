import QtQuick           2.15
import QtQuick.Controls  2.15

import xQuick.Controls   1.0 as X
import xQuick.Style      1.0 as X

SequentialAnimation
{
  id: _root
  property var target
  property double x: 0
  running: false
  NumberAnimation { target: _root.target;property: "x";to: _root.x + 4;duration: 50 }
  NumberAnimation { target: _root.target;property: "x";to: _root.x - 4;duration: 50 }
  NumberAnimation { target: _root.target;property: "x";to: _root.x;duration: 50 }
}
