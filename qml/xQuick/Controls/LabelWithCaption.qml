import QtQuick         2.15

import xQuick.Controls 1.0 as X
import xQuick.Style    1.0 as X

Item
{
    id: root

    property alias text: textLabel.text
    property alias textFont: textLabel.font
    property color textColor: Qaterial.Style.colorTheme.primaryText

    property alias caption: captionLabel.text
    property alias captionFont: captionLabel.font
    property color captionColor: Qaterial.Style.colorTheme.secondaryText

    implicitWidth: Math.max(textLabel.implicitWidth, captionLabel.implicitWidth)
    implicitHeight: (text ? textLabel.implicitHeight : 0) + (caption ? captionLabel.implicitHeight : 0) + (text && caption ? spacing : 0)

    property int horizontalAlignment: Qt.AlignLeft
    property int verticalAlignment: Qt.AlignVCenter
    property int elide: Text.ElideRight
    property int wrapMode: Text.WordWrap
    property int maximumLineCount: 1
    property int spacing: 0

    X.Label
    {
        id: textLabel

        anchors {
            right: parent.right
            left: parent.left
        }

        y:
        {
            if(root.verticalAlignment === Qt.AlignVCenter)
                return Math.floor((root.height - root.implicitHeight) / 2)
            if(root.verticalAlignment === Qt.AlignBottom)
                return root.height - root.implicitHeight
            return 0
        }

        height: text ? implicitHeight : 0

        color: enabled ? root.textColor : X.Style.textColorDisabled;
        elide: root.elide
        horizontalAlignment: root.horizontalAlignment
    }

    X.Label
    {
        id: captionLabel

        anchors {
            right: parent.right
            left: parent.left
        }

        y: textLabel.y + textLabel.height + (textLabel.text ? root.spacing : 0)

        height: text ? implicitHeight : 0

        color: enabled ? root.captionColor : X.Style.disabledTextColor
        elide: root.elide
        horizontalAlignment: root.horizontalAlignment
        wrapMode: root.wrapMode
        maximumLineCount: root.maximumLineCount
    }
}
