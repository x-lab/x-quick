import QtQuick 2.15

import xQuick.Style 1.0 as X

Rectangle
{
    id: root

    color: X.Style.accentColor;
    width: 1.5
    visible: parent.activeFocus && !parent.readOnly && parent.selectionStart === parent.selectionEnd
}
