import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xLogger          1.0 as L

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Control
{
    id: _logger;

    signal itemsChanged();

    ColumnLayout {

        anchors.fill: parent;

        spacing: 0;

        RowLayout {

            Layout.fillWidth: true;
            Layout.minimumHeight: 32;
            Layout.maximumHeight: 32;

            Label {
                text: "Type"

                Layout.leftMargin: 10;
                Layout.minimumWidth: 64;
                Layout.maximumWidth: 64;
            }

            X.Separator { Layout.fillHeight: true; }

            Label {
                text: "Description";

                Layout.fillWidth: true;
            }

            X.Separator { Layout.fillHeight: true; }

            Label {
                text: "Time"

                Layout.minimumWidth: 64;
                Layout.maximumWidth: 64;
            }
        }

        X.Separator {
            Layout.fillWidth: true;
        }

        ListView {

            id: _console_list;

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            clip: true;

            model: L.Controller.items;

            delegate: X.ConsoleDelegate {

            }

            ScrollBar.vertical: ScrollBar { visible: _console_list.contentHeight > _console_list.height; }
        }

        X.Separator {
            Layout.fillWidth: true;
        }

        RowLayout {
            Layout.fillWidth: true;
            Layout.leftMargin: 8;
            Layout.rightMargin: 8;
            Layout.minimumHeight: 32;
            Layout.maximumHeight: 32;

            spacing: 10;

            X.Badge {

                text: L.Controller.infoCount;
                backgroundColor: X.Style.backgroundColor;
                foregroundColor: X.Style.infoColor;

                icon.source: X.Icons.icons.info;
                icon.width: 16;
                icon.color: "#222222";

                Layout.minimumWidth: 42;
                Layout.maximumWidth: 42;
            }

            X.Badge {

                text: L.Controller.debugCount;
                backgroundColor: X.Style.backgroundColor;
                foregroundColor: X.Style.debugColor;

                icon.source: X.Icons.icons.info;
                icon.width: 16;
                icon.color: "#222222";

                Layout.minimumWidth: 42;
                Layout.maximumWidth: 42;
            }


            X.Badge {

                text: L.Controller.warningCount;
                backgroundColor: X.Style.backgroundColor;
                foregroundColor: X.Style.warningColor;

                icon.source: X.Icons.icons.warning;
                icon.width: 16;
                icon.color: "#222222";

                Layout.minimumWidth: 42;
                Layout.maximumWidth: 42;
            }

            X.Badge {

                text: L.Controller.errorCount;
                backgroundColor: X.Style.backgroundColor;
                foregroundColor: X.Style.errorColor;

                icon.source: X.Icons.icons.error;
                icon.width: 16;
                icon.color: "#222222";

                Layout.minimumWidth: 42;
                Layout.maximumWidth: 42;
            }

            X.Separator { Layout.fillHeight: true; }

            Item {
                Layout.fillWidth: true;
            }

            X.Separator { Layout.fillHeight: true; }

            X.ButtonRound {

                text: 'Clear';
                implicitHeight: 24;
                outlined: true;

                onClicked: L.Controller.clear();
            }
        }
    }

    Connections {
        target: L.Controller;

        function onItemsChanged() {
            _logger.itemsChanged();
        }
    }

    background: Rectangle {
        color: X.Style.alternateBaseColor;
    }
}
