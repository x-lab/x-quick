import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Style     1.0 as X

Item
{
    implicitHeight: _label.font.pixelSize * 3;
    implicitWidth: 200;
    property alias supportingText: _label.text;

    Label
    {
        id: _label;
        wrapMode: Text.WordWrap;
        elide: Text.ElideRight;
        maximumLineCount: 2;
        verticalAlignment: Text.AlignVCenter;
        anchors.fill: parent;
    }
}
