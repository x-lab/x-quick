import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

X.ButtonRaw {

    id: _control;

    Layout.fillWidth: true;

    // flat: true;
    radius: 0;

    implicitHeight: parent.height;

    foregroundColor: X.Style.foregroundColor;
    backgroundColor: {
        if(_control.pressed || _control.down)
            return Qt.darker(X.Style.backgroundColor, 1.3)

        if(_control.hovered)
            return Qt.lighter(X.Style.backgroundColor, 1.1);

        if(_control.checked)
            return Qt.darker(X.Style.backgroundColor, 1.1);

        return X.Style.backgroundColor;
    }

    function isFirst(item) {

        return parent.children[0] == item;
    }

    function isLast(item) {
        return parent.children[parent.children.length - 1] == item;
    }

    X.Separator {
        anchors.top: parent.top;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        visible: !isLast(_control);

        color: X.Style.baseColor;
    }
}
