import QtQuick          2.15
import QtQuick.Controls 2.15

//import Qt5Compat.GraphicalEffects

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

X.ButtonRound
{
    id: _control

    topInset: 0;

    font.pixelSize: 8;
    font.capitalization: Font.AllUpperCase;
    font.weight: Font.Thin;

    implicitHeight: 20;

    Rectangle
    {
        color: "transparent"
        anchors.fill: parent;
        border.width: 1
        border.color: "#E91E63"
        visible: false;
    }
}
