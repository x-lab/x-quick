import QtQuick            2.15
import QtQuick.Controls   2.15
import QtQuick.Layouts    1.15
//import QtGraphicalEffects 1.15

import Qt5Compat.GraphicalEffects

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Models    1.0 as X
import xQuick.Style     1.0 as X

import "." as XG

Control {

    id: _control;

    width: 300;

    function opened() { return _control.width != 0 }
    function open() { _control.width = 300; }
    function close() { _control.width = 0; }

    clip: true;

    Behavior on width {
        NumberAnimation {
            easing {
                type: Easing.OutElastic
                amplitude: 1.0
                period: 0.5
            }
        }
    }

    X.LabelHeadline5 {

        id: _headline

        text: "Gallery";

        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.top: parent.top;
        anchors.topMargin: 42;
    }

    X.ButtonRound {

        id: _s_b;

        anchors.top: parent.top;
        anchors.topMargin: 10;
        anchors.right: parent.right;
        anchors.rightMargin: 10;

        iconSource: X.Icons.icons.close;

        onClicked: _drawer.close();

        height: 22;
        width: 48;

        visible: window.inPortrait;
    }


    X.Separator {
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: listView.top;

        color: X.Style.variant == 'LIGHT'
            ? Qt.darker(X.Style.borderColor, 1.2)
            : Qt.lighter(X.Style.borderColor, 1.2);
    }

    TextField {

        id: _field;

        height: 32;

        anchors.top: _headline.bottom;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 10;

        onTextChanged: _model.refilter();

        X.Icon {
            icon: X.Icons.icons.search;
            color: X.Style.foregroundColor;

            anchors.right: parent.right;
            anchors.rightMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
        }
    }

    ListView {
        id: listView

        clip: true;
        focus: true
        currentIndex: -1
        anchors.topMargin: 132
        anchors.fill: parent


        section.property: "section"
        section.criteria: ViewSection.FullString
        section.delegate: ItemDelegate {

            id: _section_delegate;

            required property string section

            width: listView.width
            text: section

            hoverEnabled: false;

            contentItem: Text {
                rightPadding: _section_delegate.spacing;
                text: _section_delegate.text;
                font: _section_delegate.font;
                color: X.Style.textColor;
                elide: Text.ElideRight;
                verticalAlignment: Text.AlignVCenter;
            }

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 0.8 : 0.3
                // color: Qt.darker(X.Style.backgroundColor, 1.2)
                color: "#44000000";

                Rectangle {
                    width: parent.width
                    height: 1
                    color: X.Style.variant == 'LIGHT'
                        ? Qt.darker(X.Style.borderColor, 1.2)
                        : Qt.lighter(X.Style.borderColor, 1.2);

                    anchors.bottom: parent.bottom
                }
            }
        }

        model: X.FilterProxyModel {

            id: _model;

            model: _list_model;

            delegate: ItemDelegate {
                width: listView.width
                text: model.title
                highlighted: ListView.isCurrentItem
                onClicked: {
                    listView.currentIndex = index

                    if(_stack.depth > 1)
                        _stack.pop();

                    _footer.reset();

//                    _profiler.start();

                    _stack.push(_component, { source: _list_model.get(index).source });

//                    _profiler.stop();

                    if(inPortrait)
                        _drawer.close();
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    opacity: enabled ? 0.8 : 0.3
                    color: hovered ? "#44000000" : "#22000000";

                    Rectangle {
                        width: parent.width
                        height: 1
                        color: X.Style.variant == 'LIGHT'
                            ? Qt.darker(X.Style.borderColor, 1.2)
                            : Qt.lighter(X.Style.borderColor, 1.2);
                        anchors.bottom: parent.bottom
                    }
                }
            }

            filterAccepts: function(item) {
                return model.get(item.index).title.includes(_field.text)
            }

            Component.onCompleted: {
                console.log(_model.model.count)
            }
        }
        ScrollIndicator.vertical: ScrollIndicator {}
    }


    ListModel {

        id: _list_model;

        ListElement { section: "Modules"; title: "3D - MultiView"; source: "qrc:/examples/gallery/pages/3DMultiViewPage.qml" }
        ListElement { section: "Modules"; title: "Charts - Basics"; source: "qrc:/examples/gallery/pages/ChartsPage.qml" }
        ListElement { section: "Modules"; title: "Multimedia - Audio"; source: "qrc:/examples/gallery/pages/MultimediaAudioPage.qml" }
        ListElement { section: "Modules"; title: "Multimedia - Video"; source: "qrc:/examples/gallery/pages/MultimediaVideoPage.qml" }

        ListElement { section: "Controls"; title: "Banner"; source: "qrc:/examples/gallery/pages/BannerPage.qml" }
        ListElement { section: "Controls"; title: "Cards"; source: "qrc:/examples/gallery/pages/CardsPage.qml" }
        ListElement { section: "Controls"; title: "CheckComboBox"; source: "qrc:/examples/gallery/pages/CheckComboBoxPage.qml" }
        ListElement { section: "Controls"; title: "Console"; source: "qrc:/examples/gallery/pages/ConsolePage.qml" }
        ListElement { section: "Controls"; title: "Editor"; source: "qrc:/examples/gallery/pages/EditorPage.qml" }
        ListElement { section: "Controls"; title: "Grid"; source: "qrc:/examples/gallery/pages/GridPage.qml" }
        ListElement { section: "Controls"; title: "Icons"; source: "qrc:/examples/gallery/pages/IconsPage.qml" }
        ListElement { section: "Controls"; title: "Spinner"; source: "qrc:/examples/gallery/pages/SpinnerPage.qml" }
        ListElement { section: "Controls"; title: "Tour"; source: "qrc:/examples/gallery/pages/TourPage.qml" }


        ListElement { section: "Controls"; title: "LSP"; source: "qrc:/examples/gallery/pages/LSPPage.qml" }

        ListElement { section: "Controls"; title: "Metrics"; source: "qrc:/examples/gallery/pages/MetricsPage.qml" }
        ListElement { section: "Controls"; title: "SegmentedCell"; source: "qrc:/examples/gallery/pages/SegmentedCellPage.qml" }
//        ListElement { section: "Controls"; title: "Shell"; source: "qrc:/examples/gallery/pages/ShellPage.qml" }
        ListElement { section: "Controls"; title: "Typography"; source: "qrc:/examples/gallery/pages/TypographyPage.qml" }
        ListElement { section: "Controls"; title: "Finder"; source: "qrc:/examples/gallery/pages/FinderPage.qml" }

        ListElement { section: "Style";    title: "BusyIndicator"; source: "qrc:/examples/gallery/pages/BusyIndicatorPage.qml" }
        ListElement { section: "Style";    title: "Button"; source: "qrc:/examples/gallery/pages/ButtonPage.qml" }
        ListElement { section: "Style";    title: "CheckBox"; source: "qrc:/examples/gallery/pages/CheckBoxPage.qml" }
        ListElement { section: "Style";    title: "ComboBox"; source: "qrc:/examples/gallery/pages/ComboBoxPage.qml" }
        ListElement { section: "Style";    title: "DelayButton"; source: "qrc:/examples/gallery/pages/DelayButtonPage.qml" }
        ListElement { section: "Style";    title: "Dial"; source: "qrc:/examples/gallery/pages/DialPage.qml" }
        ListElement { section: "Style";    title: "Dialog"; source: "qrc:/examples/gallery/pages/DialogPage.qml" }
        ListElement { section: "Style";    title: "Delegates"; source: "qrc:/examples/gallery/pages/DelegatePage.qml" }
        ListElement { section: "Style";    title: "Frame"; source: "qrc:/examples/gallery/pages/FramePage.qml" }
        ListElement { section: "Style";    title: "GroupBox"; source: "qrc:/examples/gallery/pages/GroupBoxPage.qml" }
        ListElement { section: "Style";    title: "PageIndicator"; source: "qrc:/examples/gallery/pages/PageIndicatorPage.qml" }
        ListElement { section: "Style";    title: "ProgressBar"; source: "qrc:/examples/gallery/pages/ProgressBarPage.qml" }
        ListElement { section: "Style";    title: "RadioButton"; source: "qrc:/examples/gallery/pages/RadioButtonPage.qml" }
        ListElement { section: "Style";    title: "RangeSlider"; source: "qrc:/examples/gallery/pages/RangeSliderPage.qml" }
        ListElement { section: "Style";    title: "ScrollBar"; source: "qrc:/examples/gallery/pages/ScrollBarPage.qml" }
        ListElement { section: "Style";    title: "ScrollIndicator"; source: "qrc:/examples/gallery/pages/ScrollIndicatorPage.qml" }
        ListElement { section: "Style";    title: "Slider"; source: "qrc:/examples/gallery/pages/SliderPage.qml" }
        ListElement { section: "Style";    title: "SpinBox"; source: "qrc:/examples/gallery/pages/SpinBoxPage.qml" }
        ListElement { section: "Style";    title: "StackView"; source: "qrc:/examples/gallery/pages/StackViewPage.qml" }
        ListElement { section: "Style";    title: "SwipeView"; source: "qrc:/examples/gallery/pages/SwipeViewPage.qml" }
        ListElement { section: "Style";    title: "CollapsibleView"; source: "qrc:/examples/gallery/pages/CollapsibleViewPage.qml" }
        ListElement { section: "Style";    title: "Switch"; source: "qrc:/examples/gallery/pages/SwitchPage.qml" }
        ListElement { section: "Style";    title: "TabBar"; source: "qrc:/examples/gallery/pages/TabBarPage.qml" }
        ListElement { section: "Style";    title: "TextArea"; source: "qrc:/examples/gallery/pages/TextAreaPage.qml" }
        ListElement { section: "Style";    title: "TextField"; source: "qrc:/examples/gallery/pages/TextFieldPage.qml" }
        ListElement { section: "Style";    title: "ToolTip"; source: "qrc:/examples/gallery/pages/ToolTipPage.qml" }
        ListElement { section: "Style";    title: "Tumbler"; source: "qrc:/examples/gallery/pages/TumblerPage.qml" }

        ListElement { section: "Demos";    title: "TweetSearch"; source: "qrc:/examples/gallery/pages/TweetSearchDemoPage.qml" }

        Component.onCompleted: {


            if (X.QtVersion > '6') {

                _list_model.insert(0, {
                    "section": "Modules",
                    "title": "3D - Morphing",
                    "source": "qrc:/examples/gallery/pages/3DMorphingPage.qml"
                });

                _list_model.insert(2, {
                    "section": "Modules",
                    "title": "3D - Morphing",
                    "source": "qrc:/examples/gallery/pages/3DPrincipledMaterialPage.qml"
                });

            }

            if (X.Features.TTS != undefined)
                _list_model.append({
                    "section": "Crates",
                    "title": "TextToSpeech",
                    "source": "qrc:/examples/gallery/pages/TextToSpeechPage.qml"
                });
            else
                _list_model.append({
                    "section": "Crates",
                    "title": "TextToSpeech",
                    "source": "qrc:/examples/gallery/pages/FeatureNotAvailablePage.qml"
                });

            if (X.Features.VIS != undefined) {
                _list_model.append({
                    "section": "Crates",
                    "title": "Visualisation - Quadric",
                    "source": "qrc:/examples/gallery/pages/VisualisationQuadricPage.qml"
                });

                _list_model.append({
                    "section": "Crates",
                    "title": "Visualisation - Sphere Source",
                    "source": "qrc:/examples/gallery/pages/VisualisationSphereSourcePage.qml"
                });
            } else {
                _list_model.append({
                    "section": "Crates",
                    "title": "Visualisation",
                    "source": "qrc:/examples/gallery/pages/FeatureNotAvailablePage.qml"
                });
            }
        }
    }


    X.Separator {

        anchors.top: parent.top;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        // visible: window.inPortrait;
    }

    background: Rectangle {

        color: Qt.platform.os === "osx" ? "#00000000" : X.Style.baseColor;
    }
}
