import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xLogger         1.0 as L

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X
import xQuick.Fonts    1.0 as X
import xQuick.Style    1.0 as X

import "." as XG

X.Application {

    id: window;

    XG.GalleryHeader { id: _header;

        anchors.left: parent.left;
        anchors.leftMargin: inPortrait ? undefined : _drawer.width; // * _drawer.position;
        anchors.right: parent.right;
        anchors.rightMargin: _drawel.width;
    }

    XG.GalleryFooter { id: _footer;

        anchors.left: parent.left;
        anchors.leftMargin: inPortrait ? undefined : _drawer.width; // * _drawer.position;
        anchors.right: parent.right;
        anchors.rightMargin: _drawel.width;
        anchors.bottom: parent.bottom;

        onToggleConsole: {
            if (_console.height > 1)
                _console.SplitView.preferredHeight = 0;
            else
                _console.SplitView.preferredHeight = window.height * 1/4;
        }
    }

    XG.GalleryCompnt { id: _component; }

    X.SplitView {

        id: _split;

        visible: true;

        clip: true;

        orientation: Qt.Vertical;

        height: window.height - _header.height - _footer.height;

        anchors.fill: parent
        anchors.topMargin: _header.height
        anchors.leftMargin: !window.inPortrait ? _drawer.width : undefined
        anchors.rightMargin: _drawel.width;
        anchors.bottomMargin: _footer.height

        handleFunc: function() {
            return _console.height > 2 ? X.Style.borderColor : "transparent";
        }

        StackView {
            id: _stack;

            SplitView.fillWidth: true;
            SplitView.fillHeight: true;

            clip: true;

            initialItem: Pane {

                Page {
                    anchors.fill: parent
                    anchors.topMargin: 20

                    Column {

                        anchors.centerIn: parent

                        spacing: 16

                        Grid {

                            columns: 3
                            rows: 2
                            spacing: 16

                            Label {
                                text: "MEM"
                            }

                            Rectangle {
                                id: _memory_total

                                property int value: 0

                                clip: true
                                width: 300
                                height: 16
                                radius: 8

                                border.width: 1
                                border.color: "#302B27"

                                color: "#1B264F"

                                MouseArea {

                                    anchors.fill: parent
                                    hoverEnabled: true

                                    ToolTip.timeout: 5000
                                    ToolTip.visible: containsMouse
                                    ToolTip.text: _memory_total.value > 1000
                                            ? _memory_total.value / 1000 + "GB total"
                                            : _memory_total.value        + "MB total"
                                }

                                Rectangle {
                                    id: _memory_used

                                    property int value: 0

                                    x: 0
                                    y: 0

                                    width: (_memory_used.value - _memory_this.value) / _memory_total.value * _memory_total.width
                                    height: parent.height
                                    color: "#274690"
                                    radius: parent.radius - 2

                                    Rectangle {

                                        x: parent.width - parent.radius
                                        y: parent.y
                                        width: parent.radius
                                        height: parent.height
                                        color: parent.color
                                    }

                                    MouseArea {

                                        anchors.fill: parent
                                        hoverEnabled: true

                                        ToolTip.timeout: 5000
                                        ToolTip.visible: containsMouse
                                        ToolTip.text: _memory_used.value > 1000
                                            ? _memory_used.value / 1000 + "GB used"
                                            : _memory_used.value        + "MB used"
                                    }
                                }

                                Rectangle {
                                    id: _memory_this

                                    property int value: 0

                                    x: _memory_used.width
                                    y: 0

                                    width: _memory_this.value / _memory_total.value * _memory_total.width
                                    height: parent.height
                                    color: "#576CA8"

                                    MouseArea {

                                        anchors.fill: parent
                                        hoverEnabled: true

                                        ToolTip.timeout: 5000
                                        ToolTip.visible: containsMouse
                                        ToolTip.text: _memory_this.value > 1000
                                            ? _memory_this.value / 1000 + "GB self"
                                            : _memory_this.value        + "MB self"
                                    }
                                }
                            }

                            Label {
                                text: (_memory_total.value - _memory_used.value - _memory_this.value) > 1000
                                    ? (_memory_total.value - _memory_used.value - _memory_this.value) / 1000 + "GB remaining"
                                    : (_memory_total.value - _memory_used.value - _memory_this.value)        + "MB remaining"
                            }

                            // CPU

                            Label {
                                text: "CPU"
                            }

                            Rectangle {
                                id: _cpu_total

                                property int value: 100
                                property int count: 0

                                clip: true
                                width: 300
                                height: 16
                                radius: 8

                                border.width: 1
                                border.color: "#302B27"

                                color: "#1B264F"

                                MouseArea {

                                    anchors.fill: parent
                                    hoverEnabled: true

                                    ToolTip.timeout: 5000
                                    ToolTip.visible: containsMouse
                                    ToolTip.text: "Using " + _cpu_total.count + " cores"
                                }

                                Rectangle {
                                    id: _cpu_used

                                    property int value: 0

                                    x: 0
                                    y: 0

                                    width: (_cpu_used.value - _cpu_this.value) / _cpu_total.value * _cpu_total.width
                                    height: parent.height
                                    color: "#274690"
                                    radius: parent.radius - 2

                                    Rectangle {

                                        x: parent.width - parent.radius
                                        y: parent.y
                                        width: parent.radius
                                        height: parent.height
                                        color: parent.color
                                    }

                                    MouseArea {

                                        anchors.fill: parent
                                        hoverEnabled: true

                                        ToolTip.timeout: 5000
                                        ToolTip.visible: containsMouse
                                        ToolTip.text: _cpu_used.value + "% used"
                                    }
                                }

                                Rectangle {
                                    id: _cpu_this

                                    property int value: 0

                                    x: _memory_used.width
                                    y: 0

                                    width: _cpu_this.value / _cpu_total.value * _cpu_total.width
                                    height: parent.height
                                    color: "#576CA8"

                                    MouseArea {

                                        anchors.fill: parent
                                        hoverEnabled: true

                                        ToolTip.timeout: 2500
                                        ToolTip.visible: containsMouse
                                        ToolTip.text: _cpu_this.value + "% self"
                                    }
                                }
                            }

                            Label {
                                text: _cpu_total.value - _cpu_used.value - _cpu_this.value + "% remaining"
                            }
                        }

                    Label {

                        id: _info;

                        property string host_name: ""
                        property string system_name: ""

                        text: (host_name !== "" && system_name !== "") ? "On " + host_name + ", running " + system_name : ""
                    }

                    }

                    X.SystemFetcher {
                        id: _system_fetcher;
                    }

                    Timer {
                        interval: 2500
                        running: parent.visible
                        repeat: true
                        onTriggered: {
                            _system_fetcher.fetch();
                        
                            _memory_total.value = _system_fetcher.total_memory
                            _memory_used.value = (_system_fetcher.used_memory - _system_fetcher.this_memory)
                            _memory_this.value =  _system_fetcher.this_memory

                            _cpu_total.count = _system_fetcher.total_cpu_count
                            _cpu_used.value = _system_fetcher.used_cpu
                            _cpu_this.value = _system_fetcher.this_cpu
                        
                            _info.host_name = _system_fetcher.host_name
                            _info.system_name = _system_fetcher.system_name
                        }
                    }
                }
            }
        }

        X.Console {

            id: _console;

            SplitView.fillWidth: true;
            SplitView.preferredHeight: 0; // window.height * 1/4;

            Behavior on SplitView.preferredHeight {
                NumberAnimation {
                    easing {
                        type: Easing.OutElastic
                        amplitude: 1.0
                        period: 0.5
                    }
                }
            }

            onItemsChanged: {

                if (L.Controller.items.length > 0)
                    SplitView.preferredHeight = window.height * 1/4;
                else
                    SplitView.preferredHeight = 0;
            }
        }
    }

    XG.GalleryDrawer { id: _drawer; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.left: parent.left; }
    XG.GalleryDrawel { id: _drawel; anchors.top: parent.top; anchors.bottom: parent.bottom; anchors.right: parent.right; }
    XG.GallerySettgs { id: _settings; }

    Connections {
        target: _footer;

        function onTogglePage() {

            if(_stack.depth == 1)
                return;

            var left = _stack.currentItem.children[0].children[0].children[0].children[0];
            var right = _stack.currentItem.children[0].children[0].children[0].children[1];

            if (left.width > 1) {
                left.SplitView.fillWidth = false;
                left.SplitView.preferredWidth = 0;
                right.SplitView.fillWidth = true;
            } else {
                left.SplitView.preferredWidth = _stack.width / 2;
            }
        }

        function onToggleEditor() {

            if(_stack.depth == 1)
                return;

            var left = _stack.currentItem.children[0].children[0].children[0].children[0];
            var right = _stack.currentItem.children[0].children[0].children[0].children[1];

            if (right.width > 1) {
                left.SplitView.fillWidth = true;
                right.SplitView.fillWidth = false;
                right.SplitView.preferredWidth = 0;
            } else {
                right.SplitView.preferredWidth = _stack.width / 2;
            }
        }
    }

    /* X.Profiler { */
    /*     id: _profiler; */
    /* } */

    Component.onCompleted: {

        /* _profiler.initialise(); */

        _drawer.open();

        if (Qt.platform.os === "osx")
            X.Style.flavors = 'MACOS';

        // console.info("Has feature Default:", X.Features.Default != undefined);
        // console.info("Has feature TTS:",     X.Features.TTS     != undefined);

        // console.info("Using QtVersion:", QtVersion);
        // console.info("QtVersion >  5 ?", QtVersion >  '5');
        // console.info("QtVersion >= 6 ?", QtVersion >= '6');
        // console.info("QtVersion <  6 ?", QtVersion <  '6');
    }
}
