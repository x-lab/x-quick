use qmetaobject::qtcore::core_application::*;
use qmetaobject::prelude::*;

use x_logger;
use x_quick;
#[cfg(feature = "tts")]
use x_tts;
#[cfg(feature = "vis")]
use x_vis;

qrc!(register_resources, "/" {
    "examples/gallery/qmldir",
    "examples/gallery/main.qml",
    "examples/gallery/main.header.qml",
    "examples/gallery/main.footer.qml",
    "examples/gallery/main.drawer.qml",
    "examples/gallery/main.drawel.qml",
    "examples/gallery/main.settgs.qml",
    "examples/gallery/main.compnt.qml",
    "examples/gallery/pages/3DMorphingPage.mesh",
    "examples/gallery/pages/3DMorphingPage.qml",
    "examples/gallery/pages/3DMultiViewPage.mesh",
    "examples/gallery/pages/3DMultiViewPage.qml",
    "examples/gallery/pages/3DPrincipledMaterialPage.qml",
    "examples/gallery/pages/3DPrincipledMaterialPageBase.jpg",
    "examples/gallery/pages/3DPrincipledMaterialPageEnvironment.hdr",
    "examples/gallery/pages/3DPrincipledMaterialPageMetallic.jpg",
    "examples/gallery/pages/3DPrincipledMaterialPageNormal.jpg",
    "examples/gallery/pages/3DPrincipledMaterialPageRoughness.jpg",
    "examples/gallery/pages/BannerPage.qml",
    "examples/gallery/pages/BusyIndicatorPage.qml",
    "examples/gallery/pages/ButtonPage.qml",
    "examples/gallery/pages/CanvasPage.qml",
    "examples/gallery/pages/CanvasPage.js",
    "examples/gallery/pages/CardsPage.qml",
    "examples/gallery/pages/ChartsPage.qml",
    "examples/gallery/pages/CheckBoxPage.qml",
    "examples/gallery/pages/ComboBoxPage.qml",
    "examples/gallery/pages/CheckComboBoxPage.qml",
    "examples/gallery/pages/ConsolePage.qml",
    "examples/gallery/pages/DataVisualisationPage.qml",
    "examples/gallery/pages/DelayButtonPage.qml",
    "examples/gallery/pages/DelegatePage.qml",
    "examples/gallery/pages/DialPage.qml",
    "examples/gallery/pages/DialogPage.qml",
    "examples/gallery/pages/EditorPage.qml",
    "examples/gallery/pages/FeatureNotAvailablePage.qml",
    "examples/gallery/pages/FramePage.qml",
    "examples/gallery/pages/GridPage.qml",
    "examples/gallery/pages/GroupBoxPage.qml",
    "examples/gallery/pages/IconsPage.qml",
    "examples/gallery/pages/LSPPage.qml",
    "examples/gallery/pages/MetricsPage.qml",
    "examples/gallery/pages/MultimediaAudioPage.qml",
    "examples/gallery/pages/MultimediaVideoPage.qml",
    "examples/gallery/pages/PageIndicatorPage.qml",
    "examples/gallery/pages/ProgressBarPage.qml",
    "examples/gallery/pages/RadioButtonPage.qml",
    "examples/gallery/pages/RangeSliderPage.qml",
    "examples/gallery/pages/ScrollBarPage.qml",
    "examples/gallery/pages/ScrollIndicatorPage.qml",
    "examples/gallery/pages/SegmentedCellPage.qml",
    "examples/gallery/pages/ShellPage.qml",
    "examples/gallery/pages/SliderPage.qml",
    "examples/gallery/pages/SpinBoxPage.qml",
    "examples/gallery/pages/StackViewPage.qml",
    "examples/gallery/pages/SwipeViewPage.qml",
    "examples/gallery/pages/SwitchPage.qml",
    "examples/gallery/pages/CollapsibleViewPage.qml",
    "examples/gallery/pages/TabBarPage.qml",
    "examples/gallery/pages/TextAreaPage.qml",
    "examples/gallery/pages/TextFieldPage.qml",
    "examples/gallery/pages/TextToSpeechPage.qml",
    "examples/gallery/pages/ToolTipPage.qml",
    "examples/gallery/pages/TumblerPage.qml",
    "examples/gallery/pages/TweetSearchDemoPage.qml",
    "examples/gallery/pages/TweetSearchDemoPage/tweetsearch.mjs",
    "examples/gallery/pages/TweetSearchDemoPage/TweetsModel.qml",
    "examples/gallery/pages/TweetSearchDemoPage/TweetDelegate.qml",
    "examples/gallery/pages/TweetSearchDemoPage/SearchDelegate.qml",
    "examples/gallery/pages/TweetSearchDemoPage/ListHeader.qml",
    "examples/gallery/pages/TweetSearchDemoPage/ListFooter.qml",
    "examples/gallery/pages/TweetSearchDemoPage/LineInput.qml",
    "examples/gallery/pages/TweetSearchDemoPage/FlipBar.qml",
    "examples/gallery/pages/TweetSearchDemoPage/resources/icon-search.png",
    "examples/gallery/pages/TweetSearchDemoPage/resources/icon-refresh.png",
    "examples/gallery/pages/TweetSearchDemoPage/resources/icon-loading.png",
    "examples/gallery/pages/TweetSearchDemoPage/resources/icon-clear.png",
    "examples/gallery/pages/TweetSearchDemoPage/resources/bird-anim-sprites.png",
    "examples/gallery/pages/TweetSearchDemoPage/resources/anonymous.png",
    "examples/gallery/pages/TypographyPage.qml",
    "examples/gallery/pages/VisualisationQuadricPage.qml",
    "examples/gallery/pages/VisualisationSphereSourcePage.qml",
    "examples/gallery/pages/FinderPage.qml",
    "examples/gallery/pages/SpinnerPage.qml",
    "examples/gallery/pages/TourPage.qml",
});

//  "-qmljsdebugger=file:1234,block,services:CanvasFrameRate")

fn main() {

    register_resources();

    x_logger::init();
     x_quick::init();
#[cfg(feature = "tts")]
       x_tts::init();
#[cfg(feature = "vis")]
       x_vis::init();

    QCoreApplication::set_application_name("x-quick Gallery".into());
    QCoreApplication::set_organization_name("inria".into());
    QCoreApplication::set_organization_domain("fr".into());

    let mut engine = QmlEngine::new();
    engine.add_import_path("qrc:///qml".into());
    engine.add_import_path("qrc:///examples/gallery".into());
    engine.set_property("_title".into(), QVariant::from(QString::from("x-quick Gallery")));
    engine.load_file("qrc:///examples/gallery/main.qml".into());

    x_quick::wrap(&mut engine);

    engine.exec();
}
