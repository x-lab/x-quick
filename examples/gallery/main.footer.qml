import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

X.ToolBar {

    height: 42;

    property int   line: 0;
    property int column: 0;
    property string source;

    signal toggleConsole();
    signal togglePage();
    signal toggleEditor();

    situation: X.Style.Position.Bottom;

    X.SegmentedCell {

        anchors.verticalCenter: parent.verticalCenter;

        anchors.left: parent.left;
        anchors.leftMargin: 10;
        anchors.right: _cell.left;
        anchors.rightMargin: 10;

        exclusive: false;

        height: 22;

        X.SegmentedCellSegment { text: _footer.line + ':' + _footer.column; Layout.maximumWidth: 54; checkable: true; checked: false; }
        X.SegmentedCellSegment { text: _footer.source; checkable: true; checked: false; }
    }

    X.SegmentedCell {

        id: _cell;

        anchors.verticalCenter: parent.verticalCenter;
        anchors.right: parent.right;
        anchors.rightMargin: 10;

        exclusive: false;

        height: 22;

        X.SegmentedCellSegment { id: _cell_l; icon.width: 12; iconSource: X.Icons.icons.align_horizontal_left;  Layout.maximumWidth: 64; checkable: true; checked: false; onClicked: _footer.togglePage(); }
        X.SegmentedCellSegment { id: _cell_m; icon.width: 12; iconSource: X.Icons.icons.align_vertical_bottom;  Layout.maximumWidth: 64; checkable: true; checked: _console.height > 2; onClicked: _footer.toggleConsole(); }
        X.SegmentedCellSegment { id: _cell_r; icon.width: 12; iconSource: X.Icons.icons.align_horizontal_right; Layout.maximumWidth: 64; checkable: true; checked: false; onClicked: _footer.toggleEditor(); }
    }

    function reset() {
        _cell_l.checked = true;
        _cell_r.checked = true;
    }
}
