import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    ColumnLayout {

        anchors.fill: parent;
        anchors.topMargin: 10;

        spacing: 10;

        X.Banner {
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;

            text: "Single-line message."
        }

        X.Banner {
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;

            text: "Single-line message with action."
            action: "Action"
            onActionPressed: console.log("Action!")
        }

        X.Banner {
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;

            text: "Two-line message with action. This message is very long."
            action: "Action"
            onActionPressed: console.log("Action!")
        }

        X.Banner {
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;

            expandable: true
            text: "Expandable Two-line message with action. This message is very long."
            action: "Action"
            onActionPressed: console.log("Action!")
        }

        X.Separator { Layout.fillWidth: true; }

        RowLayout {
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;

            Button {
                Layout.fillWidth: true;

                text: "Push Default Banner"

                onClicked: X.BannerManager.show({
                    text: "Default Banner",
                })
            }

            Button {
                Layout.fillWidth: true;

                text: "Push Non Killable Banner"

                onClicked: X.BannerManager.show({
                    text: "Non Killable Banner",
                    timeout: 4000,
                    canBeKilled: false
                })
            }

            Button {
                Layout.fillWidth: true;

                text: "Push Action Banner"

                onClicked: X.BannerManager.show({
                    text: "Action Banner text",
                    action: "Action text",
                    timeout: 4000,
                    canBeKilled: false
                })
            }

            Button {
                Layout.fillWidth: true;

                text: "Push Demo Banner"

                onClicked: X.BannerManager.show({
                    text: "Text",
                    action: "Action",
                    timeout: 3000,
                    onAccept: function() {
                        console.log("Function called when action is pressed")
                    },
                    onClose: function() {
                        console.log("Function called when snackbar disappear")
                    },
                    canBeKilled: true
                })
            }
        }

        X.Separator { Layout.fillWidth: true; }

        Item { Layout.fillHeight: true; }
    }
}
