import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X
import xQuick.Vis       1.0 as XVis

Page {

    id: _page

    XVis.Viewer {

        anchors.fill: parent;

        mouseEnabled: true;

        XVis.Actor {
            XVis.PolyDataMapper {
                XVis.SphereSource {
                    id: source;
                }
            }
        }
    }

    X.Frame {

        id: _card;

        anchors.top: parent.top;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.margins: 20;

        width: 400;

        background: Rectangle {
            color: "#6628292a";
            border.color: X.Style.borderColor
            radius: X.Style.controls.radius;
        }

        contentItem: Flickable {

            contentHeight: _layout.height;

            clip: true;

            ColumnLayout
            {

                id: _layout;

                anchors.fill: parent;
                anchors.margins: 10;

                spacing: 10;

                X.LabelHeadline6 {

                    Layout.fillWidth: true;

                    leftPadding: 20;
                    rightPadding: 20;

                    wrapMode: Label.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    text: "Visualisation"
                }

                X.LabelCaption {
                    Layout.fillWidth: true;
                    width: parent.width;
                    wrapMode: Text.WordWrap;
                    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ligula justo, accumsan vitae mattis placerat, ornare sed nunc. Quisque tristique sapien eget lectus dapibus blandit. Quisque congue in enim eu dapibus. Aenean elit tellus, scelerisque venenatis odio quis, dictum congue leo. Nam tristique lacinia aliquam. Praesent vel massa risus. In ullamcorper sollicitudin ligula vel vestibulum. Cras gravida turpis vel sem porta, et euismod neque pretium. Maecenas mattis, tortor eu luctus posuere, diam sem lacinia tortor, ut finibus lorem ante eget mauris. In arcu enim, luctus id nulla nec, varius finibus mauris. Ut egestas nulla nec blandit sollicitudin. Phasellus erat felis, tristique eget mollis lobortis, pharetra non nisi. Donec a sem non nisi suscipit suscipit. Fusce id euismod tellus. Suspendisse potenti. Integer viverra tempus massa a mollis.";
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "radius";
                    min: 0.1; max: 1; step: 0.01; value: 0.5;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "startTheta";
                    min: 0; max: 90; step: 1; value: 0;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "endTheta";
                    min: 180; max: 360; step: 1; value: 360;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "startPhi";
                    min: 0; max: 90; step: 1; value: 0;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "endPhi";
                    min: 90; max: 180; step: 1; value: 180;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "thetaResolution";
                    min: 3; max: 64; step: 1; value: 16;
                }

                X.VisSlider {
                    Layout.fillWidth: true;
                    from: source; bind: "phiResolution";
                    min: 3; max: 64; step: 1; value: 16;
                }
            }

            ScrollIndicator.vertical: ScrollIndicator {}
        }
    }
}
