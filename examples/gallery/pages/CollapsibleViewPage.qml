import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Pane {
    id: pane

    Pane {
        Label {

            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Collapsible view provides a way of creating views that can collapse to a given side of the screen by the click of a button"
        }
    }

    X.CollapsibleView {
        id: view;
        orientation: "left";
        size: parent.width / 2;

        Column {
            spacing: 40
            width: parent.width

            Label {
                leftPadding: 20;
                rightPadding: 20;
                width: parent.width
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                text: "Collapse the menu by pressing the button"
            }
        }

    }

}
