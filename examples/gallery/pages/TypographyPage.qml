import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Typography"
        }

        SwitchDelegate {
            id: _enabledSwitch;

            width: parent.width;

            text: "Label enabled";
            checked: true;
        }

        X.LabelEntry {

            font_: X.Style.typography.headline1
            enabled: _enabledSwitch.checked
            text: "headline1"
        }

        X.LabelEntry {

            font_: X.Style.typography.headline2
            enabled: _enabledSwitch.checked
            text: "headline2"
        }

        X.LabelEntry {

            font_: X.Style.typography.headline3
            enabled: _enabledSwitch.checked
            text: "headline3"
        }

        X.LabelEntry {

            font_: X.Style.typography.headline4
            enabled: _enabledSwitch.checked
            text: "headline4"
        }

        X.LabelEntry {

            font_: X.Style.typography.headline5
            enabled: _enabledSwitch.checked
            text: "headline5"
        }

        X.LabelEntry {

            font_: X.Style.typography.headline6
            enabled: _enabledSwitch.checked
            text: "headline6"
        }

        X.LabelEntry {

            font_: X.Style.typography.subtitle1
            enabled: _enabledSwitch.checked
            text: "subtitle1"
        }

        X.LabelEntry {

            font_: X.Style.typography.subtitle2
            enabled: _enabledSwitch.checked
            text: "subtitle2"
        }

        X.LabelEntry {

            font_: X.Style.typography.body1
            enabled: _enabledSwitch.checked
            text: "body1"
        }

        X.LabelEntry {

            font_: X.Style.typography.body2
            enabled: _enabledSwitch.checked
            text: "body2"
        }

        X.LabelEntry {

            font_: X.Style.typography.button
            enabled: _enabledSwitch.checked
            text: "button"
        }

        X.LabelEntry {

            font_: X.Style.typography.overline
            enabled: _enabledSwitch.checked
            text: "overline"
        }

        X.LabelEntry {

            font_: X.Style.typography.caption
            enabled: _enabledSwitch.checked
            text: "caption"
        }

        X.LabelEntry {

            font_: X.Style.typography.hint1
            enabled: _enabledSwitch.checked
            text: "hint1"
        }

        X.LabelEntry {

            font_: X.Style.typography.hint2
            enabled: _enabledSwitch.checked
            text: "hint2"
        }
    }
}
