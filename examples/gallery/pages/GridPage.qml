import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    X.Metrics {
        id: _metrics;
    }

    X.Grid {

        id: _grid;

        anchors.fill: parent;

        columns: 12;
        columnColor: "#22444444";
        columnSpacing: _metrics.dp(20);
        rowSpacing: _metrics.dp(20);

        showGrid: true;

        fillStrategy: X.Grid.ITEM_BREAK;

        Rectangle {
            color: "red"
            height: _grid.helpers.columnsWidth(2)
            radius: X.Style.controls.radius;

            X.Grid.colWidth: _grid.adapt(9);

            X.LabelCaption {
                color: "white"
                text: "Row 1: adapt(9)"
                anchors.centerIn: parent;
                wrapMode: Text.Wrap
            }
        }
        Rectangle{
            color: "yellow"
            height: _grid.helpers.columnsWidth(2)
            radius: X.Style.controls.radius;

            X.Grid.colWidth: _grid.adapt(3);

            X.LabelCaption {
                color: "white"
                text: "Row 1: adapt(3)"
                anchors.centerIn: parent;
                wrapMode: Text.Wrap
            }
        }
        Rectangle{
            color: "orange"
            height: _grid.helpers.columnsWidth(2)
            radius: X.Style.controls.radius;

            X.Grid.colWidth: 12

            X.LabelCaption {
                color: "white"
                text: "Row 2: adapt(12)"
                anchors.centerIn: parent;
                wrapMode: Text.Wrap
            }
        }

        onColumnWidthChanged: {
            // console.log("a column is", columnWidth, "px wide")
        }

        onContentHeightChanged: {
            // console.log("the layouted content of the grid have a height of ", contentHeight, "px")
        }

        X.ButtonSquared {

            anchors.centerIn: parent;

            topPadding: 10;
            leftPadding: 10;
            rightPadding: 10;
            bottomPadding: 10;

            width: _grid.helpers.columnsWidth(5)

            text: _page.width + 'x' + _page.height + ': ' + _grid.state();
        }

        function isLarge() {
            return (_page.width >= 1200);
        }

        function isMedium() {
            return (_page.width >= 992 && _page.width < 1200);
        }

        function isSmall() {
            return (_page.width >= 768 && _page.width < 992);
        }

        function isXSmall() {
            return (_page.width < 768);
        }

        function adapt(value) {

            if(isLarge() || isMedium())
               return value;

            return 12;
        }

        function state() {

            if(isXSmall())
                return 'Extra Small';

            if(isSmall())
                return 'Small';

            if(isMedium())
                return 'Medium';

            return 'Large';
        }
    }
}
