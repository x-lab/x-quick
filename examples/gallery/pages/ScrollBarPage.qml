import QtQuick          2.15
import QtQuick.Controls 2.15

Flickable {
    id: flickable

    contentHeight: pane.height

    Pane {
        id: pane
        width: flickable.width
        height: flickable.height * 1.25

        Column {
            id: column
            spacing: 40

            anchors.fill: parent;
            anchors.topMargin: 20;

            Label {
                leftPadding: 20;
                rightPadding: 20;
                width: parent.width
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                text: "ScrollBar is an interactive bar that can be used to scroll to a specific position. "
                    + "A scroll bar can be either vertical or horizontal, and can be attached to any Flickable, "
                    + "such as ListView and GridView."
            }
        }
    }

    ScrollBar.vertical: ScrollBar { }
}
