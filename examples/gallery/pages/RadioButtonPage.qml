import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "RadioButton presents an option button that can be toggled on or off. "
                + "Radio buttons are typically used to select one option from a set of options."
        }

        Column {
            spacing: 20
            anchors.horizontalCenter: parent.horizontalCenter

            RadioButton {
                text: "First"
            }
            RadioButton {
                text: "Second"
                checked: true
            }
            RadioButton {
                text: "Third"
                enabled: false
            }
        }
    }
}
