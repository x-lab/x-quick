import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    X.Banner {

        id: _banner;

        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 10;

        text: "Feature not available."

        X.Icon {

            anchors.right: parent.right;
            anchors.rightMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;

            icon: X.Icons.icons.sticky_note_2;
            size: 22;
            color: X.Style.alertColor;
        }
    }

    X.LabelCaption {

        anchors.top: _banner.bottom;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 10;

        wrapMode: Text.WordWrap;
        text: "Use: cargo build --features ... ... to enable them.";

        font.family: "Source Code Pro";
        font.pointSize: 11;
    }
}
