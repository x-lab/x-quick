import QtQuick           2.15
import QtQuick.Controls  2.15
import QtQuick.Layouts   1.15
import QtMultimedia      5.15 as M

import xQuick.Controls   1.0 as X
import xQuick.Fonts      1.0 as X
import xQuick.Models     1.0 as X
import xQuick.Style      1.0 as X

Page {

    id: _page;

    M.Video {
        id: _vdo;
        anchors.fill: parent
        source: "http://www.peach.themazzone.com/durian/movies/sintel-2048-surround.mp4";

    }

    Component.onCompleted: _vdo.play();
}
