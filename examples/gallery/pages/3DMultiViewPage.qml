import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick3D        1.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page;

    Rectangle {
        id: topLeft
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width * 0.5
        height: parent.height * 0.5
        color: "transparent"

        border.color: X.Style.borderColor
        border.width: 0

        View3D {
            id: topLeftView
            anchors.fill: parent
            importScene: standAloneScene
            camera: cameraOrthographicFront
        }

        X.LabelButton {
            text: "Front"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10
            font.pointSize: 14
        }

        X.Separator {
            anchors.top: parent.top;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
        }

        X.Separator {
            anchors.left: parent.left;
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
        }
    }

    Rectangle {
        id: topRight
        anchors.top: parent.top
        anchors.right: parent.right
        width: parent.width * 0.5
        height: parent.height * 0.5
        color: "transparent"
        border.color: X.Style.borderColor
        border.width: 0
        clip: true;

        X.LabelButton {
            text: "Perspective"
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 10
            font.pointSize: 14
        }

        View3D {
            id: topRightView
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom;
            camera: cameraPerspectiveOne
            importScene: standAloneScene
            // renderMode: View3D.Underlay

            // environment: SceneEnvironment {
            //     clearColor: "#00000000"
            //     backgroundMode: SceneEnvironment.Color
            // }
        }

        Row {
            id: controlsContainer
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            padding: 10

            X.ButtonRound {
                text: "Camera 1"
                checkable: true;
                checked: topRightView.camera == cameraPerspectiveOne
                onClicked: {
                    topRightView.camera = cameraPerspectiveOne
                }
            }
            X.ButtonRound {
                text: "Camera 2"
                checkable: true;
                checked: topRightView.camera == cameraPerspectiveTwo
                onClicked: {
                    topRightView.camera = cameraPerspectiveTwo
                }
            }
            X.ButtonRound {
                text: "Camera 3"
                checkable: true;
                checked: topRightView.camera == cameraPerspectiveThree
                onClicked: {
                    topRightView.camera = cameraPerspectiveThree
                }
            }
        }
    }

    Rectangle {
        id: bottomLeft
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: parent.width * 0.5
        height: parent.height * 0.5
        color: "transparent"
        border.color: X.Style.borderColor
        border.width: 0

        View3D {
            id: bottomLeftView
            anchors.fill: parent
            importScene: standAloneScene
            camera: cameraOrthographicTop
        }

        X.LabelButton {
            text: "Top"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10
            font.pointSize: 14
        }
    }

    Rectangle {
        id: bottomRight
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: parent.width * 0.5
        height: parent.height * 0.5
        color: "transparent"
        border.color: X.Style.borderColor
        border.width: 0

        View3D {
            id: bottomRightView
            anchors.fill: parent
            importScene: standAloneScene
            camera: cameraOrthographicLeft
        }

        X.LabelButton {
            text: "Left"
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 10
            font.pointSize: 14
        }

        X.Separator {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.right: parent.right;
        }

        X.Separator {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.bottom: parent.bottom;
        }
    }

    Node {
        id: standAloneScene

        DirectionalLight {
            ambientColor: Qt.rgba(1.0, 1.0, 1.0, 1.0)
            brightness: 1.0
            eulerRotation.x: -25
        }

        Model {
            source: "#Cube"
            y: -104
            scale: Qt.vector3d(3, 3, 0.1)
            eulerRotation.x: -90
            materials: [
            DefaultMaterial {
                diffuseColor: Qt.rgba(0.8, 0.8, 0.8, 1.0)
            }
            ]
        }

        Model {
            source: "qrc:/examples/gallery/pages/3DMultiViewPage.mesh"
            y: -100
            scale: Qt.vector3d(50, 50, 50)
            materials: [
            PrincipledMaterial {
                baseColor: X.Style.accentColor;
                metalness: 0.75
                roughness: 0.1
                specularAmount: 1.0
                indexOfRefraction: 2.5
                opacity: 1.0
            }
            ]

            PropertyAnimation on eulerRotation.y {
                loops: Animation.Infinite
                duration: 5000
                to: 0
                from: -360
            }
        }

        Node {
            PerspectiveCamera {
                id: cameraPerspectiveOne
                z: 600
            }

            PropertyAnimation on eulerRotation.x {
                loops: Animation.Infinite
                duration: 5000
                to: -360
                from: 0
            }
        }

        PerspectiveCamera {
            id: cameraPerspectiveTwo
            z: 600
        }

        Node {
            PerspectiveCamera {
                id: cameraPerspectiveThree
                x: 500
                eulerRotation.y: 90
            }
            PropertyAnimation on eulerRotation.y {
                loops: Animation.Infinite
                duration: 5000
                to: 0
                from: -360
            }
        }

        OrthographicCamera {
            id: cameraOrthographicTop
            y: 600
            eulerRotation.x: -90
        }

        OrthographicCamera {
            id: cameraOrthographicFront
            z: 600
        }

        OrthographicCamera {
            id: cameraOrthographicLeft
            x: -600
            eulerRotation.y: -90
        }
    }
}
