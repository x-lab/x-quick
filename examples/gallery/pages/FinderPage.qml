import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {
    id: _page

    X.Finder {
        id: _finder;

        extensionFilters: ListModel {
            ListElement {
                text: "svg, pdf"
                regexp: "*.svg,*.pdf"
            }
            ListElement {
                text: ".pdf"
                regexp: "*.pdf"
            }
            ListElement {
                text: "All"
                regexp: "*"
            }
        }

        anchors.fill: parent;
    }
}
