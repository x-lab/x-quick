import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    readonly property int itemWidth: Math.max(button.implicitWidth, Math.min(button.implicitWidth * 3, page.availableWidth / 3 * 2))

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "A GroupBox provides a frame, a title on top of it, and a logical group of controls within that frame."
        }

        GroupBox {
            title: "Title"
            anchors.horizontalCenter: parent.horizontalCenter

            Column {
                spacing: 20
                width: page.itemWidth

                RadioButton {
                    text: "First"
                    checked: true
                    width: parent.width
                }
                RadioButton {
                    id: button
                    text: "Second"
                    width: parent.width
                }
                RadioButton {
                    text: "Third"
                    width: parent.width
                }
            }
        }
    }
}
