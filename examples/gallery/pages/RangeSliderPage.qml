import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "RangeSlider is used to select a range specified by two values, by sliding each handle along a track."
        }

        RangeSlider {
            id: slider
            first.value: 0.25
            second.value: 0.75
            anchors.horizontalCenter: parent.horizontalCenter
        }

        RangeSlider {
            orientation: Qt.Vertical
            first.value: 0.25
            second.value: 0.75
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
