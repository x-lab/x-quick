import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls      1.0 as X

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "CheckComboBox is a combined check button and popup list. "
            + "The user can select one or more of options from the popup list."
        }

        X.CheckComboBox {
            model: ListModel {
                ListElement { name: "One"; ischecked: false }
                ListElement { name: "Two"; ischecked: false }
                ListElement { name: "Three"; ischecked: false } 
                ListElement { name: "Four"; ischecked: false } 
            }
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
