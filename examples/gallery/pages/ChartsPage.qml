import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtCharts         2.0

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    ColumnLayout {

        spacing: 20

        anchors.fill: parent;
        anchors.topMargin: 20;

        X.LabelHeadline4 {

            Layout.fillWidth: true;

            leftPadding: 20;
            rightPadding: 20;

            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Charts"
        }

        X.Separator {
            Layout.fillWidth: true;
        }

        Flickable {

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            contentHeight: _layout.height;

            clip: true;

            Grid {

                id: _layout;

                width: parent.width;

                columns: 2;
                columnSpacing: 2;

                ChartView {
                    title: "Top-5 car brand shares in Finland"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    legend.alignment: Qt.AlignBottom
                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    theme: ChartView.ChartThemeDark;

                    PieSeries {
                        id: pieSeries
                        PieSlice { label: "Volkswagen"; value: 13.5 }
                        PieSlice { label: "Toyota"; value: 10.9 }
                        PieSlice { label: "Ford"; value: 8.6 }
                        PieSlice { label: "Skoda"; value: 8.2 }
                        PieSlice { label: "Volvo"; value: 6.8 }
                    }
                }

                ChartView {
                    title: "Line"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    LineSeries {
                        name: "LineSeries"
                        XYPoint { x: 0; y: 0 }
                        XYPoint { x: 1.1; y: 2.1 }
                        XYPoint { x: 1.9; y: 3.3 }
                        XYPoint { x: 2.1; y: 2.1 }
                        XYPoint { x: 2.9; y: 4.9 }
                        XYPoint { x: 3.4; y: 3.0 }
                        XYPoint { x: 4.1; y: 3.3 }
                    }
                }

                ChartView {
                    title: "Spline"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    SplineSeries {
                        name: "SplineSeries"
                        XYPoint { x: 0; y: 0.0 }
                        XYPoint { x: 1.1; y: 3.2 }
                        XYPoint { x: 1.9; y: 2.4 }
                        XYPoint { x: 2.1; y: 2.1 }
                        XYPoint { x: 2.9; y: 2.6 }
                        XYPoint { x: 3.4; y: 2.3 }
                        XYPoint { x: 4.1; y: 3.1 }
                    }
                }

                ChartView {
                    title: "NHL All-Star Team Players"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    ValueAxis {
                        id: valueAxis
                        min: 2000
                        max: 2011
                        tickCount: 12
                        labelFormat: "%.0f"
                    }

                    AreaSeries {
                        name: "Russian"
                        axisX: valueAxis
                        upperSeries: LineSeries {
                            XYPoint { x: 2000; y: 1 }
                            XYPoint { x: 2001; y: 1 }
                            XYPoint { x: 2002; y: 1 }
                            XYPoint { x: 2003; y: 1 }
                            XYPoint { x: 2004; y: 1 }
                            XYPoint { x: 2005; y: 0 }
                            XYPoint { x: 2006; y: 1 }
                            XYPoint { x: 2007; y: 1 }
                            XYPoint { x: 2008; y: 4 }
                            XYPoint { x: 2009; y: 3 }
                            XYPoint { x: 2010; y: 2 }
                            XYPoint { x: 2011; y: 1 }
                        }
                    }

                    AreaSeries {
                        name: "Swedish"
                        axisX: valueAxis
                        upperSeries: LineSeries {
                            XYPoint { x: 2000; y: 1 }
                            XYPoint { x: 2001; y: 1 }
                            XYPoint { x: 2002; y: 3 }
                            XYPoint { x: 2003; y: 3 }
                            XYPoint { x: 2004; y: 2 }
                            XYPoint { x: 2005; y: 0 }
                            XYPoint { x: 2006; y: 2 }
                            XYPoint { x: 2007; y: 1 }
                            XYPoint { x: 2008; y: 2 }
                            XYPoint { x: 2009; y: 1 }
                            XYPoint { x: 2010; y: 3 }
                            XYPoint { x: 2011; y: 3 }
                        }
                    }

                    AreaSeries {
                        name: "Finnish"
                        axisX: valueAxis
                        upperSeries: LineSeries {
                            XYPoint { x: 2000; y: 0 }
                            XYPoint { x: 2001; y: 0 }
                            XYPoint { x: 2002; y: 0 }
                            XYPoint { x: 2003; y: 0 }
                            XYPoint { x: 2004; y: 0 }
                            XYPoint { x: 2005; y: 0 }
                            XYPoint { x: 2006; y: 1 }
                            XYPoint { x: 2007; y: 0 }
                            XYPoint { x: 2008; y: 0 }
                            XYPoint { x: 2009; y: 0 }
                            XYPoint { x: 2010; y: 0 }
                            XYPoint { x: 2011; y: 1 }
                        }
                    }
                }

                ChartView {
                    title: "Scatters"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    ScatterSeries {
                        id: scatter1
                        name: "Scatter1"
                        XYPoint { x: 1.5; y: 1.5 }
                        XYPoint { x: 1.5; y: 1.6 }
                        XYPoint { x: 1.57; y: 1.55 }
                        XYPoint { x: 1.8; y: 1.8 }
                        XYPoint { x: 1.9; y: 1.6 }
                        XYPoint { x: 2.1; y: 1.3 }
                        XYPoint { x: 2.5; y: 2.1 }
                    }

                    ScatterSeries {
                        name: "Scatter2"
                        XYPoint { x: 2.0; y: 2.0 }
                        XYPoint { x: 2.0; y: 2.1 }
                        XYPoint { x: 2.07; y: 2.05 }
                        XYPoint { x: 2.2; y: 2.9 }
                        XYPoint { x: 2.4; y: 2.7 }
                        XYPoint { x: 2.67; y: 2.65 }
                    }
                }

                ChartView {
                    title: "Bar series"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend.alignment: Qt.AlignBottom
                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    BarSeries {
                        id: mySeries
                        axisX: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Stacked Bar series"
                    antialiasing: true

                    legend.alignment: Qt.AlignBottom

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    StackedBarSeries {
                        axisX: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Percent Bar series"
                    antialiasing: true

                    legend.alignment: Qt.AlignBottom

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    PercentBarSeries {
                        axisX: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Horizontal Bar series"
                    antialiasing: true

                    legend.alignment: Qt.AlignBottom
                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    HorizontalBarSeries {
                        axisY: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Horizontal Stacked Bar series"
                    antialiasing: true

                    legend.alignment: Qt.AlignBottom
                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    HorizontalStackedBarSeries {
                        axisY: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Horizontal Percent Bar series"
                    antialiasing: true

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    legend.alignment: Qt.AlignBottom
                    legend {
                        borderColor: X.Style.borderColor
                        color: X.Style.backgroundColor
                        font: X.Style.typography.hint1;
                        labelColor: X.Style.textColor;
                    }

                    HorizontalPercentBarSeries {
                        axisY: BarCategoryAxis { categories: ["2007", "2008", "2009", "2010", "2011", "2012" ] }
                        BarSet { label: "Bob"; values: [2, 2, 3, 4, 5, 6] }
                        BarSet { label: "Susan"; values: [5, 1, 2, 4, 1, 7] }
                        BarSet { label: "James"; values: [3, 5, 8, 13, 5, 8] }
                    }
                }

                ChartView {
                    title: "Production costs"
                    antialiasing: true

                    legend.visible: false

                    height: width;
                    width: parent.width / 2;

                    titleColor: X.Style.textColor
                    titleFont: X.Style.typography.headline5;

                    backgroundColor: "transparent";
                    backgroundRoundness: 0;

                    margins.top: 0;
                    margins.left: 0;
                    margins.right: 0;
                    margins.bottom: 0;

                    theme: ChartView.ChartThemeDark;

                    PieSeries {
                        size: 0.96
                        holeSize: 0.7
                        PieSlice { id: slice; label: "Alpha"; value: 19511; color: "#99CA53" }
                        PieSlice { label: "Epsilon"; value: 11105; color: "#209FDF" }
                        PieSlice { label: "Psi"; value: 9352; color: "#F6A625" }
                    }

                    PieSeries {
                        size: 0.7

                        holeSize: 0.25

                        PieSlice { label: "Materials"; value: 10334; color: "#B9DB8A" }
                        PieSlice { label: "Employee"; value: 3066; color: "#DCEDC4" }
                        PieSlice { label: "Logistics"; value: 6111; color: "#F3F9EB" }

                        PieSlice { label: "Materials"; value: 7371; color: "#63BCE9" }
                        PieSlice { label: "Employee"; value: 2443; color: "#A6D9F2" }
                        PieSlice { label: "Logistics"; value: 1291; color: "#E9F5FC" }

                        PieSlice { label: "Materials"; value: 4022; color: "#F9C36C" }
                        PieSlice { label: "Employee"; value: 3998; color: "#FCE1B6" }
                        PieSlice { label: "Logistics"; value: 1332; color: "#FEF5E7" }
                    }
                }
            }

            ScrollBar.vertical: ScrollBar {

                visible: _layout.height > parent.height;
            }
        }
    }
}
