import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Style     1.0 as X

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "The Dial is similar to a traditional dial knob that is found on devices such as "
            + "stereos or industrial equipment. It allows the user to specify a value within a range."
        }

        X.Dial {

            id: dial;

            anchors.horizontalCenter: parent.horizontalCenter

            from: 0;
            to: 100;
            value: 20;

            hideProgress: true
            hideTrack: true
            width: 300
            height: 300

            interactive: false

            Repeater {
                model: 72

                Rectangle {
                    id: indicator
                    width: 5
                    height: 20
                    radius: width / 2
                    color:
                       (indicator.angle >= 360 + dial.startAngle || indicator.angle <= dial.endAngle)
                    ?  (indicator.angle >= 360 + dial.startAngle && indicator.angle < 360 + dial.angle)
                    || (indicator.angle <= dial.endAngle && indicator.angle <= dial.angle)
                    ? X.Style.foregroundColor
                    : X.Style.backgroundColor
                    : "transparent";

                    readonly property real angle: index * 5
                    transform: [
                    Translate {
                        x: dial.width / 2 - width / 2
                    },
                    Rotation {
                        origin.x: dial.width / 2
                        origin.y: dial.height / 2
                        angle: indicator.angle
                    }
                    ]
                }
            }
        }
    }
}
