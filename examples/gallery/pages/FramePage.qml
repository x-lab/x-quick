import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    readonly property int itemWidth: Math.max(button.implicitWidth, Math.min(button.implicitWidth * 3, page.availableWidth / 3 * 2))

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Frame is used to layout a logical group of controls together, within a visual frame."
        }

        Frame {
            anchors.horizontalCenter: parent.horizontalCenter

            Column {
                spacing: 20
                width: page.itemWidth

                RadioButton {
                    text: "First"
                    checked: true
                    width: parent.width
                }
                RadioButton {
                    id: button
                    text: "Second"
                    width: parent.width
                }
                RadioButton {
                    text: "Third"
                    width: parent.width
                }
            }
        }
    }
}
