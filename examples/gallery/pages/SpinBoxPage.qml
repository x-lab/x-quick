import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick          1.0 as X
import xQuick.Controls 1.0 as X

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "SpinBox allows the user to choose an integer value by clicking the up or down indicator buttons, "
                + "by pressing up or down on the keyboard, or by entering a text value in the input field."
        }

        X.ScientificSpinBoxReal {
            id: _sdbox
            value: 3.14159
            height: 32
            from: -100
            to: 100
            width: parent.width * 0.5
            anchors.horizontalCenter: parent.horizontalCenter
            editable: true
        }

        X.ScientificSpinBoxInteger {
            id: _sibox
            value: 9
            height: 32
            from: -10
            to: 10
            width: parent.width * 0.5
            anchors.horizontalCenter: parent.horizontalCenter
            editable: true
        }


        SpinBox {
            id: box
            value: 50
            height: 32
            width: parent.width * 0.5
            anchors.horizontalCenter: parent.horizontalCenter
            editable: true
        }
    }
}
