import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Shell     1.0 as XS
import xQuick.Style     1.0 as X

Page {
    id: _page

    X.ShellScreen {
        id: terminal
        anchors.fill: parent
    }
}
