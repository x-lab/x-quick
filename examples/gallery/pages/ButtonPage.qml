import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X

Page {

    id: _page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Button presents a push-button that can be pushed or clicked by the user. "
                + "Buttons are normally used to perform an action, or to answer a question."
        }

        ColumnLayout {
            spacing: 20
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                text: "First"
                Layout.fillWidth: true
            }
            Button {
                id: button
                text: "Second"
                highlighted: true
                Layout.fillWidth: true
            }
            Button {
                text: "Third"
                enabled: false
                Layout.fillWidth: true
            }
            X.ButtonFlat {
                text: "Flat"
                Layout.fillWidth: true
            }
            X.ButtonFlat {
                iconSource: X.Icons.icons.circle;
                text: "Flat"
                Layout.fillWidth: true
            }
            X.ButtonSquared {
                text: "Squared"
                Layout.fillWidth: true
            }
            X.ButtonSquared {
                iconSource: X.Icons.icons.circle;
                text: "Flat"
                Layout.fillWidth: true
            }
            X.ButtonRound {
                text: "Round"
                Layout.fillWidth: true
            }
            X.ButtonRound {
                iconSource: X.Icons.icons.circle;
                text: "Round with icon"
                Layout.fillWidth: true
            }
        }
    }
}
