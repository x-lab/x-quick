import QtQuick          2.15
import QtQuick.Controls 2.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {
    id: page

    X.Tour {

        id: _tutorial;

        steps: [
            {
                title: "Step 1",
                description: "Welcome to this cool app ! Are you ready for a quick overview ? ",
                step: 1,
                hasButton: true,
                buttonText: "Let's Go ! ",
                name: "step_1",
                position: X.Tour.Position.Bottom,
                alignment: X.Tour.HorizontalAlignment.Center
            },
            {
                title: "Step 2",
                description: "This is a text area, you can type something in there. Go ahead try it out !",
                step: 2,
                hasButton: false,
                name: "step_2",
                position: X.Tour.Position.Bottom,
                alignment: X.Tour.HorizontalAlignment.Center
            },
            {
                title: "Step 3",
                description: "There is a button here ! Why don't you click it ? ",
                step: 3,
                hasButton: true,
                name: "step_3",
                position: X.Tour.Position.Left,
                alignment: X.Tour.VerticalAlignment.Bottom
            },
        ]
    }


    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;
        anchors.leftMargin: 20;
        anchors.rightMargin: 20;

        Label {
            id: _label

            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "TourStep allows to design popup tutorials for your applications."
            objectName: "step_1"

        }

        TextArea {
            width: parent.width;
            height: 300;

            anchors.horizontalCenter: parent.horizontalCenter

            wrapMode: TextArea.Wrap
            text: "TextArea\n...\n...\n..."
            objectName: "step_2"

            onTextChanged: {
                if(_tutorial.tourActive && _tutorial.currentStep === 2) _tutorial.nextStep();
            }

        }

        Button {
            anchors.right: parent.right
            text: "Cool button"
            objectName: "step_3"
        }
    }

    Component.onCompleted: {
        _tutorial.startTour();
    }

}
