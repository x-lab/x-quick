import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Models    1.0 as X
import xQuick.Style     1.0 as X
import xQuick.TTS       1.0 as X

Page {

    id: _page

    ColumnLayout {

        spacing: 20

        anchors.fill: parent;
        anchors.topMargin: 20;
        anchors.leftMargin: 10;
        anchors.rightMargin: 10;

        X.LabelHeadline4 {

            Layout.fillWidth: true;

            leftPadding: 20;
            rightPadding: 20;

            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Text To Speech (TTS)"
        }

        X.Frame {

            id: _banner;

            Layout.fillWidth: true;

            Column {
                spacing: 10

                width: parent.width;

                X.LabelIcon {
                    iconSource: X.Icons.icons.sticky_note_2;
                    text: "TODO";
                    color: X.Style.alertColor;
                    font: X.Style.typography.headline5;
                }

                X.LabelCaption {
                    width: parent.width;
                    wrapMode: Text.WordWrap;
                    text: "The presentation of this page follows jurassic design principles. Turn that into something modern, perhapes using item delegates and groups.";
                }
            }
        }

        X.TextField {

            id: _field;

            placeholderText: "Simplicity is the ultimate sophistication.";

            Layout.fillWidth: true;
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Volume";
            }

            Slider {

                id: _speech_volume;

                from: 0;
                to: 100;
                value: 100;
                Layout.fillWidth: true;

                onValueChanged: {
                    speech.setVolume(_speech_volume.value);
                }
            }
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Rate";
            }

            Slider {

                id: _speech_rate;

                from: -10;
                to: 10;
                value: 0;
                Layout.fillWidth: true;

                onValueChanged: {
                    speech.setRate(_speech_rate.value);
                }
            }
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Pitch";
            }

            Slider {
                id: _speech_pitch;

                from: -10;
                to: 10;
                value: 0;
                Layout.fillWidth: true;

                onValueChanged: {
                    speech.setPitch(_speech_pitch.value);
                }
            }
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Engine";
            }

            ComboBox {

                id: _speech_engines;

                Layout.fillWidth: true;

                model: speech.availableEngines;

                onActivated: {

                    speech.engineSelected(speech.availableEngines[index]);
                }
            }
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Language";
            }

            ComboBox {

                id: _speech_languages;

                Layout.fillWidth: true;

                model: speech.languages;

                onActivated: {

                    speech.languageSelected(speech.languages[index]);
                }
            }
        }

        RowLayout {

            Layout.fillWidth: true;

            Label {
                text: "Voice";
            }

            ComboBox {

                id: _speech_voices;

                Layout.fillWidth: true;

                model: speech.voices;

                onActivated: {

                    speech.voiceSelected(speech.voices[index]);
                }
            }
        }

        X.SegmentedCell {

            Layout.fillWidth: true;

            X.SegmentedCellSegment {
                iconSource: X.Icons.icons.play_circle;

                onClicked: {
                    speech.speak(_field.text !== '' ? _field.text : _field.placeholderText);
                }
            }
            X.SegmentedCellSegment {
                iconSource: X.Icons.icons.pause_circle;

                onClicked: speech.pause();
            }
            X.SegmentedCellSegment {
                iconSource: X.Icons.icons.play_circle_outline;

                onClicked: speech.resume();
            }
            X.SegmentedCellSegment {
                iconSource: X.Icons.icons.stop_circle;

                onClicked: speech.stop();
            }
        }

        Item { Layout.fillHeight: true; }
    }

    X.TTSEngine {
        id: speech;
    }
}
