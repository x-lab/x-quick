import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Models    1.0 as X
import xQuick.Style     1.0 as X

Page {
    id: _self;

    property var spinners: ["circles"]

    TextField {

        id: _field;

        height: 32;

        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 10;

        onTextChanged: _model.refilter();

        X.Icon {
            icon: X.Icons.icons.search;
            color: X.Style.foregroundColor;

            anchors.right: parent.right;
            anchors.rightMargin: 10;
            anchors.verticalCenter: parent.verticalCenter;
        }
    }

    GridView {

        id: _view;

        anchors.top: _field.bottom;
        anchors.topMargin: 10;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;

        cellWidth: parent.width / 4;
        cellHeight: cellWidth;

        clip: true;

        model: X.FilterProxyModel
        {
            id: _model

            model: _self.spinners;
            delegate: X.Card {

                topInset: 5;
                leftInset: 5;
                rightInset: 5;
                bottomInset: 5;

                width: _view.cellWidth;
                height: _view.cellHeight;

                X.Spinner {
                    id: _icon;
                    type: _self.spinners[index];
                    size: _view.cellWidth / 3;
                    color: X.Style.accentColor;
                    anchors.centerIn: parent;
                }
                X.LabelHint1 {
                    text: _self.spinners[index];
                    anchors.top: _icon.bottom;
                    anchors.topMargin: 10;
                    anchors.horizontalCenter: parent.horizontalCenter;
                }
            }

            filterAccepts: function(item) {

                return _self.spinners[item.index].includes(_field.text);
            }
        }

        ScrollBar.vertical: ScrollBar {}
    }

    X.Separator {
        anchors.top: _view.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
    }
}
