import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    X.Console {

        id: _console;

        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;

        height: _page.height - 32;

        X.Separator {
            anchors.top: parent.bottom;
            anchors.left: parent.left;
            anchors.right: parent.right;
        }
    }

    RowLayout {

        anchors.top: _console.bottom;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.margins: 3;

        X.TextField { id: _field;
            Layout.minimumHeight: 28;
            Layout.maximumHeight: 28;
            Layout.fillWidth: true;
        }

        Button { Layout.maximumHeight: 28; text: "Debug"; onClicked: if(_field.text !== '') console.debug(_field.text); }
        Button { Layout.maximumHeight: 28; text: "Info"; onClicked: if(_field.text !== '') console.info(_field.text); }
        Button { Layout.maximumHeight: 28; text: "Warning"; onClicked: if(_field.text !== '') console.warn(_field.text); }
        Button { Layout.maximumHeight: 28; text: "Error"; onClicked: if(_field.text !== '') console.error(_field.text); }
    }
}
