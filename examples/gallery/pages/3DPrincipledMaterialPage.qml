import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick3D        1.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page;

    View3D {
        anchors.fill: parent
        camera: camera
        // renderMode: View3D.Underlay

        DirectionalLight {
            eulerRotation.y: -100
            brightness: 1
            SequentialAnimation on eulerRotation.y {
                loops: Animation.Infinite
                PropertyAnimation {
                    duration: 5000
                    to: 360
                    from: 0
                }
            }
        }

        environment: SceneEnvironment {
            clearColor: "transparent";
            // backgroundMode: SceneEnvironment.SkyBox
            lightProbe: Texture {
                source: "qrc:/examples/gallery/pages/3DPrincipledMaterialPageEnvironment.hdr"
            }
            probeOrientation: Qt.vector3d(0, -90, 0)
        }

        PerspectiveCamera {
            id: camera
            position: Qt.vector3d(0, 0, 600)
        }

        Model {
            position: Qt.vector3d(-250, -30, 0)
            scale: Qt.vector3d(4, 4, 4)
            source: "#Sphere"
            materials: [ PrincipledMaterial {
                baseColor: "#41cd52"
                metalness: materialCtrl.metalness
                roughness: materialCtrl.roughness
                opacity: materialCtrl.opacityValue
            }
            ]
        }

        Model {
            position: Qt.vector3d(250, -30, 0)
            scale: Qt.vector3d(4, 4, 4)
            source: "#Sphere"
            materials: [ PrincipledMaterial {
                metalness: materialCtrl.metalness
                roughness: materialCtrl.roughness
                opacity: materialCtrl.opacityValue

                baseColorMap: Texture { source: "qrc:/examples/gallery/pages/3DPrincipledMaterialPageBase.jpg" }
                metalnessMap: Texture { source: "qrc:/examples/gallery/pages/3DPrincipledMaterialPageMetallic.jpg" }
                roughnessMap: Texture { source: "qrc:/examples/gallery/pages/3DPrincipledMaterialPageRoughness.jpg" }
                normalMap:    Texture { source: "qrc:/examples/gallery/pages/3DPrincipledMaterialPageNormal.jpg" }

                metalnessChannel: Material.R
                roughnessChannel: Material.R
            }
            ]

            SequentialAnimation on eulerRotation {
                loops: Animation.Infinite
                PropertyAnimation {
                    duration: 5000
                    from: Qt.vector3d(0, 0, 0)
                    to: Qt.vector3d(360, 360, 360)
                }
            }
        }
    }

    Rectangle {

        id: materialCtrl;

        property real metalness: metalness.sliderValue
        property real roughness: roughness.sliderValue
        property real opacityValue: opacityValue.sliderValue

        color: "transparent"
        width: parent.width
        height: 75

        Component {
            id: propertySlider
            RowLayout {
                X.Label {
                    id: propText
                    text: name
                }
                Slider {
                    id: slider
                    from: fromValue
                    to: toValue
                    value: sliderValue
                    stepSize: 0.01
                    onValueChanged: sliderValue = value
                    Layout.fillWidth: true;
                }
                X.Label {
                    id: valueText
                    text: slider.value.toFixed(2)
                }
            }
        }

        ColumnLayout {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.leftMargin: 10;
            anchors.right: parent.right;
            anchors.rightMargin: 10;

            spacing: 20
            
            Loader {
                id: metalness
                property real sliderValue: 1.0
                property string name: "Metalness"
                property real fromValue: 0.0
                property real toValue: 1.0
                sourceComponent:  propertySlider
                Layout.fillWidth: true;
            }
            Loader {
                id: roughness
                property real sliderValue: 0.1
                property string name: "Roughness"
                property real fromValue: 0.0
                property real toValue: 1.0
                sourceComponent:  propertySlider
                Layout.fillWidth: true;
            }

            Loader {
                id: opacityValue
                property real sliderValue: 1.0
                property string name: "Opacity"
                property real fromValue: 0.0
                property real toValue: 1.0
                sourceComponent: propertySlider
                Layout.fillWidth: true;
            }
        }
    }
}
