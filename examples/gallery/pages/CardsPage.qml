import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Cards ...."
        }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

        X.Card
        {
            id: _card1;

            media: "https://static01.nyt.com/images/2020/12/10/travel/10europe-02/10europe-02-facebookJumbo.jpg"
            headerText: "Title goes here"
            supportingText: "Cards contain content and actions about a single subject."

            anchors.left: parent.left;
            anchors.leftMargin: 20;

            anchors.right: parent.right;
            anchors.rightMargin: 20;

            contentItem: ColumnLayout
            {
                width: parent.width

                // spacing: X.Style.card.verticalPadding

                RowLayout
                {
                    Layout.topMargin: X.Style.card.horizontalPadding

                    X.CardMedia
                    {
                        id: _thumbnail1

                        source: _card1.media

                        clipTop: true

                        Layout.maximumHeight: 80;
                        Layout.maximumWidth: 80;
                        Layout.leftMargin: X.Style.card.horizontalPadding
                    }

                    ColumnLayout
                    {
                        Layout.fillWidth: true

                        X.LabelHeadline5
                        {
                            text: _card1.headerText
                            elide: Text.ElideRight
                            Layout.leftMargin: X.Style.card.horizontalPadding
                            Layout.rightMargin: X.Style.card.horizontalPadding
                            Layout.fillWidth: true
                        }

                        X.CardSupportingText
                        {
                            supportingText: _card1.supportingText
                            Layout.leftMargin: X.Style.card.horizontalPadding
                            Layout.rightMargin: X.Style.card.horizontalPadding
                            Layout.topMargin: 2
                            Layout.bottomMargin: 2
                            Layout.fillWidth: true
                        }
                    }
                }

                RowLayout
                {
                    Layout.leftMargin: X.Style.card.verticalPadding
                    Layout.rightMargin: X.Style.card.verticalPadding

                    X.ButtonFlat
                    {
                        id: _button1
                        text: "Action 1"
                        Layout.fillWidth: true;
                    }

                    X.ButtonFlat
                    {
                        id: _button2
                        text: "Action 2"
                        Layout.fillWidth: true;
                    }
                }
            }
        }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

        X.Card
        {
            id: _card2

            media: "https://assets.unenvironment.org/s3fs-public/styles/topics_content_promo/public/2021-02/seljalandsfoss-1751463_1920.jpg?VersionId=null&itok=x4XsNG_"
            headerText: "Our Changing Planet"
            subHeaderText: "by Kurt Wagner"
            supportingText: "Visit ten places on our planet that are undergoing the biggest changes today."

            anchors.left: parent.left;
            anchors.leftMargin: 20;

            anchors.right: parent.right;
            anchors.rightMargin: 20;

            contentItem: ColumnLayout
            {
                width: parent.width
                spacing: X.Style.card.horizontalPadding

                X.CardMedia
                {
                    source: _card2.media
                    clipTop: true
                    Layout.fillWidth: true
                    Layout.maximumHeight: X.Style.card.mediaImplicitHeight
                }

                X.CardTitle
                {
                    headerText: _card2.headerText
                    subHeaderText: _card2.subHeaderText
                    Layout.leftMargin: X.Style.card.horizontalPadding
                    Layout.rightMargin: X.Style.card.horizontalPadding
                    Layout.fillWidth: true
                }

                X.CardSupportingText
                {
                    supportingText: _card2.supportingText
                    Layout.leftMargin: X.Style.card.horizontalPadding
                    Layout.rightMargin: X.Style.card.horizontalPadding
                    Layout.topMargin: 2
                    Layout.bottomMargin: 2
                    Layout.fillWidth: true
                }

                RowLayout
                {
                    Layout.leftMargin: X.Style.card.verticalPadding
                    Layout.rightMargin: X.Style.card.verticalPadding
                    Layout.fillWidth: true
                    implicitHeight: _button1.implicitHeight
                    implicitWidth: 200

                    X.ButtonFlat
                    {
                        id: _button3
                        text: "Read"
                        Layout.fillWidth: true;
                    }

                    X.ButtonFlat
                    {
                        id: _button4
                        text: "Bookmark"
                        Layout.fillWidth: true;
                    }

                    X.ButtonSquared
                    {
                        id: _button5
                        foregroundColor: X.Style.foregroundColor;
                        text: "Prout 1";
                        iconSource: X.Icons.icons.ac_unit;
                        Layout.fillWidth: true;
                    }

                    X.ButtonSquared
                    {
                        id: _button6
                        foregroundColor: X.Style.foregroundColor;
                        text: "Prout 2";
                        iconSource: X.Icons.icons.water;
                        Layout.fillWidth: true;
                    }
                }
            }
        }
    }
}
