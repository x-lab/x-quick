import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "ProgressBar indicates the progress of an operation. It can be set in an "
                + "indeterminate mode to indicate that the length of the operation is unknown."
        }

        ProgressBar {
            id: bar
            value: 0.5
            anchors.horizontalCenter: parent.horizontalCenter
        }

        ProgressBar {
            indeterminate: true
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
