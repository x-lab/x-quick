import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "TextField is a single-line text editor."
        }

        TextField {
            id: field
            placeholderText: "TextField"
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
