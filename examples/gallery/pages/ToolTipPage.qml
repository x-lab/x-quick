import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "A tool tip is a short piece of text that informs the user of a control's function."
        }

        Button {
            text: "Tip"
            anchors.horizontalCenter: parent.horizontalCenter

            ToolTip.timeout: 5000
            ToolTip.visible: pressed
            ToolTip.text: "This is a tool tip."
        }
    }
}
