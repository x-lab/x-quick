import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    ColumnLayout {

        spacing: 20

        anchors.fill: parent;
        anchors.topMargin: 20;

        X.LabelHeadline4 {

            Layout.fillWidth: true;

            leftPadding: 20;
            rightPadding: 20;

            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "SegmentedCell"
        }

        X.SegmentedCell {

            id: _cell;

            Layout.leftMargin: 10;
            Layout.rightMargin: 10;
            Layout.fillWidth: true;
            // Layout.fillHeight: true;

            X.SegmentedCellSegment { text: "1"; }
            X.SegmentedCellSegment { text: "2"; }
            X.SegmentedCellSegment { text: "3"; }
        }

        Item { Layout.fillHeight: true; }
    }
}
