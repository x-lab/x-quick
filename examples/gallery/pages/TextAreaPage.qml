import QtQuick          2.15
import QtQuick.Controls 2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;
        anchors.leftMargin: 20;
        anchors.rightMargin: 20;

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "TextArea is a multi-line text editor."
        }

        TextArea {
            width: parent.width;
            height: 300;

            anchors.horizontalCenter: parent.horizontalCenter

            wrapMode: TextArea.Wrap
            text: "TextArea\n...\n...\n..."
        }
    }
}
