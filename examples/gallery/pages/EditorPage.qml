import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page

    ColumnLayout {

        spacing: 20

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {

            Layout.fillWidth: true;

            leftPadding: 20;
            rightPadding: 20;

            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Editor"
        }

        X.Editor {

            id: _editor;

            contents: "Lorem ipsum dolor sit amet, consectetur adipiscing
elit. Nam condimentum molestie purus, a vehicula quam varius ultricies. Cras
laoreet massa a augue volutpat, nec efficitur nisi tristique. In hac habitasse
platea dictumst. Etiam fringilla turpis ex, vel dignissim tortor ornare ac.
Donec scelerisque maximus tempor. Aenean tristique ante a quam pretium, id
tempus ipsum sollicitudin. Maecenas finibus, ex vel ornare ultricies, augue
metus suscipit felis, sed scelerisque nulla dolor in sem. Nulla facilisis
tincidunt sollicitudin. Vestibulum placerat, nisl non congue elementum, urna ex
sollicitudin orci, a luctus lacus diam ac nisl. Aenean accumsan vitae augue id
aliquam. Mauris pellentesque mauris est, et posuere massa vulputate sit amet.
Sed tincidunt, lacus sit amet vestibulum fringilla, nibh odio malesuada sapien,
ut tristique magna turpis non arcu.

Integer faucibus ligula et dictum ullamcorper. Nullam scelerisque, orci at
pellentesque tristique, ex mauris gravida neque, ultrices dignissim ipsum nisi
id tortor. Nam sit amet vulputate urna. Pellentesque eros arcu, elementum nec
nisi nec, eleifend ornare augue. Sed ullamcorper, est at egestas mattis, quam
augue laoreet orci, ac maximus ante neque eu nisi. Aenean tellus orci, ultrices
id lacus ac, convallis fringilla augue. Ut at tortor non ex ullamcorper porta.
Mauris id ullamcorper dui. In hac habitasse platea dictumst. Sed quis venenatis
libero. Sed sed justo sit amet neque lacinia sodales a sit amet quam. Nulla vel
leo ex.

Nunc vitae dapibus lectus. Aenean tincidunt, eros at faucibus sodales, ipsum
mauris dictum mi, eu ornare purus ipsum in tortor. Aliquam erat volutpat.
Aliquam id finibus diam, quis euismod augue. Ut sed mi at magna tempor
sollicitudin. Nulla elit nisl, viverra vitae laoreet ac, fringilla at lacus.
Pellentesque et tempus tortor. Fusce maximus accumsan congue. Donec eu odio in
diam dignissim mollis sit amet sed odio. Donec semper sit amet nisi ac dictum.
Praesent ut magna lacinia, pellentesque justo at, elementum leo. Phasellus
posuere ut augue ac semper. Maecenas mollis, dolor eget fringilla placerat,
lectus odio pharetra ante, nec placerat dolor tortor consectetur mauris. Nunc
vehicula tempor libero, tristique mattis tortor bibendum vel. Aenean ultricies
mattis lorem, at gravida diam pulvinar non. Nulla semper auctor velit non
pharetra.

Nam mattis mollis consectetur. Proin diam turpis, vulputate quis cursus at,
placerat sed erat. Integer eget viverra purus. In hac habitasse platea dictumst.
Sed justo metus, congue vitae nibh sed, consequat laoreet tellus. Nulla non mi
porttitor, efficitur mauris a, pulvinar mauris. Nunc venenatis in lorem sed
pellentesque. Aliquam sem dui, scelerisque at nisl nec, lacinia aliquet dui.
Nunc vitae venenatis lacus. Class aptent taciti sociosqu ad litora torquent per
conubia nostra, per inceptos himenaeos. Donec vel purus eget nulla venenatis
porttitor. Nulla dictum sapien at molestie euismod. Phasellus fermentum enim
felis, nec euismod tortor imperdiet in. Nam ac pharetra leo. Nam eu odio quis
felis imperdiet sollicitudin. Donec nec quam in ex semper rhoncus.

Fusce sollicitudin leo quam, sed facilisis arcu rhoncus id. Proin congue in erat
a ullamcorper. Nam porttitor lectus lectus, quis iaculis urna faucibus a.
Quisque sodales, nulla vitae venenatis ultrices, ante magna tincidunt nulla,
vitae ultricies purus ligula ut enim. Aliquam ut ligula massa. Praesent eleifend
orci risus. Nunc eget est diam.";

            // errors: ListModel {
            //
            //     id: _error_model;
            //
            //     ListElement {
            //         line: 1;
            //         message: "An example annotation for first paragraph";
            //     }
            //
            //     ListElement {
            //         line: 14;
            //         message: "Same for second one";
            //     }
            // }

            X.Separator {
                anchors.top: parent.top;
                anchors.left: parent.left;
                anchors.right: parent.right;
            }

            Layout.fillWidth: true;
            Layout.fillHeight: true;
        }
    }
}
