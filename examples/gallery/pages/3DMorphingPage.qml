import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15
import QtQuick3D        1.15

import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: _page;

    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 10
        width: parent.width
        spacing: 10

        RowLayout {
            Layout.fillWidth: true;
            X.Label { text: "Mouth:"; Layout.preferredWidth: 120; }
            Slider {
                Layout.fillWidth: true;
                id: mouthSlider
                from: 0.0
                to: 1.0
            }
        }
        RowLayout {
            Layout.fillWidth: true;
            X.Label { text: "Ears and eyebrows:"; Layout.preferredWidth: 120; }
            Slider {
                Layout.fillWidth: true;
                id: earSlider
                from: 0.0
                to: 1.0
            }
        }

        RowLayout {
            Layout.fillWidth: true;
            X.Label { text: "Cubify:"; Layout.preferredWidth: 120; }
            Slider {
                Layout.fillWidth: true;
                id: cubeSlider
                from: 0.0
                to: 1.0
                value: 0.0;
            }
        }
    }

    z:1

    View3D {
        id: view
        anchors.fill: parent

        environment: SceneEnvironment {
            clearColor: "transparent"
            backgroundMode: SceneEnvironment.Color
        }

        PerspectiveCamera {
            id: camera
            position.z: 3.0
            position.y: 0.75
            eulerRotation.x: -12
            clipNear: 1.0
            clipFar: 60.0
        }

        DirectionalLight {
            eulerRotation.x: -30
            eulerRotation.y: -70
            ambientColor: Qt.rgba(0.3, 0.3, 0.3, 1.0)
        }

        MorphTarget {
            id: morphtarget0
            weight: mouthSlider.value
            attributes: MorphTarget.Position | MorphTarget.Normal
        }
        MorphTarget {
            id: morphtarget1
            weight: earSlider.value
            attributes: MorphTarget.Position | MorphTarget.Normal
        }
        MorphTarget {
            id: morphtarget2
            weight: cubeSlider.value
            attributes: MorphTarget.Position | MorphTarget.Normal
        }

        Model {
            source: "qrc:/examples/gallery/pages/3DMorphingPage.mesh"
            morphTargets: [
            morphtarget0,
            morphtarget1,
            morphtarget2
            ]
            materials: PrincipledMaterial {
                baseColor: X.Style.foregroundColor;
                roughness: 0.1
            }
            SequentialAnimation on eulerRotation.y {
                NumberAnimation { from: -45; to: 45; duration: 10000 }
                NumberAnimation { from: 45; to: -45; duration: 10000 }
                loops: Animation.Infinite
            }
        }
    }
}
