import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X
import xQuick.Vis       1.0 as XVis

Page {

    id: _page

    RowLayout {

        anchors.fill: parent;

        XVis.Viewer {
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            mouseEnabled: true;

            XVis.Actor {

                id: actor;

                XVis.PolyDataMapper {

                    XVis.ContourFilter {

                        id: contour;

                        XVis.SampleFunction {

                            id: sampler;

                            implicitFunction: XVis.Quadric {

                                id: quadric;
                            }
                        }
                    }
                }
            }
        }

        XVis.Viewer {
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            mouseEnabled: true;

            XVis.Actor {

                id: actor2;

                XVis.PolyDataMapper {

                    XVis.ContourFilter {

                        id: contour2;

                        XVis.SampleFunction {

                            id: sampler2;

                            implicitFunction: XVis.Quadric {

                                id: quadric2;
                            }
                        }
                    }
                }
            }
        }
    }
    X.Frame {

        id: _card;

        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.margins: 20;

        width: parent.width;
        height: 100;

        background: Rectangle {
            color: "#6628292a";
            border.color: Qt.darker(X.Style.borderColor, 1.3);
            radius: X.Style.controls.radius;
        }

        contentItem: Flickable {

            contentHeight: _layout.height;

            clip: true;

            ColumnLayout
            {

                id: _layout;

                anchors.fill: parent;
                anchors.margins: 10;

                spacing: 10;

                X.VisSlider {
                    from: sampler.sampleDimension; bind: "x"; label: "dimension.x"; min: 1; max: 100; value: 50; Layout.fillWidth: true;
                }

                X.VisSlider {
                    from: sampler.sampleDimension; bind: "y"; label: "dimension.y"; min: 1; max: 100; value: 50; Layout.fillWidth: true;
                }

                X.VisSlider {
                    from: sampler.sampleDimension; bind: "z"; label: "dimension.z"; min: 1; max: 100; value: 50; Layout.fillWidth: true;
                }
            }

            ScrollIndicator.vertical: ScrollIndicator {}
        }
    }

    Component.onCompleted: {
        quadric.setCoefficients(.5,1,.2,0,.1,0,0,.2,0,0);
        contour.generateValues(5,0,1.2);

        quadric2.setCoefficients(.5,1,.2,0,.1,0,0,.2,0,0);
        contour2.generateValues(5,0,1.2);
    }
}
