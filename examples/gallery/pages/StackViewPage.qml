import QtQuick          2.15
import QtQuick.Controls 2.15

StackView {
    id: stackView
    initialItem: page

    clip: true;

    Component {
        id: page

        Pane {
            id: pane
            width: parent ? parent.width : 0 // TODO: fix null parent on destruction

            Column {
                spacing: 40

                anchors.fill: parent;
                anchors.topMargin: 20;

                Label {
                    leftPadding: 20;
                    rightPadding: 20;
                    width: parent.width
                    wrapMode: Label.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    text: "StackView provides a stack-based navigation model which can be used with a set of interlinked pages. "
                    + "Items are pushed onto the stack as the user navigates deeper into the material, and popped off again "
                    + "when he chooses to go back."
                }

                Button {
                    id: button
                    text: "Push"
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: Math.max(button.implicitWidth, Math.min(button.implicitWidth * 2, pane.availableWidth / 3))
                    onClicked: stackView.push(page)
                }

                Button {
                    text: "Pop"
                    enabled: stackView.depth > 1
                    width: Math.max(button.implicitWidth, Math.min(button.implicitWidth * 2, pane.availableWidth / 3))
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: stackView.pop()
                }
            }
        }
    }
}
