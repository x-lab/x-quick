import QtQuick             2.15
import QtQuick.Controls    2.15
import QtQuick.Layouts     1.15
import QtDataVisualization 1.15

import xQuick.Controls     1.0 as X
import xQuick.Fonts        1.0 as X
import xQuick.Style        1.0 as X

Page {

    id: _page

    Item {

        id: data;

        property alias sharedData: dataModel

        ListModel {
            id: dataModel
            ListElement{ row: "0"; col: "0"; longitude: "20.0"; latitude: "10.0"; pop_density: "4.75"; }
            ListElement{ row: "1"; col: "0"; longitude: "21.1"; latitude: "10.3"; pop_density: "3.00"; }
            ListElement{ row: "2"; col: "0"; longitude: "22.5"; latitude: "10.7"; pop_density: "1.24"; }
            ListElement{ row: "3"; col: "0"; longitude: "24.0"; latitude: "10.5"; pop_density: "2.53"; }
            ListElement{ row: "0"; col: "1"; longitude: "20.2"; latitude: "11.2"; pop_density: "3.55"; }
            ListElement{ row: "1"; col: "1"; longitude: "21.3"; latitude: "11.5"; pop_density: "3.03"; }
            ListElement{ row: "2"; col: "1"; longitude: "22.6"; latitude: "11.7"; pop_density: "3.46"; }
            ListElement{ row: "3"; col: "1"; longitude: "23.4"; latitude: "11.5"; pop_density: "4.12"; }
            ListElement{ row: "0"; col: "2"; longitude: "20.2"; latitude: "12.3"; pop_density: "3.37"; }
            ListElement{ row: "1"; col: "2"; longitude: "21.1"; latitude: "12.4"; pop_density: "2.98"; }
            ListElement{ row: "2"; col: "2"; longitude: "22.5"; latitude: "12.1"; pop_density: "3.33"; }
            ListElement{ row: "3"; col: "2"; longitude: "23.3"; latitude: "12.7"; pop_density: "3.23"; }
            ListElement{ row: "0"; col: "3"; longitude: "20.7"; latitude: "13.3"; pop_density: "5.34"; }
            ListElement{ row: "1"; col: "3"; longitude: "21.5"; latitude: "13.2"; pop_density: "4.54"; }
            ListElement{ row: "2"; col: "3"; longitude: "22.4"; latitude: "13.6"; pop_density: "4.65"; }
            ListElement{ row: "3"; col: "3"; longitude: "23.2"; latitude: "13.4"; pop_density: "6.67"; }
            ListElement{ row: "0"; col: "4"; longitude: "20.6"; latitude: "15.0"; pop_density: "6.01"; }
            ListElement{ row: "1"; col: "4"; longitude: "21.3"; latitude: "14.6"; pop_density: "5.83"; }
            ListElement{ row: "2"; col: "4"; longitude: "22.5"; latitude: "14.8"; pop_density: "7.32"; }
            ListElement{ row: "3"; col: "4"; longitude: "23.7"; latitude: "14.3"; pop_density: "6.90"; }
        }
    }

    ColumnLayout {

        spacing: 10

        anchors.fill: parent;
        anchors.topMargin: 20;

        X.LabelHeadline4 {

            Layout.fillWidth: true;

            leftPadding: 20;
            rightPadding: 20;

            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "Data Visualisation"
        }

        X.Separator {
            Layout.fillWidth: true;
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.leftMargin: 10;
            Layout.rightMargin: 10;

            X.ButtonRound {
                Layout.fillWidth: true
                text: "Clear Selections"
                outlined: true;
                onClicked: _page.clearSelections() // call a helper function to keep button itself simpler
            }

            X.ButtonRound {
                Layout.fillWidth: true
                text: "Reset Cameras"
                outlined: true;
                onClicked: _page.resetCameras() // call a helper function to keep button itself simpler
            }

            X.ButtonRound {
                Layout.fillWidth: true
                text: "Toggle Mesh Styles"
                outlined: true;
                onClicked: _page.toggleMeshStyle() // call a helper function to keep button itself simpler
            }
        }

        X.Separator {
            Layout.fillWidth: true;
        }

        GridLayout {
            columns: 2
            Layout.fillHeight: true
            Layout.fillWidth: true

            Rectangle {
                Layout.fillHeight: true
                Layout.fillWidth: true
                border.color: surfaceGraph.theme.gridLineColor
                border.width: 2
                color: "#00000000"

                Surface3D {
                    id: surfaceGraph
                    anchors.fill: parent
                    anchors.margins: parent.border.width
                    theme: Theme3D {
                        type: Theme3D.ThemePrimaryColors
                        font.pointSize: 60;
                        backgroundColor: "transparent";
                        backgroundEnabled: true;
                        windowColor: "#11ff0000";
                    }
                    scene.activeCamera.cameraPreset: Camera3D.CameraPresetIsometricLeftHigh

                    Surface3DSeries {
                        itemLabelFormat: "Pop density at (@xLabel N, @zLabel E): @yLabel"
                        ItemModelSurfaceDataProxy {
                            itemModel: data.sharedData
                            // The surface data points are not neatly lined up in rows and columns,
                            // so we define explicit row and column roles.
                            rowRole: "row"
                            columnRole: "col"
                            xPosRole: "latitude"
                            zPosRole: "longitude"
                            yPosRole: "pop_density"
                        }
                    }
                }
            }

            Rectangle {
                Layout.fillHeight: true
                Layout.fillWidth: true
                // border.color: scatterGraph.theme.gridLineColor
                // border.width: 2
                color: "transparent"

                Scatter3D {
                    id: scatterGraph
                    anchors.fill: parent
                    anchors.margins: 0; // parent.border.width
                    theme: Theme3D {
                        type: Theme3D.ThemeEbony
                        font.pointSize: 60

                        gridLineColor: "transparent";
                        backgroundColor: "transparent";
                        backgroundEnabled: true;
                        windowColor: "transparent";
                    }
                    scene.activeCamera.cameraPreset: Camera3D.CameraPresetIsometricLeftHigh

                    Scatter3DSeries {
                        itemLabelFormat: "Pop density at (@xLabel N, @zLabel E): @yLabel"
                        ItemModelScatterDataProxy {
                            itemModel: data.sharedData
                            // Mapping model roles to scatter series item coordinates.
                            xPosRole: "latitude"
                            zPosRole: "longitude"
                            yPosRole: "pop_density"
                        }
                    }
                }
            }

            // Rectangle {
            //
            //     border.color: barGraph.theme.gridLineColor
            //     border.width: 2
            //     color: "transparent"

                Bars3D {
                    id: barGraph
                    // anchors.fill: parent
                    // anchors.margins: parent.border.width

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.columnSpan: 2

                    theme: Theme3D {
                        type: Theme3D.ThemeEbony
                        font.pointSize: 60
                        backgroundColor: "transparent";
                        backgroundEnabled: true;
                        labelBackgroundColor : "transparent";
                        labelBackgroundEnabled : false;
                        windowColor: "transparent";
                    }
                    selectionMode: AbstractGraph3D.SelectionItemAndRow | AbstractGraph3D.SelectionSlice
                    scene.activeCamera.cameraPreset: Camera3D.CameraPresetIsometricLeftHigh

                    Bar3DSeries {
                        itemLabelFormat: "@seriesName: @valueLabel"
                        name: "Population density"

                        ItemModelBarDataProxy {
                            itemModel: data.sharedData
                            // Mapping model roles to bar series rows, columns, and values.
                            rowRole: "row"
                            columnRole: "col"
                            valueRole: "pop_density"
                        }
                    }
                }
            // }
        }
    }

    function clearSelections() {
        barGraph.clearSelection()
        scatterGraph.clearSelection()
        surfaceGraph.clearSelection()
    }

    function resetCameras() {
        surfaceGraph.scene.activeCamera.cameraPreset = Camera3D.CameraPresetIsometricLeftHigh
        scatterGraph.scene.activeCamera.cameraPreset = Camera3D.CameraPresetIsometricLeftHigh
        barGraph.scene.activeCamera.cameraPreset = Camera3D.CameraPresetIsometricLeftHigh
        surfaceGraph.scene.activeCamera.zoomLevel = 100.0
        scatterGraph.scene.activeCamera.zoomLevel = 100.0
        barGraph.scene.activeCamera.zoomLevel = 100.0
    }

    function toggleMeshStyle() {
        if (barGraph.seriesList[0].meshSmooth === true) {
            barGraph.seriesList[0].meshSmooth = false
            if (surfaceGraph.seriesList[0].flatShadingSupported)
            surfaceGraph.seriesList[0].flatShadingEnabled = true
            scatterGraph.seriesList[0].meshSmooth = false
        } else {
            barGraph.seriesList[0].meshSmooth = true
            surfaceGraph.seriesList[0].flatShadingEnabled = false
            scatterGraph.seriesList[0].meshSmooth = true
        }
    }
}
