import QtQuick           2.15
import QtQuick.Controls  2.15

Page {
    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "PageIndicator is used to indicate the currently active page in a container of pages."
        }

        PageIndicator {

            count: 5
            currentIndex: 2
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
