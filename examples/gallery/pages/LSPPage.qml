import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Fonts     1.0 as X
import xQuick.Style     1.0 as X

Page {

    id: page

    X.CodeHelper {
        id: _code_helper;
    }

    SplitView {

        anchors.fill: parent;

        orientation: Qt.Vertical;

        X.Editor {

            id: _ed;

            SplitView.fillWidth: true;
            SplitView.preferredHeight: page.height / 4 * 3;

            contents:"
def fib(nterms):
    n1, n2 = 0, 1
    count = 0

    if nterms <= 0:
        print(\"Please enter a positive integer\")
    elif nterms == 1:
        print(\"Fibonacci sequence upto\",nterms,\":\")
        print(n1)
    else:
        print(\"Fibonacci sequence:\")
        while count < nterms:
            print(n1)
            nth = n1 + n2
            # update values
            n1 = n2
            n2 = nth
            count += 1

if __name__ == \"__main__\":
    fib(10)
"

            X.SourceHighliter { Component.onCompleted: {
                    setup(_ed.document, X.Style.languages.python);
                }
            }
        }

        SplitView {

            orientation: Qt.Horizontal;

            SplitView.fillWidth: true;
            SplitView.preferredHeight: page.height / 4;

            Pane {

                id: _rhs;

                SplitView.fillHeight: true;
                SplitView.preferredWidth: page.width / 2;

                ColumnLayout {

                    anchors.fill: parent;

                    spacing: 0;

                    X.LabelButton {

                        topPadding: 10;
                        bottomPadding: 10;

                        Layout.fillWidth: true;
                        Layout.minimumHeight: 42 - 1;
                        Layout.maximumHeight: 42 - 1;

                        text: 'Diagnostics'
                        horizontalAlignment: Text.AlignHCenter;
                        verticalAlignment: Text.AlignVCenter;

                        Rectangle {

                            id: _diagnostics_indicator;

                            anchors.right: parent.right;
                            anchors.rightMargin: 10;
                            anchors.verticalCenter: parent.verticalCenter;

                            width: 12;
                            height: 12;
                            radius: 6;
                            color: "#7DCD85";

                            opacity: 0;

                            SequentialAnimation on opacity {

                                id: _diagnostics_indicator_animation;

                                loops: Animation.Infinite
                                running: false;

                                PropertyAnimation { to: 1; duration: 500; }
                                PropertyAnimation { to: 0; duration: 500; }
                            }
                        }
                    }

                    X.Separator {
                        Layout.fillWidth: true;
                    }

                    ListView {

                        id: _diagnostics;

                        Layout.fillWidth: true;
                        Layout.fillHeight: true;

                        clip: true;

                        model: ListModel {
                            id: _diagnostics_model;
                        }

                        delegate: Control {

                            id: _diagnostics_delegate;

                            width: _rhs.width;
                            height: 22;

                            property int severity: model.severity;
                            property string source: model.source;

                            RowLayout {

                                anchors.fill: parent;

                                X.LabelHint2 {
                                    text: model.line;
                                    Layout.minimumWidth: 30;
                                    Layout.maximumWidth: 30;
                                    leftPadding: 10;
                                }
                                X.Separator {
                                    Layout.fillHeight: true;
                                }
                                X.LabelHint2 {

                                    Layout.fillWidth: true;

                                    text: model.message;
                                }
                            }
                            X.Separator {
                                anchors.left: parent.left;
                                anchors.right: parent.right;
                                anchors.bottom: parent.bottom;
                            }

                            X.ToolTip {
                                position: X.Style.Position.Left;

                                text: 'Origin: ' + _diagnostics_delegate.sourc

                                visible: _diagnostics_delegate.hovered;

                                backgroundColor:
                                _diagnostics_delegate.severity <= 1 ? 'green'
                                    : _diagnostics_delegate.severity <= 2 ? 'yellow'
                                    : _diagnostics_delegate.severity <= 3 ? 'orange'
                                    : 'red';
                            }
                        }
                    }
                }
            }

            ColumnLayout {

                SplitView.fillHeight: true;
                SplitView.fillWidth: true;

                spacing: 0;

                X.LabelButton {
                    Layout.fillWidth: true;

                    Layout.minimumHeight: 42 - 1;
                    Layout.maximumHeight: 42 - 1;

                    horizontalAlignment: Text.AlignHCenter;
                    verticalAlignment: Text.AlignVCenter;

                    text: "Server output (" + _code_helper.server + ")";
                }

                X.Separator {
                    Layout.fillWidth: true;
                }

                X.Editor {
                    id: _output;

                    Layout.fillWidth: true;
                    Layout.fillHeight: true;

                    display_lines: _output.actualContents.length != 0;
                    display_highl: false;

                    X.SourceHighliter {
                        Component.onCompleted: {
                            setup(_output.document, X.Style.languages.json);
                        }
                    }

                    Rectangle {
                        id: _debug;

                        visible: false;

                        anchors.fill: parent;

                        color: "transparent";

                        border.width: 1;
                        border.color: "magenta";
                    }
                }
            }
        }
    }

    Connections {

        target: _code_helper;

        function onMonitoring() {

            _diagnostics_indicator.opacity = 0;
            _diagnostics_indicator_animation.running = true;
        }

        function onDiagnostics(items) {

            _ed.errors.clear();
            _diagnostics_model.clear();

            for(var i = 0; i < items.length; i++) {
                _ed.errors.append(items[i]);
                _diagnostics_model.append(items[i]);
            }

            _diagnostics_indicator_animation.running = false;
            _diagnostics_indicator.opacity = 1;
        }

        function onCompletions(items) {

            _ed.completions.clear();

            for(var i = 0; i < items.length; i++) {
                _ed.completions.append(items[i]);
            }
        }
    }

    Component.onCompleted: {
        _code_helper.setup("", _ed.edit, _output.edit);
    }
}
