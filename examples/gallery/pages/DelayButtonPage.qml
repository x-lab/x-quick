import QtQuick          2.15
import QtQuick.Controls 2.15

Page {

    id: page

    Column {
        spacing: 40

        anchors.fill: parent;
        anchors.topMargin: 20;

        Label {
            leftPadding: 20;
            rightPadding: 20;
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: "DelayButton is a checkable button that incorporates a delay before the "
                + "button is activated. This delay prevents accidental presses."
        }

        DelayButton {
            text: "DelayButton"
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }
}
