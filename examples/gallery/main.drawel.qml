import QtQuick              2.15
import QtQuick.Controls     2.15
import QtQuick.Layouts      1.15
import QtQml.XmlListModel

import Qt5Compat.GraphicalEffects

import xQuick               1.0 as X
import xQuick.Controls      1.0 as X
import xQuick.Fonts         1.0 as X
import xQuick.Style         1.0 as X

Page {

    id: _control;

    width: 300;

    function opened() { return _control.width != 0 }
    function open() { _control.width = 300; }
    function close() { _control.width = 0; }

    Behavior on width {
        NumberAnimation {
            easing {
                type: Easing.OutElastic
                amplitude: 1.0
                period: 0.5
            }
        }
    }

    /* XmlListModel { */
    /*     id: _profiler_event_model */
    /*     query: "/trace/eventData/events" */

    /*     XmlRole { name: "displayName"; query: "displayName/string()" } */
    /*     XmlRole { name: "type"; query: "type/string()" } */
    /*     XmlRole { name: "memoryEventType"; query: "memoryEventType/string()" } */
    /*     XmlRole { name: "filename"; query: "filename/string()" } */
    /*     XmlRole { name: "line"; query: "line/string()" } */
    /*     XmlRole { name: "column"; query: "column/string()" } */
    /*     XmlRole { name: "cacheEventType"; query: "cacheEventType/string()" } */
    /* } */

    /* XmlListModel { */
    /*     id: _profiler_data_model */
    /*     query: "/trace/profilerDataModel/range" */

    /*     XmlRole { name: "startTime"; query: "@startTime/string()" } */
    /*     XmlRole { name: "eventIndex"; query: "@eventIndex/string()" } */
    /*     XmlRole { name: "amount"; query: "@amount/string()" } */

    /*     // onStatusChanged: { */

    /*     //     if(status == XmlListModel.Ready) { */
    /*     //         // console.log('Profiler data model ready', _profiler_data_model.count); */
    /*     //     } */

    /*     //     if(status == XmlListModel.Error) { */
    /*     //         // console.log(_profiler_data_model.errorString()); */
    /*     //     } */
    /*     // } */
    /* } */

    /* header: ToolBar { */

    /*     height: 42; */

    /*     RowLayout { */

    /*         anchors.fill: parent; */

    /*         spacing: 10; */

    /*         X.LabelCaption { */
    /*             text: 'Profiler'; */

    /*             Layout.leftMargin: 10; */
    /*             Layout.fillWidth: true; */
    /*         } */

    /*         X.Icon { */
    /*             id: _profiler_start_i; */

    /*             enabled: !_profiler.running; */
    /*             icon: X.Icons.icons.play_circle; */

    /*             MouseArea { */
    /*                 anchors.fill: parent; */
    /*                 onClicked: _profiler.start(); */
    /*             } */
    /*         } */

    /*         X.Icon { */
    /*             id: _profiler_stop_i; */

    /*             enabled: _profiler.running; */
    /*             icon: X.Icons.icons.stop_circle; */

    /*             MouseArea { */
    /*                 anchors.fill: parent; */
    /*                 onClicked: _profiler.stop(); */
    /*             } */
    /*         } */

    /*         X.Icon { */
    /*             id: _profiler_clear_i; */

    /*             enabled: (!_profiler.running && _profiler_data_model.count); */
    /*             icon: X.Icons.icons.replay_circle_filled; */

    /*             MouseArea { */
    /*                 anchors.fill: parent; */
    /*                 onClicked: { */
    /*                     _profiler.clear(); */

    /*                     _profiler_data_model.source = "file:///tmp/1"; */
    /*                     _profiler_data_model.reload(); */
    /*                 } */
    /*             } */

    /*         } */

    /*         Switch { */
    /*             id: _profiler_auto_s; */

    /*             hoverEnabled: true; */
    /*             checked: true; */

    /*             Layout.maximumHeight: 14; */
    /*             Layout.minimumHeight: 14; */
    /*             Layout.maximumWidth: 14*3/2; */
    /*             Layout.minimumWidth: 14*3/2; */
    /*             Layout.rightMargin: 10; */

    /*             ToolTip.timeout: 5000 */
    /*             ToolTip.visible: _profiler_auto_s.hovered; */
    /*             ToolTip.text: "Automatic profiling of component loader is " + (_profiler_auto_s.checked ? "enabled" : "disabled"); */
    /*         } */
    /*     } */

    /*     X.Separator { */
    /*         anchors.top: parent.top; */
    /*         anchors.left: parent.left; */
    /*         anchors.bottom: parent.bottom; */
    /*     } */
    /* } */

    /* Item { */
    /*     id: _profiler_pane_contents; */

    /*     anchors.top: parent.top; */
    /*     anchors.left: parent.left; */
    /*     anchors.right: parent.right; */
    /*     anchors.bottom: parent.bottom; */
    /*     anchors.bottomMargin: 320; */

    /*     BusyIndicator { */

    /*         id: _profiler_pane_indicator; */

    /*         anchors.centerIn: parent; */

    /*         visible: _profiler.running; */
    /*     } */

    /*     X.LabelCaption { */
    /*         anchors.top: _profiler_pane_indicator.bottom; */
    /*         anchors.topMargin: 10; */
    /*         anchors.horizontalCenter: parent.horizontalCenter; */

    /*         text: 'Recording trace'; */

    /*         visible: _profiler.running; */
    /*     } */

    /*     ListView { */

    /*         id: _profiler_list; */

    /*         visible: !_profiler.running; */

    /*         anchors.fill: parent; */

    /*         clip: true; */

    /*         model: _profiler_data_model; */

    /*         delegate: Control { */

    /*             id: _profiler_delegate; */

    /*             width: _control.width; */
    /*             height: 32; */

    /*             RowLayout { */

    /*                 anchors.fill: parent; */

    /*                 X.LabelHint2 { */
    /*                     text: model.index; */
    /*                     Layout.minimumWidth: 30; */
    /*                     Layout.maximumWidth: 30; */
    /*                     leftPadding: 10; */
    /*                 } */
    /*                 X.Separator { */
    /*                     Layout.fillHeight: true; */
    /*                 } */
    /*                 X.LabelHint2 { */

    /*                     Layout.fillWidth: true; */

    /*                     text: model.startTime; */
    /*                 } */
    /*             } */

    /*             background: Rectangle { */
    /*                 color: index % 2 ? "transparent" : "#11000000"; */
    /*             } */

    /*             X.Separator { */
    /*                 anchors.left: parent.left; */
    /*                 anchors.right: parent.right; */
    /*                 anchors.bottom: parent.bottom; */
    /*             } */
    /*         } */

    /*         ScrollBar.vertical: X.ScrollBar { */
    /*             visible: _profiler_list.contentHeight > _profiler_list.height; */
    /*         } */
    /*     } */
    /* } */

    /* X.Separator { */
    /*     anchors.top: parent.top; */
    /*     anchors.left: parent.left; */
    /*     anchors.bottom: parent.bottom; */
    /* } */

    /* X.Separator { */
    /*     anchors.top: _profiler_pane_contents.bottom; */
    /*     anchors.left: parent.left; */
    /*     anchors.right: parent.right; */
    /* } */

    /* property var _profiling_values; */

    /* signal animate(); */

    /* Item { */

    /*     anchors.top: _profiler_pane_contents.bottom; */
    /*     anchors.topMargin: 1; */
    /*     anchors.left: parent.left; */
    /*     anchors.right: parent.right; */
    /*     anchors.bottom: parent.bottom; */

    /*     Grid { */
    /*         id: _profiler_pane_contents_gauges; */

    /*         anchors.centerIn: parent; */

    /*         columns: 3; */
    /*         rows: 5; */
    /*         spacing: 0; */

    /*         Repeater { */

    /*             model: _profiler_pane_contents_gauges.rows * _profiler_pane_contents_gauges.columns; */

    /*             X.Gauge { */

    /*                 id: _g; */

    /*                 minValue: 0; */
    /*                 maxValue: 100; */

    /*                 value: 0; */
    /*                 width: 100; */
    /*                 height: 60; */

    /*                 visible: _profiler_data_model.count > 0; */

    /*                 NumberAnimation { */

    /*                     id: _g_a; */
    /*                     target: _g; */
    /*                     property: 'value'; */
    /*                     from: 0; */
    /*                     duration: 1000; */

    /*                     easing.type: Easing.InOutBounce; */
    /*                 } */

    /*                 Connections { */
    /*                     target: _control; */

    /*                     function onAnimate() { */
    /*                         _g_a.to = Math.random() * 100; */
    /*                         _g_a.start(); */
    /*                     } */
    /*                 } */
    /*             } */
    /*         } */
    /*     } */
    /* } */

    /* Connections { */
    /*     target: _profiler; */

    /*     function onReady() */
    /*     { */
    /*         _profiler_event_model.source = "file:///tmp/f"; */
    /*         _profiler_event_model.reload(); */

    /*         _profiler_data_model.source = "file:///tmp/f"; */
    /*         _profiler_data_model.reload(); */

    /*         _control.animate(); */
    /*     } */
    /* } */
}
