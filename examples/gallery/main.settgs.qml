import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts  1.15

import xQuick           1.0 as X
import xQuick.Controls  1.0 as X
import xQuick.Style     1.0 as X

Dialog {

    id: settingsDialog

    x: Math.round((parent.width - width) / 2)
    y: Math.round(parent.height / 6)

    width: Math.round(Math.min(parent.width, parent.height) / 3 * 2)
    height: Math.round(Math.min(parent.width, parent.height) / 3 * 2)

    modal: true
    focus: true
    title: "Settings"

    standardButtons: Dialog.Ok | Dialog.Cancel

    leftMargin: 0;
    rightMargin: 0;

    spacing: 0;

    ButtonGroup {
        id: _flavor_group;
    }

    Flickable {

        id: _flicker;

        Layout.fillWidth: true;
        Layout.fillHeight: true;

        clip: true;
        contentHeight: _contents.height;

        Column {
            id: _contents;

            anchors.fill: parent;

            spacing: 10;

            X.LabelCaption { width: parent.width; text: "Flavor"; }

            RadioDelegate { width: parent.width; text: "macOS";  ButtonGroup.group: _flavor_group; onClicked: {
                    X.Style.flavors = 'MACOS'; // X.Style.flavor_macos;
                }
                checked: X.Style.flavors == 'MACOS';
            }
            RadioDelegate { width: parent.width; text: "Ubuntu"; ButtonGroup.group: _flavor_group; onClicked: {
                    X.Style.flavors = 'UBUNTU'; // X.Style.flavor_ubuntu;
                }
                checked: X.Style.flavors == 'UBUNTU';
            }
            RadioDelegate { width: parent.width; text: "Fedora"; ButtonGroup.group: _flavor_group; onClicked: {
                    X.Style.flavors = 'FEDORA'; // X.Style.flavor_fedora;
                }
                checked: X.Style.flavors == 'FEDORA';
            }

            X.LabelCaption { width: parent.width; text: "Variant"; }

            SwitchDelegate { width: parent.width; text: "Dark"; onToggled: {
                    X.Style.variant = checked ? 'DARK' : 'LIGHT';
                }
                checked: X.Style.variant == 'DARK';
            }
        }

        ScrollIndicator.vertical: ScrollIndicator { visible: _flicker.contentHeight > _flicker.height; }
    }

    onAccepted: {
        settingsDialog.close()
    }

    onRejected: {
        settingsDialog.close()
    }

    contentItem: ColumnLayout {
        id: settingsColumn
        spacing: 20
    }
}
